package strategy.book_example;

/**
 * ConcreteStrategy - obejmuje implementację algorytmu zgodną z interfejsem klasy Strategy.
 */
public class SimpleCompositor implements Compositor{

    @Override
    public int compose(Coord[] natural, Coord[] stretch, Coord[] shrink, int componentCount, int lineWidth, int[] breaks) {
        //bada komponenty po jednym wierszu naraz aby ustalić gdzie  umieścić punkty podziału wierszy.
        return -1;
    }

}
