package strategy.book_example;

/**
 * ConcreteStrategy - obejmuje implementację algorytmu zgodną z interfejsem klasy Strategy.
 */
public class ArrayCompositor implements Compositor{
    private int something;

    public ArrayCompositor(int something) {
        this.something = something;
    }
    
    @Override
    public int compose(Coord[] natural, Coord[] stretch, Coord[] shrink, int componentCount, int lineWidth, int[] breaks) {
        // dzieli komponenty na wiersze w regularnych odstępach
        return -1;
    }

}
