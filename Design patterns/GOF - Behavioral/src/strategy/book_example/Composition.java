package strategy.book_example;

import java.awt.Component;

/**
 * Context
 * - jest konfigurowany za pomocą obiektu ConcreteStrategy
 * - przechowuje referencję do obiektu Strategy
 * - może definiować interfejs dający obiektom Strategy dostęp do danych obiektu Context
 */
public class Composition {
    private Compositor compositor;
    private Component component;
    private int componentCount;
    private int lineWidth;
    private int lineBreaks;
    private int lineCount;

    public Composition(Compositor compositor) {
        this.compositor = compositor;
    }
    
    public void repair(){
        Coord[] natural = null;
        Coord[] stretchability = null;
        Coord[] shrinkability = null;
        int[] breaks = null;
        
        //przygotowanie tablic z pożądanym rozmiarem komponentów
        // …
        
        // określanie miejsca podziału wierszy:
        int breakCount;
        breakCount = compositor.compose(natural, stretchability, shrinkability, componentCount, lineWidth, breaks);
        
        // rozmieszczanie komponentów zgodnie z punktami podziału wierszy.
        // …
    }
}
