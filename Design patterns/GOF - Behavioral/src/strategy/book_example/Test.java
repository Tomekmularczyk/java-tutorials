package strategy.book_example;

/**
 * Strategy. Określa rodzinę algorytmów, kaspułkuje każdy z nich i umożliwia ich zamienne sosowanie. 
 * Wzorzec ten pozwala zmieniać algorytmy niezależnie od korzystających z nich klientów.
 * 
 * Używamy w nastepujących warunkach:
 * - Kiedy wiele powiązanych klas różni się tylko zachowaniem. Strategie umożliwiają skonfigurowanie klasy za pomocą jednego z wielu zachowań
 * - Jeśli potrzebne są różne wejse algorytmu. Można na przykład zdefiniować alogrytmy związane z różnymi korzyściami i kosztami z zakresu pamięci oraz czasu przetwarzania.
 *   Strategie można stosować, jeśli wspomniane werjse są zaimplementowane w postaci hierarchi klas algorytmów.
 * - Jeżeli algorytm korzysta z danych, o których klienty nie powinny wiedzieć. Wzorzec Strategia pozwala uniknąć ujawniania złożonych, specyficznych dla algorytmu struktur danych.
 * - Gdy klasa definiuje wiele zachowań, a te w operacjach pojawiają się w formie złożonych instrukcji warunkowych.
 *   Zamiast tworzyć wiele takich instrukcji, należy przenieść powiązane gałęzie do odrębnych klas Strategy.
 */
public class Test {
    public static void main(String[] args) {
        //Klasy konkretne strategy nie korzystają ze wszystkich przekazanych informacji do metody compose();
        Composition quick = new Composition(new SimpleCompositor());
        Composition slick = new Composition(new TeXCompositor());
        Composition iconic = new Composition(new ArrayCompositor(100));
    }
}
