package strategy.book_example;

/**
 * Strategy - obejmuje deklarację wspólnego interfejsu wszystkich obsługiwanych algorytmów.
 * Klasa Context może korzystać z tego interfejsu do wywołania algorytmów zdefiniowanych w klasach ConcreteStrategy.
 */
public interface Compositor {
    
    int compose(Coord natural[], Coord stretch[], Coord shrink[], int componentCount, int lineWidth, int breaks[]);
    
}
