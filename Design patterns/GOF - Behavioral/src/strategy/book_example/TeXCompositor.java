package strategy.book_example;

/**
 * ConcreteStrategy - obejmuje implementację algorytmu zgodną z interfejsem klasy Strategy.
 */
public class TeXCompositor implements Compositor{

    @Override
    public int compose(Coord[] natural, Coord[] stretch, Coord[] shrink, int componentCount, int lineWidth, int[] breaks) {
        // Bada ona cały akapit i uwzględnia rozmiar komponentów oraz zakres ich zwiększania.
        return -1;
    }

}
