package command.book_example2;

import static java.lang.System.out;

/**
 * ConcreteCommand
 */
public class WashDog implements Command{

    @Override
    public void execute() {
        out.println("Washing dog...");
    }

    @Override
    public void undo() {
        out.println("Undoing dog washing...");
    }

}
