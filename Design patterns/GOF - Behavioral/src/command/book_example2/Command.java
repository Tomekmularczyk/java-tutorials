package command.book_example2;

/**
 * Command
 */
public interface Command {
    void execute();
    void undo();
}
