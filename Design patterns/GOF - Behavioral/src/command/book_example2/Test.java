package command.book_example2;

/**
 * W każdym przypadku obiekt Command oddziela obiekt wywołujący operację od obiektu, który potrafi ją wykonać.
 */
public class Test {
    public static void main(String[] args) {
        Command washDog = new WashDog();
        Command investInForex = new InvestInForex();
        Command workOut = new WorkOut();
        Command talk = new Talk();
        
        Invoker invoker = new Invoker();
        invoker.execute(talk);
        invoker.execute(washDog);
        invoker.execute(workOut);
        invoker.execute(investInForex);
        
        
        System.out.println("------ undoing changes ------");
        invoker.undo();
    }
}
