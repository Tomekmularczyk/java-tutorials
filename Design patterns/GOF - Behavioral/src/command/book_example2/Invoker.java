package command.book_example2;

import java.util.LinkedList;

public class Invoker {
    private LinkedList<Command> commandHistory = new LinkedList<>();

    public void execute(Command command){
        command.execute();
        commandHistory.add(command);
    }
    
    public void undo(){
        Command com;
        while((com = commandHistory.pollLast()) != null){
            com.undo();
        }
    }
    
}
