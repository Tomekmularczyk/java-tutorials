package command.book_example2;

import static java.lang.System.out;

/**
 * ConcreteCommand
 */
public class Talk implements Command{

    @Override
    public void execute() {
        out.println("Talking and talking…");
    }

    @Override
    public void undo() {
        out.println("Unsaid everything I've said…");
    }

}
