package command.book_example2;

import static java.lang.System.out;

/**
 * ConcreteCommand
 */
public class WorkOut implements Command{

    @Override
    public void execute() {
        out.println("Working out…");
    }

    @Override
    public void undo() {
        out.println("Undoing WorkingOut and getting lazy...");
    }

}