package command.book_example2;

import static java.lang.System.out;

/**
 * ConcreteCommand
 */
public class InvestInForex implements Command{

    @Override
    public void execute() {
        out.println("Investing in forex…");
    }

    @Override
    public void undo() {
        out.println("Canceling trades in forex…");
    }

}
