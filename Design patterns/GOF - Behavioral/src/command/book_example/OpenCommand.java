package command.book_example;


/**
 * ConcreteCommand
 * - definiuje powiązane między obiektem Receiver i działaniem
 * - obejmuje implementację operacji Execute w postaci wywołania odpowiednich operacji Receiver
 */
public class OpenCommand implements Command{
    private Application application;
    private String response;

    public OpenCommand(Application application) {
        this.application = application;
    }


    /**
     * Procedura pomocnicza żądająca od użytkownika podania nazwy dokumentu.
     */
    protected String askUser(){
        return "";
    }
    @Override
    public void execute() {
        String fileName = askUser();
        
        if(!fileName.isEmpty()){
            Document document = new Document(fileName);
            application.add(document);
            document.open();
        }
    }

}
