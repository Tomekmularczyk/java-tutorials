package command.book_example;

import java.util.LinkedList;
import java.util.List;


/**
 * ConcreteCommand
 * - definiuje powiązane między obiektem Receiver i działaniem
 * - obejmuje implementację operacji Execute w postaci wywołania odpowiednich operacji Receiver
 */
public class MacroCommand  implements Command{  //ta klasa działa jak Kompozyt
    private List<Command> commands = new LinkedList<>();
    
    @Override
    public void execute() {
        for (Command command : commands) {
            command.execute();
        }
    }

    public void add(Command command){
        
    }
    
    public void remove(Command command){
        
    }
}
