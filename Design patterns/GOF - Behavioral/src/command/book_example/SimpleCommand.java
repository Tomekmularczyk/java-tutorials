package command.book_example;

/**
 * ConcreteCommand
 * - definiuje powiązane między obiektem Receiver i działaniem
 * - obejmuje implementację operacji Execute w postaci wywołania odpowiednich operacji Receiver
 */
public class SimpleCommand<T> implements Command{

    @Override
    public void execute() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}
