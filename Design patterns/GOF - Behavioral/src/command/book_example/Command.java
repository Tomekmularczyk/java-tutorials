package command.book_example;

/**
 * Command - obejmuje deklarację interfejsu przeznaczonego do wykonywania operacji.
 */
public interface Command {
    void execute();
}
