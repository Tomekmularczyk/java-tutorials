package command.book_example;

/**
 * ConcreteCommand
 * - definiuje powiązane między obiektem Receiver i działaniem
 * - obejmuje implementację operacji Execute w postaci wywołania odpowiednich operacji Receiver
 */
public class PasteCommand implements Command{
    private Document document;

    public PasteCommand(Document document) {
        this.document = document;
    }
    
    @Override
    public void execute() {
        document.paste();
    }

}
