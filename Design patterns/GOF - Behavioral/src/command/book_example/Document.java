package command.book_example;

/**
 * Receiver - potrafi wykonać operacje potrzebne do osbłużenia żądania. Funkcję odbiorcy może pełnić dowolna klasa.
 */
public class Document{
    private String fileName;

    public Document(String fileName) {
        this.fileName = fileName;
    }
    
    public void open(){
        
    }
    
    public void paste(){
        
    }
}
