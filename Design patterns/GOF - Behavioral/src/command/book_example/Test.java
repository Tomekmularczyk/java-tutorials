package command.book_example;

/**
 * Command. Kapsułkuje żądanie w formie obiektu. Umożliwia to paramteryzację klienta przy użyciu różnych żądań oraz umieszczenie żądań w kolejkach i dziennikach,
 * a także zapewnia obsługę cofania operacji.
 * Konsekwencje:
 * 1. Obiekt command oddziela obiekt wywołujący operację od tego, który potrafi ją wykonać.
 * 2. Polecenia to standardowe obiekty. Można nimi manipulować i rozszerzać je w taki sam sposób jak inne obiekty.
 * 3. Polecenia można połączyć w polecenie złożone. Polecenia złożone są zwykle tworzone zgodnie ze wzrocem kompozyt.
 * 4. Dodawanie nowych obiektów Command jest prostem ponieważ nie wymaga modyfikowania istniejących klas.
 * 
 * Ze wzorca Polecenie należy korzystac do wykonania następujących zadań:
 * - Do parametryzowania obiektów za pomocą wykonywanych działań.
 * - Do określania kolejkowania i wywoływania żądania w różnych miejscach programu. Czas życia obiektu Command nie musi zależeć od pierwotnego żądania.
 * - Do umożliwiania cofania zmian. Operacja Execute obiektu Command może w samym poleceniu przechowywać stan potrzebny do anulowania efektów jej działania.
 *   Do interfejsu klasy Command trzeba dodać operacje Unexecute, która odwróci skutki wcześniejeszego wywoływania operacji Execute. 
 *   Historia poleceń jest zapisywana na liście. Dzięki temu mozna powtarzać i cofać dowolną liczbę operacji.
 * - Do obsługi rejestrowania zmian, aby mozna je ponownie przeprowadzić w przypadku awarii systemu. 
 *   Przez wzbogacenie interfejsu klasy Command o operacje wczytania i zapisywania poleceń można utworzyć trwały dziennik zmian.
 *   Przywrócenie systemu po awarii polega na wczytaniu zarejestrowanych poleceń z dysku i ponownym ich wykonaniu.
 * - Do budowania systemu na podstawie wysokopoziomowych operacji opartych na prostych operacjach. 
 *   Taką strukturę powszechnie spotyka się w systemach informatycznych z obsługą tranzakcji. Transakcja kapsułkuje zbiór zmian w danych.
 *   Wzorzec Polecenie umożliwia modelowanie transakcji. Polecenia mają wspólny interfejs, co umożliwia przeprowadzenie wszsytkich transakcji w taki sam sposób.
 */
public class Test {

}
