package command.my_example1;

import static java.lang.System.out;

/**
 * Receiver
 */
public class Light {
    
    public void switchLightOn(){
        out.println("Light is ON");
    }
    
    public void switchLightOff(){
        out.println("Light is OFF");
    }
    
}
