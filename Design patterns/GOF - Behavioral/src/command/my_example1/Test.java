package command.my_example1;

/**
 *  * Wzorzec Command to urzeczowione wywołanie metody.
 *
 *
 * Invoker enkapsuluje Command a command przyjmuje Receiver żeby przeprowadzić daną akcję.
 */
public class Test {

    public static void main(String[] args) {
        Light light = new Light();
        Command lightOnCommand = new LightOnCommand(light);
        Command lightOffCommand = new LightOffCommand(light);

        RemoteControl remoteControl = new RemoteControl();
        remoteControl.setCommand(lightOnCommand);
        remoteControl.pressButton();

        remoteControl.setCommand(lightOffCommand);
        remoteControl.pressButton();
    }
}
