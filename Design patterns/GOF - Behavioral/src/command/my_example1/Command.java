package command.my_example1;

public interface Command {
    void execute();
}
