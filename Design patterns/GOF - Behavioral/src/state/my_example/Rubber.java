package state.my_example;

import static java.lang.System.out;
import state.my_example.DrawingController.ConcreteTool;

/**
 * ConcreteState
 */
public class Rubber extends ConcreteTool{
    private static class Holder {
        private static final Rubber INSTANCE = new Rubber();
    }
    
    private Rubber(){}
    public static Rubber getInstance(){
        return Holder.INSTANCE;
    }
    
    @Override
    public void initialize(DrawingController controller) {
        out.println("Rubber initialized");
    }

    @Override
    public void mousePressed(DrawingController controller) {
        int x = controller.getX();
        int y = controller.getY();
        String color = controller.getColor();
        out.printf("Rubber pressed [x:%d,y:%d] and erased place with %s color.\n", x, y, color);
    }

    @Override
    public void processKeyboard(DrawingController controller) {
        out.println("Rubber is processing keyboard.");
    }

}
