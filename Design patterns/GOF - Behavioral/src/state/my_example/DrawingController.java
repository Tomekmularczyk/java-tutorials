package state.my_example;

/**
 * Context
 */
public class DrawingController {
    private Tool tool;
    private int X, Y;
    private String color;

    public DrawingController() {
        tool = Tool.PENCIL;
        this.color = "Black";
    }
    
    public void changeTool(Tool tool){
        this.tool = tool;
    }
    
    public void initialize(){
        tool.getTool().initialize(this);
    }
    
    public void mousePressed(){
        tool.getTool().mousePressed(this);
    }
    
    public void processKeyboard(){
        tool.getTool().mousePressed(this);
    }

    public int getX() {
        X = (int) (Math.random() * 1600);
        return X;
    }

    public int getY() {
        Y = (int) (Math.random() * 900);
        return Y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    /**
     * State
     */
    public abstract static class ConcreteTool {
        
        //przesyłamy context, aby mieć dostęp do potrzebnych zmiennych składowych
        
        public abstract void initialize(DrawingController controller);
        
        public abstract void mousePressed(DrawingController controller);
        
        public abstract void processKeyboard(DrawingController controller);
        
    }
}
