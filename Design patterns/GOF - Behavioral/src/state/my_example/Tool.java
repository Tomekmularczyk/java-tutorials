package state.my_example;

import state.my_example.DrawingController.ConcreteTool;

public enum Tool {
    PENCIL(Pencil.getInstance()), 
    RUBBER(Rubber.getInstance()),
    FILLER(Filler.getInstance());
    
    private ConcreteTool tool;
    private Tool(ConcreteTool tool){
        this.tool = tool;
    }
    
    public ConcreteTool getTool(){
        return tool;
    }
}
