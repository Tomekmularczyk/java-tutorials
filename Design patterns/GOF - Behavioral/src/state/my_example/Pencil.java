package state.my_example;

import static java.lang.System.out;
import state.my_example.DrawingController.ConcreteTool;

/**
 * ConcreteState
 */
public class Pencil extends ConcreteTool{
    private static class Holder {
        private static final Pencil INSTANCE = new Pencil();
    }
    
    private Pencil(){}
    public static Pencil getInstance(){
        return Holder.INSTANCE;
    }
    
    @Override
    public void initialize(DrawingController controller) {
        out.println("Initializing Pencil");
    }

    @Override
    public void mousePressed(DrawingController controller) {
        int x = controller.getX();
        int y = controller.getY();
        String color = controller.getColor();
        out.printf("Pencil pressed [x:%d,y:%d] and drawed a dot with %s color.\n", x, y, color);
    }

    @Override
    public void processKeyboard(DrawingController controller) {
        out.println("Pencil is processing keyboard.");
    }

}
