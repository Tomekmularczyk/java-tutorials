package state.my_example;

/**
 * 
 */
public class Test {
    public static void main(String[] args) {
        DrawingController drawingController = new DrawingController();
        
        drawingController.initialize();
        drawingController.mousePressed();
        drawingController.setColor("Red");
        
        drawingController.changeTool(Tool.RUBBER);
        drawingController.initialize();
        drawingController.mousePressed();
        drawingController.processKeyboard();
    }
}
