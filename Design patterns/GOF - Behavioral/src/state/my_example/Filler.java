package state.my_example;

import static java.lang.System.out;
import state.my_example.DrawingController.ConcreteTool;

/**
 * ConcreteState
 */
public class Filler extends ConcreteTool {

    private static class Holder {

        private static final Filler INSTANCE = new Filler();
    }

    private Filler() {
    }

    public static Filler getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    public void initialize(DrawingController controller) {
        out.println("Initializing Filler");
    }

    @Override
    public void mousePressed(DrawingController controller) {
        int x = controller.getX();
        int y = controller.getY();
        String color = controller.getColor();
        out.printf("Filler pressed [x:%d,y:%d] and filled place with %s color.\n", x, y, color);
    }

    @Override
    public void processKeyboard(DrawingController controller) {
        out.println("Filler is processing keyboard.");
    }

}
