package state.book_example;

import state.book_example.TCPConnection.TCPState;

/**
 * ConcreteState - każda podklasa obejmuje implementację zachowania powiązaną ze stanem obiektu Context
 */
public class TCPListen extends TCPState {
    private static class Holder {
        private static final TCPListen INSTANCE = new TCPListen();
    }

    private TCPListen() {}

    public static TCPListen getInstance() {
        return Holder.INSTANCE;
    }

    @Override
    public void transmit(TCPConnection tcpConnection, TCPOctetStream octetStream) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void activeOpen(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void passiveOpen(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void close(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void synchronize(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void acknowledge(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void send(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}

