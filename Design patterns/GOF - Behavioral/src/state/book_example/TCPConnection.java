package state.book_example;
/**
 * Context
 * - definiuje interfejs udostępniany klientom.
 * - przechowuje egzemplarz podklasy klasy ConcreteState definiujący bieżący stan.
 * Deleguje żądania specyficzne dla stanu do bieżącego obiektu ConcreteState.
 */
public abstract class TCPConnection {
    private TCPState state;

    public TCPConnection() {
        state = TCPClosed.getInstance();
    }
    
    private void changeState(TCPState state){
        this.state = state;
    }

    public void activeOpen(){
        state.activeOpen(this);
    }
    public void passiveOpen(){
        state.passiveOpen(this);
    }
    public void close(){
        state.close(this);
    }
    public void send(){
        state.send(this);
    }
    public void acknowledge(){
        state.acknowledge(this);
    }
    public void synchronize(){
        state.synchronize(this);
    }
    public abstract void processOctet(TCPOctetStream stream);
    
    
    /**
     * Stan - definiuje interfejs do kapsułkowania zachowania związanego z określonym stanem obiektu Context
     */
    public abstract static class TCPState {
        public abstract void transmit(TCPConnection tcpConnection, TCPOctetStream octetStream);
        public abstract void activeOpen(TCPConnection tcpConnection);
        public abstract void passiveOpen(TCPConnection tcpConnection);
        public abstract void close(TCPConnection tcpConnection);
        public abstract void synchronize(TCPConnection tcpConnection);
        public abstract void acknowledge(TCPConnection tcpConnection);
        public abstract void send(TCPConnection tcpConnection);
        
        protected void changeState(TCPConnection tcpConnection, TCPState state){ //korzystamy z tego że klasa jest uprzywilejowana i wywołujemy prywatną metodę
            tcpConnection.changeState(state);
        }
    }
}
