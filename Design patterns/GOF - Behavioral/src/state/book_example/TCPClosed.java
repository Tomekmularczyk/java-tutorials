package state.book_example;

import state.book_example.TCPConnection.TCPState;

/**
 * ConcreteState - każda podklasa obejmuje implementację zachowania powiązaną ze stanem obiektu Context
 */
public class TCPClosed extends TCPState{
    private static class Holder {
        private static final TCPClosed INSTANCE = new TCPClosed();
    }
    
    private TCPClosed(){}
    public static TCPClosed getInstance(){
        return Holder.INSTANCE;
    }
    
    @Override
    public void transmit(TCPConnection tcpConnection, TCPOctetStream octetStream) {
        tcpConnection.processOctet(octetStream);
    }

    @Override
    public void activeOpen(TCPConnection tcpConnection) {
        // Wysyłanie komunikatów SYN, odbieranie komunikatów SYN i ACK itd.
        
        changeState(tcpConnection, TCPEstablished.getInstance());
    }

    @Override
    public void passiveOpen(TCPConnection tcpConnection) {
        changeState(tcpConnection, TCPListen.getInstance());
    }

    @Override
    public void close(TCPConnection tcpConnection) {
        // Wysyłanie komunikatów FIN, odbieranie potwierdzeń dla komunikatów FIN
        
        changeState(tcpConnection, TCPListen.getInstance());
    }

    @Override
    public void synchronize(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void acknowledge(TCPConnection tcpConnection) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void send(TCPConnection tcpConnection) {
        // Wysyłanie komunikatów SYN, odbieranie komunikatów SYN i ACK itd.
        
        changeState(tcpConnection, TCPEstablished.getInstance());
    }

}
