package state.book_example;


/**
 * Stan - Umożliwiwa obiektowi modyfikację zachowania w wyniku zmiany wewnętrznego stanu. Wygląda to tak jakby obiekt zmienił klasę.
 * Należy stosować kiedy:
 * - Zachowanie obiektu zależy od jego stanu, a obiekt musi na podstawie stanu zmienić działanie w czasie wykonania programu.
 * - Operacje obejmują długie, wieloczęściowe instrukcje warunkowe zależne od stanu obiektu. 
 *   Taki stan jest zwykle reprezentowany przez stałe wyliczeniowe. Często kilka operacji obejmuje tę samą strukturę warunkową.
 *   Wzorzec Stan powoduje umieszczenie każdej gałęzi takiej struktury w odrębnej klasie. Umożliwia to traktowanie stanu jako samodzielnego obiektu,
 *   który można modyfikować niezależnie od innych obiektów.
 */
public class Test {
    public static void main(String[] args) {
        // w tym przykłądzie zmienianie stanu odbywa się całkowicie w klasie Context
    }
}
