package memento.my_example1;

import static java.lang.System.out;

/**
 * Originator
 */
public class Rectangle {

    private String color;
    private String owner;
    private int id;
    private int leftBottomCorner;
    private int leftUpperCorner;
    private int rightBottomCorner;
    private int rightUpperCorner;

    public Rectangle(String color, String owner, int id) {
        this.color = color;
        this.owner = owner;
        this.id = id;
    }

    public void set(int leftBottomCorner, int leftUpperCorner, int rightBottomCorner, int rightUpperCorner) {
        this.leftBottomCorner = leftBottomCorner;
        this.leftUpperCorner = leftUpperCorner;
        this.rightBottomCorner = rightBottomCorner;
        this.rightUpperCorner = rightUpperCorner;
    }

    public void display() {
        out.printf("[%d,%d,%d,%d]\n", leftBottomCorner, leftUpperCorner, rightBottomCorner, rightUpperCorner);
    }

    public void moveLeftBottomCorner(int leftBottomCorner) {
        this.leftBottomCorner = leftBottomCorner;
    }

    public void moveLeftUpperCorner(int leftUpperCorner) {
        this.leftUpperCorner = leftUpperCorner;
    }

    public void moveRightBottomCorner(int rightBottomCorner) {
        this.rightBottomCorner = rightBottomCorner;
    }

    public void moveRightUpperCorner(int rightUpperCorner) {
        this.rightUpperCorner = rightUpperCorner;
    }

    public RectangleMemento createMemento() {
        return new RectangleMemento(leftBottomCorner, leftUpperCorner, rightBottomCorner, rightUpperCorner);
    }

    /**
     * Memento
     */
    public void setMemento(RectangleMemento memento) {
        this.leftBottomCorner = memento.leftBottomCorner;
        this.leftUpperCorner = memento.leftUpperCorner;
        this.rightBottomCorner = memento.rightBottomCorner;
        this.rightUpperCorner = memento.rightUpperCorner;
    }

    public static class RectangleMemento {

        private int leftBottomCorner;
        private int leftUpperCorner;
        private int rightBottomCorner;
        private int rightUpperCorner;

        public RectangleMemento(int leftBottomCorner, int leftUpperCorner, int rightBottomCorner, int rightUpperCorner) {
            this.leftBottomCorner = leftBottomCorner;
            this.leftUpperCorner = leftUpperCorner;
            this.rightBottomCorner = rightBottomCorner;
            this.rightUpperCorner = rightUpperCorner;
        }
    }
}
