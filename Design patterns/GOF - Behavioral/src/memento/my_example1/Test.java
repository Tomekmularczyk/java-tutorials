package memento.my_example1;

import static java.lang.System.out;
import memento.my_example1.Rectangle.RectangleMemento;

/**
 * Caretaker
 */
public class Test {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle("Red", "Tomek", 1);
        rectangle.set(0, 200, 0, 200);
        
        out.println("Initial state of rectangle: ");
        rectangle.display();

        RectangleMemento memento = rectangle.createMemento();
        resizeRectange(rectangle);
        
        out.println("State after modifications: ");
        rectangle.display();

        rectangle.setMemento(memento);
        
        out.println("State before modifications: ");
        rectangle.display();
    }

    public static void resizeRectange(Rectangle rectangle) {
        rectangle.moveLeftBottomCorner(12);
        rectangle.moveLeftUpperCorner(77);
        rectangle.moveRightBottomCorner(9);
        rectangle.moveRightUpperCorner(129);
    }
}
