package memento.book_example;

/**
 * Memento. Bez naruszania kapsułkowania rejestruje i zapisuje w wewnętrznej jednostce wewnętrzny stan obiektu, 
 * co umożliwia późniejsze przywrócenie obiektu według zapamiętanego stanu.
 * 
 * Należy używać w następujących warunkach:
 * - Kiedy trzeba zachować obraz (części) stanu obiektu w celu jego późniejszego odtworzenia w tym stanie oraz...
 * - bezpośredni interfejs do pobierania stanu spowodowałby ujawnienie szczegółów implementacji i naruszenie kapsułkowania obiektu.
 * 
 * Pamiątka zachowuje zasady kapsułkowania. Tylko źródło może tworzyć pamiątki i je potrafi odczytać. Zarządca może je tylko przetrzymywać.
 * 
 */
public class Test {
    public static void main(String[] args) {
        //MoveCommand to wzorzec Command
    }
}
