package memento.book_example;

import java.awt.Point;

/**
 * 
 */
public interface Graphic { //klasa bazowa obiektów graficznych w edytorze graficznym. 
    void move(Point point);
}
