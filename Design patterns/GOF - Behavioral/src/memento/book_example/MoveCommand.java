package memento.book_example;

import java.awt.Point;
import memento.book_example.ConstraintSolver.ConstraintSolverMemento;

/**
 * Caretaker - zarządca. (w tym przypadku jest to wzorzec Command)
 * - odpowiada za zarządzanie pamiątką,
 * - nigdy nie manipuluje zawartością pamiątki ani jej nie sprawdza.
 */
public class MoveCommand { 
    private ConstraintSolverMemento state;
    private Point delta;
    private Graphic target;

    public MoveCommand(ConstraintSolverMemento state, Point delta, Graphic target) {
        this.state = state;
        this.delta = delta;
        this.target = target;
    }
    
    public void execute(){
        ConstraintSolver solver = ConstraintSolver.getInstance();
        state = solver.createMemento();
        
        target.move(delta);
        solver.solve();
    }
    
    public void unexecute(){
        ConstraintSolver solver = ConstraintSolver.getInstance();
        target.move(delta);
        solver.setMemento(state);
        solver.solve();
    }
}
