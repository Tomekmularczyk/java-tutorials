package memento.book_example;

/**
 * Originator - źródło;
 * - tworzy pamiątke obejmującą zapis wewnętrznego stanu źródła,
 * - korzysta z pamiątki do przywrócenia swojego wewnętrznego stanu.
 */
public class ConstraintSolver { //klasa jest singletonem
    //złożone zmienne stanu i operacje potrzebne do utrzymania połączeń
    private int privateValue = 343;
    
    private static class Holder {
        private static final ConstraintSolver INSTANCE = new ConstraintSolver();
    }
    
    public static ConstraintSolver getInstance(){
        return Holder.INSTANCE;
    }
    
    
    
    public void solve(){
    }
    
    public void addConstraint(Graphic startConnection, Graphic endConnection){
    }
    
    public void removeConstraint(Graphic startConnection, Graphic endConnection){
    }
    
    public ConstraintSolverMemento createMemento(){
        return new ConstraintSolverMemento(privateValue);
    }
    
    public void setMemento(ConstraintSolverMemento memento){
        privateValue = memento.privateValueCopy;
    }
    
    /**
     * Memento.
     * - przechowuje wewnętrzny stan obiektu Originator. Pamiątka może obejmować dowolną określaną przez źródło część jego wewnętrznego stanu.
     * - chroni dane przed dostępem przez obiekty inne niż źródło. Pamiątki mają w istocie dwa interfejsy.
     *   Zarządca widzi zawężony interfejs pamiątki i może jedynie przekazywać ją innym obiektom, natomiast źródło ma dostęp do pełnego interfejsu,
     *   umożliwiającego dostęp do wszystkich danych potrzebnych do przywrócenia swojego wcześniejszego stanu.
     *   W idealnych warunkach dostęp do wewnętrznego stanu pamiątki ma tylko źródło, które ją utworzyło.
     */
    public static class ConstraintSolverMemento {
        //prywatne zmienne stanu ConstraintSolver
        private int privateValueCopy; //tylko ta klasa obejmująca będzie miała dostęp do tej wartości

        public ConstraintSolverMemento(int privateValueCopy) {
            this.privateValueCopy = privateValueCopy;
        }
    }
}
