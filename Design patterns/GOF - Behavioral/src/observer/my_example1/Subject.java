package observer.my_example1;

/**
 * Subject
 */
public interface Subject {

    void subscribe(GasObserver subscriber);

    void unsubscribe(GasObserver unsubscriber);

}
