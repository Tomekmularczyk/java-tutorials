package observer.my_example1;

/**
 * Observer
 */
public interface GasObserver{

    void update(Subject subject);

}
