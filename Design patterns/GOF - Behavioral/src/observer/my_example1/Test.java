package observer.my_example1;

public class Test {
    public static void main(String[] args) {
        NationalGasPrice nationalGasPrice = new NationalGasPrice(5.50);
        
        Orlen orlen = new Orlen();
        Shell shell = new Shell();
        
        nationalGasPrice.subscribe(orlen);
        nationalGasPrice.subscribe(shell);
        
        nationalGasPrice.setNewPrices(7.22);
    }
}
