package observer.my_example1;

import static java.lang.System.out;

/**
 * ConcreteObserver
 */
public class Shell implements GasObserver {

    @Override
    public void update(Subject subject) {
        if (subject instanceof NationalGasPrice) {
            NationalGasPrice casted = (NationalGasPrice) subject;
            out.println("Shell zmienia cene gazu na: " + casted.getGasPrice());
        }
    }

}