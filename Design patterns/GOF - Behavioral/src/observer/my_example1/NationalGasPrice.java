package observer.my_example1;

import static java.lang.System.out;
import java.util.LinkedList;
import java.util.List;

/**
 * ConcreteSubject
 */
public class NationalGasPrice implements Subject {
    private final List<GasObserver> subscribers;
    private double gasPrice;

    public NationalGasPrice(double gasPrice) {
        this.subscribers = new LinkedList<>();
        this.gasPrice = gasPrice;
    }

    @Override
    public void subscribe(GasObserver sub) {
        subscribers.add(sub);
    }

    @Override
    public void unsubscribe(GasObserver unsub) {
        subscribers.remove(unsub);
    }

    private void notifyObservers() {
        for (GasObserver subscriber : subscribers) {
            subscriber.update(this);
        }
    }

    public void setNewPrices(double price) {
        this.gasPrice = price;
        out.println("Zmieniono narodową cenę gazu.");
        notifyObservers();
    }

    public double getGasPrice() {
        return gasPrice;
    }
    
    
}
