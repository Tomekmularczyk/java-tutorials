package observer.my_example1;

import static java.lang.System.out;

/**
 * ConcreteObserver
 */
public class Orlen implements GasObserver {

    @Override
    public void update(Subject subject) {
        if (subject instanceof NationalGasPrice) {
            NationalGasPrice casted = (NationalGasPrice) subject;
            out.println("Orlen zmienia cene gazu na: " + casted.getGasPrice());
        }
    }

}
