package observer.book_example;

/**
 * ConcreteSubject
 * - przechowuje stan istotny dla obiektów ConcreteObserver
 * - kiedy zmieni się jego stan, wysyła powiadomienia do powiązanych z nim obserwatorów.
 */
public class ClockTimer extends Subject{
    public int getHour(){
        return -1;
    }
    
    public int getMinute(){
        return -1;
    }
    
    public int getSeconds(){
        return -1;
    }
    
    public void tick(){
         //..aktualizacja wewnętrznego stanu związanego z pomiarem czasu.
         notifyObservers();
    }
}
