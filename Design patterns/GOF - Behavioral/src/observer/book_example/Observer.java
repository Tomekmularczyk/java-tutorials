package observer.book_example;

/**
 * Observer
 * Definiuje interfejs do aktualizacji obiektów, które należy powiadamiać o zmianach podmiotu.
 */
public interface Observer {
    void update(Subject subject);
}
