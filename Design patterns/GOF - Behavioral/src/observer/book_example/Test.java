package observer.book_example;


/**
 * Observer - określa zależnośc jeden do wielu między obiektami. Kiedy zmieni się stan jednego z obiektów, 
 * wszystkie obiekty zależne od niego są o tym automatycznie powiadamiane i aktualizowane.
 * 
 * Ze wzorca observer można korzystać w dowolnej z poniższych sytuacji:
 * - kiedy abstrakcja ma dwa aspekty, a jeden zależy od drugiego. 
 *   Zakapsułkowanie tych aspektów w odrębnych obiektach umozliwia modyfikowanie i wielokrotne użytkowanie ich niezależnie od siebie.
 * - jeśli zmiana w jednym obiekcie wymaga zmodyfikowania drugiego, a nie wiadomo ile obiektów trzeba przekształcić.
 * - jeżeli obiekt powinien móc powiadamiać inne bez określania ich rodzaju. Oznacza to, że obiekty nie powinny być ściśle powiązane.
 */
public class Test {
    public static void main(String[] args) {
        ClockTimer timer = new ClockTimer();
        
        AnalogClock analogClock = new AnalogClock(timer);
        DigitalClock digitalClock = new DigitalClock(timer);
        
        //teraz po każdym "tyknięciu" obiektu timer oba zegary są zaaktualizowane i wyświetlają prawidłowo godzinę.
    }
}
