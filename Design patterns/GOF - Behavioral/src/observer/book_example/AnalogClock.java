package observer.book_example;

/**
 * ConcreteObserver - przechowuje referencje do obiektu ConcreteObject - przechowuje stan, który powinien być spójny ze stanem podmiotu. -
 * obejmuje implementację interfejsu do aktualizacji z klasy Observer potrzebną do tego, aby zachować spójność stanu obiektu
 * ConcreteObserver ze stanem podmiotu.
 */
public class AnalogClock implements Observer {
    //implementacja analogiczna do DigitalClock
    private ClockTimer subject;

    public AnalogClock(ClockTimer subject) {
        this.subject = subject;
        subject.attach(this);
    }

    @Override
    public void update(Subject subject) {
        if (subject.equals(this.subject)) {
            draw();
        }
    }

    private void draw() {
        //pobieranie nowych wartości z podmiotu
        int hour = subject.getHour();
        int minute = subject.getMinute();
        //…itd

        //wyświetlanie cyfrowego zegara.
    }
}
