package observer.book_example;

import java.util.LinkedList;
import java.util.List;

/**
 * Subject
 * - zna powiązane z nim obserwatory, podmiot może obserwować dowolna liczba obiektów observer.
 * - udostępnia interfejs do dołączania i odłączania obiektów Observer.
 */
public abstract class Subject {
    private final List<Observer> observers;

    public Subject(){
        this.observers = new LinkedList<>();
    }
    
    public void attach(Observer observer){
        observers.add(observer);
    }
    
    public void detach(Observer observer){
        observers.remove(observer);
    }
    
    public void notifyObservers(){
        for (Observer observer : observers) {
            observer.update(this);
        }
    }
}
