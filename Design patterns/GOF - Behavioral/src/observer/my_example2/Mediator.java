package observer.my_example2;

/**
 * Mediator
 */
public interface Mediator {
    
    void add(User user);
    
    void notifyOthersAboutNewPost(User user, String message);
    
}
