package observer.my_example2;

import static java.lang.System.out;

/**
 * Colleagues
 */
public class User {

    private final String name;
    private Mediator mediator;

    public User(String name, Mediator mediator) {
        this.name = name;
        this.mediator = mediator;
    }

    public void recieve(User user, String post) {
        out.println("TABLICA[" + name + "]\n" + user + ": " + post);
    }

    public void postOnMyWall(String message) {
        out.println(message);
        mediator.notifyOthersAboutNewPost(this, message);
    }

    @Override
    public String toString() {
        return name;
    }
    
}
