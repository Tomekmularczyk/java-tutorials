package observer.my_example2;

import java.io.InputStream;

/**
 * Mediator to takie centrum komunikacji miedzy obiektami, które możemy nazwać "kolegami" ponieważ w jakiś sposób ze sobą "rozmawiają"
 */
public class Test {

    public static void main(String[] args) {
        Mediator facebookMediator = new FacebookMediator();
        User adrian = new User("Adrian", facebookMediator);
        User kasia = new User("Kasia", facebookMediator);
        User janek = new User("Jan", facebookMediator);
        User bozena = new User("Bożena", facebookMediator);
        User jozek = new User("Józef", facebookMediator);
        InputStream stream;
        facebookMediator.add(adrian);
        facebookMediator.add(kasia);
        facebookMediator.add(janek);
        facebookMediator.add(bozena);
        facebookMediator.add(jozek);
        
        jozek.postOnMyWall("Jestem w związku!!!!!!");
    }
}
