package observer.my_example2;

import java.util.LinkedList;
import java.util.List;

/**
 * Concrete Mediator, zna wszystkich kolegów,
 * W tym przykładzie Mediator działa podobnie jak observer
 */
public class FacebookMediator implements Mediator{
    private final List<User> users;

    public FacebookMediator() {
        this.users = new LinkedList<>();
    }
    
    @Override
    public void add(User user){
        users.add(user);
    }
    
    @Override
    public void notifyOthersAboutNewPost(User user, String message) {
        for (User us : users) {
            us.recieve(user, message);
        }
    }

}
