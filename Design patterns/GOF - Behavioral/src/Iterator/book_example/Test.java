package Iterator.book_example;

import static java.lang.System.out;

/**
 * Iterator - zapewnia sekwencyjny dostep do elementów obiektu złożonego bez ujawniania jego wewnetrznej reprezentacji.
 * Ze wzorca należy korzystać kiedy:
 * - chcesz uzyskać dostęp do zawartości obiektu zagregowanego bez ujawniania jego wewnętrznej reprezentacji.
 * - jeśli chcesz umożliwić jednoczesne działanie wielu procesów przechodzenia po obiektach
 * - jeżeli chcesz udostępnić jednolity interfejs do poruszania się po różnych zagregowanych strukturach (czyli zapewnić obsługę iteracji polimorficznej)
 * 
 * iteratory dzielą się na: 
 * zewnętrzne - klient steruje iterowaniem
 * wewnętrzne - klient przekazuje im operacje do wykonania a iterator uruchamia ją dla każdego agregatu.
 */
public class Test {
    public static void main(String[] args) {
        List<Employee> employeesList = null; //nie mamy jeszcze implementacji dlatego null
        ListIterator<Employee> forward = new ListIterator<>(employeesList);
        ReverseListIterator<Employee> backward = new ReverseListIterator<>(employeesList);
        
        printEmployees(forward);
        printEmployees(backward);
    }
    
    private static void printEmployees(Iterator<Employee> employees){
        for (employees.first(); employees.isDone(); employees.next()) {
            out.println(employees.currentItem());
        }
    }
}
