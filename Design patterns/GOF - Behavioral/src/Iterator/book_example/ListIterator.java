package Iterator.book_example;

/**
 * ConcreteIterator
 */
public class ListIterator<T> implements Iterator<T>{
    private List<T> list;
    private long current;

    public ListIterator(List<T> aList) {
        this.list = aList;
        this.current = 0;
    }
    
    /**
     * Ustawia iterator na pierwszym miejscu
     */
    @Override
    public void first() {
        current = 0;
    }

    /**
     * Zmienia bieżący element na następny
     */
    @Override
    public void next() {
        current++;
    }

    /**
     * Sprawdza czy index wskazuje na element z listy
     */
    @Override
    public boolean isDone() {
        return current >= list.count();
    }

    /**
     * Zwraca element znajdujący się pod bierzącym elementem.
     */
    @Override
    public T currentItem() {
        if(isDone()){
            throw new IndexOutOfBoundsException();
        }
        
        return list.get(current);
    }
    
}
