package Iterator.book_example;

/**
 * Aggregate
 */
public abstract class List<T> implements Iterator<T> {

    public abstract long count();
    public abstract T get(long nr);

}
