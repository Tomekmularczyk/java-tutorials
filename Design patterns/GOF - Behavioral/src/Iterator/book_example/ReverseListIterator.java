package Iterator.book_example;

/**
 * ConcreteIterator
 */
public class ReverseListIterator<T> implements Iterator<T>{
    private List<T> list;
    private long current;

    public ReverseListIterator(List<T> aList) {
        this.list = aList;
        this.current = 0;
    }
    
    /**
     * Ustawia iterator na ostatnim miejscu
     */
    @Override
    public void first() {
        current = list.count();
    }

    /**
     * Zmienia bieżący element na poprzedni
     */
    @Override
    public void next() {
        current--;
    }

    /**
     * Sprawdza czy index wskazuje na element z listy
     */
    @Override
    public boolean isDone() {
        return current <= list.count();
    }

    /**
     * Zwraca element znajdujący się pod bierzącym elementem.
     */
    @Override
    public T currentItem() {
        if(isDone()){
            throw new IndexOutOfBoundsException();
        }
        
        return list.get(current);
    }
    
}