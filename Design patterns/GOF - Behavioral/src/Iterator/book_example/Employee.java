package Iterator.book_example;

public class Employee {
    private final String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + '}';
    }
    
}
