package Iterator.myexample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Menel implements Collector<String>{
    private String[] folijki;
    private final Collection<String> puszki;
    private final Map<String, Double> alkohole;

    public Menel() {
        this.folijki = new String[60];
        this.puszki = Collections.synchronizedCollection(new ArrayList<String>());
        this.alkohole = new HashMap<>();
    }

    public void addFolijki(String[] producent){
       this.folijki = producent;
    }
    
    public void addPuszka(String producent){
        this.puszki.add(producent);
    }
    
    public void addAlkohol(String producent, double procent){
        this.alkohole.put(producent, procent);
    }
    
    @Override
    public Iterator<String> createIterator() {
        List<String> stuff = new ArrayList<>();
        stuff.addAll(Arrays.asList(folijki));
        stuff.addAll(puszki);
        stuff.addAll(alkohole.keySet());
        return stuff.iterator();
    }

}
