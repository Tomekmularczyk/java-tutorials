package Iterator.myexample;

import java.util.Iterator;

/**
 * Aggregate
 * Nie dbamy o to jak będą przechowywane elementy w konkretnych obiektach. 
 * Chcemy dostać wspólny interfejsc do przemierzania ich. Klient może nawet zechcieć udostępnić tylko część kolekcji i to zrobic.
 */
public interface Collector<T> {
    
    //fabric method
    Iterator<T> createIterator();
}
