package Iterator.myexample;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class PokemonTrainer implements Collector<String>{

    private final List<String> waterPokemons;
    private final List<String> electricPokemons;
    private final List<String> firePokemons;

    public PokemonTrainer() {
        this.waterPokemons = new LinkedList<>();
        this.electricPokemons = new ArrayList<>();
        this.firePokemons = new CopyOnWriteArrayList();
    }

    public void addWaterPokemon(String name) {
        waterPokemons.add(name);
    }

    public void addElectricPokemon(String name) {
        electricPokemons.add(name);
    }

    public void addFirePokemon(String name) {
        firePokemons.add(name);
    }

    @Override
    public Iterator<String> createIterator() {
        List<String> fullPokemonList = new LinkedList<>();
        fullPokemonList.addAll(firePokemons);
        fullPokemonList.addAll(waterPokemons);
        fullPokemonList.addAll(electricPokemons);
        
        return fullPokemonList.iterator();
    }
}
