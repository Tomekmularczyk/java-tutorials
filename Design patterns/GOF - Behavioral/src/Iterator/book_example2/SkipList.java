package Iterator.book_example2;

/**
 * Aggregate
 */
public abstract class SkipList<T> implements AbstractList {

    public abstract long count();
    public abstract T get(long nr);

}
