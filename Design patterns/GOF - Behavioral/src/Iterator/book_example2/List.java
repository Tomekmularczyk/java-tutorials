package Iterator.book_example2;

import Iterator.book_example.Iterator;

/**
 * Aggregate
 */
public abstract class List<T> implements AbstractList<T>{

    public abstract long count();
    public abstract T get(long nr);

    @Override
    public Iterator<T> createIterator(){
        return new ListIterator<>(this);
    }
    
    

}
