package Iterator.book_example2;

import Iterator.book_example.Employee;
import Iterator.book_example.Iterator;
import static java.lang.System.out;

/**
 * Iterator ciąg dalszy… Unikanie powiązania z określoną implementacją listy.
 *
 *
 */
public class Test {

    public static void main(String[] args) {
        AbstractList<Employee> employees = null;
        Iterator<Employee> iterator = employees.createIterator();
        
        printEmployees(iterator);
    }

    private static void printEmployees(Iterator<Employee> employees) {
        for (employees.first(); employees.isDone(); employees.next()) {
            out.println(employees.currentItem());
        }
    }
}
