package Iterator.book_example2;

import Iterator.book_example.Iterator;

public interface AbstractList<T> {
    
    /**
     * Korzystamy z metody wytwórczej do wygenerowania egezmplarzy odpowiednich klas Iterator
     */
    Iterator<T> createIterator();
}
