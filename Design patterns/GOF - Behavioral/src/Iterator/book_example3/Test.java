package Iterator.book_example3;

import Iterator.book_example.Employee;
import Iterator.book_example.List;

/**
 * Iterator wewnętrzny (pasywny)
 * W tym rozwiązaniu to iterator kontroluje iterowanie i uruchamia operacje dla każdego elementu.
 * Problem polega tu na sparametryzowaniu iteratora za pomocą operacji, którą checmy uruchomić dla każdego elementu.
 */
public class Test {
    public static void main(String[] args) {
        List<Employee> employees = null;
        
        PrintNEmployees pa = new PrintNEmployees(employees, 10);
        pa.traverse();
        
        /*
            Warto zauważyć, że klient nie musiał tworzyć pętli, a całą logikę procesu iteracji można wielokrotnie wykorzystać,
            jest to główna zaleta stosowania iteratora wewnętrznego.
        
            Iteratory wewnętrzne moga kapsułkować różne metody iteracji. Na przykład klasa FilteringListTraverser kapsułkuje iteracje,
            która przetwarza tylko elementy o określonych cechach.
        */
        
        FilteringListTraverser<Employee> fLT = new FilteringListTraverser<>(employees, e -> e.toString().contains("Kowalski"));
        fLT.traverse();
    }
}
