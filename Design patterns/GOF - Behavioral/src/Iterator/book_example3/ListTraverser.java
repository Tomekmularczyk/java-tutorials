package Iterator.book_example3;

import Iterator.book_example.List;
import Iterator.book_example.ListIterator;

public abstract class ListTraverser<T> {
    private final ListIterator<T> iterator;

    public ListTraverser(List<T> aList) {
        this.iterator = new ListIterator(aList);
    }
    
    public boolean traverse(){
        boolean result = false;
        
        for(iterator.first(); !iterator.isDone(); iterator.next()){
            result = processItem(iterator.currentItem());
            if(!result){
                break;
            }
        }
        
        return result;
    }

    protected abstract boolean processItem(T currentItem);
    
}
