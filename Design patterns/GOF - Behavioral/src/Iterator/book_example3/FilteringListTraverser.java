package Iterator.book_example3;

import Iterator.book_example.Iterator;
import Iterator.book_example.List;
import java.util.function.Predicate;

public class FilteringListTraverser<T> extends ListTraverser<T> {
    private Iterator<T> iterator;
    private final Predicate predicate;
    
    public FilteringListTraverser(List<T> list, Predicate predicate) {
        super(list);
        this.predicate = predicate;
    }

    @Override
    protected boolean processItem(T currentItem) {
        boolean result = false;
        for (iterator.first(); !iterator.isDone(); iterator.next()) {
            if(testItem(currentItem)){
                result = processItem(currentItem);
                if(!result){
                    break;
                }
            }
        }
        return result;
    }
    

    protected boolean testItem(T currentItem) {
        return predicate.test(currentItem);
    }
}
