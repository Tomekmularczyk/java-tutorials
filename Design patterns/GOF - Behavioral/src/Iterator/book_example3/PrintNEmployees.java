package Iterator.book_example3;

import Iterator.book_example.Employee;
import Iterator.book_example.List;
import static java.lang.System.out;

public class PrintNEmployees extends ListTraverser<Employee>{
    private int total, count;
    
    public PrintNEmployees(List<Employee> aList, int n) {
        super(aList);
        this.total = n;
        this.count = 0;
    }

    @Override
    protected boolean processItem(Employee currentItem) {
        count++;
        out.println(currentItem);
        return count < total;
    }

    
}
