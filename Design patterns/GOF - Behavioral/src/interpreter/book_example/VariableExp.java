package interpreter.book_example;


public class VariableExp implements BooleanExp{
    private char name;

    public VariableExp(char name) {
        this.name = name;
    }
    
    @Override
    public boolean evaluate(Context ctx) { //obliczanie wartości zmiennej polega na zwróceniu jej wartości w bierzącym kontekscie
        return ctx.lookup(name);
    }

    @Override
    public BooleanExp replace(char c, BooleanExp expression) { //aby zastąpić zmienną wyrażeniem należy sprawdzić czy ma ona tę samą nazwę co zmienna przekazana jako argument
        if(name == c){
            return expression.copy();
        } else {
            return new VariableExp(name);
        }
    }

    @Override
    public BooleanExp copy() { //skopiowanie zmiennej prowadzi do zwrócenia nowego obiektu VariableExp
        return new VariableExp(name);
    }

}
