package interpreter.book_example;


public class OrExp implements BooleanExp{ //reprezentuje wyrażenie logiczne utworzone przez połączenie dwóch wyrażeń logicznych operatorem OR
    private BooleanExp operand1;
    private BooleanExp operand2;

    public OrExp(BooleanExp operand1, BooleanExp operand2) {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }

    @Override
    public boolean evaluate(Context ctx) { //oblicza wartość operandów i zwraca ich iloczyn logiczny
        return operand1.evaluate(ctx) || operand2.evaluate(ctx);
    }

    //Operacje replace i copy są zaimplementowane za pomocą wywołań rekurencyjnych kierowanych do jej operandów
    @Override
    public BooleanExp replace(char c, BooleanExp expression) {
        return new OrExp(operand1.replace(c, expression), operand2.replace(c, expression));
    }

    @Override
    public BooleanExp copy() {
        return new OrExp(operand1.copy(), operand2.copy());
    }
    
    
}
