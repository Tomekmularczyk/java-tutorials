package interpreter.book_example;

/**
 * Interpreter określa reprezentację gramatyki języka raz interpreter, który wykorzystuje tę reprezentację do interpretowania zdań
 * z danego języka.
 * 
 * Do reprezentowania poszczególnych reguł gramatyki służą klasy.
 * Wzorzec ten jest najbardziej przydatny gdy gramatyka jest prosta i wydajność nie jest najważniejsza.
 * 
 * Elementy:
 * - Client
 * - Context
 * - AbstractExpression
 * - TerminalExpression (wyrażenie końcowe)
 * - NonterminalExpression (wyrażenie pośrednie)
 */
public class Test {

    public static void main(String[] args) {
        //Teraz możemy zdefiniować wyrażenie logiczne: (true and x) or (y and (not x))
        //i obliczyć jego wartość po przypisaniu zmiennych true i false do zmiennych x i y;
        
        BooleanExp booleanExp;
        Context ctx = null;
        
        VariableExp x = new VariableExp('X');
        VariableExp y = new VariableExp('Y');
        
        booleanExp = new OrExp(
                new AndExp(/*new Constant(true)*/null, x),
                new AndExp(y, new NotExp(x)));
        
        ctx.assign(y, true);
        ctx.assign(x, false);
        
        boolean result = booleanExp.evaluate(ctx);
        
        //dla określonych tu wartości zmiennych x i y wyrażenie ma wartość true. 
        //Aby obliczyć wartość wyrażenia dla innych wartości zmiennych x i y wystarczy zmienić kontekst
        //Na koniec możemy zastąpić zmienną y nowym wyrażeniem i ponownie obliczyć wartość wcześniejszego wyrażenia:
        VariableExp z = new VariableExp('Z');
        NotExp not_Z = new NotExp(z);
        
        BooleanExp replacement = booleanExp.replace('Y', not_Z);
        ctx.assign(z, true);
        boolean result2 = replacement.evaluate(ctx);
        
        //Ten przykład ilustruje jedną ważną cechę wzorca - do interpretowania możemy używać różnorodne operacje.
    }

}
