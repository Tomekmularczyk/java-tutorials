package interpreter.book_example;


public class NotExp implements BooleanExp{ //reprezentuje wyrażenie logiczne utworzone przez połączenie dwóch wyrażeń logicznych operatorem NOT
    private BooleanExp operand1;

    public NotExp(BooleanExp operand1) {
        this.operand1 = operand1;
    }

    @Override
    public boolean evaluate(Context ctx) { //oblicza wartość operandów i zwraca ich iloczyn logiczny
        return !operand1.evaluate(ctx);
    }

    //Operacje replace i copy są zaimplementowane za pomocą wywołań rekurencyjnych kierowanych do jej operandów
    @Override
    public BooleanExp replace(char c, BooleanExp expression) {
        return new NotExp(operand1.replace(c, expression));
    }

    @Override
    public BooleanExp copy() {
        return new NotExp(operand1.copy());
    }
    
    
}
