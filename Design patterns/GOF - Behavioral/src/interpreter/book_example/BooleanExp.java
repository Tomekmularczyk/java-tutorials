package interpreter.book_example;

/**
 * Abstract Expression
 * - obejmuje deklaracje operacji abstrakcyjnej Interpret wspólnej wszystkim węzłom drzewa składni abstrakcyjnej.
 */
public interface BooleanExp { //definiuje interfejs wszystkich klas określajacych wyrażenia logiczne:

    boolean evaluate(Context ctx);
    
    BooleanExp replace(char c, BooleanExp expression);
    
    BooleanExp copy();
}
