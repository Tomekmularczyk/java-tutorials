package interpreter.book_example;

/**
 * Context - przechowuje informacje globalne interpretera.
 */
public interface Context { //definiuje odwzorowanie ze zmiennych na wartości logiczne reprezentowane za pomocą true i false
    
    boolean lookup(char c);
    void assign(VariableExp variable, boolean bool);
    
}
