package interpreter.derek_banas_example;

public abstract class Expression {
	
	public abstract String gallons(double quantity);
	public abstract String quarts(double quantity);
	public abstract String pints(double quantity);
	public abstract String cups(double quantity);
	public abstract String tablespoons(double quantity);
	
}