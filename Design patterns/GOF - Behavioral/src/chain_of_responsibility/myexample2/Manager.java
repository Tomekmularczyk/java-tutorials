package chain_of_responsibility.myexample2;

import static java.lang.System.out;
import java.util.function.Consumer;

/**
 * ConcreteHandler
 */
public class Manager extends CompanyWorker{

    public Manager(CompanyWorker parent) {
        super(parent);
    }

    @Override
    public void handle(Consumer consumer) {
        parent.handle(consumer);
    }

    @Override
    public void handle() {
        out.println("handle something");
    }

}
