package chain_of_responsibility.myexample2;

public abstract class CompanyWorker implements Handler{
    protected final CompanyWorker parent;

    public CompanyWorker(CompanyWorker parent) {
        this.parent = parent;
    }
    
}
