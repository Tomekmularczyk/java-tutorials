package chain_of_responsibility.myexample2;

import java.util.function.Consumer;

public class Worker extends CompanyWorker{

    public Worker(CompanyWorker parent) {
        super(parent);
    }
    
    @Override
    public void handle(Consumer consumer) {
        parent.handle(consumer);
    }

    @Override
    public void handle() {
        parent.handle();
    }

}
