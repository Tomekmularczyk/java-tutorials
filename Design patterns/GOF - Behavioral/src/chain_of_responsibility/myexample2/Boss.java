package chain_of_responsibility.myexample2;

import static java.lang.System.out;
import java.util.function.Consumer;

public class Boss extends CompanyWorker{

    public Boss(CompanyWorker parent) {
        super(parent);
    }

    @Override
    public void handle(Consumer consumer) {
        consumer.accept(this);
    }

    @Override
    public void handle() {
        out.println("handling");
    }

}
