package chain_of_responsibility.myexample2;

import java.util.function.Consumer;

/**
 * Handler
 */
public interface Handler {
    void handle(Consumer consumer);
    void handle();
}
