package chain_of_responsibility.book_example;

public class Application extends HelpHandler {

    public Application(Topic topic) {
        super(null, topic);
    }
    
    //operacje specyficzne dla aplikacji

    @Override
    public void handleHelp() {
        //wyswietlanie listy tematów pomocy
    }

    
}
