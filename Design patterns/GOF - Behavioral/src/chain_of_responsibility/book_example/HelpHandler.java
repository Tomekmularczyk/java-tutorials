package chain_of_responsibility.book_example;

/**
 * Handler - obiekt obsługujący: - definiuje interfejs do obsługi żądań, - (opcjonalnie) obejmuje implementacje odwołania do następnika.
 */
public abstract class HelpHandler {
    private HelpHandler successor;
    private Topic topic;

    public HelpHandler(HelpHandler successor, Topic topic) {
        this.successor = successor;
        this.topic = topic;
    }

    public boolean hasHelp() {
        return topic == Topic.NO_HELP_TOPIC;
    }

    public void handleHelp() {
        if(successor != null){
            successor.handleHelp();
        }
    }
    
    public void setHandler(HelpHandler handler){
        this.successor = handler;
    }
    
    public enum Topic {
        NO_HELP_TOPIC,
        PRINT_TOPIC,
        PAPER_ORIENTATION_TOPIC,
        APPLICATION_TOPIC;
    }
}
