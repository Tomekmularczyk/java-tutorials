package chain_of_responsibility.book_example;

import chain_of_responsibility.book_example.HelpHandler.Topic;


/**
 * Łańcuch zoobowiązań. Pozwala uniknąć wiązania nadawcy żądania z jego odbiorcą, ponieważ umożliwia obsłużenie żądania więcej niż jednemu obiektowi.
 * Łączy w łańcuch obiekty odbiorcze i przekzauje między nimi żądanie do momentu obsłużenia.
 * 
 * Wzorca należy używać w następujących warunkach:
 * - kiedy więcej niż jeden obiekt może obsłużyć żądanie, a nie wiadomo z góry, który z nich to zrobi. Obiekt obsługujący żądanie powinien być ustalan automatycznie.
 * - jeżeli chcesz przesyłać żądanie do jednego z kilku obiektów bez bezpośredniego określania odbiorcy,
 * - jeżeli zbiór obiektó, które mogą obsłużyć żądanie, należy określać dynamicznie.
 */
public class Test {
    public static void main(String[] args) {
        Application application = new Application(Topic.APPLICATION_TOPIC);
        
        Dialog dialog = new Dialog(application, Topic.PRINT_TOPIC);
        Button button = new Button(dialog, Topic.PAPER_ORIENTATION_TOPIC);
        
        button.handleHelp();
    }
}
