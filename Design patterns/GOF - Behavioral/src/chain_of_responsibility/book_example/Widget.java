package chain_of_responsibility.book_example;


public class Widget extends HelpHandler{
    protected Widget parent;
    
    public Widget(Widget parent, Topic topic) {
        super(parent, topic);
        this.parent = parent;
    }
    
}
