package chain_of_responsibility.book_example;

/**
 * ConcreteHandler
 * - obsługuje żądania, za które odpowiada
 * - może uzyskać dostęp do następnika,
 * - jeśli obiekt ConcreteHandler potrafi obsłużyć żądanie, robi to; w przeciwnym razie przekazuje je do następnika.
 */
public class Dialog extends Widget{
    
    public Dialog(HelpHandler parent, Topic topic) { //tutaj nastepnikiem nie jest widget ale dowolny obiekt obsługujący żądania pomocy
        super(null, topic);
        setHandler(parent);
    }
    
    @Override
    public void handleHelp() {
        if(hasHelp()){
            //udostepnia informacje na temat przycisku
        } else {
            super.handleHelp();
        }
    }
    
}
