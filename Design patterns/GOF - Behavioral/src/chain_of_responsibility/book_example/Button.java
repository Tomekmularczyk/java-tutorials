package chain_of_responsibility.book_example;

/**
 * ConcreteHandler
 * - obsługuje żądania, za które odpowiada
 * - może uzyskać dostęp do następnika,
 * - jeśli obiekt ConcreteHandler potrafi obsłużyć żądanie, robi to; w przeciwnym razie przekazuje je do następnika.
 */
public class Button extends Widget{

    public Button(Widget parent, Topic topic) {
        super(parent, topic);
    }

    @Override
    public void handleHelp() {
        if(hasHelp()){
            //udostepnia informacje na temat przycisku
        } else {
            parent.handleHelp();
        }
    }

    
}
