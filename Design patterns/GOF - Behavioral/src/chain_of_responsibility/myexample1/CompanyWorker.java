package chain_of_responsibility.myexample1;

public abstract class CompanyWorker implements Handler{
    protected final CompanyWorker successor;

    public CompanyWorker(CompanyWorker successor) {
        this.successor = successor;
    }
}
