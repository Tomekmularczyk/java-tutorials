package chain_of_responsibility.myexample1;

import static java.lang.System.out;

public class RandomPerson implements Handler {

    @Override
    public void handleEmergencySituation(EmergencySituation situation) {
        if (situation == EmergencySituation.SOMEONE_HAD_ACCIDENT) {
            out.println("RandomPerson: Udzielam pomocy!");
        } else {
            out.println("RandomPerson: Nie jestem w stanie wam pomóc.");
        }
    }

}
