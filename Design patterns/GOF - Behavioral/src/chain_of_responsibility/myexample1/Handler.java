package chain_of_responsibility.myexample1;

/**
 * Handler
 */
public interface Handler {
    void handleEmergencySituation(EmergencySituation situation);
}
