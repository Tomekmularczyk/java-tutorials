package chain_of_responsibility.myexample1;

import static java.lang.System.out;

public class Manager extends CompanyWorker {

    public Manager(CompanyWorker successor) {
        super(successor);
    }

    @Override
    public void handleEmergencySituation(EmergencySituation situation) { 
        switch (situation) {
            case THIEF_IN_A_SHOP:
                out.println("Manager: łapać złodzieja!");
                break;
            case SOMEONE_HAD_ACCIDENT:
                out.println("Manager: Udzielam pomocy!");
                break;
            case ANGRY_CUSTOMER:
                out.println("Manager: Proszę Pana, ale proszę nie podnosić głosu!"); 
                break;
            default:
                successor.handleEmergencySituation(situation);
                break;
        }
    }
}