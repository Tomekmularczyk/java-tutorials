package chain_of_responsibility.myexample1;

public class Test {
    public static void main(String[] args) {
        Boss boss = new Boss();
        Manager manager = new Manager(boss);
        Worker worker = new Worker(manager);
        RandomPerson randomPerson = new RandomPerson();
        
        accidentSituation(randomPerson);
        
        bussinessOffer(worker);
        
        bussinessOffer(randomPerson);
    }
    
    public static void accidentSituation(Handler handlingPerson){
        handlingPerson.handleEmergencySituation(EmergencySituation.SOMEONE_HAD_ACCIDENT);
    }
    
    public static void bussinessOffer(Handler handlingPerson){
        handlingPerson.handleEmergencySituation(EmergencySituation.BUSSINESS_OFFER);
    }
}
