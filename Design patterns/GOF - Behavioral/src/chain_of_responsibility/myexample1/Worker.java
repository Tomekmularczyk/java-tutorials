package chain_of_responsibility.myexample1;

import static java.lang.System.out;

public class Worker extends CompanyWorker {

    public Worker(CompanyWorker successor) {
        super(successor);
    }

    @Override
    public void handleEmergencySituation(EmergencySituation situation) {
        switch (situation) {
            case THIEF_IN_A_SHOP:
                out.println("Worker: łapać złodzieja!");
                break;
            case SOMEONE_HAD_ACCIDENT:
                out.println("Worker: Udzielam pomocy!");
                break;
            default:
                successor.handleEmergencySituation(situation);
                break;
        }
    }

}
