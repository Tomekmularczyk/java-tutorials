package chain_of_responsibility.myexample1;

import static java.lang.System.out;

public class Boss extends CompanyWorker {

    public Boss() {
        super(null);
    }

    //Boss może najwięcej
    @Override
    public void handleEmergencySituation(EmergencySituation situation) { 
        switch (situation) {
            case THIEF_IN_A_SHOP:
                out.println("Boss: łapać złodzieja!");
                break;
            case SOMEONE_HAD_ACCIDENT:
                out.println("Boss: Udzielam pomocy!");
                break;
            case ANGRY_CUSTOMER:
                out.println("Boss: Proszę Pana, ale proszę nie podnosić głosu!"); 
                break;
            case BUSSINESS_OFFER:
                out.println("Boss: Zapraszam do mojego gabinetu…");
                break;
            default:
                if(successor == null) out.println("Boss: przykro nam nie zajmujemy się takimi problemami.");
                break;
        }
    }
}