package chain_of_responsibility.myexample1;

public enum EmergencySituation {
    SOMEONE_HAD_ACCIDENT, //każdy
    THIEF_IN_A_SHOP, //każdy oprocz RandomPerson
    ANGRY_CUSTOMER, //manager i boss
    BUSSINESS_OFFER; //tylko boss
}
