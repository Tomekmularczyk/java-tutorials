package template_method.book_example;

/**
 * Template Method to podstawowa technika powtórnego wykorzystania kodu. Określa szkielet alogrytmu i pozostawia doprecyzowanie niektórych
 * jego kroków podklasom. Umożliwia modyfikację niektórych etapów algorytmu w podklasach bez zmiany jego struktury.
 * 
 * Należy stosować w warunkach:
 * - do jednorazowego implementowania niezmiennych części alogrytmu i umożliwienia implementowania zmieniających się zachowań w podklasach.
 * - Kiedy zachowanie wspólne dla podklas należy wyodrębnić i umieścić w jednej klasie, aby uniknąć powielania kodu. 
 *   Najpierw należy wykryć różnice w istniejącym kodzie, a następnie umieścić je w nowych operacjach. Na zakończenie trzeba zastąpić
 *   różniące się fragmenty kodu metodą szablonową wywołującą jedną z nowych operacji.
 * - Do kontrolowania rozszerzania podklas. Można zdefiniować metodę szablonową wywołującą w odpowiednich miejscach operacje stanowiące 
 *   "punkty zaczepienia", co umozliwia roszerzanie tych podklas tylko w tych punktach.
 */
public class Test {
    public static void main(String[] args) {
        View view = new MyView();
        
        //wywołanie metody szablonowej…
        view.display();
    }
}
