package template_method.book_example;

/**
 * ConcreteClass
 * - obejmuje implementację operacji prostych realizujących specyficzne dla podklasy etapy algorytmu.
 */
public class MyView extends View{

    @Override
    protected void doDisplay() {
        // wyświetlanie zawartości widoku.
    }

}
