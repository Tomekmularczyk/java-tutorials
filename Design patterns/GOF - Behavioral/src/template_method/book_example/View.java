package template_method.book_example;

/**
 * AbstractClass
 * - określa abstrakcyjne operacje proste definiowane w podklasach konkretnych w celu zaimplementowania etapów algorytmu.
 * - obejmuje implementację metody szablonowej definiującej szkielet algorytmu; metoda szablonowa wywołuje operacje proste
 *   a także operacje zdefiniowane w klasie AbstractClass lub w innych klasach.
 */
public abstract class View {

    /**
     * Metoda szablonowa
     */
    public void display(){
        setFocus();
        doDisplay();
        resetFocus();
    }

    /**
     * Punkt zaczepienia
     */
    protected abstract void doDisplay();

    /** operacje konkretne **/
    private void setFocus() {
        // ...
    }

    /** operacja prosta **/
    private void resetFocus() {
        // ...
    }
    
}
