package template_method.my_example1;

import static java.lang.System.out;

/**
 * AbstractClass, można by było z tego zrobić interfejs z defaultowymi metodami
 */
public abstract class HouseBuilder {

    /**
     * TemplateMethod
     */
    public void buildHouse() {
        getWorkers();
        setUpFromInside();
        BuildingTools buildingTools = getBuildingTools();
        buildingTools.buildKitchen();
        buildingTools.buildLivingRoom();
        cleanUp();
        finishBuilding();
    }

    //Punkt zaczepienia. Hollywood principle, "don't call us, we will call you"
    protected abstract void setUpFromInside();

    protected abstract void paintHouse();

    protected abstract void moveFamilyInside();

    /**
     * Metoda wytwórcza
     */
    protected abstract BuildingTools getBuildingTools();

    //operacja prosta i zarazem szablonowa
    private void finishBuilding() {
        paintHouse();
        moveFamilyInside();
    }

    //** operacje proste
    private void getWorkers() {
        out.println("Workers are ready!");
    }

    private void cleanUp() {
        out.println("Cleaning up the mess.");
    }

}
