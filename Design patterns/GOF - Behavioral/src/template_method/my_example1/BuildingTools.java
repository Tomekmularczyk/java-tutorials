package template_method.my_example1;

public interface BuildingTools {

    void buildLivingRoom();
    
    void buildKitchen();
    
}
