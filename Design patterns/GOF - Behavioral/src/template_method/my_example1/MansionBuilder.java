package template_method.my_example1;

import static java.lang.System.out;

public class MansionBuilder extends HouseBuilder{

    @Override
    protected void setUpFromInside() {
        out.println("1 000 000$ accessory set up.");
    }

    @Override
    protected void paintHouse() {
        out.println("Painting house yellow");
    }

    @Override
    protected void moveFamilyInside() {
        out.println("Moving famous person family.");
    }

    @Override
    protected BuildingTools getBuildingTools() {
        return new MyBuildingTools();
    }

    private class MyBuildingTools implements BuildingTools {

        @Override
        public void buildLivingRoom() {
            out.println("Big ass kitchen builded");
        }

        @Override
        public void buildKitchen() {
            out.println("Master chief kitchen builded");
        }
        
    }
    
}
