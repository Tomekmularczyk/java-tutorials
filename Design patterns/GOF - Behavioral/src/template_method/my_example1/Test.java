package template_method.my_example1;

public class Test {
    public static void main(String[] args) {
        HouseBuilder builder = new MansionBuilder();
        
        builder.buildHouse();
    }
}
