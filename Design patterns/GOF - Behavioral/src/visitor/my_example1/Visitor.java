package visitor.my_example1;

public interface Visitor {
    void visit(Human human);
    void visit(Robot robot);
    void visit(Shop shop);
    
    //void visit(Kid kid); 
    //jeżeli chcielibyśmy przeciążyć metodę z argumentem który jest podklasą np. Human to kompilator zaprotestuje, 
    //nie będzie wiedział którą metodę przeładować gdy podamy mu obiekt Kid - metodę która przyjmuje Kid, czy Human?
    //rozwiązanie może być bardzo proste, wystarczy inaczej nazwać metodę np: visitKid(Kid kid);
}
