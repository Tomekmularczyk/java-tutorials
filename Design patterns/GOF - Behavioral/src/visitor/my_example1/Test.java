package visitor.my_example1;

import static java.lang.System.out;


/**
 * Visitatora używamy gdy potrzebujemy wydzielić pewną logikę z różnego rodzaju klas (np. gdy mają różne interfejsy) i umieścić ją w jednym miejscu.
 * Przydaje się też wtedy gdy potrzebujesz przeprowadzić różne i nie powiązane operacje na danej strukturze obiektów. Innymi słowy:
 * dodaje funkcjonalność do klas bez ich zmieniania.
 * 
 * Dzięki temu kod w klasach jest przejrzystszy, a jeżeli logika algorytmu się zmienia, to nie musimy jej zmieniać w każdej klasie.
 * Innym plusem jest to, że nie dodanie logiki do nowej klasy jest łatwe, wystarczy przeładować metodę w Visitorze, nie zmieniając elementu.
 * 
 * Minusem jest to, że musimy z góry wiedzieć zwracany typ metody visit() inaczej będziemy musieli interfejs i wszystkie jego implemnetacje.
 * Innym minusem jest to, że jeżeli jest zbyt dużo implementacji Visitora to trudno jest go potem roszerzać.
 */
public class Test {
    public static void main(String[] args) {
        Human human = new Human("Kazek");
        Shop shop = new Shop("Eclerc");
        Robot robot = new Robot();
        
        Visitable[] objectsToOperate = {human, robot, shop};
        
        Visitor askQuestions = new AskQuestions();
        for (Visitable element : objectsToOperate) {
            element.accept(askQuestions);
        }
        
        out.println("===================");
        
        Visitor playWith = new PlayWith();
        for (Visitable element : objectsToOperate) {
            element.accept(playWith);
        }
        
        
    }
}
