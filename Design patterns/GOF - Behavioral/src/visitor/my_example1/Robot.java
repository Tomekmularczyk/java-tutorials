package visitor.my_example1;

import static java.lang.System.out;
import java.time.Year;

public class Robot implements Visitable{
    private String software = "Java SE + Guava";
    private double price = 20000;
    private Year productionDate = Year.of(2016);
    
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public String getSoftware() {
        return software;
    }

    public double getPrice() {
        return price;
    }

    public Year getProductionDate() {
        return productionDate;
    }

    
    //******************* jakies specyficzne metody ***********
    
    public void talk(){
        out.println("R.O.B.O.T.T.A.L.K.I.N.G");
    }
    
    public void washDishes(){
        out.println("Robot is washing dishes....");
    }
    
    public String tellAJoke(){
        return "Przychodzi baba do lekarza, a lekarz też baba.";
    }
}
