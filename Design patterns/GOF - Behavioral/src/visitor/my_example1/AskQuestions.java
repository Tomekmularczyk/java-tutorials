package visitor.my_example1;

import static java.lang.System.out;

public class AskQuestions implements Visitor{

    @Override
    public void visit(Human human) {
        human.doYouLikePets();
        human.tellYourName();
    }

    @Override
    public void visit(Robot robot) {
        robot.getPrice();
        robot.getProductionDate();
        robot.getSoftware();
        robot.talk();
        robot.tellAJoke();
    }

    @Override
    public void visit(Shop shop) {
        shop.getShopName();
        shop.askClerk();
        out.println(shop.takeItem());
    }

}
