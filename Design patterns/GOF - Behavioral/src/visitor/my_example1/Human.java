package visitor.my_example1;

import static java.lang.System.out;

public class Human implements Visitable{
    private final String name;

    public Human(String name) {
        this.name = name;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    
    //******************* jakies specyficzne metody ***********
    public void tellYourName(){
        out.println("My name is: " + name);
    }
    
    public void runForest(){
        out.println("I'm running away motherf...");
    }
    
    public boolean doYouLikePets(){
        return true;
    }
}
