package visitor.my_example1;

public class PlayWith implements Visitor{

    @Override
    public void visit(Human human) {
        human.doYouLikePets();
        human.runForest();
    }

    @Override
    public void visit(Robot robot) {
        robot.tellAJoke();
        robot.washDishes();
    }

    @Override
    public void visit(Shop shop) {
        shop.askClerk();
        shop.takeCart();
    }

}
