package visitor.my_example1;

/**
 * AbstractVisitator
 */
public interface Visitable {
    void accept(Visitor visitor);
}
