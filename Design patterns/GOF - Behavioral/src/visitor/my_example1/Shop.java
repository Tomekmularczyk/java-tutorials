package visitor.my_example1;

import static java.lang.System.out;

public class Shop implements Visitable {

    private final String shopName;

    public Shop(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public String getShopName() {
        return shopName;
    }

    //******************* jakies specyficzne metody ***********
    public void takeCart() {
        out.println("Taking shop cart");
    }

    public double calculateItemsInCart() {
        return 334.44;
    }

    public boolean askClerk() {
        return true;
    }

    public Object takeItem() {
        return new Object() {
            @Override
            public String toString() {
                return "Banan";
            }
        };
    }

}
