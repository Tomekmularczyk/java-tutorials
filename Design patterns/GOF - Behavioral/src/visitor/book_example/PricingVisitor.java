package visitor.book_example;

/**
 * ConcreteVisitor - obejmuje implementacje wszystkich operacji zdefiniowanych w klasie Visitor. 
 * Każda operacja realizuje fragment algorytmu zdefiniowanego na potrzeby odpowiedniej klasy obiektu ze struktury.
 * Klasa ConcreteVisitor udostepnia kontekst działania algorytmu i przechowuje jego lokalny stan. W stanie często akumulowane są wyniki w czasie przechodzenia po danej strukturze.
 */
public class PricingVisitor implements EquipmentVisitor{
    private double total;
    
    @Override
    public void visitFloppyDisk(FloppyDisk floppyDisk) {
        total += floppyDisk.netPrice();
    }

    @Override
    public void visitCard(Card card) {
        total += card.netPrice();
    }

    @Override
    public void visitChassis(Chassis chassis) {
        total += chassis.netPrice();
    }

    @Override
    public void visitBus(Bus bus) {
        total += bus.netPrice();
    }
    
}
