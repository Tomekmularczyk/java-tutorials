package visitor.book_example;

import java.util.Collections;

/**
 * Element - definiuje operacje accept przyjmującą odwiedzającego jako argument.
 * Wzbogaciliśmy klasę z przykładu wzorca Kompozyt o operacje accept(EquipmentVisitor visitor);
 */
public abstract class Equipment {
    private String name;
    
    protected Equipment(String name){
        this.name = name;
    }
    
    public String name(){
        return name;
    }
    
    
    public abstract int power();
    public abstract double netPrice();
    public abstract double discountPrice();
    
    
    public abstract void accept(EquipmentVisitor visitor);
    
    
    public abstract void add(Equipment equipment);
    public abstract void remove(Equipment equipment);
    /**
     * zwraca dostęp do części obiektu.
     * Domyślna implementacja zwraca pustą listę
     */
    public Iterable<Equipment> createIterator(){
        return Collections.emptyList();
    }
}
