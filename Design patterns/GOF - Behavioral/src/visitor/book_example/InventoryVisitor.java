package visitor.book_example;

import java.util.LinkedList;
import java.util.List;

/**
 * ConcreteVisitor - obejmuje implementacje wszystkich operacji zdefiniowanych w klasie Visitor. 
 * Każda operacja realizuje fragment algorytmu zdefiniowanego na potrzeby odpowiedniej klasy obiektu ze struktury.
 * Klasa ConcreteVisitor udostepnia kontekst działania algorytmu i przechowuje jego lokalny stan. W stanie często akumulowane są wyniki w czasie przechodzenia po danej strukturze.
 */
public class InventoryVisitor implements EquipmentVisitor{
    private List<Equipment> equipment = new LinkedList<>(); //zbieramy listę użytych urządzeń
    
    @Override
    public void visitFloppyDisk(FloppyDisk floppyDisk) {
        equipment.add(floppyDisk);
    }

    @Override
    public void visitCard(Card card) {
        equipment.add(card);
    }

    @Override
    public void visitChassis(Chassis chassis) {
        equipment.add(chassis);
    }

    @Override
    public void visitBus(Bus bus) {
        equipment.add(bus);
    }

    public List<Equipment> getTotalEquipment() {
        return equipment;
    }
}
