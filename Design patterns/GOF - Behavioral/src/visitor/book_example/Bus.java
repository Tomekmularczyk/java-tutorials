package visitor.book_example;

/**
 * ConcreteElement - obejmuje implementację operacji Accept przyjmującej odwiedzającego jako argument.
 */
public class Bus extends CompositeEquipment{

    public Bus(String name) {
        super(name);
    }

    @Override
    public int power() {
        return 4;
    }

    @Override
    public double discountPrice() {
        return 1.1;
    }

    /**
     * W klasach sprzętu składającego się z innych urządzeń operacja accept przechodzi po elementach podrzędnych i wywołuje operację accept dla każdego z nich.
     */
    @Override
    public void accept(EquipmentVisitor visitor) {
        Iterable<Equipment> iterator = createIterator();
        for (Equipment equipment : iterator) {
            equipment.accept(visitor);
        }
    }
    
}
