package visitor.book_example;

/**
 * Visitor. Obejmuje deklarację operacji Visit dla każdej klasu ConcreteElement ze struktury obiektów. 
 * Nazwa i sygnatura operacji określają klasę wysyłającą do odwiedzającego żądanie Visit.
 * Umożliwia to odwiedzającym ustalenie klas konkretnego odwiedzanego elementu. Następnie odwiedzający może bezposrednio uzyskać dostęp do elementu poprzez jego interfejs.
 */
public interface EquipmentVisitor {
    void visitFloppyDisk(FloppyDisk floppyDisk);
    void visitCard(Card card);
    void visitChassis(Chassis chassis);
    void visitBus(Bus bus);
    
    //i tak dalej dla pozostałych podklas konkretnych klasy Equipment
}
