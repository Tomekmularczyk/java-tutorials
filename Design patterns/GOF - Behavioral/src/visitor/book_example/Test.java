package visitor.book_example;

import static java.lang.System.out;

/**
 * Visitor. Reprezentuje operacje wykonywaną na elementach struktury obiektów. Wzorzec ten umożliwia zdefiniowanie nowej operacji bez
 * zmieniania klas elementów na których działa. 
 * Wzorzec odwiedzający należy stosować w następujących warunkach: 
 * - Jeśli struktura obiektów obejmuje wiele klas o różnych interfejsach, a chcesz wykonywać na tych obiektach operacje zależne od ich klas konkretnych. 
 * - Kiedy na obiektach z ich struktury trzeba wykonwyać wiele różnych i niepowiązancyh operacji, a chcesz uniknąć "zaśmiecania" klas tymi operacjami.
 * Wzorzec Odiwedzający umożliwia przechowywanie powiązancyh operacji razem przez zdefiniowanie ich w jednej klasie. Jeżeli struktura
 * obiektów jest współużytkowana przez wiele aplikacji, należy zastosować wzorzec Odwiedzający do umieszczenia operacji w tylko tych
 * programach, w których są potrzebne. 
 * - Gdy klasy definiujące strukturę obiektów rzadko się zmieniają, ale często chcesz zdefiniować nowe operacje dla tej struktury. 
 * Zmodyfikowanie klas ze struktury obiektów wymaga przedefiniowania interfejsu wszystkich odwiedzających, co
 * może okazać się kosztowne. Jeśli klasy ze struktury obiektów zmieniają się często, prawdopodobnie lepiej będzie zdefiniować operacje w
 * tych klasach.
 *
 * Przy stosowaniu wzorca Odwiedzający należy zdefiniować dwie hierarchie klas: jedną dla przetwarzanych elementów i drugą dla
 * odwiedzających.
 *
 * Hierarchia klas visitor może być trudna w konserwacji jeżeli nowe klasy ConcreteElement są dodawane często. Jeżeli jednak hierarchia
 * Element jest stabilna, za to nieustannie dodajesz operacje lub zmieniasz algorytmy, wtedy wzorzec odwiedzający pomoże w zarządzaniu
 * zmianami.
 *
 * We wzorcu tym zastosowano technike dwukrotnej dyspozycji: zastosowana operacja zależy zarówno od typu odwiedzającego jak i od typu
 * odwiedzanego elementu.
 *
 */
public class Test {

    public static void main(String[] args) {
        //przykład ten obejmuje wzorzec Visitor w oparciu o wzorzec Kompozyt.
        Equipment component = getEquipment();
        
        InventoryVisitor inventoryVisitor = new InventoryVisitor();
        component.accept(inventoryVisitor);
        inventoryVisitor.getTotalEquipment().forEach(e -> out.println(e.getClass().getSimpleName()));
    }

    private static Equipment getEquipment() {
        Cabinet cabinet = new Cabinet("Obudowa komputera PC");
        Chassis chassis = new Chassis("Płyta montażowa komputera PC");

        cabinet.add(chassis);

        Bus bus = new Bus("Magistrala MCA");
        bus.add(new Card("Karta 16Mbs Token Ring"));

        chassis.add(bus);
        chassis.add(new FloppyDisk("Stacja dyskietek 3,5 cala"));
        
        return cabinet;
    }
}
