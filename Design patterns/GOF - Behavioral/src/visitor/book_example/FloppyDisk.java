package visitor.book_example;

/**
 * ConcreteElement - obejmuje implementację operacji Accept przyjmującej odwiedzającego jako argument.
 */
public class FloppyDisk extends Equipment{

    public FloppyDisk(String name) {
        super(name);
    }

    @Override
    public int power() {
        return 1;
    }

    @Override
    public double netPrice() {
        return 3;
    }

    @Override
    public double discountPrice() {
        return 2.2;
    }

    @Override
    public void add(Equipment equipment) {
    }

    @Override
    public void remove(Equipment equipment) {
    }

    @Override
    public void accept(EquipmentVisitor visitor) {
        visitor.visitFloppyDisk(this);
    }
    
}
