package mediator.book_example;

import java.awt.event.MouseEvent;

/**
 * Klasa bazowa obiektów (Colleague) znających powiązanego z nimi kierownika. - każdy obiekt współpracujący zna powiązany z nią obiekt
 * mediator. - każdy obiekt współpracujący komunikuje się z mediatorem zamiast z innymi takimi obiektami.
 */
public abstract class Widget {

    private final DialogDirector dialogDirector;

    public Widget(DialogDirector dialogDirector) {
        this.dialogDirector = dialogDirector;
    }

    public void changed() {
        dialogDirector.widgetChanged(this);
    }

    public abstract void setText(char text);

    public abstract char getText();

    public abstract void handleMouse(MouseEvent event);
}
