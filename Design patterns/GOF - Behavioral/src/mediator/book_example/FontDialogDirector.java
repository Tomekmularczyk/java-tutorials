package mediator.book_example;

public class FontDialogDirector extends DialogDirector{
    private Button ok;
    private Button cancel;
    private ListBox fontList;
    private EntryField fontName;
    
    @Override
    public void showDialog() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    /**
     * Gwarantuje że widgety beda działać ze sobą prawidłowo
     */
    @Override
    public void widgetChanged(Widget widget) {
        if(widget == fontList){
            fontName.setText(fontList.getSelection());
        } else if (widget == ok) {
            //modyfikowanie czcionki i zamykanie okna dialogowego
        } else if (widget == cancel){
            //zamykanie okna dialogowego
        }
    }

    @Override
    protected void createWidgets() {
        this.ok = new Button(this);
        this.cancel = new Button(this);
        this.fontList = new ListBox(this);
        this.fontName = new EntryField(this);
        
        //zapełnianie pola listBox nazwami dostepnych czcionek
        
        //łączenie widgetów w okno dialogowe
    }

}
