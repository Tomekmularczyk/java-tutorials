package mediator.book_example;

/**
 * Mediator - definiuje interfejs do komunikowania sie z obiektami współpracującymi.
 */
public abstract class DialogDirector {

    public abstract void showDialog();

    public abstract void widgetChanged(Widget widget);

    protected abstract void createWidgets();
}
