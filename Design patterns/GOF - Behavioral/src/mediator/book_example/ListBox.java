package mediator.book_example;

import java.awt.event.MouseEvent;

public class ListBox extends Widget{

    public ListBox(DialogDirector dialogDirector) {
        super(dialogDirector);
    }

    public char getSelection(){
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void handleMouse(MouseEvent event) {
        changed();
    }

    @Override
    public void setText(char text) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public char getText() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}
