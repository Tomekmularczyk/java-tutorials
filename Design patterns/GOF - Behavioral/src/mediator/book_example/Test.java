package mediator.book_example;

/**
 * Mediator - Określa obiekt kapsułkujący informacje o interakcji między obiektami z danego zbioru.
 * Duża liczba połączeń zmienjsza prawdopodobieństwo, że obiekt będzie działał bez pozostałych i często konieczne jest tworzenie wielu podklas..
 * Wzorzec ten pomaga zapewnić luźne powiązanie ponieważ zapobiega bezpośredniemu odwoływaniu się obiektów do siebie
 * i umożliwia niezależne modyfikowanie interakcji między nimi.
 * 
 * Warunki w których należy korzystać z Mediatora:
 * - zestaw obiektów komunikuje się w dobrze zdefiniowany, ale skomplikowany sposób. Powstałe z tego wyniku zależności są nieustrukturyzowane i trudne do zrozumienia.
 * - powtórne wykorzystanie obiektu jest trudne, ponieważ odwołuje się on do wielu innych obiektów i komunikuje się z nimi.
 * - dostosowanie zachowania rozproszonego po kilku klasach nie powinno wymagać tworzenia wielu podklas.
 */
public class Test {
    public static void main(String[] args) {
        
    }
}
