package mediator.my_example1;

import static java.lang.System.out;
import java.util.Objects;

/**
 * Colleagues
 */
public class MohamedAli implements Fighter{
    private Referee referee;

    public MohamedAli(Referee referee) {
        this.referee = referee;
    }

    @Override
    public void hit() {
        referee.fighterHit(this, "Left Hook");
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MohamedAli other = (MohamedAli) obj;
        if (!Objects.equals(this.referee, other.referee)) {
            return false;
        }
        return true;
    }

    @Override
    public void takeHit() {
        out.println("Ała -20 zdrowia.");
    }
    
    
}
