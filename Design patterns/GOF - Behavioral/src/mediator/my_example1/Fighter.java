package mediator.my_example1;

public interface Fighter {
    
    void hit();
    
    void takeHit();
    
}
