package mediator.my_example1;

import static java.lang.System.out;

/**
 * Mediator
 */
public class Referee {

    private Fighter fighter1;
    private Fighter fighter2;

    public void setFighters(Fighter fighter1, Fighter fighter2) {
        this.fighter1 = fighter1;
        this.fighter2 = fighter2;
    }

    public void fighterHit(Fighter fighter, String attack) {
        String fighterOne = fighter1.getClass().getSimpleName();
        String fighterTwo = fighter2.getClass().getSimpleName();

        if (fighter.equals(fighter1)) {
            out.printf("%s atakuje %s %s ", fighterOne, attack, fighterTwo);
            fighter2.takeHit();
        }

        if (fighter.equals(fighter2)) {
            out.printf("%s atakuje %s %s ", fighterTwo, attack, fighterOne);
            fighter1.takeHit();
        }
    }

}
