package mediator.my_example1;

import static java.lang.System.out;
import java.util.Objects;

/**
 * Colleagues
 */
public class Tyson implements Fighter{
    private Referee referee;

    public Tyson(Referee referee) {
        this.referee = referee;
    }

    @Override
    public void hit() {
        referee.fighterHit(this, "Upper Cut");
    }

    
    @Override
    public void takeHit() {
        out.println("Ała -15 zdrowia.");
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tyson other = (Tyson) obj;
        if (!Objects.equals(this.referee, other.referee)) {
            return false;
        }
        return true;
    }

}
