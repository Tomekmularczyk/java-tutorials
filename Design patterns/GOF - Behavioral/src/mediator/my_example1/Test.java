package mediator.my_example1;

public class Test {
    public static void main(String[] args) {
        Referee referee = new Referee();
        Fighter tyson = new Tyson(referee);
        Fighter mohamedAli = new MohamedAli(referee);
        
        referee.setFighters(tyson, mohamedAli);
        
        
        mohamedAli.hit();
        tyson.hit();
    }
}
