package abstract_factory.myexample1;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Abstract Product
 */
public abstract class Apps {
    private List<String> apps = new LinkedList<>();
    
    public final void addApp(String app){
        apps.add(app);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + Arrays.toString(apps.toArray());
    }
}
