package abstract_factory.myexample1;

/**
 * Konkretna klasa Factory, często jest połączona ze wzorcem Singleton, ponieważ wystarcza nam jedna taka klasa w programie
 */
public class MacOSXFactory implements OSAbstractFactory {

    @Override
    public String getNameAndVersion() {
        return "Mac OSX El Capitan";
    }

    @Override
    public Drivers getDrivers() {
        return new MacOSXDrivers();
    }

    @Override
    public Codecs getCodecs() {
        return new MacOSXCodecs();
    }

    @Override
    public Apps getApps() {
        return new MacApps();
    }

    //***************** CONCRETE PRODUCTS ****************************
    private class MacOSXDrivers implements Drivers {

        @Override
        public String toString() {
            return "MacOSXDrivers";
        }
    }
    
    private class MacOSXCodecs implements Codecs {

        @Override
        public String toString() {
            return "MacOSXCodecs";
        }
    }
    
    private class MacApps extends Apps {

        public MacApps() {
            addApp("Garage Band");
            addApp("Safari");
            addApp("iTunes");
        }
    }
}
