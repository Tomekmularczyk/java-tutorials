package abstract_factory.myexample1;

/**
 * Fabryka abstrakcyjna przypomina wzorzec Builder ponieważ też może służyć do tworzenia złożonych obiektów, ukrywając konkretne obiekty
 * z jakich produkt będzie stworzony. Główna różnica polega na tym, że Builder opisuje proces tworzenia obiektu krok po kroku, zwracając go na końcu
 * We wzorcu Fabryka Abstrakcyjna nacik położony jest na RODZINY obiektów-produktów, które są udostępniane natychmiast.
 */
public class Test {
    /**
     * Warto zauważyć jak w poniższym przykładzie łatwe jest podmienienie systemu, wystarczy podmienić fabryke
     * @param args 
     */
    public static void main(String[] args) {
        Computer mac = new Computer();
        Computer pc = new Computer();
        
        OSAbstractFactory macOSX = new MacOSXFactory();
        mac.installOperatingSystem(macOSX);
        
        OSAbstractFactory windows = new WindowsFactory();
        pc.installOperatingSystem(windows);
        
        System.out.println(mac);
        System.out.println();
        System.out.println(pc);
    }
}
