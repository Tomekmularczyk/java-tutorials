package abstract_factory.myexample1;

/**
 * Konkretna klasa Factory, często jest połączona ze wzorcem Singleton, ponieważ wystarcza nam jedna taka klasa w programie
 */
public class WindowsFactory implements OSAbstractFactory {

    @Override
    public String getNameAndVersion() {
        return "Windows XP";
    }

    @Override
    public Drivers getDrivers() {
        return new WindowsDrivers();
    }

    @Override
    public Codecs getCodecs() {
        return new WindowsCodecs();
    }

    @Override
    public Apps getApps() {
        return new WindowsApps();
    }

    //***************** CONCRETE PRODUCTS ****************************
    private class WindowsDrivers implements Drivers {

        @Override
        public String toString() {
            return "Windows Drivers";
        }
    }
    
    private class WindowsCodecs implements Codecs {

        @Override
        public String toString() {
            return "K-Little codec Pack";
        }
    }
    
    private class WindowsApps extends Apps {

        public WindowsApps() {
            addApp("MS Office 2016");
            addApp("Internet Explorer");
            addApp("Pasjans");
        }
    }
}
