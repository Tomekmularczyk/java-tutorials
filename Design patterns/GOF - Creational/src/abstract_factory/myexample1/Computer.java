package abstract_factory.myexample1;

public class Computer {
    private String nameAndVersion;
    private Drivers drivers;
    private Codecs codecs;
    private Apps apps;
    
    
    /**
     * Nie musimy wiedzieć (i nie będziemy wiedzieć) jakiego dokładnie typu są poniższe elementy,
     * Ważne dla nas jest to by dostarczała ona nam obiekty które mają wspólny interfejs
     * @param factory 
     */
    public void installOperatingSystem(OSAbstractFactory factory) { //mógłby być to konstruktor
        this.nameAndVersion = factory.getNameAndVersion();
        this.drivers = factory.getDrivers();
        this.codecs = factory.getCodecs();
        this.apps = factory.getApps();
    }

    @Override
    public String toString() {
        return nameAndVersion 
                + "\ndrivers: " + drivers 
                + "\ncodecs: " + codecs 
                + "\napps: " + apps;
    }
}
