package abstract_factory.myexample1;

/**
 * Abstract Factory
 * deklaruje interfejs do tworzenia rodziny produktów bez określania ich klas konkretnych
 * 
 * Popularnym zastosowaniem jest stworzenie konkretnej klasy Factory, po to by ją rozszerzać i przełonić tylko niektóre potrzebne metody.
 * 
 * Popularne jest wykorzystanie Metody Wytwórczej do generowania konkretnych produktów.
 * Jeżeli aplikacja może obejmować wiele rodzin produktów to można wykorzystać wzorzec Prototyp by nie musieć za każdym razem tworzyć konkrente produkty.
 * Fabryki konkretne często są Singletonami
 */
public interface OSAbstractFactory {

    public String getNameAndVersion();

    public Drivers getDrivers();

    public Codecs getCodecs();

    public Apps getApps();
    
    /**
     * Jeżeli problemem byłoby to, że po rozszerzeniu interfejsu klasy abstrakcyjnej konieczne będzie updateowanie wszystkich podklas,
     * to można zrobić tak by klient podawał identyfikator obiektu np. String: "Drivers" do metody, a ta jedna metoda zajmowałaby się
     * rozszyfrowywaniem parametru i podawaniem obiektu jeżeli takowy istnieje. Jest to jednak mało bezpieczne.
     */
}
