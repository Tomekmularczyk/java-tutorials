package abstract_factory.book_example;

import introductory_example_mazegame.Door;
import introductory_example_mazegame.Maze;
import introductory_example_mazegame.Room;
import introductory_example_mazegame.Wall;

/**
 * Programy, które przyjmują MazeFactory pozwalają programiście określić z czego zbudowane będą poszczególne elementy Zwykle główna fabryka
 * jest abstrakcyjna, natomiast często stosowanym rozwiązaniem jest stworzenie jej jako konkretna klasa ze zdefiniowanymi metodami
 * wytwórczymi dzięki czemu potem wystarczy nadpisać tylko te metody, które nas interesują
 */
public class MazeFactory {

    //==== Zbiór metod fabrycznych (Factory Method)
    
    public Maze makeMaze() {
        return new Maze();
    }
    
    public Wall makeWall(){
        return new Wall();
    }
    
    public Door makeDoor(Room room1, Room room2){
        return new Door(room1, room2);
    }
    
    public Room makeRoom(int roomNo){
        return new Room(roomNo);
    }
}
