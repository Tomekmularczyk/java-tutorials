package abstract_factory.book_example;

/**
 * Abstract Factory/Kit różni się od Buildera tym, że budowniczy opisuje PROCES tworzenia produktu i jest on zwracany w ostatnim kroku.
 * Natomiast fabryka abstrakcyjna kładzie nacisk na natychmiastowe zwracanie rodziny produktów.
 * Klient wie, że dostaje coś z rodziny produktów, ale nie musi wiedzieć dokładnie co.
 * 
 * Dzięki temu system może być niezależny od sposobu składania, tworzenia i reprezentowania jego produktów,
 * a my mamy możliwość konfigurowania go za pomocą rodziny produktów, dzięki wspólnemu interfejsowi fabryk.
 * 
 * Minusem jest to, że przy rozszerzaniu interfejsu fabryki o nowe produkty, trzeba zaaktualizować wszystkie podklasy. 
 * (Rozwiązaniem mogłoby np. być podawanie parametru który mówiłby jaki obiekt chcemy tworzyć, lecz jest to mniej bezpieczne)
 * 
 * Fabryki konkretne często są implementowane przy użyciu wzorca Singleton
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        MazeFactory standardMazeFactory = new MazeFactory();
        MazeFactory enchantedMazeFactory = new EnchantedMazeFactory();
        MazeFactory bombedMazeFactory = new BombedMazeFactory();

        MazeGame.createMaze(standardMazeFactory);
        MazeGame.createMaze(enchantedMazeFactory);
        MazeGame.createMaze(bombedMazeFactory);
    }
}
