package abstract_factory.book_example;

import introductory_example_mazegame.Door;
import introductory_example_mazegame.Room;

/**
 * Konkretna klasa MazeFactory, która przesłania tylko potrzebne metody wytwórcze. Takie klasy zwykle są singletonami, gdyż nie potrzeba ich
 * więcej niż jedna w programie.
 */
public class EnchantedMazeFactory extends MazeFactory {    
    
    @Override
    public Room makeRoom(int roomNo) {
        return super.makeRoom(roomNo);
    }

    @Override
    public Door makeDoor(Room room1, Room room2) {
        return new EnchantedDoor(room1, room2);
    }

    protected Spell castSpell(){
        return new Spell();
    }
    
    
    //-- Nie jestem pewny gdzie powinny być umieszczane tego rodzaju konkretne produkty, ale umieszcze je tutaj dla wygody…
    public static class EnchantedDoor extends Door {

        public EnchantedDoor(Room room1, Room room2) {
            super(room1, room2);
        }
    }

    public static class EnchantedRoom extends Room {

        public EnchantedRoom(int roomNo) {
            super(roomNo);
        }
    }
    
    public static class Spell {
        
    }

}
