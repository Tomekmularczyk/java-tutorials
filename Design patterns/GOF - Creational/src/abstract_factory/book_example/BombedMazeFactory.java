package abstract_factory.book_example;

import introductory_example_mazegame.Room;
import introductory_example_mazegame.Wall;

/**
 * Fabryka konkretna. 
 * Można użyć kowariancji typów dla produktów zwracanych, chociaż wtedy konkretne produkty nie będą zakapsułkowane.
 */
public class BombedMazeFactory extends MazeFactory {

    @Override
    public Room makeRoom(int roomNo) {
        return new BombedRoom(roomNo);
    }

    @Override
    public Wall makeWall() {
        return new BombedWall();
    }
    
    //-- Nie jestem pewny gdzie powinny być umieszczane tego rodzaju konkretne produkty, ale umieszcze je tutaj dla wygody…

    public static class BombedRoom extends Room {

        public BombedRoom(int roomNo) {
            super(roomNo);
        }
    }    

    public static class BombedWall extends Wall {
    }
}
