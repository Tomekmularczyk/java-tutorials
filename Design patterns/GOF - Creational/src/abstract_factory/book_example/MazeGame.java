package abstract_factory.book_example;

import introductory_example_mazegame.Direction;
import introductory_example_mazegame.Door;
import introductory_example_mazegame.Maze;
import introductory_example_mazegame.Room;

/**
 * Klient tworzący labirynt. Warto zwrócić uwagę, że we wcześniejszej matodzie createMaze elementy były zapsiane na stałe co utrudniało tworzenie
 * labiryntu o różnych elementach.
 */
public class MazeGame {

    public static Maze createMaze(MazeFactory factory) {
        Maze maze = factory.makeMaze();

        int i = 0;
        Room room1 = factory.makeRoom(i++); //wiemy tylko że coś z rodziny Room będzie zwrócone
        Room room2 = factory.makeRoom(i++);
        Room room3 = factory.makeRoom(i++);

        maze.addRoom(room1);
        maze.addRoom(room2);
        maze.addRoom(room3);

        Door door1 = factory.makeDoor(room1, room2);
        Door door2 = factory.makeDoor(room2, room3);

        room1.setSide(Direction.NORTH, factory.makeWall());
        room1.setSide(Direction.EAST, factory.makeWall());
        room1.setSide(Direction.SOUTH, door1);
        room1.setSide(Direction.WEST, factory.makeWall());

        room2.setSide(Direction.NORTH, factory.makeWall());
        room2.setSide(Direction.EAST, door1);
        room2.setSide(Direction.SOUTH, door2);
        room2.setSide(Direction.WEST, factory.makeWall());

        room3.setSide(Direction.NORTH, factory.makeWall());
        room3.setSide(Direction.EAST, factory.makeWall());
        room3.setSide(Direction.SOUTH, factory.makeWall());
        room3.setSide(Direction.WEST, door2);

        return maze;
    }
}
