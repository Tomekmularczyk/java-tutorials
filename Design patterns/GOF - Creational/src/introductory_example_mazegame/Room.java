package introductory_example_mazegame;

/**
 * Room Konkretna klasa MapSite, określa kluczowe relacje między komponentami labiryntu
 */
public class Room implements MapSite{
    private int roomNo;
    private final MapSite[] sides = new MapSite[4];
    
    public Room(int roomNo){
    }
    
    public MapSite getDirection(Direction direction){
        return null;
    }
    
    public void setSide(Direction direction, MapSite element){
    }
    
    @Override
    public void enter() {
    }

}
