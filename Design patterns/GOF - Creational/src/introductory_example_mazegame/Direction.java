package introductory_example_mazegame;

public enum Direction {
    NORTH(0), SOUTH(1), EAST(2), WEST(3);
    
    public final int value;
    private Direction(int value){
        this.value = value;
    }
}
