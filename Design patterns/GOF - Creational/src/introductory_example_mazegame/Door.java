package introductory_example_mazegame;

/**
 * Komponent labiryntu
 */
public class Door implements MapSite {
    private Room room1, room2;
    private boolean isOpen;
    
    public Door(Room room1, Room room2){
        this.room1 = room1;
        this.room2 = room2;
    }

    public Room otherSideFrom(Room room){
        return null;
    }
    
    @Override
    public void enter() {
    }

}
