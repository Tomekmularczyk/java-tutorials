package introductory_example_mazegame;

/**
 * Tworzy labirynt 
 */
public class MazeGame {

    /**
     * @return labirynt składający się z trzech pomieszczeń rozdzielonych dzrzwiami
     * problemem może być zapisanie na stałe układu labiryntu oraz rodzaju użytych klas. 
     */
    public static Maze mazeGame() {
        Maze maze = new Maze();

        int i = 0;
        Room room1 = new Room(i++);
        Room room2 = new Room(i++);
        Room room3 = new Room(i++);

        maze.addRoom(room1);
        maze.addRoom(room2);
        maze.addRoom(room3);

        Door door1 = new Door(room1, room2);
        Door door2 = new Door(room2, room3);

        room1.setSide(Direction.NORTH, new Wall());
        room1.setSide(Direction.EAST, new Wall());
        room1.setSide(Direction.SOUTH, door1);
        room1.setSide(Direction.WEST, new Wall());

        room2.setSide(Direction.NORTH, new Wall());
        room2.setSide(Direction.EAST, door1);
        room2.setSide(Direction.SOUTH, door2);
        room2.setSide(Direction.WEST, new Wall());

        room3.setSide(Direction.NORTH, new Wall());
        room3.setSide(Direction.EAST, new Wall());
        room3.setSide(Direction.SOUTH, new Wall());
        room3.setSide(Direction.WEST, door2);

        return maze;
    }
}
