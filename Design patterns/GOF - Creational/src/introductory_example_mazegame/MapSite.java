package introductory_example_mazegame;

/**
 * Klasa abstrakcyjna dla wszystkich komponentów labiryntu
 */
public interface MapSite {
    void enter();
}
