package prototype.book_example;

/**
 * Wzorzec Fabryka
 */
public class MazeFactory {

    public Maze makeMaze() {
        return new Maze();
    }
    
    public Wall makeWall(){
        return new Wall();
    }
    
    public Door makeDoor(Room room1, Room room2){
        return new Door(room1, room2);
    }
    
    public Room makeRoom(int roomNo){
        return new Room(roomNo);
    }
}
