package prototype.book_example;

/**
 * Konkretna fabryka abstrakcyjna, która tylko klonuje prototypy zamiast tworzyć je od nowa.
 * Jest jednocześnie "Klientem" we wzozrcu prototyp ponieważ to ona nakazuje prototypom klonować się
 */
public class MazePrototypeFactory extends MazeFactory {

    private final Maze prototypeMaze;
    private final Door prototypeDoor;
    private final Wall prototypeWall;
    private final Room prototypeRoom;

    public MazePrototypeFactory(Maze maze, Door door, Wall wall, Room room) {
        this.prototypeMaze = maze;
        this.prototypeDoor = door;
        this.prototypeWall = wall;
        this.prototypeRoom = room;
    }

    //******** Klonujemy prototypy i gdzie niegdzie inicjujemy
    @Override
    public Maze makeMaze() {
        return prototypeMaze.clone();
    }

    @Override
    public Door makeDoor(Room r1, Room r2) {
        Door door = prototypeDoor.clone();
        door.initialize(r1, r2);
        return door;
    }

    @Override
    public Wall makeWall() {
        return prototypeWall.clone();
    }

    @Override
    public Room makeRoom(int roomNo) {
        Room room = prototypeRoom.clone();
        room.initialize(roomNo);
        return room;
    }

}
