package prototype.book_example;

/**
 * Konkretny prototyp
 */
public class Wall implements MapSitePrototype<Wall> {

    public Wall() {
    }

    /**
     * Konstruktor kopiujący
     */
    public Wall(Wall wall) {
    }

    @Override
    public Wall clone() {
        return new Wall(this);
    }

    @Override
    public void enter() {
    }
}
