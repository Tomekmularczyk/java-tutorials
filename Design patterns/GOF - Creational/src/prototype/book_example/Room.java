package prototype.book_example;

/**
 * Konkretny prototyp
 */
public class Room implements MapSitePrototype<Room> {

    int roomNo;

    public Room(){
    }
    
    public Room(int roomNo) {
        this.roomNo = roomNo;
    }

    /**
     * Konstruktor kopiujący
     */
    public Room(Room room) {
        this.roomNo = room.roomNo;
    }

    /**
     * Klonowanie przy pomocy konstruktora klonującego
     */
    @Override
    public Room clone() {
        return new Room(this);
    }

    /**
     * Zwykle jest też potrzebna operacja do ponownego zainicjowania wewnętrznego stanu
     */
    public void initialize(int roomNo) {
        this.roomNo = roomNo;
    }

    @Override
    public void enter() {
    }

}
