package prototype.book_example;

/**
 * Klient fabryki abstrakcyjnej
 */
public class MazeGame {

    public static Maze createMaze(MazeFactory factory) {
        Maze maze = factory.makeMaze();

        //standardowe działanie fabryki
        Wall wall = factory.makeWall();
        Room r1 = factory.makeRoom(1);
        Room r2 = factory.makeRoom(2);
        Door door = factory.makeDoor(r1, r2);
        
        maze.addRoom(r1);

        return maze;
    }
}
