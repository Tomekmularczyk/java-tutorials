package prototype.book_example;

public class Test {

    /**
     * Wzorzec Prototyp pozwala stworzyć kopię danego egzemplarza obiektu.
     * Należy go używać między innymi wtedy kiedy:
     * - chcemy ograniczyć liczbę klas w systemie
     * - chcemy ukryć prze klientem klasy konkretne (jak we wzorcu abstract factory czy builder)
     * - lub kiedy klasy tworzonych egzemplarzy są określane w trakcie działania programu.
     * 
     * Prototype redukuje potrzebę tworzenia wielu niezacznie różniących się podklas
     * 
     * Prototype i Abstract Factory to pod pewnymi względami wzorce konkurencyjne, lecz można je używać razem jak w tym przykładzie.
     * Prototyp musi posiadać konstruktor kopiujący i metodę klonującą. Czasem potrzebna jest też metoda do zainicjowania wewnętrznego stanu.
     */
    public static void main(String[] args) {
        MazePrototypeFactory mazePrototypeFactory = new MazePrototypeFactory(new Maze(), new Door(), new Wall(), new Room());
        MazeGame.createMaze(mazePrototypeFactory);

        //Aby zmienić rodzaj labiryntu należy zainicjować MazePrototypeFactory za pomocą innego zestawu prototypów…
        MazePrototypeFactory bombedMazePrototypeFactory = new MazePrototypeFactory(new Maze(), new Door(), new BombedWall(), new BombedRoom());
        MazeGame.createMaze(bombedMazePrototypeFactory);
    }

    
    /**
     * W podklasach należy przesłonić metodę clone() oraz zaimplementować odpowiedni konstruktor kopiujący
     */
    static class BombedWall extends Wall {
        private int boomb;

        public BombedWall() {
        }

        public BombedWall(BombedWall boombedWall) {
            this.boomb = boombedWall.boomb;
        }

        @Override
        public Wall clone() {
            return new BombedWall(this);
        }

    }
    
    static class BombedRoom extends Room {
        private boolean boomb;

        public BombedRoom() {
        }

        public BombedRoom(BombedRoom boombedRoom) {
            this.boomb = boombedRoom.boomb;
            this.roomNo = boombedRoom.roomNo;
        }

        @Override
        public Room clone() {
            return new BombedRoom(this);
        }

    }
}
