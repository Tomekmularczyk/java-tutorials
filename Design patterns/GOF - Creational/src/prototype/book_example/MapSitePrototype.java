package prototype.book_example;

/**
 * Przez rozszerzanie Cloneable mówimy Javie, że może śmiało kopiować te obiekty. Będą one przechowywane także w innym miejscu w pamięci
 * @param <T> 
 */
public interface MapSitePrototype<T> extends Cloneable {
    void enter();
    
    T clone();
}
