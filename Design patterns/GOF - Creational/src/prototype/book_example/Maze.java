package prototype.book_example;

/**
 * Konkretny prototyp
 */
public class Maze {
  
    public Maze() {
    }
    
    public Maze(Maze maze) {
    }

    public void addRoom(Room room){
    }
    
    public Room getRoom(int roomNo) {
        return null;
    }
    

    @Override
    public Maze clone() {
        return new Maze(this);
    }

}
