package prototype.book_example;

import introductory_example_mazegame.MapSite;

/**
 * Konkretny prototyp
 */
public class Door implements MapSite {

    private Room room1, room2;

    public Door(){
    }
    
    public Door(Room room1, Room room2) {
        this.room1 = room1;
        this.room2 = room2;
    }

    /**
     * Konstruktor kopiujący
     */
    public Door(Door door) {
        room1 = door.room1;
        room2 = door.room2;
    }

    /**
     * Klonowanie przy pomocy konstruktora klonującego
     */
    public Door clone() {
        return new Door(this);
    }

    /**
     * Zwykle jest też potrzebna operacja do ponownego zainicjowania wewnętrznego stanu
     */
    public void initialize(Room r1, Room r2) {
        room1 = r1;
        room2 = r2;
    }

    @Override
    public void enter() {
    }

    public Room otherSideFrom(Room room) {
        return null;
    }
}
