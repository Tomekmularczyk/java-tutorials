package prototype.myexample1;

/**
 * Interfejs upewniający nas że dany obiekt da się skopiować
 * @param <T> typ zwracanego obiektu
 */
public interface PrototypeToys<T> {
    T clone();
}
