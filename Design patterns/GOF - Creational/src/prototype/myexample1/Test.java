package prototype.myexample1;

/**
 * Prototype
 * 
 * Przy tworzeniu prototypu i konstruktora kopiującego musimy wziąć pod uwagę czy tworzymy shallow czy deep copy. 
 * Shallow copy to kopia wskazująca na ten sam obiekt w pamięci(kopia referencji). Możemy tworzyć taką kopię jeżeli obiekt jest Immutable.
 * Deep copy kopiuje obiekt w całości, także tworzy dla niego nowe miejsce w pamięci.
 */
public class Test {
    public static void main(String[] args) {
        Girl girl = new Girl("Kasia", new Dolls(new String[]{"barbi", "wiedźma"}));
        girl.age = 13;
        Boy boy = new Boy("Kazik", new Cars(new String[]{"Burrago Dodge Viper", "Hot Wheels"}));
        boy.age = 10;
        
        HumanGhost boyGhost1 = new HumanGhost(boy);
        HumanGhost boyGhost2 = new HumanGhost(boy);
        HumanGhost girlGhost1 = new HumanGhost(girl);
        HumanGhost girlGhost2 = new HumanGhost(girl);
        
        System.out.printf("%s\n%s\n\n", boy, girl);
        System.out.println("==========TERAZ DUCHY..==============");
        System.out.printf("%s\n\n%s\n\n%s\n\n%s\n", boyGhost1, boyGhost2, girlGhost1, girlGhost2);
    }
}
