package prototype.myexample1;


public abstract class Human {

    protected Sex sex;
    protected String name;
    protected int age;

    public Human(Sex sex, String name) {
        this.sex = sex;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public Sex getSex() {
        return sex;
    }

    public String getName() {
        return name;
    }

    public static enum Sex {
        Mezczyzna, Kobieta
    }

    @Override
    public String toString() {
        return "Miejsce w pamięci: " + this.hashCode() + "\n"
                + sex + " - " + name + ", wiek: " + age;
    }
}
