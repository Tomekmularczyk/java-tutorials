package prototype.myexample1;

/**
 * Konkretna klasa prototypu
 */
public class Boy extends Human implements HumanPrototype<Boy> {

    private final PrototypeToys<Cars> cars;

    /**
     * Zwykły konstruktor inicjujący obiekt
     */
    public Boy(String name, PrototypeToys cars) {
        super(Sex.Mezczyzna, name);
        this.cars = cars;
    }

    /**
     * Konstruktor kopiujący
     */
    public Boy(Boy another) {
        super(another.sex, another.getName()); //możemy spokojnie robić shallow copy ponieważ enum i String jest Immutable
        this.cars = another.cars.clone(); //Musimy zrobić deep copy ponieważ niechcemy by klony dzieliły wspólny obiekt
        this.age = another.age; //Prymityw - możemy spokojnie kopiować
    }

    @Override
    public Boy copy() {
        return new Boy(this);
    }

    @Override
    public String toString() {
        return super.toString() + ". Zabawki: " + cars;
    }
}
