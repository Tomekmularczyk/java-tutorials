package prototype.myexample1;

/**
 * Konkretna klasa prototypu
 */
public class Girl extends Human implements HumanPrototype<Girl> {

    private final PrototypeToys<Dolls> dolls;

    /**
     * Zwykły konstruktor inicjujący obiekt
     */
    public Girl(String name, PrototypeToys dolls) {
        super(Sex.Kobieta, name);
        this.dolls = dolls;
    }

    /**
     * Konstruktor kopiujący
     */
    public Girl(Girl another) {
        super(another.getSex(), another.getName()); //możemy spokojnie kopiować ponieważ enum i String są Immutable, więc obiekty mogą je spokojnie dzielić
        this.dolls = another.dolls.clone(); //musimy zrobić kopię inaczej kopia dziewczyny miała by referencę do tego samego obiektu!!
        this.age = another.age; //Prymityw - możemy spokojnie kopiować
    }

    @Override
    public Girl copy() {
        return new Girl(this);
    }

    
    @Override
    public String toString() {
        return super.toString() + ". Zabawki: " + dolls;
    }
}
