package prototype.myexample1;

/**
 * Dzięki tej klasi nie musimy tworzyć masy podklas typu np. BoyGhost czy GirlGhost w systemie
 */
public class HumanGhost {
    private final Human humanPrototype;
    
    public HumanGhost(HumanPrototype<? extends Human> human){
        this.humanPrototype = human.copy();
        humanPrototype.age = humanPrototype.age + 100;
    }

    @Override
    public String toString() {
        return "Buuu! jestem duchem! " + humanPrototype;
    }
    
}
