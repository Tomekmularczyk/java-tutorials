package prototype.myexample1;

import java.util.Arrays;

/**
 * Konkretna klasa prototypu
 */
public class Cars implements PrototypeToys<Cars> {

    private String[] cars;

    public Cars(String[] cars) {
        this.cars = cars;
    }

    /**
     * Konstruktor kopiujący. Robimy głeboką kopię. Sam string jest immutable, ale nie chcemy by obiekty dzieliły tę samą tablicę, w której
     * mogłyby podmieniać elementy.
     */
    public Cars(Cars another) {
        cars = new String[another.cars.length];
        System.arraycopy(another.cars, 0, this.cars, 0, another.cars.length);
    }

    @Override
    public Cars clone() {
        return new Cars(this);
    }

    @Override
    public String toString() {
        return Arrays.toString(cars);
    }
}
