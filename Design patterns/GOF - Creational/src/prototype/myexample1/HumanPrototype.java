package prototype.myexample1;

/**
 * Interfejs upewniający nas że dany obiekt da się skopiować
 * @param <T> typ zwracanego obiektu
 */
public interface HumanPrototype<T> {

    T copy();
}
