package prototype.myexample1;

import java.util.Arrays;

/**
 * Konkretna klasa prototypu
 */
public class Dolls implements PrototypeToys {

    private String[] dolls;

    public Dolls(String[] toys) {
        this.dolls = toys;
    }

    /**
     * Konstruktor kopiujący. Robimy głeboką kopię. Sam string jest immutable, ale nie chcemy by obiekty dzieliły tę samą tablicę, w której
     * mogłyby podmieniać elementy.
     */
    public Dolls(Dolls another) {
        dolls = new String[another.dolls.length];
        System.arraycopy(another.dolls, 0, this.dolls, 0, another.dolls.length);
    }

    @Override
    public Dolls clone() {
        return new Dolls(this);
    }

    @Override
    public String toString() {
        return Arrays.toString(dolls);
    }

}
