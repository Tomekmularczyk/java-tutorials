package factory_method.book_example;

import abstract_factory.book_example.BombedMazeFactory.BombedRoom;
import abstract_factory.book_example.BombedMazeFactory.BombedWall;
import introductory_example_mazegame.Room;
import introductory_example_mazegame.Wall;

/**
 * Konkretna klasa Kreatora przesłaniająca wybrane metody wytwórcze.
 */
public class BombedMazeGame extends MazeGame{

    @Override
    public Wall makeWall() {
        return new BombedWall();
    }

    @Override
    public Room makeRoom(int roomNo) {
        return new BombedRoom(roomNo);
    }
    
}
