package factory_method.book_example;

import abstract_factory.book_example.EnchantedMazeFactory.EnchantedDoor;
import abstract_factory.book_example.EnchantedMazeFactory.EnchantedRoom;
import abstract_factory.book_example.EnchantedMazeFactory.Spell;
import introductory_example_mazegame.Door;
import introductory_example_mazegame.Room;

/**
 * Konkretna klasa Kreatora przesłaniająca wybrane metody wytwórcze.
 */
public class EnchantedMazeGame extends MazeGame{

    @Override
    public Door makeDoor(Room room1, Room room2) {
        return new EnchantedDoor(room1, room2);
    }

    @Override
    public Room makeRoom(int roomNo) {
        return new EnchantedRoom(roomNo);
    }
    
    protected Spell castSpell() {
        return null;
    }
    
}
