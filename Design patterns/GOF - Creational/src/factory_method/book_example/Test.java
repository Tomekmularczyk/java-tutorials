package factory_method.book_example;

import introductory_example_mazegame.Maze;

/**
 * Dzięki metodom wytwórczym nie musimy na stałe określać jakie obiekty będą użyte do utworzenia obiektu.
 * W konkretnych podklasach Wytwórcy(Creator) programista może nadpisać konkretne metody wytwórcze by zwracały pożądane obiekty.
 * 
 */
public class Test {
    
    public static void main(String[] args) {
        Maze standardMaze = new MazeGame().createMaze();
        
        Maze enchantedMaze = new EnchantedMazeGame().makeMaze();
        
        Maze bombedMaze = new BombedMazeGame().createMaze();
    }
}
