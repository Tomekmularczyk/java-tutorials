package factory_method.book_example;

import introductory_example_mazegame.Direction;
import introductory_example_mazegame.Door;
import introductory_example_mazegame.Maze;
import introductory_example_mazegame.Room;
import introductory_example_mazegame.Wall;

/**
 * Kreator - posiada metody fabryczne, które podklasy mogą przesłonić by zdefiniować własną reprezentację obiektu
 */
public class MazeGame {

    /**
     * Metody te pozwalają zakapsułkować informację o tym jakie konkretnie podklasy należy użyć do konstrukcji obiektu
     * i zapisać te dane poza kreatorem
     * @return zwraca produkt
     */
    public Maze createMaze() {
        Maze maze = makeMaze();

        int i = 0;
        Room room1 = makeRoom(i++); //wiemy tylko że coś z rodziny Room będzie zwrócone
        Room room2 = makeRoom(i++);
        Room room3 = makeRoom(i++);

        maze.addRoom(room1);
        maze.addRoom(room2);
        maze.addRoom(room3);

        Door door1 = makeDoor(room1, room2);
        Door door2 = makeDoor(room2, room3);

        room1.setSide(Direction.NORTH, makeWall());
        room1.setSide(Direction.EAST, makeWall());
        room1.setSide(Direction.SOUTH, door1);
        room1.setSide(Direction.WEST, makeWall());

        room2.setSide(Direction.NORTH, makeWall());
        room2.setSide(Direction.EAST, door1);
        room2.setSide(Direction.SOUTH, door2);
        room2.setSide(Direction.WEST, makeWall());

        room3.setSide(Direction.NORTH, makeWall());
        room3.setSide(Direction.EAST, makeWall());
        room3.setSide(Direction.SOUTH, makeWall());
        room3.setSide(Direction.WEST, door2);

        return maze;
    }

    /*========== METODY WYTWÓRCZE Z DOMYŚLNĄ IMPLEMENTACJĄ. Posłużą one do tworzenia obiektów reprezentujących labirynt.
    */
    public Maze makeMaze() {
        return new Maze();
    }

    public Room makeRoom(int roomNo) {
        return new Room(roomNo);
    }

    public Wall makeWall() {
        return new Wall();
    }

    public Door makeDoor(Room room1, Room room2) {
        return new Door(room1, room2);
    }

}
