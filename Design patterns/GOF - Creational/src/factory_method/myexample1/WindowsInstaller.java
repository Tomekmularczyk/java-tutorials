package factory_method.myexample1;

import java.util.Arrays;

/**
 * Konkretna klasa kreatora
 */
public class WindowsInstaller extends OSInstaller {

    /**
     * Konkretna implementacja metod wytwórczych
     */
    @Override
    protected Apps getApps() {
        return new WindowsApps();
    }

    @Override
    protected Drivers getDrivers() {
        return new WindowsDriversPack();
    }

    @Override
    protected String getNameAndVersion() {
        return "Windows Vista";
    }

    
    
    
    private class WindowsApps implements Apps {

        @Override
        public String getInstalledApps() {
            return Arrays.toString(new String[] {"MS Office 2010", "GaduGadu", "InternetExplorer"});
        }
    }

    private class WindowsDriversPack implements Drivers {

        @Override
        public String getInstalledDrivers() {
            return Arrays.toString(new String[] {"K-Little Codec Pack", "Ge-Force MS-300 drivers", "Soundforce 200 drivers pack"});
        }
    }
}
