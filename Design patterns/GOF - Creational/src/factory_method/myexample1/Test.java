package factory_method.myexample1;

public class Test {
    /**
     * Warto wziąć pod uwagę, że kreator może łączyć klasy równoległe jeżeli klasa produktu jest powiązana z innym produktem, możemy 
     * stworzyć metodę/metody wytwórcze także dla klasy powiązanej np. "System" i "Komputer"
     */
    public static void main(String[] args) {
        OSInstaller ubuntuInstaller = new UbuntuInstaller();
        OSInstaller windowsInstaller = new WindowsInstaller();
        
        OperatingSystem ubuntu = ubuntuInstaller.installOperatingSystem();
        OperatingSystem windows = windowsInstaller.installOperatingSystem();
        
        System.out.printf("%s\n\n%s\n", ubuntu, windows);
    }
}
