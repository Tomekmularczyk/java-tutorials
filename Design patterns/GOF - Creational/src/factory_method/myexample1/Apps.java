package factory_method.myexample1;

/**
 * Element abstrakcyjny
 */
public interface Apps {

    String getInstalledApps();
    
    default String somethin(){
        return getInstalledApps();
    }
}
