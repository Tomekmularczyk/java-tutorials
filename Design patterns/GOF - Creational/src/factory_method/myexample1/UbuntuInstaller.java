package factory_method.myexample1;

import java.util.Arrays;

/**
 * Konkretna klasa kreatora
 */
public class UbuntuInstaller extends OSInstaller {

    /**
     * Konkretna implementacja metod wytwórczych
     */
    @Override
    protected Apps getApps() {
        return new LinuxApps();
    }

    @Override
    protected Drivers getDrivers() {
        return new LinuxDrivers();
    }

    @Override
    protected String getNameAndVersion() {
        return "Ubuntu 10";
    }

    
    
    
    private class LinuxApps implements Apps {
        @Override
        public String getInstalledApps() {
            return Arrays.toString(new String[] {"Linux app 1", "Linux app 2"});
        }
    }

    private class LinuxDrivers implements Drivers {
        @Override
        public String getInstalledDrivers() {
            return Arrays.toString(new String[] {"Wine", "Desktop drivers"});
        }
    }
}
