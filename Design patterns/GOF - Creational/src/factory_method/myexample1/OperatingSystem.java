package factory_method.myexample1;

/**
 * Product
 */
public class OperatingSystem {
    private String nameAndVersion;
    private Drivers drivers;
    private Apps Apps;

    public String getNameAndVersion() {
        return nameAndVersion;
    }

    public void setNameAndVersion(String nameAndVersion) {
        this.nameAndVersion = nameAndVersion;
    }

    public Drivers getDrivers() {
        return drivers;
    }

    public void setDrivers(Drivers drivers) {
        this.drivers = drivers;
    }

    public Apps getApps() {
        return Apps;
    }

    public void setApps(Apps Apps) {
        this.Apps = Apps;
    }

    @Override
    public String toString() {
        return nameAndVersion + "\ndrivers: " + drivers.getInstalledDrivers() + "\nApps: " + Apps.getInstalledApps();
    }
    
}
