package factory_method.myexample1;
    
/**
 * Creator, dzięki niemu nie musimy z góry określać klasy obiektów jakie będą używane do konstruckcji (Można by było zrobić interface z defaultową metodą)
 * 
 */
public abstract class OSInstaller { 
    /**
     * To jakie konkretnie elementy zostaną użyte do stworzenia produktu bedzie zakapsułkowane w klasach pochodnych
     */
    public OperatingSystem installOperatingSystem(){
        OperatingSystem operatingSystem = new OperatingSystem();
        operatingSystem.setNameAndVersion(getNameAndVersion());
        operatingSystem.setDrivers(getDrivers());
        operatingSystem.setApps(getApps());
        
        return operatingSystem;
    }

    /********************  FACTORY METHODS (ewentualnie mogłyby posiadać już defaultową implementację) ***/
    
    protected abstract Apps getApps();

    protected abstract Drivers getDrivers();

    protected abstract String getNameAndVersion();
}
