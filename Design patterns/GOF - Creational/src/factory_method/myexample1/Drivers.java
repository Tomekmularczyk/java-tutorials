package factory_method.myexample1;

/**
 * Element abstrakcyjny
 */
public interface Drivers {
    
    String getInstalledDrivers();
}
