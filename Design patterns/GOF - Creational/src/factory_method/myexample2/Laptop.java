package factory_method.myexample2;

public class Laptop {

    private String name;
    private Ram ram;
    private Processor processor;
    private HDD hdd;
    private OperatingSystem system;

    public Ram getRam() {
        return ram;
    }

    public void setRam(Ram ram) {
        this.ram = ram;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public HDD getHdd() {
        return hdd;
    }

    public void setHdd(HDD hdd) {
        this.hdd = hdd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void installOperatingSystem(OperatingSystem system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "Laptop " + name + ", " + ram + ", " + processor + ", " + hdd + "\nOperating System: " + system;
    }

}
