package factory_method.myexample2;

public class Ram {
    private int ramGB;

    public Ram(int ramGB) {
        this.ramGB = ramGB;
    }

    @Override
    public String toString() {
        return ramGB + "GB RAM";
    }
}
