package factory_method.myexample2;

public class HDD {
    private int gb;

    public HDD(int gb) {
        this.gb = gb;
    }

    @Override
    public String toString() {
        return "HDD " + gb + "GB";
    }

}
