package factory_method.myexample2;

public class MacbookCreator implements LaptopCreator{

    @Override
    public Processor getProcessor() {
        return new Processor("Intell pentium", 2200);
    }

    @Override
    public HDD getHDD() {
        return new HDD(320);
    }

    @Override
    public Ram getRam() {
        return new Ram(8);
    }

    @Override
    public Apps getApps() {
        return new Apps("Safari, Garage Band");
    }

    @Override
    public String getOSVersion() {
        return "OS X El Capitan";
    }

    @Override
    public String getLaptopName() {
        return "MacBook Pro - late 2011";
    }

}
