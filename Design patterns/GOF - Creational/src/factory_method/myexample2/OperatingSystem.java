package factory_method.myexample2;

public class OperatingSystem {
    private String versionAndName;
    private Apps apps;

    public String getVersion() {
        return versionAndName;
    }

    public void setVersion(String version) {
        this.versionAndName = version;
    }

    public Apps getApps() {
        return apps;
    }

    public void setApps(Apps apps) {
        this.apps = apps;
    }

    @Override
    public String toString() {
        return versionAndName + ", apps: " + apps;
    }
}
