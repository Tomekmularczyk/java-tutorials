package factory_method.myexample2;

public class Processor {
    private String producent;
    private int mhz;

    public Processor(String producent, int mhz) {
        this.producent = producent;
        this.mhz = mhz;
    }

    @Override
    public String toString() {
        return producent + " " + mhz + "Mhz";
    }
}
