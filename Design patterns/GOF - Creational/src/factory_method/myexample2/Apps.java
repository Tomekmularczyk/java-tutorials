package factory_method.myexample2;

public class Apps {
    private String apps;

    public Apps(String apps) {
        this.apps = apps;
    }

    @Override
    public String toString() {
        return "Apps: " + apps;
    }
    
}
