package factory_method.myexample2;

/**
 */
public interface LaptopCreator {

    default Laptop createHardware() {
        Laptop laptop = new Laptop();
        laptop.setName(getLaptopName());
        laptop.setProcessor(getProcessor());
        laptop.setHdd(getHDD());
        laptop.setRam(getRam());
        
        return laptop;
    }

    String getLaptopName();
    
    Processor getProcessor();

    HDD getHDD();

    Ram getRam();
    
    default OperatingSystem createSystem() {
        OperatingSystem operatingSystem = new OperatingSystem();
        operatingSystem.setApps(getApps());
        operatingSystem.setVersion(getOSVersion());
        
        return operatingSystem;
    }

    Apps getApps();

    String getOSVersion();

}
