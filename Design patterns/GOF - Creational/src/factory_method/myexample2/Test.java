package factory_method.myexample2;

public class Test {
 
    public static void main(String[] args) {
        LaptopCreator macCreator = new MacbookCreator();
        Laptop mac = macCreator.createHardware();
        OperatingSystem os_x = macCreator.createSystem();
        
        mac.installOperatingSystem(os_x);
        System.out.println(mac);
    }
}
