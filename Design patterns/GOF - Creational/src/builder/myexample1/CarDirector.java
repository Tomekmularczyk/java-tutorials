package builder.myexample1;

import java.time.Year;

/**
 * Dyrektor, który składa produkty przy pomocy buildera
 * To z jakich elementów konkretnie zostanie złożony samochód jest zakapsułkowane
 */
public class CarDirector {

    public Car getSportCar(CarBuilder builder){
        builder.build();
        
        builder.setSportDesign();
        builder.setNitro();
        builder.setMaxSpeed(289);

        return builder.getCar();
    }
    
    public Car getClassicCar(CarBuilder builder){
        builder.build();
        
        builder.setClassicDesign();
        builder.setFamilySpace();
        builder.setMaxSpeed(220);
        builder.setProductionDate(Year.of(2002));

        return builder.getCar();
    }
    
    public Car getMusscleCar(CarBuilder builder){
        builder.build();

        builder.setMuscleDesign();
        builder.setLoudEngine();
        builder.setMaxHorsePower(670);
        
        return builder.getCar();
    }
    
    
}
