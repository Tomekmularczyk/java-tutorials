package builder.myexample1;

import java.util.Arrays;

public abstract class Design {
    private final String color;
    private String[] shapeElements;

    public Design(String color) {
        this.color = color;
    }

    public final void setShapeElements(String[] shapeElements) {
        this.shapeElements = shapeElements;
    }

    public final String getColor() {
        return color;
    }

    public final String[] getShapeElements() {
        return shapeElements;
    }

    @Override
    public String toString() {
        return "Design color: " + color + ", elements: " + Arrays.toString(shapeElements);
    }
}
