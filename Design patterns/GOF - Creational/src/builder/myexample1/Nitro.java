package builder.myexample1;

public class Nitro {
    private final String details;

    public Nitro(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return details;
    }
    
}
