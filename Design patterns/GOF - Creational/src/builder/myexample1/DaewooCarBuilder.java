package builder.myexample1;

import java.time.Year;
import java.util.Arrays;
import java.util.Objects;

public class DaewooCarBuilder implements CarBuilder {

    private Car car;

    @Override
    public void build() {
        car = new Car();
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public void setSportDesign() {
        car.setDesign(new DaewooSportDesign("Bloody red"));
    }

    @Override
    public void setClassicDesign() {
        car.setDesign(new DaewooClassicDesign("White"));
    }

    @Override
    public void setMuscleDesign() {
        car.setDesign(new DaweooMuscleDesign("Black"));
    }

    @Override
    public void setMaxSpeed(int speed) {
        car.setMaxSpeed(speed);
    }

    @Override
    public void setFamilySpace() {
        car.setSpace(97);
    }

    @Override
    public void setNitro() {
        car.setNitro(new Nitro("Standard Daewoo Nitro"));
    }

    @Override
    public void setProductionDate(Year date) {
        car.setProductionDate(date);
    }

    @Override
    public void setLoudEngine() {
        car.setEngine(new DaewooV10Engine(499));
    }

    @Override
    public void setMaxHorsePower(int horsePower) {
        boolean hasEngine = Objects.isNull(car.getEngine());
        if (hasEngine) {
            car.getEngine().setHorsePower(horsePower);
        } else if (horsePower > 500) {
            car.setEngine(new DaewooV10Engine(690));
        } else {
            car.setEngine(new Engine("Standard Daewoo Engine", 196));
        }
    }
    
    //========== Specific Ford Components that builder client or director don't know about…

    private class DaewooSportDesign extends Design {

        public DaewooSportDesign(String color) {
            super(color);
            String[] shapeElements = {"Średnio niskie progi", "Lekki spoiler", "Neonowe lampy"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "DaewooSportDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }

    private class DaewooClassicDesign extends Design {

        public DaewooClassicDesign(String color) {
            super(color);
            String[] shapeElements = {"5 drzwiowy", "Hatchbad", "Abs"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "DaewooClassicDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }

    private class DaweooMuscleDesign extends Design {

        public DaweooMuscleDesign(String color) {
            super(color);
            String[] shapeElements = {"Tytanowe progi", "Karbonowa maska", "Reflektory"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "DaewooMuscleDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }

    private class DaewooV10Engine extends Engine {

        public DaewooV10Engine(int horsePower) {
            super("Loud Ford V12", horsePower);
        }
    }
}
