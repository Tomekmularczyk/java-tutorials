package builder.myexample1;

import java.time.Year;
import java.util.Arrays;
import java.util.Objects;

public class FordCarBuilder implements CarBuilder{
    private Car car;
    
    @Override
    public void build() {
        this.car = new Car();
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public void setSportDesign() {
        car.setDesign(new FordSportDesign("Yellow"));
    }

    @Override
    public void setClassicDesign() {
        car.setDesign(new FordClassicDesign("Gray"));
    }

    @Override
    public void setMuscleDesign() {
        car.setDesign(new FordMuscleDesign("Black"));
    }

    @Override
    public void setMaxSpeed(int speed) {
        car.setMaxSpeed(speed);
    }

    @Override
    public void setFamilySpace() {
        car.setSpace(89.5f);
    }

    @Override
    public void setNitro() {
        class FordNitro extends Nitro {
            public FordNitro(String details) {
                super(details);
            }
        }
        car.setNitro(new FordNitro("N32o Ford Nitro"));
    }

    @Override
    public void setProductionDate(Year date) {
        car.setProductionDate(date);
    }

    @Override
    public void setLoudEngine() {
        car.setEngine(new FordV12Engine(690));
    }

    @Override
    public void setMaxHorsePower(int horsePower) {
        boolean hasEngine = Objects.isNull(car.getEngine());
        if(hasEngine){
            car.getEngine().setHorsePower(horsePower);
        } else {
            if(horsePower > 500) {
                car.setEngine(new FordV12Engine(690));
            } else {
                car.setEngine(new Engine("Standard Ford Engine", 232));
            }
        }
    }
    
    //========== Specific Ford Components that builder client or director don't know about…
    
    private class FordSportDesign extends Design {
        public FordSportDesign(String color) {
            super(color);
            String[] shapeElements = {"Niskie progi", "Sportowy bumper", "Spoiler", "Neony"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "FordSportDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }
    private class FordClassicDesign extends Design {
        public FordClassicDesign(String color) {
            super(color);
            String[] shapeElements = {"Zwykłe progi", "Typowy bumper", "Prosty zesaw świateł"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "FordClassicDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }
    private class FordMuscleDesign extends Design {
        public FordMuscleDesign(String color) {
            super(color);
            String[] shapeElements = {"Wysokie progi", "Szeroki bumper", "Wzmocnione przednie lampy", "Ciemne szyby"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "FordMuscleDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }
    
    private class FordV12Engine extends Engine {
        public FordV12Engine(int horsePower) {
            super("Loud Ford V12", horsePower);
        }
    }
}
