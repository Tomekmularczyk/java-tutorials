package builder.myexample1;

import java.time.Year;

/**
 * Interfejs do tworzenia samochodów
 */
public interface CarBuilder {

    void build();

    Car getCar();

    public void setSportDesign();

    public void setClassicDesign();

    public void setMuscleDesign();

    public void setMaxSpeed(int speed);

    public void setFamilySpace();

    public void setNitro();

    public void setProductionDate(Year date);

    public void setLoudEngine();

    public void setMaxHorsePower(int horsePower);
}
