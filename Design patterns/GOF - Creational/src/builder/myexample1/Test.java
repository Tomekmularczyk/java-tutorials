package builder.myexample1;

public class Test {
    public static void main(String[] args) {
        CarBuilder fordBuilder = new FordCarBuilder();
        CarBuilder daewooBuilder = new DaewooCarBuilder();
        CarBuilder mazdaBuilder = new MazdaCarBuilder();
        
        CarDirector carDirector = new CarDirector();
        Car classicCar = carDirector.getClassicCar(daewooBuilder);
        Car sportCar = carDirector.getSportCar(mazdaBuilder);
        Car musscleCar = carDirector.getMusscleCar(fordBuilder);

        Car classicCar1 = carDirector.getClassicCar(fordBuilder);
        Car sportCar1 = carDirector.getSportCar(daewooBuilder);
        Car musscleCar1 = carDirector.getMusscleCar(mazdaBuilder);
        
        System.out.printf("classic car:\n%s"
                + "\n\nmusscle car:\n%s"
                + "\n\nsport car:\n%s", 
                classicCar, sportCar, musscleCar);

        System.out.printf("classic1 car:\n%s"
                + "\n\nmusscle1 car:\n%s"
                + "\n\nsport1 car:\n%s", 
                classicCar1, sportCar1, musscleCar1);
    }
}
