package builder.myexample1;

import java.time.Instant;
import java.time.LocalTime;
import java.time.Year;
import java.util.Arrays;
import java.util.Objects;

public class MazdaCarBuilder implements CarBuilder{
    private Car car;
    
    @Override
    public void build() {
        car = new Car();
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public void setSportDesign() {
        car.setDesign(new MazdaSportDesign("Yellow"));
    }

    @Override
    public void setClassicDesign() {
        car.setDesign(new MazdaClassicDesign("Blue"));
    }

    @Override
    public void setMuscleDesign() {
        car.setDesign(new MazdaMuscleDesign("Red"));
    }

    @Override
    public void setMaxSpeed(int speed) {
        car.setMaxSpeed(speed);
    }

    @Override
    public void setFamilySpace() {
        car.setSpace(56f);
    }

    @Override
    public void setNitro() {
        class MazdaNitro extends Nitro {
            private Instant timeOut;
            public MazdaNitro(String details) {
                super(details);
                timeOut = Instant.ofEpochSecond(LocalTime.of(0, 3, 10).toSecondOfDay());
            }

            @Override
            public String toString() {
                return "MazdaNitro" + "(timeOut = " + timeOut.toEpochMilli() + "s)";
            }
            
            
        }
        car.setNitro(new MazdaNitro("Mazda specific nitro"));
    }

    @Override
    public void setProductionDate(Year date) {
        car.setProductionDate(date);
    }

    @Override
    public void setLoudEngine() {
        car.setEngine(new MazdaV14Engine(600));
    }

    @Override
    public void setMaxHorsePower(int horsePower) {
        if(horsePower > 600){
            horsePower = 600;
            System.out.println("\nMazda doesn't produces over 600 horse power engines\n");
        }
        
        boolean hasEngine = Objects.isNull(car.getEngine());
        if(hasEngine){
            car.getEngine().setHorsePower(horsePower);
        } else {
            car.setEngine(new Engine("Mazda standar engine", horsePower));
        }
    }
    
    
    //========== Specific Ford Components that builder client or director don't know about…

    private class MazdaSportDesign extends Design {

        public MazdaSportDesign(String color) {
            super(color);
            String[] shapeElements = {"Wysoki spoiler", "Neonowe lampy", "Karbonowe pokrycie"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "MazdaSportDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }

    private class MazdaClassicDesign extends Design {

        public MazdaClassicDesign(String color) {
            super(color);
            String[] shapeElements = {"Abs", "Poduszki powietrzne", "Zestaw lamp"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "MazdaClassicDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }

    private class MazdaMuscleDesign extends Design {

        public MazdaMuscleDesign(String color) {
            super(color);
            String[] shapeElements = {"Drzwi otwierane do góry", "Przyciemniane szyby", "Neonowe podwozie"};
            setShapeElements(shapeElements);
        }

        @Override
        public String toString() {
            return "MazdaMuscleDesign: " + Arrays.toString(getShapeElements()) + " w kolorze " + getColor();
        }
    }

    private class MazdaV14Engine extends Engine {

        public MazdaV14Engine(int horsePower) {
            super("Loud Mazda V12", horsePower);
        }
    }
}
