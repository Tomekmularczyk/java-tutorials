package builder.myexample1;

import java.time.Year;

/*
 *   Product
 */
public class Car {

    private Design design;
    private int maxSpeed;
    private float space;
    private Nitro nitro;
    private Year productionDate;
    private Engine engine;

    public Design getDesign() {
        return design;
    }

    public void setDesign(Design design) {
        this.design = design;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getSpace() {
        return space;
    }

    public void setSpace(float space) {
        this.space = space;
    }

    public Nitro getNitro() {
        return nitro;
    }

    public void setNitro(Nitro nitro) {
        this.nitro = nitro;
    }

    public Year getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Year productionDate) {
        this.productionDate = productionDate;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return design
                + "\nengine = " +engine
                + "\nmaxSpeed = " + maxSpeed
                + "\nspace inside car(cm3) = " + space
                + "\nnitro = " + nitro
                + "\nproductionDate = " + productionDate;
    }

}
