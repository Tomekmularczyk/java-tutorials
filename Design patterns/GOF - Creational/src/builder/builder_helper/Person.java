package builder.builder_helper;

/**
 * Ze statycznymi klasami wewnetrznymi. Uczynienie ich nie statycznymi nie miałoby tutaj sensu, ponieważ wtedy 
 * to by była zwykła klasa z setterami
 * 
 * Wg mnie to nie jest klasyczny builder pattern, a raczej zwykła klasa/helper pomagający konstruować skomplikowane obiekty.
 * Klasyczny builder pattern udostępnia interfejs do budowania abstrakcyjnych obiektów, dzięki czemu jeden proces konstrukcji
 * …może prowadzić do kilku reprezentacji danego produktu. 
 * @author tomek
 */
public class Person {

    private String name;
    private String surname;
    private int age;
    private String hobby;
    private Body body;
    private int numberOfFriends;
    private Person partner;

    @Override
    public String toString() {
        return "Name: " + name + "\nSurname: " + surname + "\nAge: " + age + "\nHobby: " + hobby + "\n"
                + "Body: " + body + "\nNumber of friends: " + numberOfFriends + "\nPartner: " + partner;
    }

    public static class PersonBuilder {

        private final int age;
        private final PersonBodyBuilder bodyBuilder;
        //default fields
        private String name = "no-name";
        private String surname = "nobody";
        private String hobby = "no-hobby";
        private int numberOfFriends = 0;
        private Person partner;

        public PersonBuilder(int age, int height, String color) {
            this.bodyBuilder = new PersonBodyBuilder(height, color);
            this.age = age;
        }

        public PersonBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public PersonBuilder setHobby(String hobby) {
            this.hobby = hobby;
            return this;
        }

        public PersonBuilder setNumberOfFriends(int numberOfFriends) {
            this.numberOfFriends = numberOfFriends;
            return this;
        }

        public PersonBuilder setPartner(Person partner) {
            this.partner = partner;
            return this;
        }

        public PersonBodyBuilder startBodyBuilding() {
            return bodyBuilder;
        }

        public Person build() {
            Person person = new Person();
            person.age = age;
            person.body = bodyBuilder.build();
            person.name = name;
            person.surname = surname;
            person.hobby = hobby;
            person.numberOfFriends = numberOfFriends;
            person.partner = partner;
            return person;
        }

        public class PersonBodyBuilder extends Body.BodyBuilderBase<PersonBodyBuilder> {

            public PersonBodyBuilder(int height, String color) {
                super(height, color);
            }
            
            public PersonBuilder stopBodyBuilding() {
                return PersonBuilder.this;
            }
            
        }
    }

    public static class Body {

        private float bodyFat;
        private int height;
        private String skinColor;
        private int weight;

        protected Body() {
        }

        @Override
        public String toString() {
            return "Body fat = " + bodyFat + ", height = " + height + ", color = " + skinColor + ", weight = " + weight;
        }

        public class BodyBuilder extends BodyBuilderBase<BodyBuilder> {

            public BodyBuilder(int height, String color) {
                super(height, color);
            }
        }
        
        /**
         * We want Body builder to return specific class of its subclass. 
         * So we can return reference to PersonBuilder just by extending a BodyBuilderBase and typing return type
         */
        private abstract static class BodyBuilderBase<T extends BodyBuilderBase> {

            private final int height;
            private final String skinColor;
            //default fields
            private float bodyFat = 15;
            private int weight = 60;

            public BodyBuilderBase(int height, String color) {
                this.height = height;
                this.skinColor = color;
            }

            public T setBodyFat(float bodyFat) {
                this.bodyFat = bodyFat;
                return (T) this;
            }

            public T setWeight(int weight) {
                this.weight = weight;
                return (T) this;
            }

            public Body build() {
                Body body = new Body();
                body.height = height;
                body.skinColor = skinColor;
                body.bodyFat = bodyFat;
                body.weight = weight;
                return body;
            }
        }
        
        
    }

}
