package builder.book_example;

import introductory_example_mazegame.Maze;

public interface MazeBuilder {

    void buildRoom(int roomNo);

    void buildDoor(int roomFrom, int roomTo);

    /**
     * tworzy egzemplarz Labiryntu, który pozostałe operacje będą składać
     */
    void buildMaze();
    
    /**
     * @return labirynt stworzony dla klienta
     */
    Maze getMaze();
}
