package builder.book_example;

import introductory_example_mazegame.Maze;

public class Test {
    
    /**
     * Wzorzec Builder.
     * Oddziela tworzenie złożonego obiektu od jego reprezentacji, dzięki czemu ten sam proces konstrukcji
     * może prowadzić do powstawania różnych reprezentacji.
     * 
     * Maze - ostateczna postać składanego obiektu
     * MazeBuilder - określa abstrakcyjny interfejs do tworzenia składników obiektu Product
     * MazeGame - director używa interfejsu buildera do tworzenia reprezentacji obiektu
     * 
     * Klient teraz może tworzyć różne rodzaje labiryntu
     */
    public static void main(String[] args) {
        MazeBuilder mazeBuilder = new StandardMazeBuilder();

        Maze simpleMaze = MazeGame.createSimpleMaze(mazeBuilder);
        Maze complexMaze = MazeGame.createComplexMaze(mazeBuilder);
        
        CountingMazeBuilder countingMazeBuilder = new CountingMazeBuilder();
        MazeGame.createComplexMaze(countingMazeBuilder);
        CountingMazeBuilder.MazeStatistics statistics = countingMazeBuilder.getStatistics();
    }
}
