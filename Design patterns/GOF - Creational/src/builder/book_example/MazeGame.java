package builder.book_example;

import introductory_example_mazegame.Maze;

public class MazeGame {

    /**
     * Warto zauważyć że nie znamy dokładnie obiektów jakie są używane do tworzenia drzwi czy pomieszczeń, wewnętrzna implementacja jest
     * ukryta a obiekty zakapsułkowane. Ułatwia to potem podmiane reprezentacji labiryntu, ponieważ nie trzeba modyfikować kodu żadnego z
     * klientów używających MazeBuilder.
     *
     * @param builder obiekt który udostępnia interfejs do tworzenia labiryntu
     * @return zwraca Produkt, czyli labirynt (Maze)
     */
    public static Maze createSimpleMaze(MazeBuilder builder) {
        builder.buildMaze();

        builder.buildRoom(1);
        builder.buildRoom(2);
        builder.buildDoor(1, 2);

        return builder.getMaze();
    }

    /**
     * Przykład jak możemy wykorzystać wielokrotnie klase do tworzenia labiryntów różnego rodzaju
     * @param builder
     * @return 
     */
    public static Maze createComplexMaze(MazeBuilder builder) {
        builder.buildMaze();

        builder.buildRoom(15);
        builder.buildRoom(66);
        builder.buildDoor(15, 66);
        
        builder.buildRoom(114);
        builder.buildDoor(66, 114);

        builder.buildRoom(33);
        builder.buildDoor(15, 33);
        
        return builder.getMaze();
    }
    
}
