package builder.book_example;

import introductory_example_mazegame.Maze;

/**
 * Klasa w ogóle nie tworzy obiektów labiryntu a jedynie zlicza skomplikowaność
 * algorytmu używającego buildera.
 * @author tomek
 */
public class CountingMazeBuilder implements MazeBuilder{
    private int numberOfRooms = 0;
    private int numberOfDoors = 0;
    
    @Override
    public void buildRoom(int roomNo) {
        ++numberOfRooms;
    }

    @Override
    public void buildDoor(int roomFrom, int roomTo) {
        ++numberOfDoors;
    }

    @Override
    public void buildMaze() {
    }

    @Override
    public Maze getMaze() {
        return null;
    }
    
    public MazeStatistics getStatistics(){
        return new MazeStatistics(numberOfRooms, numberOfDoors);
    }
    
    
    //tworzymy *strukturę danych* reprezentującą dane, a nie abstrakcję danych(klasa), dlatego zmienne są publiczne
    public static class MazeStatistics {
        public final int numberOfRooms;
        public final int numberOfDoors;

        public MazeStatistics(int numberOfRooms, int numberOfDoors) {
            this.numberOfRooms = numberOfRooms;
            this.numberOfDoors = numberOfDoors;
        }
    }
}
