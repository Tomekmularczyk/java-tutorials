package builder.book_example;

import introductory_example_mazegame.Direction;
import introductory_example_mazegame.Door;
import introductory_example_mazegame.Maze;
import introductory_example_mazegame.Room;
import introductory_example_mazegame.Wall;

public class StandardMazeBuilder implements MazeBuilder{
    private final Maze currentMaze;
    
    public StandardMazeBuilder(){
        currentMaze = new Maze();
    }
    
    /**
     * tworzy pusty pokój
     * @param roomNo numer pokoju
     */
    @Override
    public void buildRoom(int roomNo) {
        Room newRoom = new Room(roomNo);
        currentMaze.addRoom(newRoom);
        
        newRoom.setSide(Direction.NORTH, new Wall());
        newRoom.setSide(Direction.EAST, new Wall());
        newRoom.setSide(Direction.SOUTH, new Wall());
        newRoom.setSide(Direction.WEST, new Wall());
    }
    
    /**
     * Wyszukuje odpowiednie pokoje oraz łączącą je ściane a potem tworzy pomiedzy nimi drzwi
     * @param roomFrom
     * @param roomTo 
     */
    @Override
    public void buildDoor(int roomFrom, int roomTo) {
        Room r1 = currentMaze.getRoom(roomFrom);
        Room r2 = currentMaze.getRoom(roomTo);
        Door door = new Door(r1, r2);
        
        r1.setSide(commonWall(r1, r2), door);
        r2.setSide(commonWall(r2, r1), door);
    }

    @Override
    public void buildMaze() {
    }

    @Override
    public Maze getMaze() {
        return null;
    }

    /**
     * Operacja narzędziowa określająca kierunek standardowej ściany pomiedzy dwoma pomieszczeniami
     * @param r1
     * @param r2
     * @return 
     */
    private Direction commonWall(Room r1, Room r2){
        return null;
    }
}
