package builder.myexample2;


/**
 * Builder pattern służy do tworzenia złożonych obiektów podczas gdy ten sam proces konstrukcji 
 * może prowadzić do różnych reprezentacji tego samego obiektu, ponieważ 
 * konkretne elementy z których jest złożony są zakapsułkowane.
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        MotorbikeBuilder suzukiBuilder = new SuzukiMotorbikeBuilder();
        MotorbikeBuilder yamahaBuilder = new YamahaMotorbikeBuilder();
        
        Motorbike suzukiCruiser = Motorbike.getMeCruiser(suzukiBuilder);
        Motorbike yamahaOffroad = Motorbike.getMeOffRoad(yamahaBuilder);
        Motorbike yamahaScooter = Motorbike.getMeScooter(yamahaBuilder);
        
        System.out.printf("Suzuki cruiser:\n%s \n\n"
                + "Yamaha Off-road:\n%s \n\n"
                + "Yamaha scooter:\n%s\n", 
                suzukiCruiser,
                yamahaOffroad,
                yamahaScooter);
    }
}
