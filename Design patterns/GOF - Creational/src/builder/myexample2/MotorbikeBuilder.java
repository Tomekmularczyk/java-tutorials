package builder.myexample2;

import java.time.Year;

/**
 * Lista rzeczy które można zamówić:
 * @author tomek
 */
public interface MotorbikeBuilder {
    
    /**
     * Tworzenie obiektu dzieje się w jednym miejscu co sprzyja wielowątkowści. Effective Java 2
     * @return 
     */
    Motorbike build();

    /************** TYM MA SIĘ ZAJMOWAĆ BUILDER *****************/
    
    MotorbikeBuilder setScooterEngine();

    MotorbikeBuilder setCruiserEngine();

    MotorbikeBuilder setOffRoadEngine();

    MotorbikeBuilder setScooterSpeed();

    MotorbikeBuilder setCruiserSpeed();

    MotorbikeBuilder setOffRoadSpeed();

    MotorbikeBuilder setScooterBodyparts();

    MotorbikeBuilder setCruiserBodyparts();

    MotorbikeBuilder setOffRoadBodyparts();
    
    MotorbikeBuilder setBadCondition();
    
    MotorbikeBuilder setGoodCondition();
    
    MotorbikeBuilder setSemiGoodCondition();
    
    /************** TE RZECZY CIEŻKO PRZYGOTOWAĆ W ZESTAWIE DLATEGO KLIENT POWINIEN JE PODAĆ *****************/

    MotorbikeBuilder setProductionYear(Year year);
    
    MotorbikeBuilder setColor(String color);
}
