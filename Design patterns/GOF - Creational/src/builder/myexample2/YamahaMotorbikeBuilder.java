package builder.myexample2;

import java.time.Year;

public class YamahaMotorbikeBuilder implements MotorbikeBuilder {

    private final String producent;
    private Motorbike.Engine engine;
    private double maxSpeed;
    private Motorbike.BodyParts bodyParts;
    private Year productionYear;
    private String condition;
    private String color;

    /**
     * Ustawiamy defaultowe wartości w konstruktorze
     */
    public YamahaMotorbikeBuilder() {
        this.producent = "YAMAHA 2015";
        this.engine = new YamahaStandarEngine();
        this.bodyParts = new Motorbike.BodyParts(new String[]{"Standard Wheels", "Standard body-parts"});
        this.productionYear = Year.now();
        this.maxSpeed = 200;
        this.condition = "New";
        this.color = "Black";
    }

    @Override
    public Motorbike build() {
        Motorbike motorbike = new Motorbike();
        motorbike.setProducent(producent);
        motorbike.setEngine(engine);
        motorbike.setBodyParts(bodyParts);
        motorbike.setProductionYear(productionYear);
        motorbike.setMaxSpeed(maxSpeed);
        motorbike.setCondition(condition);
        motorbike.setColor(color);

        return motorbike;
    }

    @Override
    public MotorbikeBuilder setScooterEngine() {
        //We do not produce specific scooter engines, we use our standard engines instead…
        
        this.engine = new YamahaStandarEngine();
        return this;
    }

    @Override
    public MotorbikeBuilder setCruiserEngine() {
        class YamahaCruiserEngine extends Motorbike.Engine {

            public YamahaCruiserEngine(String details) {
                super(details);
            }
        }

        this.engine = new YamahaCruiserEngine("Yamaha-Spec");
        return this;
    }

    @Override
    public MotorbikeBuilder setOffRoadEngine() {
        class YamahaOffRoadEngine extends Motorbike.Engine {

            public YamahaOffRoadEngine(String details) {
                super(details);
            }
        }

        this.engine = new YamahaOffRoadEngine("Yamaha-Spec-XX");
        return this;
    }

    @Override
    public MotorbikeBuilder setScooterSpeed() {
        this.maxSpeed = 45;
        return this;
    }

    @Override
    public MotorbikeBuilder setCruiserSpeed() {
        this.maxSpeed = 95;
        return this;
    }

    @Override
    public MotorbikeBuilder setOffRoadSpeed() {
        this.maxSpeed = 110;
        return this;
    }

    @Override
    public MotorbikeBuilder setScooterBodyparts() {
        final String[] scooterParts = {"Small wheels", "two rear views"};
        class YamahaScooterParts extends Motorbike.BodyParts {

            public YamahaScooterParts() {
                super(scooterParts);
            }
        }

        this.bodyParts = new YamahaScooterParts();
        return this;
    }

    @Override
    public MotorbikeBuilder setCruiserBodyparts() {
        final String[] scooterParts = {"Heavy-Weight parts", "5-level gearbox"};
        class YamahaCruiserParts extends Motorbike.BodyParts {

            public YamahaCruiserParts() {
                super(scooterParts);
            }
        }

        this.bodyParts = new YamahaCruiserParts();
        return this;
    }

    @Override
    public MotorbikeBuilder setOffRoadBodyparts() {
        final String[] offRoadParts = {"Small fans", "big seats", "lightweight"};
        class YamahaOffroadParts extends Motorbike.BodyParts {

            public YamahaOffroadParts() {
                super(offRoadParts);
            }
        }

        this.bodyParts = new YamahaOffroadParts();
        return this;
    }

    @Override
    public MotorbikeBuilder setBadCondition() {
        this.condition = "Bad condition, filter needs to be changed";
        return this;
    }

    @Override
    public MotorbikeBuilder setGoodCondition() {
        this.condition = "Good condition, everything works as it's new";
        return this;
    }

    @Override
    public MotorbikeBuilder setSemiGoodCondition() {
        this.condition = "Semi-Good condition, scratches on paint, some parts may stop working";
        return this;
    }

    @Override
    public MotorbikeBuilder setProductionYear(Year year) {
        this.productionYear = year;
        return this;
    }

    @Override
    public MotorbikeBuilder setColor(String color) {
        this.color = color;
        return this;
    }

    private class YamahaStandarEngine extends Motorbike.Engine {
        public YamahaStandarEngine() {
            super("Yamaha Dongo-5");
        }
    }
}
