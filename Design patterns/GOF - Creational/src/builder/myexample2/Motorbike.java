package builder.myexample2;

import java.time.Year;
import java.util.Arrays;

/**
 * Motorbike będzie i Produktem i Dyrektorem
 * @author tomek
 */
public class Motorbike {
    private String producent;
    private Engine engine;
    private double maxSpeed;
    private BodyParts bodyParts;
    private Year productionYear;
    private String condition;
    private String color;

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public BodyParts getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(BodyParts bodyParts) {
        this.bodyParts = bodyParts;
    }

    public Year getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(Year productionYear) {
        this.productionYear = productionYear;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Producent: " + producent 
                + "\nengine: " + engine 
                + "\nmaxSpeed: " + maxSpeed 
                + "\nbodyParts: " + bodyParts 
                + "\nproductionYear: " + productionYear 
                + "\ncondition: " + condition
                + "\ncolor: " + color;
    }
    
    /*************** DIRECTOR ****************/
    public static Motorbike getMeScooter(MotorbikeBuilder builder){
        return builder.setColor("White")
                .setScooterEngine()
                .setGoodCondition()
                .setScooterSpeed()
                .setScooterBodyparts()
                .build();
    }
    
    public static Motorbike getMeCruiser(MotorbikeBuilder builder){
        return builder.setCruiserBodyparts()
                .setCruiserEngine()
                .setCruiserSpeed()
                .build();
    }
    
    public static Motorbike getMeOffRoad(MotorbikeBuilder builder){
        return builder.setBadCondition()
                .setProductionYear(Year.of(2002))
                .setColor("Green")
                .setScooterEngine()
                .build();
    }
    
    /************ STANDARD PARTS *************/
    public static class Engine {
        private final String details;

        public Engine(String details) {
            this.details = details;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "-" + details;
        }
    }
    
    public static class BodyParts {
        private final String[] bodyParts;

        public BodyParts(String[] bodyParts) {
            this.bodyParts = bodyParts;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + ": " + Arrays.toString(bodyParts);
        }
    }
}
