package builder.myexample2;

import java.time.Year;
import builder.myexample2.Motorbike.Engine;

public class SuzukiMotorbikeBuilder implements MotorbikeBuilder {

    private final String producent;
    private Motorbike.Engine engine;
    private double maxSpeed;
    private Motorbike.BodyParts bodyParts;
    private Year productionYear;
    private String condition;
    private String color;

    /**
     * Ustawiamy defaultowe wartości w konstruktorze
     */
    public SuzukiMotorbikeBuilder() {
        this.producent = "SUZUKI";
        this.engine = new Motorbike.Engine("Suzuki standard engine");
        this.bodyParts = new Motorbike.BodyParts(new String[]{"Standard Wheels", "Standard body-parts"});
        this.productionYear = Year.now();
        this.maxSpeed = 220;
        this.condition = "New";
        this.color = "White";
    }

    @Override
    public Motorbike build() {
        Motorbike motorbike = new Motorbike();
        motorbike.setProducent(producent);
        motorbike.setEngine(engine);
        motorbike.setBodyParts(bodyParts);
        motorbike.setProductionYear(productionYear);
        motorbike.setMaxSpeed(maxSpeed);
        motorbike.setCondition(condition);
        motorbike.setColor(color);

        return motorbike;
    }

    @Override
    public MotorbikeBuilder setScooterEngine() {
        class SuzukiScooterEngine extends Engine {

            public SuzukiScooterEngine(String details) {
                super(details);
            }
        }

        this.engine = new SuzukiScooterEngine("V3-s");
        return this;
    }

    @Override
    public MotorbikeBuilder setCruiserEngine() {
        class SuzukiCruiserEngine extends Engine {

            public SuzukiCruiserEngine(String details) {
                super(details);
            }
        }

        this.engine = new SuzukiCruiserEngine("V4-CE");
        return this;
    }

    @Override
    public MotorbikeBuilder setOffRoadEngine() {
        class SuzukiOffRoadEngine extends Engine {

            public SuzukiOffRoadEngine(String details) {
                super(details);
            }
        }

        this.engine = new SuzukiOffRoadEngine("D9.5x");
        return this;
    }

    @Override
    public MotorbikeBuilder setScooterSpeed() {
        this.maxSpeed = 60;
        return this;
    }

    @Override
    public MotorbikeBuilder setCruiserSpeed() {
        this.maxSpeed = 120;
        return this;
    }

    @Override
    public MotorbikeBuilder setOffRoadSpeed() {
        this.maxSpeed = 100;
        return this;
    }

    @Override
    public MotorbikeBuilder setScooterBodyparts() {
        final String[] scooterParts = {"Lot of space for legs", "classic scooter design"};
        class SuzukiScooterParts extends Motorbike.BodyParts {

            public SuzukiScooterParts() {
                super(scooterParts);
            }
        }

        this.bodyParts = new SuzukiScooterParts();
        return this;
    }

    @Override
    public MotorbikeBuilder setCruiserBodyparts() {
        final String[] scooterParts = {"Big front wheel", "Wide handlebar", "Comfortable seat"};
        class SuzukiCruiserParts extends Motorbike.BodyParts {

            public SuzukiCruiserParts() {
                super(scooterParts);
            }
        }

        this.bodyParts = new SuzukiCruiserParts();
        return this;
    }

    @Override
    public MotorbikeBuilder setOffRoadBodyparts() {
        final String[] offRoadParts = {"Off-road tire", "Short handlebar", "Light-weight parts"};
        class SuzukiOffroadParts extends Motorbike.BodyParts {

            public SuzukiOffroadParts() {
                super(offRoadParts);
            }
        }

        this.bodyParts = new SuzukiOffroadParts();
        return this;
    }

    @Override
    public MotorbikeBuilder setBadCondition() {
        this.condition = "Bad condition, engine not working";
        return this;
    }

    @Override
    public MotorbikeBuilder setGoodCondition() {
        this.condition = "Good condition, everything works as it's new";
        return this;
    }

    @Override
    public MotorbikeBuilder setSemiGoodCondition() {
        this.condition = "Semi-Good condition, scratches on paint, some parts may stop working";
        return this;
    }

    @Override
    public MotorbikeBuilder setProductionYear(Year year) {
        this.productionYear = year;
        return this;
    }

    @Override
    public MotorbikeBuilder setColor(String color) {
        this.color = color;
        return this;
    }

}
