package singleton.book_example;

import abstract_factory.book_example.MazeFactory;

/**
 * Konkretna implementacje wzorca singleton
 */
public class MazeFactorySingleton extends MazeFactory {
    private static MazeFactorySingleton instance;
    
    /**
     * Definiujemy chroniony konstruktor by nikt przez przypadek nie stworzył więcej egzemplarzy
     */
    protected MazeFactorySingleton(){}
    
    /**
     * Lazy Instantiation, obiekt jest tworzony dopiero wtedy gdy jest pierwszy raz potrzebny
     */
    public MazeFactorySingleton getInstance() {
        if(instance == null){
            instance = new MazeFactorySingleton(); //problemem może być na stałe ustalona konkretna klasa
        }
        
        return instance;
    }
}
