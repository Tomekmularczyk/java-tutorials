package singleton.book_example;

/**
 * Konkretna implementacje wzorca singleton
 */
public class MazeFactorySingleton2 {
    private static MazeFactorySingleton2 instance;
    
    /**
     * Definiujemy chroniony konstruktor by nikt przez przypadek nie stworzył więcej egzemplarzy
     */
    protected MazeFactorySingleton2() {
    }

    
    public MazeFactorySingleton2 getInstance() {
        if (instance == null) {
            String mazeStyle = getEnvironmentVariable("MAZESTYLE");
            
            switch(mazeStyle){
                case "bombed":
                    instance = new BombedMazeFactory();
                    break;
                case "enchanted":
                    instance = new EnchantedMazeFactory();
                    break;
                default:
                    instance = new MazeFactorySingleton2();
                    break;
            }
            
        }

        return instance;
    }
    
    private String getEnvironmentVariable(String key){
        return null;
    }
    
    private class BombedMazeFactory extends MazeFactorySingleton2 {
    }
    
    private class EnchantedMazeFactory extends MazeFactorySingleton2 {
    }
    
}
