package singleton.my_example1;

public class Test {
    public static void main(String[] args) {
        CompanyPrinterEnum printer1 = CompanyPrinterEnum.Instance;
        CompanyPrinterEnum printer2 = CompanyPrinterEnum.Instance;
        CompanyPrinterEnum printer3 = CompanyPrinterEnum.Instance;

        System.out.printf("%s\n%s\n%s\n", printer1.hashCode(), printer2.hashCode(), printer3.hashCode());
        
        //****************** PART 2
        System.out.println("======================");
        
        MovieCharactersFactory gotFactory = new GameOfThronesFactory();
        MovieClient.introduceMovieCharacters(gotFactory);
    }
}
