package singleton.my_example1;

public class RamsaySnow implements MovieCharacter {
    private static RamsaySnow instance;
    
    protected RamsaySnow() {}
    
    public static RamsaySnow getInstance() {
        if(instance == null)
            instance = new RamsaySnow();
        
        return instance;
    }
}
