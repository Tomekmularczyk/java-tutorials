package singleton.my_example1;

/**
 * Singleton
 */
public class TyrionLannister implements MovieCharacter {
    private static TyrionLannister instance;
    
    protected TyrionLannister() {}
    
    /**
     * Nie jest bezpieczne wątkowo jak GameOfThronesFactory czy JonSnow
     */
    public static TyrionLannister getInstance() {
        if(instance == null){
            instance = new TyrionLannister();
        }
        
        return instance;
    }
}
