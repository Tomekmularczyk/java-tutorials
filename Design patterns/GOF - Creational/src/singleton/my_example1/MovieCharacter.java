package singleton.my_example1;

/**
 * Interfejs do rodziny obiektów. Niestety nie wiem/ nie da się wymósić by konkretna klasa była singletonem 
 * ponieważ nie możemy wymósić prywatnego konstruktora czy tworzyć abstrakcyjnych metod statycznych.
 */
public interface MovieCharacter {
    
    default String introduceYourSelf() {
        return "My name is " + getClass().getSimpleName() + "!(#Code: " + hashCode() + ")";
    };
    
}
