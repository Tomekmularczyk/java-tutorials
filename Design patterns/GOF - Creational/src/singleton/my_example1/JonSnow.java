package singleton.my_example1;

/**
 * Konkretny Singleton też bezpieczny wątkowo chociaż w mniej czysty (i podobno tak szybki) sposób jak i-o-d holder idiom
 */
public class JonSnow implements MovieCharacter {

    private static volatile JonSnow instance; //volatile - nie sięgaj do keszu

    protected JonSnow() {
    }

    public static JonSnow getInstance() {
        if (instance == null) {
            synchronized (JonSnow.class) { //zamiast całej metody synchronizujemy tylko ten jeden moment tworzenia obiektu
                if (instance == null) { //potrzebujemy sprawdzenia jeszcze raz
                    instance = new JonSnow();
                }
            }
        }

        return instance;
    }
}
