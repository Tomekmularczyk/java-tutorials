package singleton.my_example1;

public class MovieClient {

    public static void introduceMovieCharacters(MovieCharactersFactory factory) {
        MovieCharacter mainHero1 = factory.getMainHero();
        MovieCharacter blackCharacter1 = factory.getBlackCharacter();
        MovieCharacter theComic1 = factory.getTheComic();
        
        System.out.printf("%s:\n%s\n%s\n%s\n\n", 
                factory.getClass().getSimpleName(), 
                mainHero1.introduceYourSelf(), 
                blackCharacter1.introduceYourSelf(), 
                theComic1.introduceYourSelf());

        System.out.println("A teraz pobieramy te obiekty jeszcze raz przy uzyciu getInstance(), czy będą to te same obiekty?");

        MovieCharacter mainHero2 = factory.getMainHero();
        MovieCharacter blackCharacter2 = factory.getBlackCharacter();
        MovieCharacter theComic2 = factory.getTheComic();

        System.out.printf("%s:\n%s\n%s\n%s\n\n", 
                factory.getClass().getSimpleName(), 
                mainHero2.introduceYourSelf(), 
                blackCharacter2.introduceYourSelf(), 
                theComic2.introduceYourSelf());
        
    }
}
