package singleton.my_example1;

public enum CompanyPrinterEnum {
    Instance;
    
    private final String drivers;
    private final String location;
    private final int modelSN;
    private CompanyPrinterEnum(){
        drivers = "Driver1, 2, 3";
        location = "Room no 203";
        modelSN = 888;
    }

    public String getDrivers() {
        return drivers;
    }

    public String getLocation() {
        return location;
    }

    public int getModelSN() {
        return modelSN;
    }
    
}
