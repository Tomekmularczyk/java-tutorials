package singleton.my_example1;

/**
 * Konkretna klasa Singleton, produkująca Singletony
 */
public class GameOfThronesFactory extends MovieCharactersFactory {
    /**
     * Initialization-on-demand holder idiom. To zapewnia nam bezpieczeństwo przy wielowątkowości.
     * Klasa pomimo, że zawiera zmienną finalną, jest ładowana dopiero przy pierwszym użyciu getInstance();
     */
    private static class Holder {
        static final GameOfThronesFactory Instance = new GameOfThronesFactory();
    }
    
    /**
     * Nie publiczny konstruktor
     */
    protected GameOfThronesFactory() {}

    
    /**
     * Używamy kowariancji typów żeby zwrócić konretne klasy obiektów
     */
    public GameOfThronesFactory getInstance() {
        return Holder.Instance;
    }

    @Override
    public JonSnow getMainHero() {
        return JonSnow.getInstance();
    }

    @Override
    public MovieCharacter getBlackCharacter() {
        return RamsaySnow.getInstance();
    }

    @Override
    public MovieCharacter getTheComic() {
        return TyrionLannister.getInstance();
    }
    
    



}
