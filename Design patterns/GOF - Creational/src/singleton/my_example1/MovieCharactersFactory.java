package singleton.my_example1;

/**
 * niestety nie możemy wymósić by podklasy były singletonami ponieważ nie możemy wymósić statycznych metod 
 * czy prywatnych konstruktorów
 */
public abstract class MovieCharactersFactory{
    
    //public abstract static MovieCharactersFactory getInstance();
    
    public abstract MovieCharacter getMainHero();
    
    public abstract MovieCharacter getBlackCharacter();
    
    public abstract MovieCharacter getTheComic();
}
