package adapter;

public class Lenovo implements Laptop{

	@Override
	public void typeTheText(String text) {
		System.out.println("Typed: " + text);
	}

	@Override
	public void closeTheLid() {
		System.out.println("Lid closed");
	}

	@Override
	public void putInBag() {
		System.out.println("Laptop in bag");
	}

}
