package adapter;

public class TEST {
	/* Adapter("przejściówka"):
	 * - pozwala dwóm niekompatybilnym interfejsom na działanie razem
	 * - używany jest wtedy gdy oczekiwany jest inny interfejs niż dostępny
	 * 
	 */
	
	
	public static void main(String[] args) {
		playWithLaptop(new Lenovo());
		System.out.println();
		
		
		Smartphone lg = new LGLeon();
		SmartphoneAdapter lgAdapter = new SmartphoneAdapter(lg);
		playWithLaptop(lgAdapter);
	}
	
	private static void playWithLaptop(Laptop laptop){
		laptop.closeTheLid();
		laptop.putInBag();
		laptop.typeTheText("random");
	}
}
