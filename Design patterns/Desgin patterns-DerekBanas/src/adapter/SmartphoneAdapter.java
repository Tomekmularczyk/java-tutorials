package adapter;

public class SmartphoneAdapter implements Laptop{
	private Smartphone smartphone;
	
	public SmartphoneAdapter(Smartphone smartphone){
		this.smartphone = smartphone;
	}
	
	@Override
	public void typeTheText(String text) {
		smartphone.swipeTheWord(text);
	}

	@Override
	public void closeTheLid() {
		smartphone.lock();
	}

	@Override
	public void putInBag() {
		smartphone.putInPocket(true);
	}

}
