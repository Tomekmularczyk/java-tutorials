package adapter;

public interface Smartphone {
	public void swipeTheWord(String text);
	public void lock();
	public void putInPocket(boolean put);
}
