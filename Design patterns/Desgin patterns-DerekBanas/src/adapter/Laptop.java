package adapter;

public interface Laptop {
	public void typeTheText(String text);
	public void closeTheLid();
	public void putInBag();
}
