package adapter;

public class LGLeon implements Smartphone{

	@Override
	public void swipeTheWord(String text) {
		System.out.println("Swiped the word " + text);
	}

	@Override
	public void lock() {
		System.out.println("Locked");
	}

	@Override
	public void putInPocket(boolean put) {
		if(put)
			System.out.println("Smartphone in pocket");
	}

}
