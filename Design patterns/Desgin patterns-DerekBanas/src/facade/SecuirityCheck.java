package facade;

public class SecuirityCheck {
	private Account account;
	public SecuirityCheck(Account account){
		this.account = account;
	}
	
	public boolean checkPin(int pin){
		if(pin == account.getPin()){
			System.out.println("Pin is OK...");
			return true;
		}
		
		System.out.println("Wrong Pin Number!");
		return false;
	}
}
