package facade;

public class Account {
	private double deposit = 1000;
	private long accountNumber = 987654321;
	private int pin = 1234;

	public long getAccountNumber() {
		return accountNumber;
	}

	public int getPin() {
		return pin;
	}

	public double depositSize(){
		return deposit;
	}
	
	public double getMoney(double amount) {
		deposit -= amount;
		return amount;
	}

	public void addDeposit(double money) {
		this.deposit += money;
	}
	
	
}
