package facade;

public class AccountNumberCheck {
	private Account account;
	public AccountNumberCheck(Account account){
		this.account = account;
	}
	
	public boolean checkAccountNumber(long accountNumber){
		if(accountNumber == account.getAccountNumber()){
			System.out.println("Account number is OK...");
			return true;
		}
		
		System.out.println("Wrong Account Number!");
		return false;
	}
}
