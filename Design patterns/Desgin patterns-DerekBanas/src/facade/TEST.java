package facade;

public class TEST {
	/* Fasada to pattern, którego używa się by stworzyć prosty interfejs dla użytkownika, a który wykonuje dodatkowe operacje "pod maską"
	 * 
	 */
	
	
	public static void main(String[] args) {
		Bank bank = new TMobileBank(987654321, 1234);
		
		double cash = bank.widthrawCash(2000);
		bank.depositCash(50000);
	}

}
