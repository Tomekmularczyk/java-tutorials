package facade;

public interface Bank {
	public double widthrawCash(double amount);
	public void depositCash(double amount);
}
