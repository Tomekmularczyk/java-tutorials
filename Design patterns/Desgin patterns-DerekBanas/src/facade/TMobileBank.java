package facade;

public class TMobileBank implements Bank{
	private Account userAccount;
	private long accountNumber;
	private int pin;
	
	private AccountNumberCheck accountNumberCheck;
	private SecuirityCheck secuirityCheck;
	private FundsCheck fundsCheck;
	
	public TMobileBank(long accountNumber, int pin){
		userAccount = new Account();
		this.accountNumber = accountNumber;
		this.pin = pin;
		
		accountNumberCheck = new AccountNumberCheck(userAccount);
		secuirityCheck = new SecuirityCheck(userAccount);
		fundsCheck = new FundsCheck(userAccount);
	}
	
	@Override
	public double widthrawCash(double amount) {		
		if(accountNumberCheck.checkAccountNumber(accountNumber) && 
				secuirityCheck.checkPin(pin) &&
				fundsCheck.checkIfEnoughFunds(amount)){
			
			double cash = userAccount.getMoney(amount);
			System.out.println("Transaction complete!\nPlease take your money(== " + cash + " ==) and have a nice day!"); 
		}else{
			System.err.println("Transaction Failed!");
		}
			
		return 0;
	}

	@Override
	public void depositCash(double amount) {
		if(accountNumberCheck.checkAccountNumber(accountNumber) &&
				secuirityCheck.checkPin(pin)){
			
			userAccount.addDeposit(amount);
			System.out.println("Transaction complete!");
		}else{
			System.err.println("Transaction Failed!");
		}
	}

}
