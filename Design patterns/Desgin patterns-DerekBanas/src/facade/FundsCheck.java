package facade;

public class FundsCheck {
	private Account account;
	public FundsCheck(Account account){
		this.account = account;
	}
	
	public boolean checkIfEnoughFunds(double amount){
		if(amount <= account.depositSize()){
			System.out.println("Funds are sufficient...");
			return true;
		}
		
		System.out.println("Insufficient Funds!");
		return false;
	}
}
