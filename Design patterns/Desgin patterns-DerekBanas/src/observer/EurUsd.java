package observer;

public class EurUsd implements MarketObserver{
	private double value;

	@Override
	public void update(double eur_usd, double gbp_jpy, double usd_jpy) {
		value = eur_usd;
		System.out.println(this);
	}

	@Override
	public String toString(){
		return String.format("EurUsd: %.5f", value);
	}
}
