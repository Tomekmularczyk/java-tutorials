package observer;

public interface MarketObserver {
	public void update(double eur_usd, double gbp_jpy, double usd_jpy);
}
