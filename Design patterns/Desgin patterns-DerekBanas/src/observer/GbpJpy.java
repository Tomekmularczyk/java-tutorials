package observer;

public class GbpJpy implements MarketObserver{
	private double value;

	@Override
	public void update(double eur_usd, double gbp_jpy, double usd_jpy) {
		value = gbp_jpy;
		System.out.println(this);
	}

	@Override
	public String toString(){
		return String.format("GbpJpy: %.4f", value);
	}
}
