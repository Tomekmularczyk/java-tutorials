package observer;

public class TEST {
	/* - Wzorca observer używamy gdy wiele obiektów potrzebuje aktualizacji gdy jakiś obiekt się zmienia
	 * - Nadawca(publisher) wysyła informacje do obserwujących(subskrybentów)
	 * - Zaletą jest mniejsza zależność między modułami. Publisher nie musi nic wiedzieć o subskrybentach
	 * - Minusem jest że publisher może wysyłać nie istotne informacje.
	 */
	public static void main(String[] args) throws InterruptedException {
		Market forexMarket = new Market(); //market bedzie powiadamiał swoich subskrybentów(implementujących odpowiedni interfejs)
	
		EurUsd eurUsd = new EurUsd();
		forexMarket.register(eurUsd);
		forexMarket.register(new GbpJpy());
		forexMarket.register(new UsdJpy());
		
		for(int i=0; i<3; ++i){
			forexMarket.movePrices(); //wystarczy to zachowanie spowoduje ze wszyscy obserwujący zostaną poinformowani o zmianie
			Thread.sleep(1500);
		}
		
		forexMarket.unregister(eurUsd);
		forexMarket.unregister(0);
		forexMarket.unregister(new UsdJpy());
		
		forexMarket.movePrices();
	}

}
