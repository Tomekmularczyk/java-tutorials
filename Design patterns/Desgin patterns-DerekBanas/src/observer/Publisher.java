package observer;

public interface Publisher {
	public void register(MarketObserver observer);
	public void unregister(MarketObserver observer);
}