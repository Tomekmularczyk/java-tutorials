package observer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Market implements Publisher {
	private List<MarketObserver> observers = new LinkedList<>();
	private double eur_usd, gbp_jpy, usd_jpy;
	
	
	@Override
	public void register(MarketObserver observer) {
		observers.add(observer);
	}

	@Override
	public void unregister(MarketObserver observer) {
		observers.remove(observer);
	}
	public void unregister(int which){
		try{
			observers.remove(which);
		}catch(IndexOutOfBoundsException ex){
			System.err.println("Element nie istnieje!");
		}
	}

	public void movePrices(){
		Random rand = new Random();
		eur_usd = rand.nextFloat()+1;
		gbp_jpy = (rand.nextFloat()+168) + rand.nextInt(3);
		usd_jpy = (rand.nextFloat()+115) + rand.nextInt(3);
		
		//after price moved we have to notify subscribers
		System.out.println("Prices in market has changed.");
		notifyObservers();
	}
	
	private void notifyObservers() {
		for(MarketObserver observer : observers)
			observer.update(eur_usd, gbp_jpy, usd_jpy);
	}
	
}
