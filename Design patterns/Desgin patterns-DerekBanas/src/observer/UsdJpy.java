package observer;

public class UsdJpy implements MarketObserver{
	private double value;

	@Override
	public void update(double eur_usd, double gbp_jpy, double usd_jpy) {
		value = usd_jpy;
		System.out.println(this);
	}

	@Override
	public String toString(){
		return String.format("UsdJpy: %.3f", value);
	}
}
