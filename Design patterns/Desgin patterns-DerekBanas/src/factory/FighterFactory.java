package factory;

public class FighterFactory {
	public Fighter getFighter(String name){
		switch(name.toLowerCase()){
			case "jin": return new Jin();
			case "deviljin": return new DevilJin();
			case "kazuya": return new Kazuya();
			default: return null;
		}
	}
}
