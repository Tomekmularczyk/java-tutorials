package factory;

public abstract class Fighter {
	private String name;
	private String ability;
	
	public Fighter(String name, String ability){
		this.name = name;
		this.ability = ability;
	}
	
	@Override
	public String toString(){
		return String.format("Hey, I'am %s and my special move is %s!", name, ability);
	}
}
