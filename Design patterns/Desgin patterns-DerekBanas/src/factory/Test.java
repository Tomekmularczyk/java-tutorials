package factory;

import java.util.Scanner;

public class Test {
	/* Factory pattern uzywa się gdy:
	 * - metoda ma zwracać różne typy obiektów które mają wspólną klasę macierzystą
	 * - kiedy nie wiesz naprzód jaki obiekt będzie zwracany
	 * - użytkownik nie musi wiedzieć jaki dokładnie dokładnie typ będzie zwracany
	 * */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		String fighterName;
		Fighter fighter = null;
		FighterFactory factory = new FighterFactory(); 
		while(fighter == null){ //dopoki nie uda nam sie stworzyc fightera,
			System.out.print("Jakiego chcesz fightera(Kazuya/Jin/DevilJin): ");
			fighterName = in.nextLine();
			fighter = factory.getFighter(fighterName); //nie musimy robić warunków tutaj, cała logika jest przeniesiona do klasy factory 
		}
		
		if(fighter != null)
			System.out.println(fighter);
	}

}
