package abstract_factory;

public class Test {
	/* Wzorca Abstract Factory używamy gdy chcemy tworzy obiekty z pewnej rodziny bez precyzowania konkretnego typu, oraz
	 * gdy chcemy dodawać lub zmieniać obiekty dynamicznie w trakcie działania programu, dzięki wspólnym interfejsom(i abstrakcyjnym klasom).
	 * Tak więc wzorzec Abstract Factory jest tym co Factory za wyjątkiem tego, że wszystko jest hermetyzowane(encapsulated):
	 *  - fabryka fabryk - inaczej superfabryka tworząca fabryki tworzące obiekty
	 *  - metoda która zamawia obiekty
	 *  - fabryka która tworzy obiekty,
	 *  - końcowe obiekty, które używają strategy pattern
	 * Taki kod może być skomplikowany, ale daje nieporównywalną elastyczność. 
	 * Wyabstrahowane jest wiele poziomów:
	 * - istnieje główna abstrakcyjna fabryka która jest pośrednikiem między fabrykami które produkują obiekty mające wspólną rodzinę...
	 * 	..., a z kolei obiekty te używają fabryki która implementuje im składowe przez metodę make/build(tak więc nawet proces tworzenia tych obiektów... 
	 *       ...jest wyabstrachowany i jest zależny od przesłanej im fabryki składowych) co pozwala na dynamiczne tworzenia obiektów.
	 * - fabryki które tworzą składowe obiektów posiadają wspólny interfejs określający co fabryka musi spełniać(gdyby ktoś chciał zaimplementować swoją fabrykę)
	 * - obiekty składowe też są wyabstrachowane(użycie wzorca strategy) i dostarczane przez wytwórnie, co daje dynamikę którą można wykorzystywać w trakcie działania programu
	 * */
	
	/* Ship(abstract class)
	 * ShipEngine(interface)	| to będzie implementowane
	 * ShipWeapon(interface)	| za pomocą strategy pattern
	 * ShipJob(interface)		| i implementowane przez factory
	 * ShipBuilder(abstract class)
	 *   - UfoShipBuilder extends ShipBuilder
	 *      - UfoWorkerShip - extends Ship
	 *      - UfoFighterShip - extends Ship
	 *      ShipFactory(interface) (realizacja strategy pattern)
	 *        - UfoWorkerFactory implements ShipFactory  
	 *        - UfoFighterFactory implements ShipFactory
	 *      	  - UfoWorkerEngine
	 *      	  - UfoFighterEngine
	 *            - UfoWorkerWeapon
	 *            - UfoFighterWeapon
	 *            - UfoWorkerJob
	 *            - UfoFighterJob
	 *   - another building class
	 */
			
	public static void main(String[] args){
		ShipBuilder shipBuilder = new UfoShipBuilder();
		Ship ship1 = shipBuilder.createShip(UfoShipBuilder.UFO_WORKER);
		Ship ship2 = shipBuilder.createShip(UfoShipBuilder.UFO_FIGHTER);
		
		System.out.println(ship1 + "\n" + ship2);
	}
}
