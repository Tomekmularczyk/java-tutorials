package abstract_factory;

public class UfoFighterFactory implements ShipFactory{

	@Override
	public ShipEngine getEngine() {
		return new UfoFighterEngine();
	}

	@Override
	public ShipWeapon getWeapon() {
		return new UfoFighterWeapon();
	}

	@Override
	public ShipJob getJob() {
		return new UfoFighterJob();
	}

}
