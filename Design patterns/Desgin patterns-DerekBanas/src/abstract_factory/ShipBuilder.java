package abstract_factory;

public abstract class ShipBuilder {
	
	protected abstract Ship makeShip(String typeOfShip);
	
	public Ship createShip(String typeOfShip){
		Ship ship = makeShip(typeOfShip);
		ship.buildShip();
		
		return ship;
	}
}
