package abstract_factory;

public abstract class Ship {
	private String name;
	private ShipFactory factory;
	private ShipEngine engine;
	private ShipWeapon weapon;
	private ShipJob job;
	
	public Ship(String name, ShipFactory factory){
		this.name = name;
		this.factory = factory;
	}
	
	
	public ShipFactory getFactory() {
		return factory;
	}
	public void setFactory(ShipFactory factory) {
		this.factory = factory;
	}
	public String getName() {
		return name;
	}
	public void setEngine(ShipEngine engine) {
		this.engine = engine;
	}
	public void setWeapon(ShipWeapon weapon) {
		this.weapon = weapon;
	}
	public void setJob(ShipJob job) {
		this.job = job;
	}

	public abstract void buildShip(); //we will abstract building which might be different to ships
	
	@Override
	public String toString() {
		return getName() + "(" + engine.describeEngine() + ") my job is " +
				job.describeJob() + " and my weapon is " + weapon.describeWeapon();
	}
}
