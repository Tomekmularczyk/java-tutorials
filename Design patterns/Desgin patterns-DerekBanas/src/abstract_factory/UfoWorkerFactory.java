package abstract_factory;

public class UfoWorkerFactory implements ShipFactory{

	@Override
	public ShipEngine getEngine() {
		return new UfoWorkerEngine();
	}

	@Override
	public ShipWeapon getWeapon() {
		return new UfoWorkerWeapon();
	}

	@Override
	public ShipJob getJob() {
		return new UfoWorkerJob();
	}

}
