package abstract_factory;

public class UfoShipBuilder extends ShipBuilder{
	public static final String UFO_WORKER = "UFO_WORKER";
	public static final String UFO_FIGHTER = "UFO_FIGHTER";
	
	@Override
	protected Ship makeShip(String typeOfShip) {
		Ship ship = null;
		
		ShipFactory shipFactory = null;
		switch(typeOfShip){
		case UFO_WORKER:
			shipFactory = new UfoWorkerFactory();
			ship = new UfoWorkerShip("Ufo Worker",shipFactory);  //tworzymy obiekt i podajemy mu fabryke która go skonstruuje
			break;
	/*	case UFO_FAST_WORKER:
			shipFactory = new UfoFastWorkerFactory();  //realizacja strategy pattern, tworzymy ten sam obiekt, ale dynamicznie podmieniamy jego zachowanie
			ship = new UfoWorkerShip("Fast Ufo Worker", shipFactory);
			break; */
		case UFO_FIGHTER:
			shipFactory = new UfoFighterFactory();
			ship = new UfoFighterShip("Ufo Fighter",shipFactory);
			break;
		default: break;
		}
		return ship;
	}

}
