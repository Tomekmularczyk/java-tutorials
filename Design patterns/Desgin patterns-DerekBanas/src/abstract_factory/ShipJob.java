package abstract_factory;

public interface ShipJob {
	public String describeJob();
}
