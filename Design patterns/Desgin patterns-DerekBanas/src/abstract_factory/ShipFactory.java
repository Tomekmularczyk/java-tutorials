package abstract_factory;

//Ship realizuje strategy pattern poniewaz jego składowe(engine, job, weapon) mogą byc dynamicznie wymieniane,
//lecz cała logika jest wyabstrahowana do fabryk,
public interface ShipFactory {  //czyli co potrzebuje dostarczać obiekt by mógł być fabryką
	public ShipEngine getEngine();
	public ShipWeapon getWeapon();
	public ShipJob getJob();
}
