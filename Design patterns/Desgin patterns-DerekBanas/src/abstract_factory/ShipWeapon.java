package abstract_factory;

public interface ShipWeapon {
	public String describeWeapon();
}
