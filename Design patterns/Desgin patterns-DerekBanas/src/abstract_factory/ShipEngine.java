package abstract_factory;

public interface ShipEngine {
	public String describeEngine();
}
