package abstract_factory;

public class UfoFighterShip extends Ship{

	public UfoFighterShip(String name, ShipFactory factory) {
		super(name, factory);
	}

	@Override
	public void buildShip() {
		ShipFactory factory = getFactory();
		setEngine(factory.getEngine());
		setWeapon(factory.getWeapon());
		setJob(factory.getJob());
	}

}
