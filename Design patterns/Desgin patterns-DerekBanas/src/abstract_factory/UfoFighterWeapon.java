package abstract_factory;

public class UfoFighterWeapon implements ShipWeapon{

	@Override
	public String describeWeapon() {
		return "Two lazers";
	}

}
