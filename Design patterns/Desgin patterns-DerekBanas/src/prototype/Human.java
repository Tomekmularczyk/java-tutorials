package prototype;

public interface Human extends Cloneable{
	Human makeCopy();
}
