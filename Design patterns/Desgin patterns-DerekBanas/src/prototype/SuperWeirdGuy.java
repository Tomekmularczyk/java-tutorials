package prototype;

public class SuperWeirdGuy implements Human{

	@Override
	public SuperWeirdGuy makeCopy() {
		System.out.println("SuperWeirdGuy is being made");
		SuperWeirdGuy weirdGuy = null;
		
		try {
			weirdGuy = (SuperWeirdGuy) super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Sorry, couldn't make copy of this object");
			e.printStackTrace();
		}
		
		return weirdGuy;
	}

}
