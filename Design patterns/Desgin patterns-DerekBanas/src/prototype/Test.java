package prototype;

public class Test {
	/* Prototyp
	 * - pozwala na tworzenie nowych obiektów poprzez kopiowanie innych obiektów
	 * - przydaje sie wtedy kiedy mamy klasy które będą używane jeżeli będą potrzebne w trakcie działania programu
	 * - redukuje potrzebe tworzenia podklas
	 * 
	 * */
	
	public static void main(String[] args){
		Human weirdGuy = new SuperWeirdGuy();
		Human weirdGuyHimself = weirdGuy;
		Human weirdGuyFanboy = weirdGuy.makeCopy();
		
		System.out.println("weirdGuy: " + weirdGuy.hashCode());
		System.out.println("weirdGuyHimself: " + weirdGuyHimself.hashCode());
		System.out.println("weirdGuyFanboy: " + weirdGuyFanboy.hashCode());
	}
}
