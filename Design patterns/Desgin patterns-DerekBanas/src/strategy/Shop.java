package strategy;

public class Shop {
	public enum PaymentType { CASH, CREDIT_CARD, DEBIT_CARD };
	private String name;
	private Transaction paymentMethod;
	
	public Shop(String name){
		this.name = name;
	}
	
	public double sell(PaymentType paymentType, double amount){
		switch(paymentType){
			case CASH: paymentMethod = new CashSale();
				break; 
			case CREDIT_CARD: paymentMethod = new CreditCardSale();
				break;
			case DEBIT_CARD: paymentMethod = new DebitCardSale();
				break;
		}
		
		return paymentMethod.deal(amount);
	}
	
	public String toString(){
		return getClass().getSimpleName() + "(" + name + ")";
	}
}
