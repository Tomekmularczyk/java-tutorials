package strategy;

import strategy.Shop;

public class Test {
    /* Wzorcu projektowego Strategy używamy gdy: 
     * - potrzebujemy by by jakieś zachowanie zmieniało się dynamicznie, w zależności od sytuacji
     * - wskazówką jest gdy w kodzie zaczynamy używać zbyt dużo instrukcji warunkowych
     * Do tego celu tworzymy rodzinę algorytmów, którą są wymienne dla klasy
     * Zaletą jest redukcja powtarzającego się kodu, ukrycie implementacji przed użytkownikami i brak sprzężenia między klasami
     * Minusem może być zwiększona liczba obiektów/klas 
     */
	public static void main(String[] args) {
		Shop shop = new Shop("Biedronka");
		
		double koszt = 78.59; 
		System.out.println(shop);
		System.out.printf("Kosz zakupów wyniósł: %.2fpln\n", koszt);
		
		
		//w zależności od tego jaki typ zapłaty dokona użytkownik, obiekt sklep przyjmie określone działanie/zachowanie.
		//w tym przypadku do kwoty doliczany będzie vat, który będzie się różnił w zależności od metody płatności(zajmie sie tym odpowiednia strategia)
		System.out.printf("Po zapłaceniu gotówką, klient zapłacił %.2fpln\n", shop.sell(Shop.PaymentType.CASH, koszt));
		System.out.printf("Po zapłaceniu gotówką, klient zapłacił %.2fpln\n", shop.sell(Shop.PaymentType.DEBIT_CARD, koszt));
		System.out.printf("Po zapłaceniu gotówką, klient zapłacił %.2fpln\n", shop.sell(Shop.PaymentType.CREDIT_CARD, koszt));
	}
}
