package strategy;

public interface Transaction {
	public double deal(double amount);
}

class CashSale implements Transaction{
	@Override
	public double deal(double amount){
		int VAT = 0;
		return amount + (amount/100 * VAT);
	}
}
class CreditCardSale implements Transaction{
	@Override
	public double deal(double amount){
		int VAT = 13;
		return amount + (amount/100 * VAT);
	}
}
class DebitCardSale implements Transaction{
	@Override
	public double deal(double amount){
		int VAT = 23;
		return amount + (amount/100 * VAT);
	}
}