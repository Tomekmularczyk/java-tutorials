package iterator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StuffFromChurch implements StuffIterator{
	private Map<Integer, Thing> stuff;
	
	public StuffFromChurch(){
		stuff = new HashMap<>();
		
		int i = 0;
		stuff.put(i++, new Thing("Krzyż", 15.5f, 780.90));
		stuff.put(i++, new Thing("Patena", 4.5f, 17.99));
		stuff.put(i++, new Thing("Ołtarz", 2670.30f, 4070.0));
	}

	@Override
	public Iterator<Thing> getIterator() {
		return stuff.values().iterator();
	}
	
	public static void main(String[] args){
		
		int count =0;
		
		label:
		for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
				count++;
				
				if(count>2)
					break label;
			}
			
			count+=10;
		}
		
		System.out.println(16>>2); //podzielone przez 2 dwa razy
	}
	
}
