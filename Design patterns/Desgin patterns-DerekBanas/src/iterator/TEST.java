package iterator;

public class TEST {
	/* Wzorzec Iterator pozwala na jednolity sposób przemierzania różnych kolekcji np.
	 * Jeżeli mamy kolekcje ArrayList, HashTable, Array, TreeSet to możemy wyciągnąć z nich iterator i traktować je jednakowo.
	 * Dzięki takiemu zabiegowi możemy korzystać z polimorfizmu.
	 */
	

	public static void main(String[] args) {
		StoreHouse storeHouse = new StoreHouse(new StuffFromChurch(),
												new StuffFromLibrary(),
												new StuffFromSchool());
		
		storeHouse.listEverything();
	}

}
