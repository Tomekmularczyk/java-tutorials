package iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class StuffFromSchool implements StuffIterator{
	private List<Thing> stuff;
	
	public StuffFromSchool(){
		stuff = new LinkedList<>();
		
		stuff.add(new Thing("Tablica", 730f, 200.0));
		stuff.add(new Thing("Kreda", 5.5f, 10.50));
		stuff.add(new Thing("Zeszyt", 3.3f, 5.50));
	}

	@Override
	public Iterator<Thing> getIterator() {
		return stuff.iterator();
	}
	
}
