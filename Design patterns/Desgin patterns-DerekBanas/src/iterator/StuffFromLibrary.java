package iterator;

import java.util.Arrays;
import java.util.Iterator;

public class StuffFromLibrary implements StuffIterator{
	
	private Thing[] stuff;
	
	public StuffFromLibrary(){
		stuff = new Thing[3];
		stuff[0] = new Thing("Książka Symofonia C++", 20.0f, 32.0);
		stuff[1] = new Thing("Regał", 2000.0f, 1000.0);
		stuff[2] = new Thing("Stolik", 500.0f, 450.0);
	}

	@Override
	public Iterator<Thing> getIterator() {
		return Arrays.asList(stuff).iterator();
	}
	
	
}
