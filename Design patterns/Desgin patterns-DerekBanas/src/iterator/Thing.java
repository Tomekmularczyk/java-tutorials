package iterator;

public class Thing {
	private String name;
	private float size;
	private double cost;
	
	public Thing(String name, float size, double cost) {
		super();
		this.name = name;
		this.size = size;
		this.cost = cost;
	}

	public String getName() {
		return name;
	}
	public float getSize() {
		return size;
	}
	public double getCost() {
		return cost;
	}
	
	@Override
	public String toString(){
		return String.format("%s, size: %4.4f, cost: %4.2f pln", name, size, cost);
	}
}
