package iterator;

import java.util.Iterator;

public interface StuffIterator {
	public Iterator<Thing> getIterator();
}
