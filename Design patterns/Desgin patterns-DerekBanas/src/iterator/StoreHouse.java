package iterator;

import java.util.Iterator;

/*	Pomiżo, że każda klasa w inny sposób trzyma swoje elementy będziemy mogli w prosty sposób je wymienić, jeżeli
 *  będą implementować jeden interfejs(StuffIterator)
 * 
 */

public class StoreHouse {
	private StuffIterator[] totalStuff;
	
	public StoreHouse(StuffIterator... stuff){
		totalStuff = stuff;
	}
	
	public void listEverything(){
		for(StuffIterator stuff : totalStuff){
			Iterator<Thing> iterator = stuff.getIterator();
			
			while(iterator.hasNext()){
				Thing thing = iterator.next();
				System.out.println(thing);
			}
		}
			
	}
}
