package command;

public class TurnTvOn implements Command{
	private Television tv;
	
	public TurnTvOn(Television tv){
		this.tv = tv;
	}
	
	public void execute(){
		tv.on();
	}
	
	public void undo(){
		tv.off();
	}
}
