package command;

public class Test {
	/* Command jest to wzorzec gdzie obiekt jest wykorzystywany do enkapsulacji wszystkich informacji potrzebnych do wywołania metody.
	 * 		Te informacje obejmują nazwę metody, obiekt który posiada metodę i wartości podawane jako parametry
	 * Inaczej też:
	 * - pozwala ci przechowywać listę kodu który będzie wykonywany później lub wiele razy
	 * - przeważnie jest też możliwość by "cofnąć zmiany"
	 * 1. klient chce żeby została wywołana odpowiednia "Komenda(Command)" wtedy kiedy zostanie wywołana metoda execute() na którymś z tych ukrytych(zakapsułkowanych) obiektów 
	 * 2. obiekt nazywany invoker przesyła tę "Komende" do obiektu reciever by ten wykonał odpowiedni kod
	 * 1a. klient definiuje urządzenie oraz komende z 1 metodą execute()
	 * 1b. przycisk urządzenia(invoker) uruchamia komendę podaną od klienta
	 * Przycisk -> komenda -> urządzenie
	 * 
	 * Benefity tego wzorca:
	 * - pozwala stworzyć liste komend to późniejszego użytku 
	 * - w klasie możesz przechowywać wiele komend do późniejszego użytku
	 * - możesz zaimplementować komendy "undo"
	 * Minusem jest to że potrzeba stworzyć sporo małych klas, które przechowuja liste komend
	 */
	
	public static void main(String[] args) {
		Television tv = new Television();
		TurnTvOn turnTvOn = new TurnTvOn(tv);
		TurnTvOff turnTvOff = new TurnTvOff(tv);
		
		TvRemote remote = new TvRemote(turnTvOn);
		remote.press();
		remote.undo();
		
		remote = new TvRemote(turnTvOff);
		remote.press();
		remote.undo();
	}

}
