package command;

public class TurnTvOff implements Command{
	private Television tv;
	
	public TurnTvOff(Television tv){
		this.tv = tv;
	}
	
	public void execute(){
		tv.off();
	}
	
	public void undo(){
		tv.on();
	}
}
