package command;

public class TvRemote {
	private Command command;
	
	public TvRemote(Command command){
		this.command = command;
	}
	
	public void press(){
		command.execute();
	}
	
	public void undo(){
		command.undo();
	}
}
