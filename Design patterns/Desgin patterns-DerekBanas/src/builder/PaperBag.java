package builder;

public class PaperBag implements Packing{

	@Override
	public String packing() {
		return "Paper bag";
	}

}
