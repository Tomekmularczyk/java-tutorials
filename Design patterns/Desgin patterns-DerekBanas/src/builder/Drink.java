package builder;

public abstract class Drink implements Item{
	
	@Override
	public Packing packing(){
		return new Bottle();
	}

	@Override
	public String toString(){
		return name() + "(" + packing().packing() + "): " + getPrice();
	}
}
