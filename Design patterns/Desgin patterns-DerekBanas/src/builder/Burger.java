package builder;

public abstract class Burger implements Item{
	public Packing packing(){
		return new Box();
	}
	
	@Override
	public String toString(){
		return name() + "(" + packing().packing() + "): " + getPrice();
	}
}
