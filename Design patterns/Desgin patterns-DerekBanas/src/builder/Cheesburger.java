package builder;

public class Cheesburger extends Burger{

	@Override
	public String name() {
		return "Cheesburger";
	}

	@Override
	public double getPrice() {
		return 3.00;
	}
	
}
