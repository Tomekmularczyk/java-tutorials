package builder;

public class BigMac extends Burger{
	@Override
	public String name() {
		return "Big Mac";
	}

	@Override
	public double getPrice() {
		return 9.05;
	}
}
