package builder;

public class Sprite extends Drink{

	@Override
	public String name() {
		return "Sprite";
	}

	@Override
	public double getPrice() {
		return 5.45;
	}

}
