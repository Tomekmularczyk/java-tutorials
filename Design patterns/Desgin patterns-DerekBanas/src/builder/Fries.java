package builder;

public abstract class Fries implements Item{
	@Override
	public Packing packing(){
		return new PaperBag();
	}
	
	@Override
	public String toString(){
		return name() + "(" + packing().packing() + "): " + getPrice();
	}
}
