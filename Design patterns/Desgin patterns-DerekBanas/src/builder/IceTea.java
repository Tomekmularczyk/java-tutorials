package builder;

public class IceTea extends Drink{
	@Override
	public String name() {
		return "IceTea";
	}

	@Override
	public double getPrice() {
		return 5.15;
	}
	

}
