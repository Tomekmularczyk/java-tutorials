package builder;

public class ChickenBurger extends Burger{
	@Override
	public String name() {
		return "Chicken burger";
	}

	@Override
	public double getPrice() {
		return 8.79;
	}

}
