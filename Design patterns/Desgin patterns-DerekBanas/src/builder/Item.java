package builder;

public interface Item {
	public String name();
	public double getPrice();
	public Packing packing();
}
