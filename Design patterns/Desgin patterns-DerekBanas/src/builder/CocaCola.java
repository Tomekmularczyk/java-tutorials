package builder;

public class CocaCola extends Drink{

	@Override
	public String name() {
		return "Coca Cola";
	}

	@Override
	public double getPrice() {
		return 5.50;
	}
	
}
