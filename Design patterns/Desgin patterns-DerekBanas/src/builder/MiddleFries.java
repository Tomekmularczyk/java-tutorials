package builder;

public class MiddleFries extends Fries{

	@Override
	public String name() {
		return "Middle size fries";
	}

	@Override
	public double getPrice() {
		return 3.99;
	}
	
}
