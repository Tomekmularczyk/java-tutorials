package builder;

public class MealBuilder {
	
	public Meal getHappyMeal(){
		Meal meal = new Meal("HappyMeal");
		
		meal.addItem(new MiddleFries());
		meal.addItem(new Cheesburger());
		meal.addItem(new IceTea());
		
		return meal;
	}
	
	public Meal getLargeMeal(){
		Meal meal = new Meal("Large size meal");
		
		meal.addItem(new BigFries());
		meal.addItem(new BigMac());
		meal.addItem(new CocaCola());
		
		return meal;
	}
	
	public Meal getMiniMeal(){
		Meal meal = new Meal("Mini size meal");
		
		meal.addItem(new MiddleFries());
		meal.addItem(new ChickenBurger());
		meal.addItem(new Sprite());
		
		return meal;
	}
}
