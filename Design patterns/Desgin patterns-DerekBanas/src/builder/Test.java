package builder;

public class Test {
	/* Builder Pattern używamy 
	 * - gdy chcemy stworzyć obiekt składający się z innych obiektów,
	 * - kiedy chcemy by te części były niezależne od tego głównego obiektu
	 * - kiedy chcemy ukryć tworzenie tych części przed klientem(builder zna szczególy i nikt inny)
	 */
	public static void main(String[] args) {
		MealBuilder builder = new MealBuilder();
		Meal[] meals = { builder.getMiniMeal(), 
						 builder.getHappyMeal(), 
						 builder.getLargeMeal() };
		
		for(Meal meal : meals){
			System.out.println("\n" + meal.getName() + "("+ meal.getCost() +")");
			for(Item item : meal.getItems())
				System.out.println("\t" + item);
		}
	}

}
