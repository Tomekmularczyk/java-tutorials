package builder;

public class BigFries extends Fries{

	@Override
	public String name() {
		return "Big size fries";
	}

	@Override
	public double getPrice() {
		return 4.45;
	}
	
}
