package builder;

import java.util.LinkedList;
import java.util.List;

public class Meal {
	private List<Item> items = new LinkedList<>();
	private String name;
	
	public Meal(String name){
		this.name = name;
	}
	
	public void addItem(Item item){
		items.add(item);
	}
	
	public double getCost(){
		double cost = 0;
		for(Item item : items)
			cost += item.getPrice();
		
		return cost;
	}
	
	public List<Item> getItems(){
		return items;
	}
	
	public String getName(){
		return name;
	}
}
