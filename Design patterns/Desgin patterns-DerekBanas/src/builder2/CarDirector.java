package builder2;

public class CarDirector {
	private CarBuilder carBuilder;
	
	public CarDirector(CarBuilder builder){
		this.carBuilder = builder;
	}
	
	
	public void makeCar(){
		carBuilder.buildEngine();
		carBuilder.buildWheels();
		carBuilder.buildElectronics();
		carBuilder.buildSteeringWheel();
	}
	
	public Car getCar(){
		return carBuilder.getCar();
	}
}
