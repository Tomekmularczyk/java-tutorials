package builder2;

public interface CarBuilder {
	public void buildEngine();
	public void buildSteeringWheel();
	public void buildWheels();
	public void buildElectronics();
	public Car getCar();
}
