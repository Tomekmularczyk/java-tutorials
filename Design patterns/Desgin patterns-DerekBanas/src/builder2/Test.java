package builder2;

public class Test {
/* Derek Way
 * 
 **/
	public static void main(String[] args){
		CarBuilder carBuilder = new MatizBuilder();
		CarDirector director = new CarDirector(carBuilder);
		
		director.makeCar();
		Car car1 = director.getCar();
		
		carBuilder = new PolonezBuilder();
		director = new CarDirector(carBuilder);
		
		director.makeCar();
		Car car2 = director.getCar();
		
		System.out.println(car1 + "\n" + car2);
	}
}
