package builder2;

public class MatizBuilder implements CarBuilder{
	private Car car;
	
	public MatizBuilder(){
		this.car = new Matiz();
	}
	
	@Override
	public void buildEngine(){
		car.setEngine("V12");
	}
	@Override
	public void buildSteeringWheel(){
		car.setSteeringWheel("Sport steering wheel");
	}
	@Override
	public void buildWheels(){
		car.setWheels("4 platinum wheels");
	}
	@Override
	public void buildElectronics(){
		car.setElectronics("Dolby Digital");
	}
	@Override
	public Car getCar(){
		return car;
	}
}
