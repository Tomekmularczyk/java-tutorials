package builder2;

public class PolonezBuilder implements CarBuilder{
	private Car car;
	
	public PolonezBuilder(){
		this.car = new Polonez();
	}
	
	@Override
	public void buildEngine(){
		car.setEngine("V8");
	}
	@Override
	public void buildSteeringWheel(){
		car.setSteeringWheel("Casual steering wheel");
	}
	@Override
	public void buildWheels(){
		car.setWheels("3 chromed wheels");
	}
	@Override
	public void buildElectronics(){
		car.setElectronics("2.1 subwoofer");
	}
	@Override
	public Car getCar(){
		return car;
	}
}
