package builder2;

public abstract class Car {
	private String name = getClass().getSimpleName();
	private String engine;
	private String wheels;
	private String steeringWheel;
	private String electronics;
	
	
	public void setEngine(String engine) {
		this.engine=engine;
	}
	public void setWheels(String wheels) {
		this.wheels=wheels;
	}
	public void setSteeringWheel(String steeringWheel) {
		this.steeringWheel = steeringWheel;
	}
	public void setElectronics(String electronics) {
		this.electronics=electronics;
	}

	public String getEngine() {
		return engine;
	}

	public String getWheels() {
		return wheels;
	}

	public String getSteeringWheel() {
		return steeringWheel;
	}

	public String getElectronics() {
		return electronics;
	}
	
	@Override
	public String toString(){
		return name + ", engine: " + engine +
					  ", wheels: " + wheels +
					  ", steering wheel: " + steeringWheel +
					  ", electronics: " + electronics;
	}
}
