package singleton;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* Klasyczna implementacja Singleton Pattern */
public class DeckOfCards implements Deck{
	private static DeckOfCards singletonObject = null;  //to bedzie null dopoki ktos nie wywola getInstance. Statyczne bo ma być globalne
	private List<Card> cards;
	
	private DeckOfCards(){  //no one can create this object than us
		cards = new LinkedList<>(Arrays.asList(Card.values())); //musimy skopiowac elemnty inaczej dostalibysmy opakowaną w liste tablice, w której nie można nic usuwac ani dodawać
	}

	@Override
	public void shuffle() {
		Collections.shuffle(cards);
	}

	@Override
	public int nrOfCards() {
		return cards.size();
	}

	@Override
	public Card[] getCards(int howMany) {
		List<Card> takeAway = null;
		
		if(cards.size() <= howMany){
			takeAway = new LinkedList<>(cards); //tworzymy kopie, nie chcemy przesłać "widoku"
			cards.clear();
		}else{
			takeAway = new LinkedList<>(cards.subList(0, howMany));
			cards.removeAll(takeAway);  
			//można też: cards.subList(0, howMany).clear();
		}
		 
		return takeAway.toArray(new Card[takeAway.size()]);
	}
	
	public static DeckOfCards getInstance(){  
		if(singletonObject == null){   //pierwsze wywołanie bo wciąż null. Lazy instantiation
			singletonObject = new DeckOfCards();
		}
		
		/* Poniże zabezpieczenie dla wielowątkowści, ponieważ gdy zanim jeden wątek zdąży wygenerować obiekt dla singletonObject, 
		 * to może nadejść inny wątek który sprawdzi że obiekt nadal nie jest wygenerowany i przejdzie do tworzenia obiektu, pomimo, że już inny obiekt go zaczął tworzyć.
		 * Moglibyśmy zadeklarować całą metodę jako synchronized, ale to mogłoby drastycznie zwalniać działanie programu,
		 * dlatego zsynchronizujemy tylko tę częśc kodu która wykonywać się będzie tylko jeden raz przy tworzeniu obiektu
		if(singletonObject == null){
			synchronized(DeckOfCards.class){ //tutaj zatrzymają się wszystkie wątki
				if(singletonObject == null){ //ponownie sprawdzenie wrazie gdyby okazało się że w trakcie przechodzenia przez "bramkę" ktoś już tworzył obiekt
					singletonObject = new DeckOfCards();
				}
			}
		}
		*/
		
		return singletonObject;
	}
	
	
	@Override
	public String toString(){
		return nrOfCards() + " karty w talii";
	}
}
