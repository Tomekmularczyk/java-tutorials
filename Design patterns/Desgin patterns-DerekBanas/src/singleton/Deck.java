package singleton;

public interface Deck {
	void shuffle();
	int nrOfCards();
	Card[] getCards(int howMany);
}
