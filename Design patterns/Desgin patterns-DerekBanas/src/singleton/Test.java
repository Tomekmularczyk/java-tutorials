package singleton;

import java.util.Arrays;

public class Test {
	/* Wzorca Singleton używamy kiedy chcemy upewnić się, że powstanie tylko jeden obiekt danej klasy.
	 * - minusem może być to że trudno wykonywać testy jednostkowe z użyciem takich obiektów i lepsze jest dependency injection(tak wyczytałem na stackoverflow)
	 * - jeżeli nie zajmiemy się poprawne wielowątkowścią, to możliwe będzie utworzenie więcej niz jednego obiektu
	 * w pakiecie wykorzystamy klasyczną implementacje za pomocą klasy, oraz rekomendowaną za pomocą enum
	 * */
	public static void main(String[] args){
		Deck deck = DeckOfCards.getInstance();
		deck.shuffle();
		deck = DeckOfCards.getInstance();//specjalnie probujemy tworzyc nowe obiekty, żeby wykazać że się nie da i cały czas bedziemy pracować z tym samym
		
		System.out.println(deck);
		System.out.println("Player 1 wylosował 5 kart: " + Arrays.toString(deck.getCards(5)));
		
		deck = DeckOfCards.getInstance();
		System.out.println(deck);
		
		System.out.println("Player 2 wylosował 3 karty: " + Arrays.toString(deck.getCards(3)));
		
		deck = DeckOfCards.getInstance();
		System.out.println(deck);
		
		
		
		System.out.println("======= singleton z wykorzystaniem klasy ENUM ============");
		
		deck = EnumDeckOfCards.INSTANCE;
		deck.shuffle();
		deck = EnumDeckOfCards.INSTANCE;//specjalnie probujemy tworzyc nowe obiekty, żeby wykazać że się nie da i cały czas bedziemy pracować z tym samym
		
		System.out.println(deck);
		System.out.println("Player 1 wylosował 5 kart: " + Arrays.toString(deck.getCards(5)));
		
		deck = EnumDeckOfCards.INSTANCE;
		System.out.println(deck);
		
		System.out.println("Player 2 wylosował 3 karty: " + Arrays.toString(deck.getCards(3)));
		
		deck = EnumDeckOfCards.INSTANCE;
		System.out.println(deck);
	}
}
