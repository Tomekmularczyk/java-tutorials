package singleton;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* Rekomendowany przez Joshua Blocha sposób na singleton pattern(Enum to to samo co klasa tylko z pewnymi właściwościami) */
public enum EnumDeckOfCards implements Deck{
	INSTANCE;
	
	private List<Card> cards;
	
	private EnumDeckOfCards(){
		cards = new LinkedList<>(Arrays.asList(Card.values())); //defensive copying
	}
	
	@Override
	public void shuffle() {
		Collections.shuffle(cards);
	}

	@Override
	public int nrOfCards() {
		return cards.size();
	}

	@Override
	public Card[] getCards(int howMany) {
		List<Card> takeAway = null;
		
		if(cards.size() <= howMany){
			takeAway = new LinkedList<>(cards);  //tworzymy kopie, nie chcemy przesłać "widoku"
			cards.clear();
		}else{
			takeAway = new LinkedList<>(cards.subList(0, howMany));
			cards.removeAll(takeAway);
		}
		
		return takeAway.toArray(new Card[takeAway.size()]);
	}
	
	@Override
	public String toString(){
		return nrOfCards() + " karty w talii";
	}
}
