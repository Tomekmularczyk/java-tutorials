package decorator;

public interface Cloth {
	public String getDescription();
	public double getPrice();
}
