package decorator;

public class WhiteTankTop extends MannequinDecorator{

	public WhiteTankTop(Cloth cloth) {
		super(cloth);
		System.out.println("adding white tank-top");
	}

	@Override
	public String getDescription() {
		return cloth.getDescription() + ", white tank-top";
	}

	@Override
	public double getPrice() {
		return cloth.getPrice() + 55.90;
	}

}
