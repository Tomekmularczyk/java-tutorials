package decorator;

public class Mannequin implements Cloth{

	@Override
	public String getDescription() {
		return "Mannequin";
	}

	@Override
	public double getPrice() {
		return 0.00;
	}

}
