package decorator;

public class BaseballHat extends MannequinDecorator{

	public BaseballHat(Cloth cloth) {
		super(cloth);
		System.out.println("adding basseball hat");
	}

	@Override
	public String getDescription() {
		return cloth.getDescription() + ", baseball hat";
	}

	@Override
	public double getPrice() {
		return cloth.getPrice() + 25.80;
	}

}
