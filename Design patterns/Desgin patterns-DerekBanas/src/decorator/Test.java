package decorator;

public class Test {
	/* Dekoratora używamy wtedy kiedy potrzebujemy możliwości dziedziczenia ale w trakcie działania programu, 
	 * czyli modyfikowania obiektu dynamicznie. Działa to tak, że mamy podstawowy obiekt(szkielet), który następnie
	 * za pomocą listy dekoratorów może zostać zmodyfikowany dla potrzeb programu.
	 * Tak więc nie trzeba modyfikować starego kodu, a wystarczy rozszerzyć liste dekoratorów.
	 */
	
	public static void main(String[] args) {
		Cloth cloth = new RedBoots(new BlueShortJeens(new WhiteTankTop(new BaseballHat(new Mannequin()))));
		
		System.out.println("Description: " + cloth.getDescription());
		System.out.println("Price: " + String.format("%.2f", cloth.getPrice()));
	}

}
