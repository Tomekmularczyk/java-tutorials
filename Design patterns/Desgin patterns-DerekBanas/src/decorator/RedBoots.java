package decorator;

public class RedBoots extends MannequinDecorator{

	public RedBoots(Cloth cloth) {
		super(cloth);
		System.out.println("adding red boots");
	}

	@Override
	public String getDescription() {
		return cloth.getDescription() + ", Red Boots";
	}

	@Override
	public double getPrice() {
		return cloth.getPrice() + 259.39;
	}

}
