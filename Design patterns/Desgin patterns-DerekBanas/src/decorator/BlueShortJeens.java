package decorator;

public class BlueShortJeens extends MannequinDecorator{

	public BlueShortJeens(Cloth cloth) {
		super(cloth);
		System.out.println("adding blue short jeens");
	}

	@Override
	public String getDescription() {
		return cloth.getDescription() + ", blue short jeens";
	}

	@Override
	public double getPrice() {
		return cloth.getPrice() + 199.50;
	}

}
