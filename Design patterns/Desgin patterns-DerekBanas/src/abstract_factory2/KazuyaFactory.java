package abstract_factory2;

public class KazuyaFactory implements TekkenCharacterFactory{

	@Override
	public UniqueMoves getUniqueMoves() {
		return new KazuyaUniqueMoves();
	}

	@Override
	public SpecialAttack getSpecialAttack() {
		return new KazuyaSpecialAttack();
	}

	@Override
	public FamilyMoves getFamilyMoves() {
		return new MishimaFamilyMoves();
	}

}
