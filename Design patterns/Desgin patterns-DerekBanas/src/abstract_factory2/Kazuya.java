package abstract_factory2;

public class Kazuya extends TekkenCharacter{

	public Kazuya(TekkenCharacterFactory factory){
		super("Kazuya Mishima",factory);
	}
	@Override
	public void buildCharacter() {
		TekkenCharacterFactory factory = getFactory();
		setFamilyMoves(factory.getFamilyMoves());
		setSpecialAttack(factory.getSpecialAttack());
		setUniqueMoves(factory.getUniqueMoves());
	}

}
