package abstract_factory2;

public class JinFactory implements TekkenCharacterFactory{

	@Override
	public UniqueMoves getUniqueMoves() {
		return new JinUniqueMoves();
	}

	@Override
	public SpecialAttack getSpecialAttack() {
		return new JinSpecialAttack();
	}

	@Override
	public FamilyMoves getFamilyMoves() {
		return new KazamaFamilyMoves();
	}

}
