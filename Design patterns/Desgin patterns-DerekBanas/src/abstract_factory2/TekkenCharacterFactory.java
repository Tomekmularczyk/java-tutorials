package abstract_factory2;

public interface TekkenCharacterFactory {
	public UniqueMoves getUniqueMoves();
	public SpecialAttack getSpecialAttack();
	public FamilyMoves getFamilyMoves();
}
