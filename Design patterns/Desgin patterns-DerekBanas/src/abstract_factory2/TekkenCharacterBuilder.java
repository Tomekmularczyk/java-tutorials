package abstract_factory2;

public abstract class TekkenCharacterBuilder {
	
	protected abstract TekkenCharacter makeCharacter(String characterName);
	
	public TekkenCharacter createCharacter(String characterName){
		TekkenCharacter character = makeCharacter(characterName);
		character.buildCharacter();
		
		return character;
	}
}
