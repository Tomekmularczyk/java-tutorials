package abstract_factory2;

public class AsukaFactory implements TekkenCharacterFactory{

	@Override
	public UniqueMoves getUniqueMoves() {
		return new AsukaUniqueMoves();
	}

	@Override
	public SpecialAttack getSpecialAttack() {
		return new AsukaSpecialAttack();
	}

	@Override
	public FamilyMoves getFamilyMoves() {
		return new KazamaFamilyMoves();
	}

}
