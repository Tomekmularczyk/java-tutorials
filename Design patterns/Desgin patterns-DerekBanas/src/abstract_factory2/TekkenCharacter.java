package abstract_factory2;

public abstract class TekkenCharacter {
	private String name;
	private TekkenCharacterFactory factory;
	private UniqueMoves uniqueMoves;
	private FamilyMoves familyMoves;
	private SpecialAttack specialAttack;

	public TekkenCharacter(String characterName, TekkenCharacterFactory factory){
		this.factory = factory;
		this.name = characterName;
	}
	
	public abstract void buildCharacter();
	
	
	public String getName() {
		return name;
	}

	public void setFactory(TekkenCharacterFactory factory) {
		this.factory = factory;
	}
	public TekkenCharacterFactory getFactory() {
		return factory;
	}

	public void setUniqueMoves(UniqueMoves uniqueMoves) {
		this.uniqueMoves = uniqueMoves;
	}


	public void setFamilyMoves(FamilyMoves familyMoves) {
		this.familyMoves = familyMoves;
	}


	public void setSpecialAttack(SpecialAttack specialAttack) {
		this.specialAttack = specialAttack;
	}

	@Override
	public String toString(){
		return "My name is " + name + ".\n\tMy family moves are: " + familyMoves +
				",\n\tmy Unique moves are: " + uniqueMoves + ", \n\tand my special attack is: " + specialAttack;
	}
}
