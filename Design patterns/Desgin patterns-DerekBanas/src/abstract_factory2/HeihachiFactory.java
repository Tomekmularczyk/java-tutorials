package abstract_factory2;

public class HeihachiFactory implements TekkenCharacterFactory{

	@Override
	public UniqueMoves getUniqueMoves() {
		return new HeihachiUniqueMoves();
	}

	@Override
	public SpecialAttack getSpecialAttack() {
		return new HeihachiSpecialAttack();
	}

	@Override
	public FamilyMoves getFamilyMoves() {
		return new MishimaFamilyMoves();
	}

}
