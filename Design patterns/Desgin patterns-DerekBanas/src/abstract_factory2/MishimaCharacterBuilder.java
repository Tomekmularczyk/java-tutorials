package abstract_factory2;

public class MishimaCharacterBuilder extends TekkenCharacterBuilder{
	public static final String KAZUYA = "KAZUYA";
	public static final String HEIHACHI = "HEIHACHI";
	
	@Override
	protected TekkenCharacter makeCharacter(String characterName) {
		TekkenCharacter character = null;
		TekkenCharacterFactory factory = null;
		
		switch(characterName){
		case KAZUYA:
			factory = new KazuyaFactory();
			character = new Kazuya(factory);
			break;
		case HEIHACHI:
			factory = new HeihachiFactory();
			character = new Heihachi(factory);
			break;
		default: break;
		}
		
		return character;
	}

}
