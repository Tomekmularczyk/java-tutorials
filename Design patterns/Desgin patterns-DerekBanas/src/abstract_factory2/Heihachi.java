package abstract_factory2;

public class Heihachi extends TekkenCharacter{

	public Heihachi(TekkenCharacterFactory factory){
		super("Heihachi Mishima", factory);
	}
	
	@Override
	public void buildCharacter() {
		TekkenCharacterFactory factory = getFactory();
		setFamilyMoves(factory.getFamilyMoves());
		setSpecialAttack(factory.getSpecialAttack());
		setUniqueMoves(factory.getUniqueMoves());
	}

}
