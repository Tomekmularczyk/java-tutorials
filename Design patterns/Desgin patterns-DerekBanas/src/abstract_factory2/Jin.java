package abstract_factory2;

public class Jin extends TekkenCharacter{

	public Jin(TekkenCharacterFactory factory){
		super("Jin Kazama",factory);
	}
	
	@Override
	public void buildCharacter() {
		TekkenCharacterFactory factory = getFactory();
		setFamilyMoves(factory.getFamilyMoves());
		setSpecialAttack(factory.getSpecialAttack());
		setUniqueMoves(factory.getUniqueMoves());
	}

}
