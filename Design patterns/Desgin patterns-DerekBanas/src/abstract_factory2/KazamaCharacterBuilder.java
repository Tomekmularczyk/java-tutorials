package abstract_factory2;

public class KazamaCharacterBuilder extends TekkenCharacterBuilder{
	public static final String JIN = "JIN";
	public static final String ASUKA = "ASUKA";
	
	@Override
	protected TekkenCharacter makeCharacter(String characterName) {
		TekkenCharacter character = null;
		TekkenCharacterFactory factory = null;
		
		switch(characterName){
		case JIN: 
			factory = new JinFactory();
			character = new Jin(factory);
			break;
		case ASUKA:
			factory = new AsukaFactory();
			character = new Asuka(factory);
			break;
		default: break;
		}
		
		return character;
	}

}
