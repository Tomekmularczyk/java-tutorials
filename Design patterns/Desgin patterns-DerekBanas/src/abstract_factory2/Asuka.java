package abstract_factory2;

public class Asuka extends TekkenCharacter{

	public Asuka(TekkenCharacterFactory factory){
		super("Asuka Kazama", factory);
	}
	
	@Override
	public void buildCharacter() {
		TekkenCharacterFactory factory = getFactory();
		
		setFamilyMoves(factory.getFamilyMoves());
		setSpecialAttack(factory.getSpecialAttack());
		setUniqueMoves(factory.getUniqueMoves());
	}

}
