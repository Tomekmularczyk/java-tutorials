package abstract_factory2;

import java.util.LinkedList;
import java.util.List;

public class Test {
	
	
	public static void main(String[] args){
		List<TekkenCharacter> characters = new LinkedList<>();
		TekkenCharacterBuilder characterBuilder;
		TekkenCharacter character;
		
		
		characterBuilder = new KazamaCharacterBuilder();
		character = characterBuilder.createCharacter(KazamaCharacterBuilder.JIN);
		characters.add(character);
		character = characterBuilder.createCharacter(KazamaCharacterBuilder.ASUKA);
		characters.add(character);
		
		characterBuilder = new MishimaCharacterBuilder();
		character = characterBuilder.createCharacter(MishimaCharacterBuilder.HEIHACHI);
		characters.add(character);
		character = characterBuilder.createCharacter(MishimaCharacterBuilder.KAZUYA);
		characters.add(character);
		
		
		for(TekkenCharacter tekkenCharacter : characters){
			System.out.println(tekkenCharacter);
		}
	}
}
