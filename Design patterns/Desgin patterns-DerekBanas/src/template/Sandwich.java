package template;

public abstract class Sandwich {
	
	final void makeSandwich(){ //--finalna, nie chcemy by ktokolwiek zmeiniał szkielet algorytmu
		cutTheBread();
		addMeat();
		addCheese();
		addVegetables();
		addKetchup();
		rollInPaper();
	}
	
	
	void cutTheBread(){
		System.out.println("Kroję chleb. ");
	}
	abstract void addMeat();
	abstract void addCheese();
	abstract void addVegetables();
	abstract void addKetchup();
	void rollInPaper(){
		System.out.println("Zawijam kanapkę w papierek");
	}
	
	
	
}
