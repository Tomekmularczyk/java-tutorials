package template;

public class TEST {

	public static void main(String[] args) {
		Sandwich meatSandwich = new MeatSandwich();
		meatSandwich.makeSandwich();
		
		System.out.println();
		
		Sandwich vegetarianSandwich = new VegetarianSandwich();
		vegetarianSandwich.makeSandwich();
	}

}
