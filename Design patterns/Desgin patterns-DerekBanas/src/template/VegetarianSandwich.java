package template;

public class VegetarianSandwich extends Sandwich{

	@Override
	void addMeat() {
		//no meat
	}

	@Override
	void addCheese() {
		System.out.println("Dodaje plasterek sera podlaskiego.");
	}

	@Override
	void addVegetables() {
		System.out.println("Dodaje plasterek pomidora, ogórka i rzodkiewki.");
	}

	@Override
	void addKetchup() {
		System.out.println("Dodaje ketchup");
	}

}
