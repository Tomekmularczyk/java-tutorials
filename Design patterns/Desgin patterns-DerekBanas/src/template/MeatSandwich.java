package template;

public class MeatSandwich extends Sandwich{

	@Override
	void addMeat() {
		System.out.println("Dodaję szynkę wieprzową oraz plasterek kiełbasy krakowskiej.");
	}

	@Override
	void addCheese() {
		System.out.println("Dodaje dwa plasterki goudy.");
	}

	@Override
	void addVegetables() {
		System.out.println("Dodaje pomidora i ogórek.");
	}

	@Override
	void addKetchup() {
		System.out.println("Dodaje ketchup");
	}
}
