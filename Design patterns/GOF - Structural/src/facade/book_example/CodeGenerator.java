package facade.book_example;

/**
 * Klasa CodeGenerator pełni rolę odwiedzającego(Visitor). Ma podklasy np. StackMachineCodeGenerator, RISCCodeGenerator
 * generujace kod maszynowy dla róznych maszyn.
 */
public abstract class CodeGenerator {
    private final BytecodeStream output;
    
    protected CodeGenerator(BytecodeStream byteCodeStream) {
        this.output = byteCodeStream;
    }

    public abstract void visit(StatementNode node);

    public abstract void visit(ExpressionNode node);

}
