package facade.book_example;

/**
 * Służy do tworzenia drzewa składni
 */
public abstract class ProgramNodeBuilder {

    private ProgramNode node;

    public abstract ProgramNode newVariable(String variableName);

    public abstract ProgramNode newAssigment(ProgramNode variable, ProgramNode expression);

    public abstract ProgramNode newReturnStatement(ProgramNode value);

    public abstract ProgramNode newCondition(ProgramNode condition, ProgramNode truePart, ProgramNode falseParte);

    public abstract ProgramNode getRootNode();
}
