package facade.book_example;

import java.awt.Point;
import java.util.Iterator;

public class ExpressionNode extends ProgramNode{

    @Override
    public Point getSourcePosition() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void add(ProgramNode element) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void remove(ProgramNode element) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    /**
     * Obiekty rekurencyjnie wywołują operację traverse
     */
    @Override
    public void traverse(CodeGenerator codeGenerator) {
        codeGenerator.visit(this);
        Iterable<ProgramNode> list = null;
        
        for (Iterator<ProgramNode> iterator = list.iterator(); iterator.hasNext();) {
            ProgramNode next = iterator.next();
            next.traverse(codeGenerator);
        }
    }

}
