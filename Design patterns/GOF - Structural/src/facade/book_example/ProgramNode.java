package facade.book_example;

import java.awt.Point;

/**
 * Klasa ta definiuje interfejs do manipulowania elementem ProgramNode i jego elementami podrzędnymi.
 */
public abstract class ProgramNode {

    //manipulowanie obiektem ProgramNode
    public abstract Point getSourcePosition();

    //manipulowanie elementami podrzędnymi jeżeli takie istnieją (np. ExpressionNode, StatementNode)
    public abstract void add(ProgramNode element);

    public abstract void remove(ProgramNode element);

    //podklasy będą wykorzystywać CodeGenerator do tworzenia kodu maszynowego w postaci ByteCode
    public abstract void traverse(CodeGenerator codeGenrator);
}
