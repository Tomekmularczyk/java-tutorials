package facade.book_example;

/**
 * Fasa która łączy opisane do tej pory elementy składające się na system kompilujący. 
 * Udostępnia ona prosty interfejs do kompilowania kodu źródłowego i generowania kodu dla określonej maszyny
 * 
 * Klasa ta mogłaby być abstrakcyjna jeżeli chcielibyśmy mieć różnego rodzaju fasady
 */
public class Compiler {
    
    public void compile(int istream, BytecodeStream output){
        Scanner scanner = new Scanner();
        ProgramNodeBuilder builder = null;
        Parser parser = null;
        
        parser.parse(scanner, builder);
        
        RISCCodeGenerator generator = new RISCCodeGenerator(output);
        ProgramNode parseTree = builder.getRootNode();
        parseTree.traverse(generator);
    }
    
}
