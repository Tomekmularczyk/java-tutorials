package facade.book_example;

/**
 * Fasada tworzy niewidzialną dlas klas podsystemu powłokę dzieląc ją na warstwe nisko i wysoko-poziomową co ułatwia korzystanie z systemu.
 * Zna klasy podsystemu odpowiadające za obsługę żądań i deleguje do nich żądania od klienta.
 * Ujmuje swoim interfejsem najczęsciej stosowane operacje wystarczające dla większości klientów.
 * Udostępniamy klientowi uproszczony interfejs do korzystania z systemu, jednocześnie nie odbierając mu dostępu do bardziej szczegółowych operacji.
 */
public class Test {

}
