package composite.myexample1;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Composite
 */
public class Ancestor extends FamilyTree {

    private final List<FamilyTree> ancestors; //przodek może mieć potomków

    public Ancestor(String name, int year) {
        super(name, year);
        this.ancestors = new LinkedList<>();
    }

    @Override
    public void add(FamilyTree ancestor) {
        ancestors.add(ancestor);
    }

    @Override
    public int countMembersTotalLivingTime() {
        return ancestors.stream()
                .mapToInt(FamilyTree::countMembersTotalLivingTime)
                .sum()
                + getAge();
    }

    @Override
    public double countMembersAverageLifeTime() {
        //TODO poprawić działanie algorytmu
        return ancestors.stream()
                .mapToDouble(FamilyTree::countMembersAverageLifeTime)
                .average()
                .orElse(-1);
    }

    @Override
    public int countMembers() {
        return ancestors.stream().collect(Collectors.summingInt(FamilyTree::countMembers)) + 1;
    }

}
