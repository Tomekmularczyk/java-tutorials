package composite.myexample1;

/**
 * Leaf. Nie ma potomków, dla tego nie mamy dla niego listy
 */
public class StillLiving extends FamilyTree{

    public StillLiving(String name, int year) {
        super(name, year);
    }

    @Override
    public void add(FamilyTree familyMember) {
        //nic nie robimy, ponieważ nie można dodać
        throw new UnsupportedOperationException("nie dodajemy");
    }

    @Override
    public int countMembersTotalLivingTime() {
        return getAge();
    }

    @Override
    public double countMembersAverageLifeTime() {
        return getAge();
    }

    @Override
    public int countMembers() {
        return 1;
    }

}
