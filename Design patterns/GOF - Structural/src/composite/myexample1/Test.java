package composite.myexample1;

public class Test {
    public static void main(String[] args) {
        FamilyTree headOfFamily = new Ancestor("Kazimierz Deyna", 86);
        FamilyTree grandpa = new Ancestor("Zbynio Deyna", 76);
        FamilyTree grandma = new Ancestor("Genowefa Deyna", 79);
        FamilyTree ant = new Ancestor("Alicja Deyna", 83);
        FamilyTree me = new StillLiving("Tomek M", 26);
        FamilyTree mySister = new StillLiving("Gosia M", 45);
        
        headOfFamily.add(grandpa);
        grandpa.add(grandma);
        grandma.add(ant);
        grandma.add(me);
        grandma.add(mySister);
        
        System.out.println("average life time: " + headOfFamily.countMembersAverageLifeTime() + "\n" 
                + "total life time: " + headOfFamily.countMembersTotalLivingTime() + "\n" 
                + "memebers count: " + headOfFamily.countMembers());
    }
}
