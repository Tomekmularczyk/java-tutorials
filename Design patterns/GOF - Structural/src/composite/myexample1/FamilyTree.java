package composite.myexample1;

public abstract class FamilyTree {

    private final String name;
    private final int yearOfDeath;

    protected FamilyTree(String name, int year) {
        this.name = name;
        this.yearOfDeath = year;
    }

    @Override
    public String toString() {
        return name + ", age=" + yearOfDeath;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return yearOfDeath;
    }

    public abstract void add(FamilyTree familyMember);
    public abstract int countMembersTotalLivingTime();
    public abstract double countMembersAverageLifeTime();
    public abstract int countMembers();
}
