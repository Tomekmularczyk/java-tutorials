package composite.myexample2;

import java.util.Collections;
import java.util.Set;

/**
 * Leaf
 */
public class File implements SystemComponent{
    private final String name;

    public File(String name) {
        this.name = name;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<SystemComponent> getSubComponents() {
        return Collections.emptySet();
    }
    
}
