package composite.myexample2;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Composite
 */
public class Folder implements SystemComponent {
    private final Set<SystemComponent> subComponents;
    private final String name;

    public Folder(String name) {
        this.name = name;
        this.subComponents = new TreeSet<>(SystemComponent::compareTo);
    }
    
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Set<SystemComponent> getSubComponents() {
        return subComponents;
    }  
    
    public void addSubComponent(SystemComponent component){
        subComponents.add(component);
    }
    
    public void addSubComponents(List<SystemComponent> components){
        subComponents.addAll(components);
    }
    
}
