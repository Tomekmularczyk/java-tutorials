package composite.myexample2;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public enum SystemComponentFactory {
    Instance;

    private final String[] extensions = {".txt", ".psd", ".jpg", ".gif", ".mov", ".mp4", ".docx", ".exe"};

    public SystemComponent getUserCatalog() {
        Folder mainFolder = new Folder("Tomek");
        mainFolder.addSubComponents(generateRandomFiles(6));

        Folder videos = new Folder("Videos");
        videos.addSubComponents(generateRandomFiles(1));
        Folder music = new Folder("Music");
        music.addSubComponents(generateRandomFiles(12));

        mainFolder.addSubComponent(videos);
        mainFolder.addSubComponent(music);
        return mainFolder;
    }

    public SystemComponent getSystemCatalog() {
        Folder systemCatalog = new Folder("System");
        systemCatalog.addSubComponents(generateRandomFiles(3));

        Folder programFiles = new Folder("Program Files");
        programFiles.addSubComponents(generateRandomFiles(5));
        Folder java = new Folder("Java");
        java.addSubComponents(generateRandomFiles(4));
        programFiles.addSubComponent(java);
        Folder userFiles = new Folder("Commons");
        userFiles.addSubComponents(generateRandomFiles(10));

        systemCatalog.addSubComponent(programFiles);
        systemCatalog.addSubComponent(userFiles);
        return systemCatalog;
    }

    public static void main(String[] args) {
        SystemComponentFactory.Instance.generateRandomFiles(10).forEach(System.err::println);
    }

    private List<SystemComponent> generateRandomFiles(int count) {
        List<SystemComponent> files = new LinkedList<>();
        for (int i = 0; i < count; i++) {
            int extensionNo = (int) (Math.random() * 8);
            int nameLength = 2 + (int) (Math.random() * 5);
            String fileName = new Random().ints(nameLength, 98, 121)
                    .boxed()
                    .map(n -> Character.toChars(n)[0])
                    .map(String::valueOf)
                    .collect(Collectors.joining());
            files.add(new File(fileName + extensions[extensionNo]));
        }
        
        return files;
    }
}
