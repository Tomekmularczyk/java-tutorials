package composite.myexample2;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Component
 */
public interface SystemComponent extends Comparable<SystemComponent>{
    
    String getName();
    
    Set<SystemComponent> getSubComponents();

    @Override
    default int compareTo(SystemComponent o) {
        return getName().compareToIgnoreCase(o.getName());
    }

    default String toString(String parentFolder){
        return getSubComponents().stream()
                .map(component -> component.toString(getName()))
                .collect(Collectors.joining("\n", parentFolder + "\\" + getName() +"\\", ""));

    }
}
