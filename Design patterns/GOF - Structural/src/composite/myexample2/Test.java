package composite.myexample2;

/**
 * Ten przykład uniemożliwia traktowania komponentów jednako, natomiast daje nam większe bezpieczeństwo (nie dodamy komponentu do LEAF)
 */
public class Test {

    public static void main(String[] args) {
        SystemComponentFactory factory = SystemComponentFactory.Instance;
        
        Folder mainFolder = new Folder("Home");
        mainFolder.addSubComponent(new File("README.txt"));
        mainFolder.addSubComponent(factory.getUserCatalog());
        mainFolder.addSubComponent(factory.getSystemCatalog());
        
        //nie wyświetla do końca tkajak trzeba..
        System.out.println(mainFolder.toString("root"));
    }
}
