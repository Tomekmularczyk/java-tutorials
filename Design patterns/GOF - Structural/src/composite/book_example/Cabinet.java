package composite.book_example;

/**
 * Konkretny Composite. Dziedziczy po klasie CompositeEquipment operacje związane z elementami podrzędnymi
 */
public class Cabinet extends CompositeEquipment{

    public Cabinet(String name) {
        super(name);
    }

    @Override
    public int power() {
        return 20;
    }

    @Override
    public double discountPrice() {
        return 3.4;
    }
    
}
