package composite.book_example;

/**
 * Composite składa obiekty struktury drzewiaste, tzn jeden obiekt może mieć w sobie kilka obiektów, które mają w sobie kilka obiektów itd.
 * Umożliwia to klientom traktowanie poszczególnych obiektów i ich złożeń w taki sam sposób
 */
public class Test {
    public static void main(String[] args) {
        Cabinet cabinet = new Cabinet("Obudowa komputera PC");
        Chassis chassis = new Chassis("Płyta montażowa komputera PC");
        
        cabinet.add(chassis);
        
        Bus bus = new Bus("Magistrala MCA");
        bus.add(new Card("Karta 16Mbs Token Ring"));
        
        chassis.add(bus);
        chassis.add(new FloppyDisk("Stacja dyskietek 3,5 cala"));
        
        System.out.println("cena netto wynosi: " + chassis.netPrice());
    }
    
}
