package composite.book_example;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Composite - klasa bazowa składająca się z innych sprzętów.
 * - definiuje zachowanie komponentów mających elementy podrzędne.
 * - przechowuje komponenty podrzędne
 * - implementuje operacje z klasy komponent związane z zarządzaniem elementami podrzędnymi
 * 
 * Dzięki tej klasie możemy tworzyć komposyty zawierające w sobie inne kompozyty lub liście klasy Equipment
 */
public abstract class CompositeEquipment extends Equipment{
    private final List<Equipment> elements;
    
    protected CompositeEquipment(String name) {
        super(name);
        elements = new LinkedList<>();
    }
   
    //------------ implementujemy domyslne implementacje związane z elementami podrzędnymi
    
    /**
     * Zlicza rekurencyjnie cene elementów
     */
    @Override
    public double netPrice() {
        Iterable<Equipment> iter = createIterator();
        double total = 0;
        
        for (Iterator<Equipment> i = iter.iterator(); i.hasNext() ;) {
            Equipment next = i.next();
            total += next.netPrice();
        }
        
        return total;
    }

    @Override
    public Iterable<Equipment> createIterator() {
        return elements;
    }

    @Override
    public void remove(Equipment equipment){
        elements.remove(equipment);
    }

    @Override
    public void add(Equipment equipment){
        elements.add(equipment);
    }
    
}
