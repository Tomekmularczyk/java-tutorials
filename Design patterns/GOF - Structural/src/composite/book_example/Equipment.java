package composite.book_example;

import java.util.Collections;

/**
 * Component 
 * - obejmuje implementacje domyślnego zachowania na potrzeby interfejsu wspólnego dla wszystkich klas Leaf i Composite
 * - obejmuje deklaracje interfejsu umożliwającego dostęp do komponentów podrzędnych i zarządzanie nimi
 * - opcjonalnie definiuje interfejs umożliwiający dostęp do elementu nadrzędnego
 */
public abstract class Equipment {
    private String name;
    
    protected Equipment(String name){
        this.name = name;
    }
    
    public String name(){
        return name;
    }
    
    
    public abstract int power();
    public abstract double netPrice();
    public abstract double discountPrice();
    
    public abstract void add(Equipment equipment);
    public abstract void remove(Equipment equipment);
    /**
     * zwraca dostęp do części obiektu.
     * Domyślna implementacja zwraca pustą listę
     */
    public Iterable<Equipment> createIterator(){
        return Collections.emptyList();
    }
}
