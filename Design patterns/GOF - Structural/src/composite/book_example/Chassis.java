package composite.book_example;

/**
 * Konkretny Composite. Dziedziczy po klasie CompositeEquipment operacje związane z elementami podrzędnymi
 */
public class Chassis extends CompositeEquipment{

    public Chassis(String name) {
        super(name);
    }

    @Override
    public int power() {
        return 10;
    }

    @Override
    public double discountPrice() {
        return 2.5;
    }
    
}
