package composite.book_example;
/**
 * Leaf - obiekt bez elementów podrzednych.
 * Definiuje zachowanie obiektów prostych w złożeniu.
 */
public class Card extends Equipment{

    public Card(String name) {
        super(name);
    }

    @Override
    public int power() {
        return 1;
    }

    @Override
    public double netPrice() {
        return 3;
    }

    @Override
    public double discountPrice() {
        return 2.2;
    }

    @Override
    public void add(Equipment equipment) {
    }

    @Override
    public void remove(Equipment equipment) {
    }
    
}