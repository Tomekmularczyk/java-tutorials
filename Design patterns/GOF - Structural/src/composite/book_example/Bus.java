package composite.book_example;

/**
 * Konkretny Composite. Dziedziczy po klasie CompositeEquipment operacje związane z elementami podrzędnymi
 */
public class Bus extends CompositeEquipment{

    public Bus(String name) {
        super(name);
    }

    @Override
    public int power() {
        return 4;
    }

    @Override
    public double discountPrice() {
        return 1.1;
    }
    
}
