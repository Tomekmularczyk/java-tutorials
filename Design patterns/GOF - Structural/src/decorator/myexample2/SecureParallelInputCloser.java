package decorator.myexample2;

public class SecureParallelInputCloser extends FileWriterDecorator {

    public SecureParallelInputCloser(FileWriter writer) {
        super(writer);
    }

    @Override
    public void writeToFile(String input) {
        lockTheFileFromOtherThreads();
        super.writeToFile(input);
        doubleCheckIfInputIsClosed();
        releaseTheLock();
    }

    private boolean doubleCheckIfInputIsClosed() {
        System.out.println("\nChecking if input is closed, for the 1st… and 2nd time… - It is closed.");
        return true;
    }

    private void lockTheFileFromOtherThreads() {
        System.out.println("locking file from other threads…");
    }

    private void releaseTheLock() {
        System.out.println("…unlocking file for other threads");
    }
}
