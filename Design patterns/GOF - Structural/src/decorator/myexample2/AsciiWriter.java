package decorator.myexample2;

import java.util.stream.Collectors;

/**
 * Adapter
 */
public class AsciiWriter extends FileWriterDecorator {

    public AsciiWriter(FileWriter writer) {
        super(writer);
    }

    @Override
    public void writeToFile(String input) {
        input = changeToAscii(input);
        super.writeToFile(input);
    }

    private String changeToAscii(String string) {
        System.out.println("Changing input to ascii code points");
        return string.chars()
                .mapToObj(String::valueOf)
                .collect(Collectors.joining());
    }
}
