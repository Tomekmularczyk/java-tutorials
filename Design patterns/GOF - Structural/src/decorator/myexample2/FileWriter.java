package decorator.myexample2;

/**
 * Abstract component
 */
public interface FileWriter {
    void writeToFile(String input);
}
