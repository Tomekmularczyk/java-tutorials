package decorator.myexample2;

/**
 * Concrete component
 */
public class FileWriterImpl implements FileWriter{

    @Override
    public void writeToFile(String input) {
        System.out.println("Writing input to file");
        for (int i = 0; i < input.length(); i++) {
            char charAt = input.charAt(i);
            System.out.print(charAt + ".");
        }
    }

}
