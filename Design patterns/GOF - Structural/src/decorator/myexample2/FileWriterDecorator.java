package decorator.myexample2;

/**
 *  Decorator
 */
public abstract class FileWriterDecorator implements FileWriter{
    private final FileWriter writer;

    public FileWriterDecorator(FileWriter writer) {
        this.writer = writer;
    }

    @Override
    public void writeToFile(String input){
        writer.writeToFile(input);
    }

}
