package decorator.myexample2;

import static java.lang.System.out;

/**
 * It is easy to add functionality to an entire class of objects by subclassing an object, 
 * but it is impossible to extend a single object this way. 
 * With the Decorator Pattern, you can add functionality to a single object and leave others like it unmodified.
 */
public class Test {
    public static void main(String[] args) {
        FileWriterImpl fileWriter = new FileWriterImpl();
        
        //Simple use case
        writeToFile(fileWriter, "Standard writer");
        out.println(); 
        
        //decorated...
        writeToFile(new AsciiWriter(fileWriter), "decorated input");
        out.println(); 
        
        //double-decorated
        writeToFile(new AsciiWriter(new AsciiWriter(fileWriter)), "decorated input");
        out.println();        

        writeToFile(new AsciiWriter(new SecureParallelInputCloser(fileWriter)), "secured input writer");
        
        //what if we reverse the order
        writeToFile(new SecureParallelInputCloser(new AsciiWriter(fileWriter)), "secured input writer");
    }
    
    public static void writeToFile(FileWriter fileWriter, String input){
        fileWriter.writeToFile(input);
        System.out.println();
    }
    
    /**
     * With this approach we can add different functionalities to an object without having to make multipple subclasses
     */
}
