package decorator.myexample1;

public class WashYourSelf extends ClothesDecorator{

    public WashYourSelf(Person person) {
        super(person);
    }

    @Override
    public void presentYourSelf() {
        System.out.println("I'm taking a shower");
        super.presentYourSelf();
    }
    
    

}
