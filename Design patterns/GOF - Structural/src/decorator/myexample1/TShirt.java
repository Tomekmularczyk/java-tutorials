package decorator.myexample1;

public class TShirt extends ClothesDecorator{

    public TShirt(Person person) {
        super(person);
    }

    @Override
    public void presentYourSelf() {
        super.presentYourSelf();
        System.out.println("I'm wearing T-Shirt");
    }

    
}
