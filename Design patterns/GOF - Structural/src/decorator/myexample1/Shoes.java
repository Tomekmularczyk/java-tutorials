package decorator.myexample1;

public class Shoes extends ClothesDecorator {

    public Shoes(Person person) {
        super(person);
    }

    @Override
    public void presentYourSelf() {
        super.presentYourSelf();
        System.out.println("I'm wearing shoes.");

    }

}
