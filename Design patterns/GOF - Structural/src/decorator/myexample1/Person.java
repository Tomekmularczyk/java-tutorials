package decorator.myexample1;

public class Person {
    private final String name;

    public Person(String name) {
        this.name = name;
    }
    
    public void presentYourSelf(){
        System.out.println("My name is " + name + ".");
    }

    public String getName() {
        return name;
    }
    
}
