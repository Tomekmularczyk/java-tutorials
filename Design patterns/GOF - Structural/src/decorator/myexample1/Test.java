package decorator.myexample1;

public class Test {

    public static void main(String[] args) {
        Person tomek = new Person("Tomek");

        ClothesDecorator dressedTomek = new WashYourSelf(
                new Shoes(
                        new TShirt(
                                new Hat(
                                        new WashYourSelf(tomek)
                                )
                        )
                )
        );
        
        presentPerson(dressedTomek);
    }

    public static void presentPerson(Person person) {
        person.presentYourSelf();
    }
}
