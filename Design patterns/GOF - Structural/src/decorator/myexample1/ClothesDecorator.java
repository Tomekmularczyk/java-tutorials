package decorator.myexample1;

public abstract class ClothesDecorator extends Person{
    private final Person person;

    public ClothesDecorator(Person person) {
        super(person.getName());
        this.person = person;
    }

    @Override
    public void presentYourSelf() {
        person.presentYourSelf();
    }
    
    

}
