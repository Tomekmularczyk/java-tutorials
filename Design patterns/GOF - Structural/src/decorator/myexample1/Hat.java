package decorator.myexample1;

public class Hat extends ClothesDecorator{

    public Hat(Person person) {
        super(person);
    }

    @Override
    public void presentYourSelf() {
        super.presentYourSelf();
        System.out.println("I'm wearing Hat.");
    }

    
    
}
