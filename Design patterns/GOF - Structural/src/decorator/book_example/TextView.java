package decorator.book_example;

/**
 * Konkretny komponent, który bedziemy dekorować
 */
public class TextView extends VisualComponent{

    @Override
    public void draw() {
        //…drawing
    }

    @Override
    public void resize() {
        //…resizing
    }

}
