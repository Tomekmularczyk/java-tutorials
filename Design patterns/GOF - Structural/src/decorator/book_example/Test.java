package decorator.book_example;

/**
 * Dekorator - dynamicznie dołącza dodatkowe obowiązki do obiektu. 
 * Przedstawia alternatywny elastyczny sposób tworzenia podklas o wzbogaconych funkcjach.
 * 
 * Przydaje się szczególnie gdy chcemy dodać dodatkowe obowiązki do pojedyńczych obiektów a nie całej klasy.
 * Jego obecność jest niezauważalna dla klientów danego komponentu, dlatego jest idealny gdy trzeba dodawać zadania
 * do poszczególnych obiektów w przeźroczysty sposób.
 * 
 * Przydaje się też wtedy gdy zamiast tworzyć klasy z milionem funkcji przewidujących każdą sytuację,
 * lepiej jest zbudować prosty mechanizm i dodawać funkcje stopniowo za pomocą dekoratorów.
 * Pozwala to uniknąć kosztów związanych z masą nie używanych funkcji
 * 
 * Minusem może być powstawanie wielu małych i podobnych do siebie obiektów, które mogą okazać się potem trudne do opanowania
 * lub zdiagnozowania.
 */
public class Test {
    public static void main(String[] args) {
        VisualComponent textView = new TextView();
        
        //zamiast dodać zwykły komponent możemy go udekorować
        VisualComponent decoratedTextView = new BorderDecorator(new ScrollDecorator(textView), 3);
        setContentInWindow(decoratedTextView); //metoda nie wykrywa obecności dekoratorów
    }
    
    public static void setContentInWindow(VisualComponent visualComponent){
        //… metoda dodająca komponent do okna
    }
}
