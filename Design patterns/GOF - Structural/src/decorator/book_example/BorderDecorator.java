package decorator.book_example;

/**
 * Konkretny obiekt dekoratora. Ponieważ wszystkie operacje są już oddelegowane w nadklasie, 
 * tutaj możemy skupić się na się na dekorowaniu
 */
public class BorderDecorator extends Decorator {
    private int borderWidth;
    
    public BorderDecorator(VisualComponent component, int borderWidth) {
        super(component);
        this.borderWidth = borderWidth;
    }

    @Override
    public void draw() {
        super.draw();
        drawBorder(borderWidth);
    }
    
    private void drawBorder(int width){
        
    }
}
