package decorator.book_example;

/**
 * Dekorator - w celu dodawania "skórek" czy "nakładek" na obiekt definujemy jego podklasę, 
 * która posiada referencje do obiektu tego samego typu jaki rozszerza.
 * 
 * Jeżeli chce się dodać tylko jedno zadanie, nie jest konieczne tworzenie abstrakcyjnego dekoratora.
 */
public abstract class Decorator extends VisualComponent{
    private final VisualComponent component;

    public Decorator(VisualComponent component) {
        this.component = component;
    }
    
    /**
     * Dla każdej odziedziczonej operacji należy oddelegować żądanie do zmiennej component…
     */
    @Override
    public void draw() {
        component.draw();
    }

    @Override
    public void resize() {
        component.resize();
    }

}
