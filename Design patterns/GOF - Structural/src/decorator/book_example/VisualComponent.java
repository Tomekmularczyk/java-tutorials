package decorator.book_example;

/**
 * Component - Definiuje interfejs obiektów do których można dynamicznie dodawać usługę zadań
 * 
 * Złożoność klasy komponent może sprawić, że dekoratory staną się zbyt rozbudowane by można było korzystać z wielu obiektów tego rodzaju.
 * Dlatego należy zadbać by wspólna klasa była jak najprostsza. W javie najlepiej sprawdzi się do tego interfejs.
 * 
 * Jeżeli jednak klasa komponentu jest już mocno rozbudowana być może lepsze okaże się zastosowanie wzorca Strategy.
 * Z tym że Strategy zmienia zachowanie obiektu, a Decorator jedynie je "opakowuje" pewne zachowanie.
 */
public abstract class VisualComponent {

    public abstract void draw();
    
    public abstract void resize();
}
