package decorator.book_example;

public class ScrollDecorator extends Decorator {

    public ScrollDecorator(VisualComponent component) {
        super(component);
    }

    @Override
    public void draw() {
        super.draw();
        addScrollToObject();
    }

    private void addScrollToObject(){
        //...
    }
}
