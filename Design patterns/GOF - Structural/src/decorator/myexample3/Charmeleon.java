package decorator.myexample3;

/**
 * Concrete Decorator
 */
public class Charmeleon extends PokemonDecorator {

    public Charmeleon(Pokemon pokemon) {
        super(pokemon);
        this.attackSkill += 35;
        this.defenseSkill += 20;
        this.experience += 62;
    }

    /**
     * Dekorowana metoda
     */
    @Override
    public void attack() {
        pokemon.attack();
        System.out.println("Zieje ogniem!");
    }

    /**
     * Ta metoda nie jest tak naprawdę dekorowana, tylko napisana od nowa
     */
    @Override
    public Pokemon evolve() {
        System.out.println(getClass().getSimpleName() + " is evolving…");
        return new Charizard(this);
    }

}
