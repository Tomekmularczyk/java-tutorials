package decorator.myexample3;

/**
 * Concrete decorator
 */
public class Charizard extends PokemonDecorator {

    public Charizard(Pokemon pokemon) {
        super(pokemon);
        this.attackSkill += 80;
        this.defenseSkill += 66;
        this.experience += 102;
    }

    /**
     * Dekorowana metoda
     */
    @Override
    public void attack() {
        pokemon.attack();
        System.out.println("Przelatuje nad przeciwnikiem i pluje lawą!");
    }

    /**
     * Ta metoda nie jest tak naprawdę dekorowana, tylko napisana od nowa
     */
    @Override
    public Pokemon evolve() {
        throw new FinalFormException(getClass().getSimpleName() + " is this pokemon final form.");
    }

}
