package decorator.myexample3;


/**
 * Dekorator dynamicznie dołącza dodatkowe obowiązki do obiektu, dzięki czemu nie jest konieczne statyczne tworzenie wielu podklas.
 * Inną zaletą jest to, że możemy dodać do obiektu nowe zadania i modyfikacje bez konieczności tworzenia podtypu i przepisywania wartości.
 * Dzięki temu obiekty mogą "ewoluuować" w trakcie działania programu.
 */
public class Test {
    public static void main(String[] args) {
        Pokemon charmander = new Charmander(10, 3, 0);
        
        charmander.displayAbilities();
        
        System.out.println("UWAGA, CHARMANDER BĘDZIE EWOLUOWAŁ!");
        
        Pokemon evolvedCharmander = charmander.evolve();
        evolvedCharmander.displayAbilities();
        
        System.out.println("JA PIERDOLE, BEDZIE EWOLUOWAŁ JESZCZE RAZ!");
        
        Pokemon charmander3rdForm = evolvedCharmander.evolve();
        charmander3rdForm.displayAbilities();
        
        System.out.println("KAŻMY MU WYEWOLUOWAĆ JESZCZE RAZ!!!");
        try{
            charmander3rdForm.evolve();
        }catch(FinalFormException ex){
            System.out.println(ex.getMessage());
        }
    }
}
