package decorator.myexample3;

public class Charmander extends Pokemon{

    public Charmander(int initialAttack, int initialDefense, int initialExperience) {
        super(initialAttack, initialDefense, initialExperience);
    }

    @Override
    public void attack() {
        System.out.println("drapie przeciwnika!");
    }

    @Override
    public Pokemon evolve() {
        System.out.println(getClass().getSimpleName() + " is evolving…");
        return new Charmeleon(this);
    }

}
