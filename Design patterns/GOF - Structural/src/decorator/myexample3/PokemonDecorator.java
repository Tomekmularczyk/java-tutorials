package decorator.myexample3;

/**
 * Abstract decorator + Bridge
 */
public abstract class PokemonDecorator extends Pokemon{
    protected final Pokemon pokemon;

    public PokemonDecorator(Pokemon pokemon) {
        super(pokemon.attackSkill, pokemon.defenseSkill, pokemon.experience);
        this.pokemon = pokemon;
    }
}