package decorator.myexample3;

/**
 * Abstract Component
 */
public abstract class Pokemon {

    protected int attackSkill;
    protected int defenseSkill;
    protected int experience;

    public Pokemon(int initialAttack, int initialDefense, int initialExperience) {
        this.attackSkill = initialAttack;
        this.defenseSkill = initialDefense;
        this.experience = initialExperience;
    }

    public abstract void attack();

    public abstract Pokemon evolve();

    public void displayAbilities() {
        System.out.printf(getClass().getSimpleName() + ": attack - %d, defense - %d, experience - %d\n", attackSkill, defenseSkill, experience);
    }

    public void sleep() {
        System.out.println(getClass().getSimpleName() + " is sleeping…");
    }
}
