package decorator.myexample3;

public class FinalFormException extends UnsupportedOperationException{

    public FinalFormException(String message) {
        super(message);
    }

}
