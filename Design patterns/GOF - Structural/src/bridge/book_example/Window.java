package bridge.book_example;

import java.awt.Point;

/**
 * Abstraction
 * - definiuje interfejs abstrakcji (tutaj dla okienek)
 * - przechowuje referencje do obiektu typu Implementator
 * 
 * Operacje klasy Window są zdefiniowane z kategoriach interfejsu WindowImp
 */
public abstract class Window {

    private WindowImp windowImp;
    private View contents;

    public Window(View contents) {
        this.contents = contents;
    }

    // żądania obsługiwane przez okna
    public abstract void drawContents();
    
    public abstract void open();
    public abstract void close();
    public abstract void iconify();
    public abstract void deiconify();
    
    // żądania przekazywane do implementacji
    public abstract Point setOrigin();
    public abstract Point setExtent();
    public abstract void raise();
    public abstract void lower();
    
    public abstract void drawLine(Point a, Point b);
    public void drawRect(Point a, Point b){
        windowImp.deviceRect(a.x, a.y, b.x, b.y);
    }
    public abstract void drawPolygon(Point[] points, int n);
    public abstract void drawText(Point point, String text);
    
    /**
     * W jaki sposób okno może uzyskać egzemplarz odpowiedniej podklasy WindowImp?
     * Na potrzeby tego przykładu posłóżymy się fabryką abstrakcyjną, która jest singletonem.
     */
    protected WindowImp getWindowImp() {
        if(windowImp == null){
            windowImp = WindowSystemFactory.Instance.makeWindowImp();
        }
        return windowImp;
    }

    protected View getView() {
        return contents;
    }

}
