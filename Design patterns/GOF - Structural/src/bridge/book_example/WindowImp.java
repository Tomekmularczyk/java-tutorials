package bridge.book_example;

import java.awt.Point;

/**
 * Implementator - klasa abstrakcyjna z deklaracją interfejsu do używania w abstrakcji
 */
public abstract class WindowImp {
    
    //przykładowe funkcje
    public abstract void impTop();
    public abstract void impBottom();
    public abstract Point impSetExtent();
    public abstract Point impSetOrigin();
    
    public abstract void deviceRect(int a, int b, int c, int d);
    public abstract void deviceText(String text, int a, int b);
    public abstract void deviceBitmap(String text, int a, int b);
}
