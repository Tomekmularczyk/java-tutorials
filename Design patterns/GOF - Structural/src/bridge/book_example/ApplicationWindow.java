package bridge.book_example;

import java.awt.Point;

/**
 * RefinedAbstraction 
 */
public class ApplicationWindow extends Window{

    public ApplicationWindow(View contents) {
        super(contents);
    }

    //** implementacja 
    @Override
    public void drawContents() {
        getView().drawOn(this);
    }

    @Override
    public void open() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void iconify() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void deiconify() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Point setOrigin() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Point setExtent() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void raise() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void lower() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void drawLine(Point a, Point b) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void drawPolygon(Point[] points, int n) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void drawText(Point point, String text) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}
