package bridge.book_example;

import java.awt.Point;

public class PMWindowImp extends WindowImp{
    // dużo specyficznych dla PM składowych stanu
    private double hps;
    
    
    //=========== podstawowe operacje dla systemu okienkowego
    @Override
    public void impTop() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void impBottom() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Point impSetExtent() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Point impSetOrigin() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void deviceRect(int a, int b, int c, int d) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void deviceText(String text, int a, int b) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void deviceBitmap(String text, int a, int b) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}
