package bridge.book_example;

/**
 * Fabryka abstrakcyjna, która skutecznie kapsułkuje wszystkie mechanizmy dla systemów okienkowych
 * Zwraca interfejs specyficzny dla platformy
 */
public enum WindowSystemFactory {
    Instance;

    public WindowImp makeWindowImp(){
        return new XWindowImp();  //przykładowo zwracamy taka implementacje
    }
}
