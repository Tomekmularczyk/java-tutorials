package bridge.book_example;

public class Test {
/**
 * Wzorzec Bridge odziela abstrakcje od implementacji dzięki czemu można modyfikować te dwa obiekty niezależnie.
 * Podczas gdy ze wzorca adapter używa się do istniejącego systemu by dopasować jeden interfejs do drugiego, tak
 * Bridge stosuje się przed rozpoczęciem projektowania tak by umożliwić niezależne modyfikowanie abstrackcji i implementacji
 */
}
