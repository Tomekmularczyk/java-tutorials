package bridge.myexample1;

public enum SystemFactory {
    Instance;
    
    public OperatingSystem getOperatingSystem(){
        int deviceType = detectDeviceType();
        
        switch(deviceType){
            case 1:
                return new WindowdowsPhone();
            case 2:
                return new IOS();
            case 3:
                return new Android();
            default:
                throw new UnsupportedOperationException("Device not supported!");
        }
    }
    
    
    public int detectDeviceType(){
        return 1; //gdzieś tam wykryliśmy typ urządzenia i zwracamy jego identyfikator
    }
}
