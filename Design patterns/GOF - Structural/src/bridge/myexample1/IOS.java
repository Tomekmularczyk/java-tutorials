package bridge.myexample1;

/**
 * ConcreteImplementator
 */
public class IOS implements OperatingSystem {
    private final String[] iosApps = {
        "Facebook", "Insta", "iMessage"
    };
    
    
    @Override
    public void installOnDevice(double width, double height) {
        System.out.println("Installing IOS 9…, …matching to width and height…");
    }

    @Override
    public void loginToAppMarket(String userName, String password) {
        System.out.println("Username: " + userName + " logged correctly to AppStore.");
    }

    @Override
    public String getApp(String appName) {
        for (String app : iosApps) {
            if(app.equalsIgnoreCase(appName))
                return app;
        }
        
        return "There is no " + appName + " app for IOS.";
    }

    @Override
    public String getBasicBrowser() {
        return "Safari";
    }

}