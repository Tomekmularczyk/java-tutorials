package bridge.myexample1;

/**
 * ConcreteImplementator
 */
public class Android implements OperatingSystem {
    private final String[] androidApps = {
        "Facebook", "Insta", "Gmail"
    };
    
    
    @Override
    public void installOnDevice(double width, double height) {
        System.out.println("Installing Android 5 Lollipop…, …matching to width and height…");
    }

    @Override
    public void loginToAppMarket(String userName, String password) {
        System.out.println("Username: " + userName + " logged correctly to Android Market.");
    }

    @Override
    public String getApp(String appName) {
        for (String app : androidApps) {
            if(app.equalsIgnoreCase(appName))
                return app;
        }
        
        return "There is no " + appName + " app for Android System.";
    }

    @Override
    public String getBasicBrowser() {
        return "Chrome";
    }

}
