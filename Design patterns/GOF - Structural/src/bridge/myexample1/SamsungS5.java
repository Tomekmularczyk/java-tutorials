package bridge.myexample1;

/**
 * RefinedAbstraction
 */
public class SamsungS5 extends Smartphone {

    private static final double WIDTH = 3.2;
    private static final double HEIGHT = 5.5;

    @Override
    public void installSystem() {
        getOperatingSystem().installOnDevice(WIDTH, HEIGHT);
    }

    @Override
    public void installBasicApps() {
        OperatingSystem operatingSystem = getOperatingSystem();
        operatingSystem.loginToAppMarket("user", "passoword");

        String facebook = operatingSystem.getApp("Facebook");
        String insta = operatingSystem.getApp("Insta");
        String gmail = operatingSystem.getApp("gmail");

        System.out.printf("Installing apps:\n%s\n%s\n%s\n", facebook, insta, gmail);
    }

    @Override
    public void browseInternet() {
        System.out.println("Browsing internet with SamsungS5 on " + getOperatingSystem().getBasicBrowser());
    }

}
