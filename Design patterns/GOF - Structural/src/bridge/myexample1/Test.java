package bridge.myexample1;

public class Test {

    public static void main(String[] args) {
        Smartphone smartphone = SmartfonShop.getInstance().buyMeASmartphone(700);
        System.out.println("I bought " + smartphone.getClass().getSimpleName() + "!");
        smartphone.installSystem();
        smartphone.installBasicApps();
        smartphone.browseInternet();
    }

    public static class SmartfonShop {

        private SmartfonShop() {}

        private static class LazyHolder { //holder idiom
            private static final SmartfonShop INSTANCE = new SmartfonShop();
        }

        public static SmartfonShop getInstance() {
            return LazyHolder.INSTANCE;
        }

        public Smartphone buyMeASmartphone(double money) {
            if (money < 250) {
                throw new NullPointerException("Sorry you don't have enough money.");
            }

            if (money >= 250 && money < 1200) {
                return getNokiaLumia();
            }

            if (money >= 1200 && money < 2800) {
                return getSamsungS5();
            }

            if (money >= 2800) {
                return getIphone7();
            }

            throw new UnsupportedOperationException("Wrong amount of money.");
        }

        private SamsungS5 getSamsungS5() {
            return new SamsungS5();
        }

        private Iphone7 getIphone7() {
            return new Iphone7();
        }

        private NokiaLumia getNokiaLumia() {
            return new NokiaLumia();
        }
    }
}
