package bridge.myexample1;

/**
 * RefinedAbstraction
 */
public class NokiaLumia extends Smartphone {

    private static final double WIDTH = 3.2;
    private static final double HEIGHT = 5.5;

    @Override
    public void installSystem() {
        getOperatingSystem().installOnDevice(WIDTH, HEIGHT);
    }

    @Override
    public void installBasicApps() {
        OperatingSystem operatingSystem = getOperatingSystem();
        operatingSystem.loginToAppMarket("user33", "zaq2sd");

        String facebook = operatingSystem.getApp("Facebook");
        String insta = operatingSystem.getApp("Insta");
        String office = operatingSystem.getApp("Office");

        System.out.printf("Installing apps:\n%s\n%s\n%s\n", facebook, insta, office);
    }

    @Override
    public void browseInternet() {
        System.out.println("Browsing internet with Nokia Lumia 8 on " + getOperatingSystem().getBasicBrowser());
    }

}
