package bridge.myexample1;

/**
 * Abstraction
 */
public abstract class Smartphone {
    private final OperatingSystem operatingSystem;
    
    public Smartphone(){
        this.operatingSystem = SystemFactory.Instance.getOperatingSystem();
    }
    
    public abstract void installSystem();
    
    public abstract void installBasicApps();
    
    public abstract void browseInternet();
    
    public OperatingSystem getOperatingSystem(){
        return operatingSystem;
    }
}
