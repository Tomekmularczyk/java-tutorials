package bridge.myexample1;

/**
 * ConcreteImplementator
 */
public class WindowdowsPhone implements OperatingSystem {
    private final String[] windowsPhoneApps = {
        "Facebook", "Insta", "Office"
    };
    
    
    @Override
    public void installOnDevice(double width, double height) {
        System.out.println("Installing WinPhone 8.1…, …matching to width and height…");
    }

    @Override
    public void loginToAppMarket(String userName, String password) {
        System.out.println("Username: " + userName + " logged correctly to WindowsPhone Market.");
    }

    @Override
    public String getApp(String appName) {
        for (String app : windowsPhoneApps) {
            if(app.equalsIgnoreCase(appName))
                return app;
        }
        
        return "There is no " + appName + " app for WindowsPhone.";
    }

    @Override
    public String getBasicBrowser() {
        return "InternetExplorer";
    }

}
