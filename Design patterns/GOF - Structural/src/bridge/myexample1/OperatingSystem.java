package bridge.myexample1;

/**
 * Implementator
 */
public interface OperatingSystem {
    
    void installOnDevice(double width, double height);
    
    void loginToAppMarket(String userName, String password);
    
    String getApp(String appName);
    
    String getBasicBrowser();
}
