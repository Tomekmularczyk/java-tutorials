package bridge.myexample1;

/**
 * RefinedAbstraction
 */
public class Iphone7 extends Smartphone{
    private static final double WIDTH = 3.8;
    private static final double HEIGHT = 6.0;
    
    @Override
    public void installSystem() {
        getOperatingSystem().installOnDevice(WIDTH, HEIGHT);
    }

    @Override
    public void installBasicApps() {
        OperatingSystem operatingSystem = getOperatingSystem();
        operatingSystem.loginToAppMarket("user1", "passoword");
        
        String facebook = operatingSystem.getApp("Facebook");
        String insta = operatingSystem.getApp("Insta");
        String legoStarWars = operatingSystem.getApp("Lego StarWars");
        
        System.out.printf("Installing apps:\n%s\n%s\n%s\n", facebook, insta, legoStarWars);
    }

    @Override
    public void browseInternet() {
        System.out.println("Browsing internet with Iphone 7 on " + getOperatingSystem().getBasicBrowser());
    }

}
