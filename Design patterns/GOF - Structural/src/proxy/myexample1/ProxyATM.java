package proxy.myexample1;

public class ProxyATM implements ATMMachine {

    private final ATMMachine atm;
    private String currentCardNumber;
    private int currentPin;
    private boolean isAuthorized;

    public ProxyATM() {
        this.atm = new RealATMMachine();
        this.isAuthorized = false;
        this.currentPin = -1;
        this.currentCardNumber = "";
    }

    @Override
    public double withdrawCash(double amount) {
        if (!isAuthorized) {
            System.out.println("You are not allowed to do withdraw cash");
            return 0;
        }

        return atm.withdrawCash(amount);
    }

    @Override
    public boolean depositCash(double amount) {
        if (!isAuthorized) {
            System.out.println("You are not allowed to deposit cash");
            return false;
        }

        return atm.depositCash(amount);
    }

    @Override
    public void insertCard() {
        currentCardNumber = "343edfeDd3Fd300";
        atm.insertCard();
    }

    @Override
    public void insertPin(int pin) {
        this.currentPin = pin;
        if (authorize()) {
            System.out.println("Pin number correct, you are now allowed to withdraw or deposit cash.");
            isAuthorized = true;
        } else {
            System.out.println("Pin number incorrect.");
            resetUser();
        }
    }

    @Override
    public void ejectCard() {
        resetUser();
    }

    private boolean authorize() {
        System.out.println("Matching card number with pin in the bank central…");
        //veryfiyng
        if (currentCardNumber.isEmpty()) {
            System.out.println("insert credit card first");
            return false;
        }

        return currentPin > 999 && currentPin < 10000;
    }

    private void resetUser() {
        if (!currentCardNumber.isEmpty()) {
            atm.ejectCard();
        }
        this.isAuthorized = false;
        this.currentPin = -1;
        this.currentCardNumber = "";
    }

}
