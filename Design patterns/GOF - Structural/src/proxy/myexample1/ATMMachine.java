package proxy.myexample1;

/**
 * Subject
 */
public interface ATMMachine {
    double withdrawCash(double amount);
    boolean depositCash(double amount);
    void insertCard();
    void ejectCard();
    void insertPin(int pin);
}
