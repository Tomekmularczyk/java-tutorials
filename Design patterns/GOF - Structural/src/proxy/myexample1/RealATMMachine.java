package proxy.myexample1;

/**
 * RealSubject
 */
public class RealATMMachine implements ATMMachine {

    private boolean hasCardInserted;

    public RealATMMachine() {
        this.hasCardInserted = false;
    }

    @Override
    public double withdrawCash(double amount) {
        System.out.println("Cash withdrawed successfuly.");
        return amount;
    }

    @Override
    public boolean depositCash(double amount) {
        if (amount <= 0) {
            return false;
        }

        System.out.println("Cash deposited successfuly.");
        return true;
    }

    @Override
    public void insertCard() {
        if (hasCardInserted) {
            System.out.println("ATM already has a card");
        }

        hasCardInserted = true;
        System.out.println("Credit card inserted.");
    }

    @Override
    public void insertPin(int pin) {
        if (!hasCardInserted) {
            System.out.println("Insert credit card first");
        }
        if (String.valueOf(pin).length() != 4) {
            System.out.println("Wrong Pin Number!");
        }

        System.out.println("Pin inserted: ****");
    }

    @Override
    public void ejectCard() {
        if (!hasCardInserted) {
            System.out.println("ATM have not a card");
        }

        hasCardInserted = false;
        System.out.println("Credit card ejected.");
    }

}
