package proxy.myexample1;

/**
 * Przykład zabezpieczającego Proxy.
 * Część konkretnych zadań jest obsługiwana w RealSubject, a kwestie bezpieczeństwa są obsłużone w Proxy
 */
public class Test {
    public static void main(String[] args) {
        setPublicATM(new ProxyATM());
    }
    
    public static void setPublicATM(ATMMachine atm){
        atm.insertPin(4542);
        atm.withdrawCash(34333);
        
        System.out.println("==================");
        
        atm.insertCard();
        atm.insertPin(3989);
        atm.depositCash(200);
        atm.withdrawCash(1000);
        atm.ejectCard();
    }
}
