package proxy.book_example;

/**
 * Proxy (ang. pełnomocnik, posrednik) - kontroluje dostęp do obiektu.
 * Powszechne zastosowania: 
 * - pre-processing, post-processing
 * - przesłanianie funkcjonalności
 * - mechanizm keszowania
 * - mechanizmy bezpieczeństwa
 * - odraczanie inicjalizacji kosztownego obiektu
 * 
 * Z pełnomocnika korzystamy wtedy gdy potrzebna jest bardziej wszechstronna referencja do obiektu niż zwykła zmienna.
 * 
 *  Znane rodzaje Proxy:
 *  $ pełnomocniki zdalne - kodowanie żądań i argumentów oraz przekazywanie ich do obiektu w innej przestrzeni adresowej.
 *  $ pełnomocniki wirtualne - przechowują dodatkowe informacje o obiekcie co umożliwia opóźniony dostęp do niego.
 *  $ pełnomocniki zabezpieczające - przed wysłaniem żądania do obiektu sprawdzają czy nadawca ma odpowiednie uprawnienia.
 */
public class Test {
    // w tym pakiecie zaimplementowany jest pośrednik wirtualny, który tworzy obiekty na żądanie
    
    
    public static void main(String[] args) {
        ImageProxy imageProxy = new ImageProxy("C:\\some\\path\\to.file");
        insert(imageProxy);
    }
    /**
     * Załóżmy że mamy obiekt zawierający elementy Graphic. Możemy teraz użyć pełnomocnika.
     */
    public static void insert(Graphic graphic){
        //doing something…
    }
    
}
