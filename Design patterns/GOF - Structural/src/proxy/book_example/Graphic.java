package proxy.book_example;

import java.awt.Event;
import java.awt.Point;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Subject - definiuje wspólny interfejs klas RealSubject i Proxy, dzięki czemu obiektów Proxy można używać wszędzie tam, gdzie oczekiwane
 * są obiekty RealSubject.
 */
public interface Graphic {

    void draw(Point at);

    void handleMouse(Event event);

    Point getExtent();

    void load(InputStream from);

    void save(OutputStream to);
}
