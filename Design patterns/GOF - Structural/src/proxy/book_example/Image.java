package proxy.book_example;

import java.awt.Event;
import java.awt.Point;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * RealSubject
 * - definiuje rzeczywisty obiekt reprezentowany przez pełnomocnika(Proxy)
 */
public class Image implements Graphic{

    public Image(String imagePath) {
        //wczytuje image z pliku
    }
    
    
    @Override
    public void draw(Point at) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void handleMouse(Event event) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Point getExtent() {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void load(InputStream from) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void save(OutputStream to) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}
