package proxy.book_example;

import java.awt.Event;
import java.awt.Point;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Proxy
 * - przechowuje referencje umożliwającą dostęp do rzeczywistego obiektu
 * - udostępnia interfejs identyczny zdo klasy Subject, dzięki czemu można go podstawić pod rzeczywisty obiekt.
 * - kontroluje dostęp do obiektu i może odpowiadać za jego tworzenie oraz usuwanie.
 * - pełni inne zależne od pełnomocnika zadania (pełnomocniki zdalne, wirtualne, zabezpieczające)
 */
public class ImageProxy implements Graphic{
    private Image image;
    private Point extent;
    private String imagePath;
    
    public ImageProxy(String imagePath) {
        this.imagePath = imagePath;
        this.extent = null;
        this.image = null;
    }

    protected Image getImage(){
        if(image == null) {
            image = new Image(imagePath);
        }
        
        return image;
    }
    
    @Override
    public void draw(Point at) {
        getImage().draw(at);
    }

    @Override
    public void handleMouse(Event event) {
        getImage().handleMouse(event);
    }

    @Override
    public Point getExtent() {
        if(extent == null){
            extent = getImage().getExtent();
        }
        
        return extent;
    }

    /**
     * Wczytuje informacje i inicjalizuje składowe
     */
    @Override
    public void load(InputStream from) {
        //extent = from.read();
    }

    /**
     * Zapisujemy do strumienia dane z pamięci podręcznej - rozmiar rysunku i nazwę pliku
     */
    @Override
    public void save(OutputStream to) {
        //to.write(extent);
        //to.write(imagePath);
    }

}
