package adapter.myexample1;

public interface Car {

    void startEngineByTurningKeys();

    void signalLeftWithLight();

    void signalRightWithLight();

    void workAsTaxi();
}
