package adapter.myexample1;

/**
 * Adaptee
 */
public class Trike {
    
    public void startEngineByKickingEngine(){
        System.out.println("startEngineByKickingEngine");
    }
    
    public void signalLeftWithHand(){
        System.out.println("signalLeftWithHand");
    }
    
    public void signalRightWithHand(){
        System.out.println("signalRightWithHand");
    }
    
    public void transportHayOnTrailer() {
        System.out.println("transportHayOnTrailer");
    }
    
}
