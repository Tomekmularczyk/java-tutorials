package adapter.myexample1;

public class CarAdapter2 implements Car {
    private final Trike trike;

    public CarAdapter2(Trike trike) {
        this.trike = trike;
    }
    
    @Override
    public void startEngineByTurningKeys() {
        trike.startEngineByKickingEngine();
    }

    @Override
    public void signalLeftWithLight() {
        trike.signalLeftWithHand();
    }

    @Override
    public void signalRightWithLight() {
        trike.signalRightWithHand();
    }

    @Override
    public void workAsTaxi() {
        System.out.println("Trike working as a taxi");
    }

}
