package adapter.myexample1;

public class Test {
    public static void main(String[] args) {
        testTheCar(new CarImpl());
        testTheCar(new CarAdapter());
        testTheCar(new CarAdapter2(new Trike()));
    }
    
    private static void testTheCar(Car car){
        car.startEngineByTurningKeys();
        car.signalLeftWithLight();
        car.signalRightWithLight();
        car.workAsTaxi();
    }
}
