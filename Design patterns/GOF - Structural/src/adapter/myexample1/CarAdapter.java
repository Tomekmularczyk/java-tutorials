package adapter.myexample1;

/**
 * Adapter klasowy,
 */
public class CarAdapter extends Trike implements Car{

    @Override
    public void startEngineByTurningKeys() {
        startEngineByKickingEngine();
    }

    @Override
    public void signalLeftWithLight() {
        signalLeftWithHand();
    }

    @Override
    public void signalRightWithLight() {
        signalRightWithHand();
    }

    @Override
    public void workAsTaxi() {
        System.out.println("Specific implementation for Trike");
    }

}
