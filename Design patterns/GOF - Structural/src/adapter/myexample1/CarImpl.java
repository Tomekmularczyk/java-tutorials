package adapter.myexample1;

public class CarImpl implements Car{

    @Override
    public void startEngineByTurningKeys() {
        System.out.println("startEngineByTurningKeys");
    }

    @Override
    public void signalLeftWithLight() {
        System.out.println("signalLeftWithLight");
    }

    @Override
    public void signalRightWithLight() {
        System.out.println("signalRightWithLight");
    }

    @Override
    public void workAsTaxi() {
        System.out.println("workAsTaxi");
    }


}
