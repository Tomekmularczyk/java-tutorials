package adapter.book_example;

import javafx.scene.effect.Light.Point;

/**
 * ADAPTEE - klasa którą trzeba zaadoptować
 */
public class TextView {
    /**
     * @return punkt poczatkowy
     */
    public Point getOrigin(){
        return null; 
    }
    
    /**
     * @return zwraca wysokość i szerokość 
     */
    public Point getExtent(){
        return null;
    }
    
    public boolean isEmpty(){
        return true;
    }
}
