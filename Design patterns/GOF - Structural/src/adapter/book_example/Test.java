package adapter.book_example;

/**
 * Adapter używamy gdy potrzebujemy interfejs jednej klasy przekształcić na interfejs drugiej klasy,
 * czyli najczęściej gdy chcemy by jedna klasa udawała inną.
 */
public class Test {
    public static void main(String[] args) {
        Shape shape = new ShapeImpl();
        useShape(shape);
        
        TextShape textShape = new TextShape();
        useShape(textShape);
    }

    private static void useShape(Shape shape) {
        
    }
}
