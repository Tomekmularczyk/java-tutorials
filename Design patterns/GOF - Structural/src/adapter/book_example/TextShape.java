package adapter.book_example;

import adapter.book_example.ShapeImpl.Manipulator;
import javafx.scene.effect.Light.Point;

/**
 * ADAPTER KLASOWY - wykorzystujący wielodziedziczenie.
 * Jedna gałąź dziedziczy interfejs a druga implementacje.
 */
public class TextShape extends TextView implements Shape{

    /**
     * przekształcamy interfejs klasy TextView tak by pasował do interfejsu Shape
     */
    @Override
    public void boundingBox(Point botomLeft, Point topRight) {
        Point origin = getOrigin();
        Point extent = getExtent();
        
        botomLeft.setX(origin.getX());
        botomLeft.setY(origin.getY());
        topRight.setX(extent.getX() + extent.getY());
        topRight.setY(origin.getX() + extent.getY());
    }

    /**
     * Tworzymy od podstaw metodę createManipulator ponieważ TextView jej nie udostępnia
     */
    @Override
    public Manipulator createManipulator() {
        return new TextManipulator(this);
    }
    
    private class TextManipulator extends Manipulator {
        
        public TextManipulator(Shape shape) {
        }
    }
}
