package adapter.book_example;

import adapter.book_example.ShapeImpl.Manipulator;
import javafx.scene.effect.Light.Point;

/**
 * Target
 */
public interface Shape {

    void boundingBox(Point botomLeft, Point topRight);

    /**
     * tworzy manipulator do przekształcania obiektów. Jest to metoda wytwórcza.
     */
    Manipulator createManipulator();

}
