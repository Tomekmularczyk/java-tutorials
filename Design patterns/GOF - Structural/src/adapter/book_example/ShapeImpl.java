package adapter.book_example;

import javafx.scene.effect.Light.Point;

/**
 * TARGET
 */
public class ShapeImpl implements Shape {
    
    @Override
    public void boundingBox(Point botomLeft, Point topRight) {
        //ramka ograniczająca wyznaczone przez przeciwległe wierzchołki - inaczej niż w TextView
    }

    @Override
    public Manipulator createManipulator() {
        return new Manipulator(); //zwraca manipulator pozwalający graficznie zmieniać shape
    }

    public static class Manipulator {

    }
}
