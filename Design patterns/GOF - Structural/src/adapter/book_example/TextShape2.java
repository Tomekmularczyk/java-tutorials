package adapter.book_example;

import adapter.book_example.ShapeImpl.Manipulator;
import javafx.scene.effect.Light.Point;

/**
 * ADAPTER OBIEKTOWY - stosuje się składanie obiektów do połączenia klas o różnych interfejsach, dzięki czemu zapewniona jest większa
 * elastyczność ponieważ klasa delegowana może być różnego podtypu.
 */
public class TextShape2 implements Shape {

    private final TextView textView; //większa elastyczność, bo może byc różnego podtypu

    public TextShape2(TextView textView) {
        this.textView = textView;
    }

    @Override
    public void boundingBox(Point botomLeft, Point topRight) {
        Point origin = textView.getOrigin();
        Point extent = textView.getExtent();

        botomLeft.setX(origin.getX());
        botomLeft.setY(origin.getY());
        topRight.setX(extent.getX() + extent.getY());
        topRight.setY(origin.getX() + extent.getY());
    }

    /**
     * Tworzymy od podstaw metodę createManipulator ponieważ TextView jej nie udostępnia
     */
    @Override
    public Manipulator createManipulator() {
        return new TextManipulator(this);
    }

    private class TextManipulator extends Manipulator {

        public TextManipulator(Shape shape) {
        }
    }

}
