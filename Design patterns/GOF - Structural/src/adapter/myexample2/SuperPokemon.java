package adapter.myexample2;

/**
 * Target
 */
public interface SuperPokemon {
    
    void fireAttack();
    
    void waterAttack();
    
    void electricAttack();
    
}
