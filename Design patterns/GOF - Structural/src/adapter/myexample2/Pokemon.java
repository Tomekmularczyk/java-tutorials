package adapter.myexample2;

/**
 * Adaptee
 */
public interface Pokemon {
    
    void attack();
    
}
