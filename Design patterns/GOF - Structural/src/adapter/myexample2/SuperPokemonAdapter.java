package adapter.myexample2;

/**
 * Object Adapter + Bridge
 */
public class SuperPokemonAdapter implements SuperPokemon{
    private final Pokemon flyingPokemon, waterPokemon, electricPokemon;
    
    public SuperPokemonAdapter(Pokemon flyingPokemon, Pokemon waterPokemon, Pokemon electricPokemon) {
        this.flyingPokemon = flyingPokemon;
        this.waterPokemon = waterPokemon;
        this.electricPokemon = electricPokemon;
    }
    
    @Override
    public void fireAttack() {
        flyingPokemon.attack();
    }

    @Override
    public void waterAttack() {
        waterPokemon.attack();
    }

    @Override
    public void electricAttack() {
        electricPokemon.attack();
    }

}
