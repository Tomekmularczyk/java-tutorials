package adapter.myexample2;

/**
 * Object Adapter - modifies one interface to another
 */
public class Test {

    public static void main(String[] args) {
        Pikachu pikachu = new Pikachu();
        Vaporeon vaporeon = new Vaporeon();
        Charizard charizard = new Charizard();
        SuperPokemonAdapter superPokemon = new MotherFuckerCocksucker(charizard, vaporeon, pikachu);
        destroy(superPokemon);
    }

    public static void destroy(SuperPokemon superPoke) {
        superPoke.fireAttack();
        superPoke.waterAttack();
        superPoke.electricAttack();
    }
}
