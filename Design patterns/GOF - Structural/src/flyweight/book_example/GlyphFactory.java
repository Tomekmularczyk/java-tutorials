package flyweight.book_example;


/**
 * FlyweightFactory
 * - tworzy obiekty pyłki i zarządza nimi,
 * - gwarantuje, że pyłki są prawidłowo współużytkowane. Jeśli kient zażąda pyłku, fabryka udostępni istniejący egzemplarz lub stworzy nowy jeśli nie ma.
 */
public class GlyphFactory {
    private final int NCHARCODES = 128;
    private final Character[] characters;

    public GlyphFactory() {
        characters = new Character[NCHARCODES];
    }
    
    
    public Character createCharacter(char c){
        if(characters[c] == null){
            characters[c] = new Character(c);
        }
        
        return characters[c];
    }
    
    
    /**
     * Poniższe opracje po prostu tworzą nowe obiekty ponieważ glify nieznakowe nie są współużytkowwane
     */
    public Row createRow(){
        return new Row();
    }
    
    public Column createColumn(){
        return new Column();
    }
}
