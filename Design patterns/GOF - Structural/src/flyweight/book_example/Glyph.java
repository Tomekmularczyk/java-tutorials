package flyweight.book_example;

import java.awt.Font;
import java.awt.Window;
import javax.naming.Context;

/**
 * Flyweight (+ Composite)
 * - obejmuje deklaracje interfejsu, przez który pyłki mogą otrzymywać zewnętrzny stan i wykorzystywać go do działania.
 */
public interface Glyph {
    

    
    void draw(Window window, Context glyphContext);
    void setFont(Font font, Context glyphContext);
    Font getFont(Context glyphContext);
    void first(Context glyphContext);
    void next(Context glyphContext);
    boolean isDone(Context glyphContext);
    Glyph current(Context glyphContext);
    
    void insert(Glyph glyph, Context glyphContext);
    void remove(Context glyphContext);
}
