package flyweight.book_example;


/**
 * Flyweight. 
 * Wykorzystuje współdzielenie do wydajnej obsługi dużej liczby małych obiektów. 
 * Jeżeli jakiś obiekt był już utworzony to zamiast tworzyć nowy podaje się klientowi istniejący. Wpływa to znacząco na wydajność i zużycie pamięci.
 * 
 * Działanie pyłków nie może być zależne od kontekstu i nie powinno dać się go odróżnić od niewspółdzielonych obiektów.
 * Kluczowe jest tu rozróżnienie na stan wewnętrzny i zewnętrzny. 
 * - stan wewnętrzny jest zapisany w pyłku (obiekt jest immutable)
 * - stan zewnętrzny jest zależny od kontekstu dlatego nie można go współużytkować. Obiekty klienckie odpowiadają za przekazanie stanu zewnętrznego.
 * 
 * Wzorca pyłek należy stosować gdy spełnione są wszystkie poniższe warunki:
 * - aplikacja korzysta z dużej liczby obiektów,
 * - koszty przechowywania obiektów są wysokie ze względu na samą ich liczbę,
 * - większość stanu obiektów można zapisać poza nimi,
 * - po przeniesieniu stanu za zewnątrz wiele grup obiektów można zastąpić stosunkowo nielicznymi obiektami współużytkowanymi.
 * - aplikacja nie jest zależna od tożsamości obiektów (czyli np. do jakiego konkretnie miejsca w pamięciu się odwółują).
 * 
 * Klienty nie powinny same tworzyć obiektów ConcreteFlyweight, muszą je otrzymywać wyłącznie za pomocą FlyweightFactory co gwarantuje,
 * że obiekty będą prawidłowo współużytkowane.
 * 
 */
public class Test {
    //klient przechowuje referencje do pyłków, oraz oblicza lub przechowuje zewnętrzny stan pyłków
}
