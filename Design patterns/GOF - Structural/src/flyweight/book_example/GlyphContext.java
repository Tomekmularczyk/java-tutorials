package flyweight.book_example;

import java.awt.Font;

/**
 * Aby uniknąć przydzielania pamięci na czcionkę w każdym obiekcie, zapiszemy ją zewnętrznie. Parametr GlyphContext jest przekazywany do
 * każdej opracji która potrzebuje informacji o czcionce glifu w danym miejscu.
 */
public abstract class GlyphContext {
    private int index;
    //private BTree bTree; //przechowuje odwzorowanie glifów na czcionki
    
    public abstract void next(int step);
    
    public abstract void insert(int quantity);
    
    public abstract Font getFont();
    
    public abstract void setFont(Font font, int span);
}
