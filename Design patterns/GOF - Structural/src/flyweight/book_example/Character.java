package flyweight.book_example;

import java.awt.Font;
import java.awt.Window;
import javax.naming.Context;

/**
 * ConcreteFlyweight
 * - obejmuje implementacje interfejsu i przechowuje stan wewnętrzny(jeśli jest potrzebny)
 * - zapisany w nich stan musi być wewnetrzny, tzn. nie może zależeć od kontekstu działania takich obiektów
 */
public class Character implements Glyph{
    private final char character;

    public Character(char character) {
        this.character = character;
    }

    @Override
    public void draw(Window window, Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void setFont(Font font, Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Font getFont(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void first(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void next(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public boolean isDone(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Glyph current(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void insert(Glyph glyph, Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void remove(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }
    
}
