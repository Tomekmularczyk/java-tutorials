package flyweight.book_example;

import java.awt.Font;
import java.awt.Window;
import javax.naming.Context;

/**
 * UnsharedConcreteFlyweight - niewspółużytkowany pyłek konkretny - nie wszystkie klasy Flyweight muszą być współużytkowane. Interfejs klasy
 * Flyweight umożliwia współużytkowanie ale go nie wymusza.
 */
public class Column implements Glyph {

    @Override
    public void draw(Window window, Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void setFont(Font font, Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Font getFont(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void first(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void next(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public boolean isDone(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public Glyph current(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void insert(Glyph glyph, Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void remove(Context glyphContext) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

}
