package flyweight.my_example2;

/**
 * Concrete Flyweight
 */
public class Number implements Flyweight{
    private final int number;

    public Number(int number) {
        this.number = number;
    }
    
    @Override
    public int add(int howMuch){
        return number + howMuch;
    }
    
    @Override
    public int substract(int howMuch){
        return number - howMuch;
    }
    
    @Override
    public int devide(int by){
        return number / by;
    }

    @Override
    public String toString() {
        return "Number " + number;
    }

    @Override
    public void drawSomething(SomeContext context) {
        //…
    }
    
    
}
