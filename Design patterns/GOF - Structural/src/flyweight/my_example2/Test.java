package flyweight.my_example2;

import static java.lang.System.out;
import java.time.Duration;
import java.time.Instant;
import java.util.Random;

/**
 * Koszty związane z pamięcią są mniejsze na rzecz zuużycia procesora
 */
public class Test {

    public static void main(String[] args) {
        Duration duration1 = createObjectEachTime(NR_OF_OBJECTS);
        out.println("Stworzenie " + NR_OF_OBJECTS + " zajęło " + duration1.getSeconds() + " sekund i " + duration1.getNano() + " nano sekund.");

        Duration duration2 = createObjectWithFlyweightFactory(NR_OF_OBJECTS);
        out.println("Stworzenie " + NR_OF_OBJECTS + " zajęło " + duration2.getSeconds() + " sekund i " + duration2.getNano() + " nano sekund.");
    }

    private static final int NR_OF_OBJECTS = 100000000;

    private static Duration createObjectEachTime(int howMany) {
        Instant before = Instant.now();
        new Random().ints(howMany, 0, 100)
                .forEach(Number::new);
        Instant after = Instant.now();
        return Duration.between(before, after);
    }

    private static Duration createObjectWithFlyweightFactory(int howMany) {
        Instant before = Instant.now();
        new Random().ints(howMany, 0, 100)
                .forEach(NumberFactory.Instance::getNumber);
        Instant after = Instant.now();
        return Duration.between(before, after);
    }
}
