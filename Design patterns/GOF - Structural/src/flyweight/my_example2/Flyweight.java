package flyweight.my_example2;

/**
 * Flyweight
 */
public interface Flyweight {
    
    int add(int howMuch);
    
    int substract(int howMuch);
    
    int devide(int by);
    
    void drawSomething(SomeContext context);
    
}