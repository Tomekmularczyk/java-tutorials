package flyweight.my_example2;

import java.util.HashMap;
import java.util.Map;

/**
 * FlyweightFactory
 */
public enum NumberFactory {
    Instance;
    
    private final Map<Integer, Number> numbers;
    private NumberFactory() {
        this.numbers = new HashMap<>();
    }
    
    public Number getNumber(int number){
        Number num = numbers.get(number);
        if(num == null){
            Number newNumber = new Number(number);
            numbers.put(number, newNumber);
            return newNumber;
        }
        
        return num;
    }
    
}
