package flyweight.my_example1;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Random;

public class Test {

    private static final int NR_OF_OBJECTS = 10000000;

    public static void main(String[] args) throws IOException {
        System.out.println("Zmierzymy czas tworzenia " + NR_OF_OBJECTS + " pyłków przez wirtualną maszynę: ");
        System.out.println("Wciśnij enter…");
        System.in.read();

        Duration period = createObjectsAndMeasureTime(NR_OF_OBJECTS);
        printTestResults(period);

        System.out.println("A teraz zmierzymy czas tworzenia pyłków przy pomocy FlyweightFactory, która kontroluje współdzielenie obiektów.");
        System.out.println("Wciśnij enter…");
        System.in.read();

        
        Duration periodWithFactory = createObjectsWithFlyweightFactoryAndMeasureTime(NR_OF_OBJECTS);
        printTestResults(periodWithFactory);
    }

    public static Duration createObjectsAndMeasureTime(int numberOfObjects) {
        Instant start = Instant.now();
        new Random()
                .ints(numberOfObjects, 0, ColorType.values().length)
                .forEach(Test::createSquare);
        Instant finish = Instant.now();

        return Duration.between(start, finish);
    }
    
    public static Duration createObjectsWithFlyweightFactoryAndMeasureTime(int numberOfObjects){
        Instant start = Instant.now();
        new Random()
                .ints(numberOfObjects, 0, ColorType.values().length)
                .forEach(Test::createSquareWithFactory);
        Instant finish = Instant.now();
        
        return Duration.between(start, finish);
    }
    
    /**
     * Tworzy za każdym razem nowy obiekt
     */
    private static void createSquare(int value) {
        Square square = new Square(ColorType.getColorByIndex(value));  
        square.draw(1, 1, 20.5);
    }
    
    /**
     * Tworzy obiekty przy pomocy fabryki, która jeżeli posiada już podany obiekt, nie tworzy nowego a zwraca istniejący
     */
    private static void createSquareWithFactory(int value){
        Shape square = ShapeFactory.Instance.getSquare(ColorType.getColorByIndex(value));
        square.draw(1, 1, 20.5);
    }

    private static void printTestResults(Duration duration) {
        System.out.printf("\nCzas utworzenia %d obiektów typu Square wyniósł: %d sekund i %d nanosekund.\n",
                NR_OF_OBJECTS,
                duration.getSeconds(),
                duration.getNano());
    }
}
