package flyweight.my_example1;

/**
 * Flyweight
 */
public abstract class Shape {
    private final Color color; //wewnętrzny stan

    public Shape(ColorType colorType){
        this.color = new Color(colorType);
    }
    
    public abstract void draw(int x, int y, double size); //stan zewnętrzny jest przekazywany
    
    protected Color getColor(){
        return color;
    }
    
    
    public static class Color {
        private final ColorType type;

        public Color(ColorType type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type.name().toLowerCase();
        }
    }
    
}
