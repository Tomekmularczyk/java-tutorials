package flyweight.my_example1;

import java.util.HashMap;
import java.util.Map;

public enum ColorType {
    WHITE(0), GREEN(1), YELLOW(2), BLACK(3), RED(4), ORANGE(5), PURPLE(6), GRAY(7);
    
    public final int index;
    private ColorType(int index){
        this.index = index;
    }
    
    private static final Map<Integer, ColorType> types;
    static {
        types = new HashMap<>();
        for (ColorType type : ColorType.values()) {
            types.put(type.index, type);
        }
    }
    public static ColorType getColorByIndex(int index){
        return types.get(index);
    }
}
