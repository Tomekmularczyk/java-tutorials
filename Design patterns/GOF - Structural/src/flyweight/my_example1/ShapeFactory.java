package flyweight.my_example1;

/**
 * FlyweightFactory - niezbędna do prawidłowego współużytkowania pyłków
 */
public enum ShapeFactory {
    Instance;
    
    private final Square[] coloredSquares;
    private ShapeFactory() {
        coloredSquares = new Square[ColorType.values().length];
    }
    
    public Shape getSquare(ColorType color){
        if(coloredSquares[color.index] == null){
            coloredSquares[color.index] = new Square(color);
        }
        
        return coloredSquares[color.index];
    }
}
