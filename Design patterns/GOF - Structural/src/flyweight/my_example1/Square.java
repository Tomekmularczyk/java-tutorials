package flyweight.my_example1;

/**
 * ConcreteFlyweight
 */
public class Square extends Shape{

    public Square(ColorType colorType) {
        super(colorType);
    }

    @Override
    public void draw(int x, int y, double size) {
        //drawind something 
    }

}
