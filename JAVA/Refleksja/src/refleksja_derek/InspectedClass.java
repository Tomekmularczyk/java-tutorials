package refleksja_derek;

import java.io.Serializable;
import java.util.Arrays;

public final class InspectedClass implements Cloneable, Serializable{
	private static final int A = 5;
	protected float B = 4.5f;
	public volatile String name;
	int C;
	
	private InspectedClass(String name, int C){
		this.name = name;
		this.C = C;
	}
	public InspectedClass(int C){
		this("Inspected", C);
	}
	
	private strictfp double getHalf(double value){ //strictfp zapewnia taki sam wynik na każdej platformie. Przydaje sie dla naukowych obliczeń.
													// można tak zadeklarować także całą klasę bądź interfejs
		return value / 2;
	}
	
	int getTheA(){
		return A;
	}
	
	public String smth(Object... objects){
		return Arrays.toString(objects);
	}
	
	protected synchronized Class getClassObject(){
		return this.getClass();
	}
	
	@Override
	public String toString(){
		return String.format("A = %d, B = %f, name = %s, C(passed in constructor) = %d", A,B,name,C);
	}
	
	private abstract class PrivateAbstractInnerClass{
		
	}
}
