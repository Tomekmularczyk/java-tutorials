package refleksja_derek;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Test {

	public static void main(String[] arg){
		Class reflectionClass = InspectedClass.class;
		
		out("======= NAZWA KLASY =======");
		out(reflectionClass.getName());
		
		out("======= Modyfikatory =======");
		int modifiers = reflectionClass.getModifiers();
		out(Modifier.toString(modifiers));
		
		out("======= Kanoniczna nazwa klasy nadrzędnej =======");
		Class superClass = reflectionClass.getSuperclass();
		out(superClass.getCanonicalName());
		
		out("======= Zaimplementowane interfejsy =======");
		Class[] interfaces = reflectionClass.getInterfaces();
		out(Arrays.toString(interfaces));
		
		out("======= Metody, ich parametry i typy zwracane =======");
		Method[] methods = reflectionClass.getMethods();
		for(Method method : methods){
			String methodName = method.getName();
			String returnType = method.getReturnType().getSimpleName();
			String parametersType = Arrays.toString(method.getParameterTypes());
			outf("Method name: %s, return type: %s, parameters: %s\n", methodName, returnType, parametersType);
		}
		
		out("======= Konstruktory klasy =======");
		Constructor[] constructors = reflectionClass.getConstructors();
		for(Constructor constr : constructors){
			out(constr.toString());
			out(Arrays.toString(constr.getParameterTypes()));
		}
		
		out("======= Tworzenie obiektu przy pomocy konstruktora =======");
		InspectedClass objectIC = null;
		try {
			Constructor constructor = reflectionClass.getConstructor(int.class);
			objectIC = (InspectedClass) constructor.newInstance(5);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		out(objectIC.toString());
		
		out("======= dostęp do prywatnych pól klasy =======");
		try {
			Field privateField = InspectedClass.class.getDeclaredField("A");
			privateField.setAccessible(true);
			int valueOfPrivateA = (int) privateField.get(objectIC);
			
			out("prywatne pole int A wynosi: " + valueOfPrivateA);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		out("======= dostęp do prywatnych metod klasy =======");
		try {
			Method method = InspectedClass.class.getDeclaredMethod("getHalf", double.class);
			method.setAccessible(true);
			
			double result = (double) method.invoke(objectIC, 433); //jesli metoda nie miała by parametrów, podalibyśmy null
			out("Wynik wywołania prywatnej metody " + method.getName() + "(433) = " + result);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	
	//=====================================================================
	public static void outf(String string, Object... args){
		System.out.printf(string, args);
	}
	public static void out(String string){
		System.out.println(string);
	}
}
