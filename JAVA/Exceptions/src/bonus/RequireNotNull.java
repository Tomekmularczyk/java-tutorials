package bonus;

import java.util.Objects;

/**
 * @author tomek
 */
public class RequireNotNull {
    /*
        Objects.requireNotNull wyrzuca NullPointerException jeżeli obiekt jest nullem.
        Nie wydaje się to dużym usprawnieniem, ale potem patrząc na ślad stosu widać odrazu co poszło nie tak.
    */
    
    public static void main(String[] args) {
        Integer value = null;
        Objects.requireNonNull(value, "nie może być nullem bo bedziesz chujem");
    }
}
