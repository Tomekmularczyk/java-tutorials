package _3_przechwytywanie_wyjatku;
import static java.lang.System.*;
import java.util.stream.IntStream;
import model.MyException;
import model.MyRuntimeException;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        Test test = new Test();
        
        try{
            test.doSomething("show tits");
        } catch(MyException ex){
            err.println(ex);
        }
        
        out.println("Po obsłudze wyjątku!");
        
        try {
            test.doSomething("play footbal");
            out.println("ten println się nie wyświetli ponieważ po wykonaniu wcześniejszej metody została uruchomiona obsługa wyjątków");
        } catch ( Exception ex ) {  //możemy wyłapywać bardziej ogólny typ wyjątków
            err.println(ex.toString());
        }
        
        try {
            test.flyAway(3);
        } catch (RuntimeException ex){ //nie musimy ponieważ jest to runtime exception, ale jeżeli zależy nam by program dalej działał…
            err.print(ex);
        }
        
        test.flyAway(3); //zostanie wyrzucony NullPointerException który rozszerza RuntimeException i nie trzeba go wychwytywać. Zazwyczaj są to błędy programisty
        //kod poniżej się już nie wykona ponieważ został wyrzucony nie przewidziany wyjątek
        
        IntStream.range(1, 10).forEach(i -> out.println(i + "")); 
        
        
    }
    
    private void doSomething(String task) throws MyException{
        //doing something bad
        throw new MyException("Failed to complete: " + task);
    }
    
    private int flyAway(int howHigh){
        out.printf("we are flyin %d feets high!\n", howHigh);
        throw new MyRuntimeException("Ups! sufit!");
        //w tym miejscu nic się nie wykona, ponieważ kompilator nigdy nie dojdzie do tego miejsca
        
        //return howHigh * 2;
    }
}
