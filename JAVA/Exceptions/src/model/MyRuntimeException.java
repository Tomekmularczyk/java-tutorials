package model;
/**
 * @author tomek
 */
public class MyRuntimeException extends RuntimeException{
    public MyRuntimeException(String str){
        super(str);
    }
}
