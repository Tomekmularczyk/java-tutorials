package _2_deklarowanie_wyjatkow_rzucanych;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.activation.ActivationException;
import java.util.function.BiFunction;
import org.omg.CORBA.UserException;

/**
 * @author tomek
 */
public class Test {

    /*
        Złotą zasadą wyjątków jest wyrzucaj wcześnie, przechwytuj późno.
        Wielu programistów czuje potrzebę obsługi każdego wyjątku w metodze w której jest wyrzucany
        zamiast pozwolić by był obsłużony "wyżej". Deklarowanie wyjątków nie stanowi żadnej ujmy dla programisty.
     */
    //--------------- Przeładowywanie metod wyrzucających wyjątki
    public static double divide(double a, double b) {
        return a / b;
    }

    public static double divide(int a, double b) throws Exception {  //nie ma problemu, to dwie różne metody
        if (b == 0) {
            throw new Exception();
        }
        return a / b;
    }

    //-------------- Przesłanianie metod wyrzucających wyjątki
    static class Doktor {
        public void cure() {
        }
        public void investigate() throws IOException {
        }
    }

    static class Chirurg extends Doktor {
//        @Override
//        public void cure() throws Exception{  //metoda przesłaniana nie może wyrzucać więcej wyjątków niż w klasie nadrzędnej…
//            super.cure();
//        }
        @Override
        public void investigate() { //… za to z metodzie przesłaniającej możemy deklarować mniej wyjątków
            //super.investigate();//Deklarowane w przesłanianej metodzie wyjątki, nie są dziedziczone, trzeba je zawsze deklarować
        }
    }

    BiFunction<Integer, Double, Double> function = (i,d) -> { //nie można deklarować wyjątków w wyrażeniu lambda…
        //return divide(i,d); //… jeżeli jednak jakaś metoda wyrzuca wyjątek trzeba go obsłużyć w bloku
        return null;
    };
    
//    Function<String,Integer> function2 = new Function<String, Integer>(){ 
//        @Override
//        public Integer apply(String t) throws Exception{ //tak jak poprzednio - przesłaniana metoda nie może wyrzucać więcej wyjątków niż oryginalna
//            throw new Exception();
//        }
//    };
    
    
    private interface Zonk {
        void doSome() throws ActivationException, UserException, URISyntaxException;
    }
    
    //Zonk zonk = () throws ActivationException ->  {}; nie da rady
}
