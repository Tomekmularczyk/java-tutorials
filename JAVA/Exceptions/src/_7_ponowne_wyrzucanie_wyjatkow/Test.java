package _7_ponowne_wyrzucanie_wyjatkow;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) throws Exception {
        //czasem gdy nie wiesz co zrobić z wyjątkiem możesz go np. zalogować i wyrzucić wyżej
        try{
            doSomething();
        }catch(Exception exception){
            exception.printStackTrace();
            throw exception;
        }
        
        System.out.println("==================================");
        //czasami potrzebne jest zmienienie typu łapanego wyjątku na inny bardziej zrozumiały, ogólny 
        //lub gdy metoda w której się wyjątek pojawia nie może wyrzucać wyjątków kontrolowanych
        try {
            readMyEyes();
        } catch (EyesReadingException ex){
            throw new UnsupportedOperationException("Reading eyes is not supported yet", ex); //podajemy przyczynę w konstruktorze (cause)
        }
    }
    
    
    private static void doSomething() throws IOException {
        try {
            //throw new DataFormatException(); //nie przeszłoby, trzeba by było zadeklarować albo wyłapać
            throw new FileNotFoundException(); 
        } catch (Exception ex){
            throw ex; 
            //wydawać by się mogło, że potrzebne będzie zmodyfikowanie throws na throws Exception,
            //…przecież dochodzi do niebezpiecznego rzutowania z typu ogólnego na szczegółowy.
            //jednak kompilator Javy śledzi klauzulę try i widzi, jakiego typu może być "ex".
            //jeżeli by wyrzucany był inny wyjątek niż IOException - kompilator by zaprotestował
        }
    }
    
    
    static class EyesReadingException extends IOException{}
    private static void readMyEyes() throws EyesReadingException{
        throw new EyesReadingException();
    }
    
    
}
