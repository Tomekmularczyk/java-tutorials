package _4_przechwytywanie_wyjatkÓW;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.System.*;
import java.nio.channels.ClosedChannelException;
import java.util.Random;
import java.util.zip.ZipException;

/**
 * @author tomek
 */
public class Test {

    private static void calculate() throws FileNotFoundException, EOFException, ClosedChannelException, ZipException {
        //gdy mamy wyjątki ze wspólną nadklasą, lepiej nieraz jest zadeklarować bardziej ogólny wyjątek tutaj: IOException
        int _1_to_4 = new Random().ints(1, 5).findFirst().getAsInt();
        switch (_1_to_4) {
            case 1:
                throw new FileNotFoundException();
            //break; //w tym wypadku też nie trzeba returna skoro jest wyrzucany wyjątek!
            case 2:
                throw new EOFException();
            case 3:
                throw new ClosedChannelException();
            case 4:
                throw new ZipException();
            default:
                throw new RuntimeException("cos sie popsuło...");
        }
    }

    public static void main(String[] args) {
        //możemy wyłapać ich wspólną klasę nadrzędną…
        try {
            calculate();
        } catch (IOException ex) { //możemy wyłapać odrazu bardziej ogólny wyjątek
            err.println(ex);  //…natomiast tracimy szczegółową informacje na temat wyrzucanego typu
        }

        //lub możemy obsłużyć wszystkie wyjątki po kolei…
        try {
            calculate();
        } catch (FileNotFoundException ex) {
            err.println(ex);
        } catch (EOFException ex) { //możemy nadawać zmiennym te samą nazwę!
            err.println(ex);
        } catch (ClosedChannelException ex) {
            err.println(ex);
        } catch (ZipException ex) {
            err.println(ex);
        }

        //lub użyć multicatcha…
        try {
            calculate();
        } catch (FileNotFoundException | EOFException | ClosedChannelException | ZipException exc) { //exc będzie zamienione przez kompilator na najbliższy wspólny typ
            if (exc instanceof IOException) {
                out.println("TRUE!");
            }
            workWithIOException(exc); //java sprawdziła że wszystkie klasy mają te klasę jako nadrzędną, gdyby tak nie było zgłosiła by błąd, jak poniżej…
        }
        //kompilator rzutuje zmienną w multicatchu na najbliższy wspólny typ nadrzędny
        try {
            calculate();
        } catch (IOException | NumberFormatException ex) {
            //workWithIOException(ex); //nie da rady, metoda oczekuje IOException, a kompilator znalazł najbliższy wspólny typ: Exception
        }

        //możemy też obsłużyć wyjątki częściowo…
        try {
            calculate();
        } catch (ZipException ex) { //łapiemy jeden interesujący nas wyjątek
            out.println("Cholera jasna! ZipException!");
            err.println(ex);
        } catch (IOException ex) { //a reszte obsługujemy standardowo
            err.println(ex);
        }

        //natomiast poniższe nie ma sensu…
        try {
            calculate();
        } catch (IOException ex) {
            //działanie metody zostaje przerwane, więc na darmo nam pisać kolejne catche
        } //catch (ZipException ex) {
//            //exception has already been caught!!!
//        }

        //Wyrażenia catch są przeszukiwane od góry do dołu, dlatego na początku należy umieścić
        //klasy najbardziej precyzyjnie wskazujące rodzaj błędu.
    }

    private static void workWithIOException(IOException ex) {
    }
}
