package _1_wyrzucanie_wyjatkow;

/**
 * @author tomek
 */
public class Test {
    /*
        W hierarchi klas wyjątków na samej górze jest klasa Throwable. Dzieli się ona 2 subklasy Error i Exception.
        - Throwable  -  nie powinniśmy rozszerzać tej klasy, a klase Exception
            - Error  -  błędy sprzętowe, systemu, pamięci itp. coś nad czym niemamy kontroli
            - Exception  -  klasy, które po niej dziedziczą są wyjątkami kontrolowanymi
                - RuntimeException  -  klasy, które po niej dziedziczą są wyjątkami niekontrolowanymi i najczęściej są z winy programisty
        
        Stworzenie, wyrzucenie i obsługa wyjątków to dość kosztowna operacja, licząca nieraz tysiące instrukcji do wykonania,
        dlatego warto pamiętać by ich nie nadurzywać. 
    
    
        Kiedy używać wyjątków niekontrolowanych a kiedy kontrolowanych? Zdania są mocno podzielone, jedni twierdzą że nie powinno wogóle używać się
        wyjątków kontrolowanych, tak jak to jest w C#. Jednak generalna zasada jest taka by używać wyjątków kontrolowanych do deklarowania metod
        których działanie może mimo najlepszych chęci nie udać się z jakiegoś względu. Np. użytkownik usunie plik, który trzeba odczytać.
    
        Generalnie powinno też unikać się tworzenia klas wyjątków, tylko po to by stworzyć klasę o nowej nazwie. 
        lepiej jest wyrzucić zwykły Exception z informacją lub w nowej klasie zaimplementować metody, które do czegoś się użytkownikowi przydadzą
    
        Jeżeli wyjątek nie zostanie nigdzie przechwycony to zostanie wyświetlony ślad wywołań metod ze stosu (stack trace),
        zaraz przed tym kiedy wystąpił wyjątek. Ślad stosu jest przesyłania do strumienia System.err.
        Zaraz potem jest kończony wątek w którym wyjątek wystąpił.
    */
    
    
    public static void main(String[] args) {
        double result = divide(3,0);
        System.out.println(result);

    }
    
    
    private static double divide(double a, double b){
        if( b == 0 ){
            throw new IllegalArgumentException("Nie dzielimy przez zero!");
        }
        return a / b;
    }
    
    private static class MyClass extends Throwable { //nie powinno się rozszerzać Throwable ani Error
    }
    private static class MyRuntimeException extends RuntimeException {} //tego wyjątku nie trzeba będzie jawnie wyłapywać
    
    private static class MyException extends Exception {} //ten wyjątek trzeba jawnie deklarować i obsługiwać
    
}
