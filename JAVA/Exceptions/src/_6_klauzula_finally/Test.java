package _6_klauzula_finally;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author tomek
 */
public class Test {

    /*
        Klauzuli finally używamy by upewnić się, że coś zostanie wykonane posprzątane niezaleznie czy zostanie wyrzucony wyjątek czy
        poprawnie zwrócona wartość.
        
     */
    public static void main(String[] args) {
        try {
            standardFinally();
        } catch (Exception e) {
            //przeniosłem kod do metody po to by nie przerywać mainu\
            System.err.println(e);
        }

        //podobnie w finally nie powinniśmy umieszczać return, ponieważ przesłoni ono return z bloku try
        System.out.println(doSomething() + " - finally zamieniło return z bloku try."); //zwróci 9 zamiast 25

        //a co jeśli close też wyrzuca wyjątek?? wtedy lepiej jest zagnieździć try-finally w try-catchu
        try {
            CorruptedResource corruptedResource = null;
            try {
                corruptedResource = CorruptedResource.open(""); //NullPointerException 
                throw new NullPointerException();
            } finally {
                 //nie musimy tego robić skoro i tak opatrzyliśmy ten kod zewnętrznym try-catchem
                corruptedResource.close(); //FileNotFoundException
            }
        } catch (Exception ex) {
            System.err.println(ex); //nie zadziałało tłumienie
        }
    }

    private static void standardFinally() {
        MyResource myResource = null; //niestety musimy tak to zrobić
        try {
            myResource = MyResource.open("/path/to/resource");
            //working…
            throw new UnsupportedOperationException();
        } finally {  //finally zostanie wywołanie niezależnie czy blok try zakończył się normalnie czy wyjątkiem
            if (myResource != null) {
                myResource.close(); //powinniśmy sprawdzić czy myResource nie jest nullem!
            }
            throw new NullPointerException();
            //nie powinniśmy rzucać wyjątków w klauzuli finally
            //mechanizm tłumienia wyjątków jak w try-with-resources nie działa w przypadku klauzuli finally,
            //tutaj jeżeli zostanie wyrzucony wyjątek to przesłoni on oryginalny wyjątek (a nie stłumi).
        }
    }

    private static double doSomething() {
        try {
            //działamy sobie normalnie
            return 5 * 5;
        } finally {
            return 3 * 3; //przesłania return z bloku try
        }
    }

    static class MyResource {

        public static MyResource open(String resourcePath) {
            if (resourcePath.isEmpty()) {
                return null;
            } else {
                return new MyResource();
            }
        }

        public void close() {
            //closing…
        }
    }

    static class CorruptedResource {
        public static CorruptedResource open(String resourcePath) {
            return new CorruptedResource();
        }

        public void close() throws IOException{
            throw new FileNotFoundException();
        }
    }
}
