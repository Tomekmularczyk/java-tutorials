package _5_try_z_zasobami;

import java.io.Closeable;
import java.io.IOException;
import static java.lang.System.*;
import java.util.Scanner;
import javax.swing.undo.CannotUndoException;

/**
 * @author tomek
 */
public class Test {

    /*
        Jeżeli pracujemy z jakimś zasobem jak plik i w trakcie zostanie rzucony wyjątek, to nie zostanie on zamknięty.
        Może to sprawić, że zmiany nie zostaną zapisane, albo powodować to inne problemy dla użytkownika.
        Jeżeli dana klasa implementuje AutoClosable albo podklasę Closeable to możemy użyć try-with-resources,
        które zadba by nawet jeżeli zostanie wyrzucony wyjątek to zostanie wywołana procedura zamykająca zasób.
     */
    public static void main(String[] args) {
        String str = "Vivamus sit amet venenatis ex";

//        try(Object obj = new Object()){   //musi implementować Autocloseable
//        }
        try (MyClass myClass = new MyClass()) {   //deklarujemy i tworzymy zmienną w nawiasie, będzie ona widoczna tylko w odrębie bloku
            //czy będzie wyjątek czy nie, to zasoby zostaną zamknięte na końcu bloku
        }

        //możemy stworzyć dowolną ilość obiektów w try z zasobami
        try (MyClass myClass = new MyClass(); Scanner scanner = new Scanner(str)) {
            //throw new UnsupportedOperationException(); 
            //procedura try zajmie się wywołaniem metody close();
            //zamykanie zasobów będzie w odwrotnej kolejności niż były tworzone FILO - najpierw scanner potem myClass
        }

        //a co jeśli wyjątek zostanie wyrzucony w metodzie close() zasobu?
        //…wtedy wyjątek zostaje przekazany do kodu wywołującego metodę
        try {
            try (CorruptedClass resource = new CorruptedClass()) {
                //pracujemy z zasobem
            } //zamykanie zasobu które kończy się wyjątkiem
        } catch (Exception ex) {
            err.println(ex);
        }

        //Opcjonalnie klauzula try-with-resources może mieć wyrażenia catch, a nawet finally
        try (MyClass myClass = new MyClass()) {
            throw new IOException();
        } catch (IOException ex) {
            out.println(ex);
        } finally{
            
        }

        //A co w sytuacji gdy wyjątek zostaje wyrzucony w trakcie pracy z zasobem, a potem przy próbie jego zamknięcia????
        //…wtedy Java uznaje że wyjątek powstały w trakcie pracy na zasobie jest ważniejszy i to jego przekazuje do kodu wywołującego metodę
        //…a wyjątek powstały w trakcie zamykania zasobu jest tłumiony (suppressed)
        try (CorruptedClass resource = new CorruptedClass()) {
            throw new NullPointerException();
        }//jest to przydatny mechanizm, który trudno by było samemu zaimplementować

    }

    static class MyClass implements AutoCloseable {

        @Override
        public void close() {
            out.println("Poprawne zamykanie zasobów");
        }
    }

    static class CorruptedClass implements Closeable {

        @Override
        public void close() {
            throw new CannotUndoException();
        }

    }
}
