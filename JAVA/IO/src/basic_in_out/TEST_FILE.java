package basic_in_out;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class TEST_FILE {

	public static void main(String[] args) {
		for(String name: new File(".").list())
			System.out.println(name);
		
		try(BufferedReader br = new BufferedReader(new FileReader("long_text.rtf"))){
			try(PrintWriter pr = new PrintWriter(new FileWriter("copy.txt"))){
				System.out.println("======== wczytalem i otworzylem nowy plik =============");
				
				String line = br.readLine();
				while(line != null){
					pr.println(line);
					line = br.readLine();
				}
			}
		}catch(IOException ex){
			
		}
	}

}
