package basic_in_out;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.Scanner;

public class Czytnik implements Readable{

	
	
	public static void main(String[] args) throws InterruptedException{
		Scanner scanner = new Scanner(new Czytnik());
		while(scanner.hasNext()){
			System.out.println(scanner.nextLine());
			Thread.sleep(300);
		}
	}

	@Override
	public int read(CharBuffer cb) throws IOException {
		for(int i=0; i<10; i++)
			cb.append((char) (Math.random()*70));
		return 0;
	}
}
