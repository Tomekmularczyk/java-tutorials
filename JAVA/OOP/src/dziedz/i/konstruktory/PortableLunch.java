package dziedz.i.konstruktory;

public class PortableLunch extends Lunch{	
	public PortableLunch(){
		System.out.println("Konstruktor PortableLunch");
		make();   //wywołujemy publiczną metodę w konstruktorze, jeżeli zostanie ona przesłonięta w klasie pochodnej, to zastosowane tutaj będzie wiązanie dynamiczne czyli polimorfizm
		          //a to znaczy że przy konstrukcji obiektu pochodnego zostanie wywołany konstruktor klasy bazowej wywołujący przesłoniętą metodę klasy pochodnej...
	}
	
	public void make(){
		//...a co jeśli ta przesłonięta metoda będzie korzystać ze składowych w swojej klasie pochodnej? przecież w tym momencie nie zostały jeszcze zainicjalizowane!
		System.out.println("PortableLunch make()");
	}
	
	@Override
	public void dispose(){
		System.out.println("Sprzątenie PortableLunch");
		super.dispose();
	}
}
