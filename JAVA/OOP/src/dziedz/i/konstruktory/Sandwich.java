package dziedz.i.konstruktory;

public class Sandwich extends PortableLunch{
	private int i = 666;
	
	Bread bread = new Bread();              //
	Cheese cheese = new Cheese();			// 2. Inicjalizowane są składowe 
	Lettuce lettuce = new Lettuce();		//
	public Sandwich(){
		System.out.println("Konstruktor Sandwich"); // 3. wykonywane jest ciało kontruktora
	}
	@Override
	public void dispose(){  //dodatkowa metoda sprzatająca. wykonujemy wszystko w odwrotnej kolejności
		System.out.println("==Sprzątenie Sandwich===");
		lettuce.dispose();
		cheese.dispose();
		bread.dispose();
		super.dispose();
	}
	@Override
	public void make(){
		//UWAGA! teraz przy konstrukcji Sandwich najpierw konstruowana jest klasa pochodna, która wywołuje metode make(),
		//z tym że nie wywoła swojej metody make, ale tę przesłoniętą!
		//problem jest w tym, że ta przesłonięta metoda operuje na jeszcze nie zainicjalizowanych zmiennych!!!
		System.out.printf("(PortableLunch make(), i = %d)\n", i);  //dlatego w konstruktorze o ile to możliwe należy wywowływać tylko mtody prywatne i finalne
	}
	
	public static void main(String[] args){
		Sandwich portableLunch = new Sandwich(); //1. njapierw wywoływane są konstruktory klas bazowych, zaczynając od korzenia
		portableLunch.dispose();
	}
	
}
