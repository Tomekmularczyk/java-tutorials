package kontrola_dostepu;

class OnlyThree { //this class allows to create only 3 instances of this object
	private static int count = 0;
	private int nr;
	private static OnlyThree jedynak = new OnlyThree(0); //do realizacji singleton pattern
	
	private OnlyThree(int nr){//konstruktor jest prywatny więc tylko my możemy tworzyć obiekty tej klasy(poprzez metode)
		this.nr = nr;
	}
	
	static OnlyThree takeOne() throws OutOfLimit{ //przydaje sie to gdy np chcemy cos z obiektem zrobic zanim go utworzymy lub ograniczyc liczbe obiektow
		if(count >= 3) throw new OutOfLimit();
		
		++count;
		return new OnlyThree(count);
	}
	
	int whichAmI(){
		return nr;
	}
	
	OnlyThree returnSigleton(){ //gdyby nie metoda takeOne() to ta klasa sprawiałaby że będzie tylko jeden jej egzemplarz uzyskany przez te metode
		return jedynak;
	}
	
	static class OutOfLimit extends Exception{ //musi byc statyczna inaczej nie dalo by sie stworzyc egzemplarza tej klasy bez tworzenia egzemplarza OnlyThree

		/**
		 * nie wiem co to???
		 */
		private static final long serialVersionUID = 1L;
		
	}
}
