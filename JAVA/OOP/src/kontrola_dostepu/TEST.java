package kontrola_dostepu;

public class TEST {

	public static void main(String[] args) {
		B b = new B();
		A a = new A();
		a.j = 4;  //j oraz in s protected czyli dostępne w odrębie pakietu oraz dziedziczone
		b.in = 55;
		
		a.metoda(); // da sie bo dostep pakietowy
		
		//OnlyThree ot = new OnlyThree(1);  //prywatny konstruktor
		try{
			OnlyThree one = OnlyThree.takeOne();
			OnlyThree two = OnlyThree.takeOne();
			OnlyThree three = OnlyThree.takeOne();
			OnlyThree four = OnlyThree.takeOne();
		}catch(OnlyThree.OutOfLimit ex){
			ex.printStackTrace();
			//stworzono juz 3 egzemplarze
		}
	}

}
