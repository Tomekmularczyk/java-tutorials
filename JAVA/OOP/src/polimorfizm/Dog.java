package polimorfizm;

public class Dog {
	public int age = 5;
	
	public void bark(){
		System.out.println("HAU HAU HAU");
		goToMaster();
	}
	
	protected void goToMaster(){
		System.out.println("I'm going back to my master!");
	}
	
	public void getAge(){
		System.out.printf("Mam %d lat.\n", age);
	}
	
	public static void main(String[] args){
		Husky husky = new Husky();
		Dog dog = husky;
		
		dog.bark();
		dog.getAge();
		husky.getAge(); //ponieważ metoda nie została przesłonięta, zwraca zmienną z klasy bazowej
	}
}
