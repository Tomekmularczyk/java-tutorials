package kontrola_dostepu_test;

import kontrola_dostepu.B;

public class ExtendedB extends B{
	
	final void changeIN(){  //tej metody nie nadpiszemy bo jest finalna
		in = 5; //in jest protected w B
	}
	
	
	private void metoda(){ //tak naprawde nie nadpisujemy metody z klasy B, tylko tworzymy nową. Warto posługiwać się adnotacjami 
		
	}
}
