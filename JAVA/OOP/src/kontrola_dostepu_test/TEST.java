package kontrola_dostepu_test;

import kontrola_dostepu.B;

public class TEST {

	public static void main(String[] args) {
		//A a = new A(); //a jest o dostepie pakietowym, tutaj jej nie uzyjemy
		//B b = new B();  //nie da rady bo konstruktor jest protected
		//b.in = 3; // in jest protected
		ExtendedB exB = new ExtendedB();
		exB.changeIN();
		//a.metoda(); //metoda ma dostep pakietowy, nie dziedziczymy jej
	}

}
