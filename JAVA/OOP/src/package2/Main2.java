package package2;
import java.util.Date;

import package1.Employee;

public class Main2 {

	
	public static void main(String[] args){
		Employee employee = new Employee("Adrian", 300, new Date());
		//employee.protectedString = "";     //błąd, klasa Employee znajduje się w innym pakiecie, nie mamy do niej dostępu bezpośredniego
	}
	
	public class ProtectedTest extends Employee{ //klasa zawierająca się w innej klasie musi być public

		public ProtectedTest(){
			super("", 0, new Date());
			protectedString = "mamy dostęp bo dziedziczymy klase Employee";
		}
	}
}
