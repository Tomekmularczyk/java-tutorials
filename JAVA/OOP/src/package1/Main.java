package package1;
import java.util.Date;

public class Main {

	
	public static void main(String[] args){
		Employee[] pracownicy = new Employee[3];
		pracownicy[0] = new Employee("Kazek", 2500, new Date());
		pracownicy[1] = new Employee("Andrzej", 2500, new Date());
		pracownicy[2] = new Manager("Jurek", 3000, new Date()); //kazdy manager jest też pracownikiem(ale nie w drugą strone)
		Manager manager = (Manager) pracownicy[2];
		manager.setBonus(300);
		
		for(Employee empl : pracownicy)
			wypisz(empl.getImieINazwisko() + ", hire date: " + empl.getHireDate() + ", salary: " + empl.getSalary());
		
		for(Employee e: pracownicy)
			wypisz(e.przedstawSie());
		
		Manager[] managers = new Manager[10];
		Employee[] employees = managers;      //tablica employees wskazuje teraz na tablice obiektów manager(każdy manager jest przecież employee)
		employees[0] = new Employee("", 333, new Date()); //employees wskazuje na tablice typu manager a my tworzymy w niej obiekt typu employee!
		managers[0].setBonus(200); //ArrayStoreException - utworzyliśmy w typie manager typ employee mimo, że nie każdy employee jest manager
		//każdy manager może wykonać zadania employee, ale employee nie może wykonywać zadań managera
		
		pracownicy[2].protectedString = "dozwolone ponieważ klasa jest w tym samym pakiecie"; //Żeby mieć dostęp do tej zmiennej z poza pakietu trzeba dziedziczyć klasę
	}
	
	public static void wypisz(String s){
		System.out.println(s);
	}
}
