package package1;
import java.util.Date;


public class Employee{
		private String imieINazwisko;
		private double salary;
		private Date hireDate;
		protected String protectedString; //do tej zmiennej będą miały dostęp wszyscy z tego pakietu oraz wszystkie klasy dziedziczące
		
		
		//bez argumentowy konstruktor domyślny, Java będzie używać go dla klasy pochodnej
		//jeżeli nie będzie w niej użyty inny konstruktor klasy macierzystej
//		public Employee(){
//			this("", 0.0, new Date());
//		}
		public Employee(String name, double salary, Date hireDate){
			imieINazwisko = name;
			this.salary = salary;
			this.hireDate = new Date(hireDate.getTime());//diverse copying
			doSomething();
		}
		public String getImieINazwisko() {
			return imieINazwisko;
		}
		public double getSalary() {
			return salary;
		}
		public Date getHireDate() {
			return new Date(hireDate.getTime()); //diverse copying
		}
		public void raiseSalary(double byPercent){
			salary += salary * byPercent / 100;
		}
		
		private void doSomething(){
			/* prywatne metody też są dziedziczone do klasy pochodnej mimo iż nie może ona ich używać.
			 * Dowodem na to jest to, że jeżeli wywołamy z klasy pochodnej publiczną metodę klasy matki
			 * która z kolei wywołuje metode prywatną, to kompilator nie zgłosi problemu.
			 */
		}
		
		
		//tę metody przesłonimy w klasie Manager
		protected Employee getKolega(){
			return new Employee("Kolega employee", 0, new Date());
		}
		
		public final String przedstawSie(){ //ta metoda nie może być przesłonięta ponieważ final zapobiega polimorfizmowi
			return "Cześć jestem " + imieINazwisko;
		}
	}
	
	

