package package1;
import java.util.Date;

/* klasa Manager posiada wszystkie pola i metody klasy Employee 
	 * jednak nie ma bezpośredniego dostępu do prywatnych pól i metod
	 * musi do tego używać publicznego interfejsu(public lub protected)*/
	public final class Manager extends Employee{ //final oznacza, że nie można dziedziczyć po klasie Manager. Wszystkie metody stają się final
		private double bonus;
		
		//Klasa pochodna musi mieć konstruktor który wywołuje konstruktor klasy macierzystej!!!
		//Chyba że klasa macierzysta posiada bezargumentowy konstruktor wtedy java użyje go domyślnie 
		public Manager(String name, double salary, Date hireDate){
			//bonus = 0;             //--- konstruktor klasy macierzystej musi być pierwszą instrukcją
			super(name, salary, hireDate);
			bonus = 0;
	
		}
		
		public double getSalary(){
			//jak wywołać metodę którą akurat przesłaniamy? - super
			double wyplata = super.getSalary() + bonus;
			return wyplata;
		}
		
		public void setBonus(double bonus){
			this.bonus = bonus;
		}
		
		
		/* przesłaniając metodę z nadklasy musi ona:
		 * - mieć taką samą sygnaturę(te same parametry)
		 * - przynajmniej tę samą widoczność. Jeżeli pierwotna metoda była public/protected nie możemy przesłonić ją na private
		 * - musi zwracać ten sam obiekt lub klase pochodną(jak w przykładzie)
		 */
		@Override
		public Manager getKolega(){
			return new Manager("Kolega manager",0,new Date());
		}
		
	}