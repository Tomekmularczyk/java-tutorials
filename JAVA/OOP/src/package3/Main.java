package package3;

public class Main {

	public static void main(String[] args) {
		Cycle[] cycles = new Cycle[4];
		cycles[0] = new Cycle();
		cycles[1] = new Unicycle();
		cycles[2] = new Bicycle();
		cycles[3] = new Tricycle();
		
		for(Cycle cycle : cycles)
			cycle.ride();
	}

}
