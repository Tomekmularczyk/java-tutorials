package def;

public class Player {
	private String name;
	private int position = 1;
	private int money = 7;
	
	public Player(String name){
		this.name = name;
	}
	
	public void takeMoney(int howMuch){ //money never less than 0
		money = (howMuch > money)? 0 : money - howMuch;
	}
	public void giveMoney(int howMuch){
		money += howMuch;
	}
	public int howMuchMoney(){
		return money;
	}
	
	public void moveSteps(int steps){
		position += steps;
	}
	
	public int getPosition(){
		return position;
	}
	
	@Override
	public String toString(){
		return name + "(money: " + money + "$)";
	}
}
