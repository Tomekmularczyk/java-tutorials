package def;

import java.io.IOException;

public class Game {
	private Player[] players;
	private Board board = new Board();
	private Dice dice = new Dice();
	private boolean running = false;

	
	public void play(int nrOfPlayers) throws IOException{
		running = true;
		players = new Player[nrOfPlayers];
		for(int i =0; i < nrOfPlayers; i++){
			players[i] = new Player("Player " + (i+1));
		}
		
		while(running){
			System.out.println(board.toString(players));
			System.out.println("\nKontynuuj...\n==================================");
			System.in.read();
			nextTurn();
		}
	}
	
	private void nextTurn(){
		for(Player player: players){
			int steps = dice.rollTheDice();
			System.out.println(player + " wylosował: " + steps);
			player.moveSteps(steps);
			if(player.getPosition() > 40){ //someone crossed the finish
				if(player.getPosition() == 40) //last action before finish
					board.getSquare(player.getPosition()).action(player);
				finish(player);
				return;
			}
			board.getSquare(player.getPosition()).action(player);
		}
	}
	
	private void finish(Player player){
		running = false;
		System.out.println("\nThe game is over! " + player + " went to the finish line!\n\nAnd the winner is...");
		sortWinnersDescending(players);
		for(int i = 0; i<players.length; i++){
			System.out.println(i+1 + " Place: " + players[i]);
		}
	}
	
	private void sortWinnersDescending(Player[] players){
		for(int j=0; j<players.length; j++)
			for(int i=0; i<players.length-1; i++){
				if(players[i].howMuchMoney() < players[i+1].howMuchMoney()){
					Player temp = players[i];
					players[i] = players[i+1];
					players[i+1] = temp;
				}
			}
	}
}
