package def;

import java.io.IOException;
import java.util.Scanner;

public class TestClass {

	public static void main(String[] args) {		
		Scanner scanner = new Scanner(System.in);
		System.out.print("How many players: ");
		int players = scanner.nextInt();
		
		Game newGame = new Game();
		try {
			newGame.play(players);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
