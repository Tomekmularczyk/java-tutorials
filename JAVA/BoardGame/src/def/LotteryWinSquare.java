package def;

public class LotteryWinSquare extends Square{

	@Override
	public void action(Player player){
		player.giveMoney(10);
	}
	
	@Override
	public String toString(){
		return "|++|";
	}
}
