package def;

public class BadInvestmentSquare extends Square{

	@Override
	public void action(Player player){
		player.takeMoney(5);
	}
	
	@Override
	public String toString(){
		return "|--|";
	}
}
