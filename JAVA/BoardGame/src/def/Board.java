package def;

public class Board {
	private Square[] squares = new Square[40];
	
	public Board(){
		for(int i=1; i<=40; i++){
			if(i % 10 == 0)
				squares[i-1] = new LotteryWinSquare();
			else if(i % 5 == 0 && i % 10 != 0)
				squares[i-1] = new BadInvestmentSquare();
			else
				squares[i-1] = new Square();
		}
	}
	
	
	public String toString(Player[] players){
		StringBuilder board = new StringBuilder();
		
		for(int i=0; i<40; i++){
			board.append("\n" + (i+1) + squares[i].toString());
			for(Player player : players){
				if(i+1 == player.getPosition())
					board.append(" " + player);
			}
		}
		
		return board.toString();
	}
	
	public Square getSquare(int nr){
		return squares[nr-1];
	}
}
