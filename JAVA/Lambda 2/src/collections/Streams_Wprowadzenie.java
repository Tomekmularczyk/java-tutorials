package collections;


import domain.Person;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

/**
 *
 * @author tomek
 */
public class Streams_Wprowadzenie {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Person> personList = Person.getSimpleList();
        
        Collections.sort(personList, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getSurname().compareTo(p2.getSurname());
            }
        });
        
        
        /*
        Laziness: In programming, laziness refers to processing only the objects that you want to process when you need to process them. 
        In the previous example, the last loop is "lazy" because it loops only through the two Person objects left after the List is filtered. 
        The code should be more efficient because the final processing step occurs only on the selected objects.
        Eagerness: Code that performs operations on every object in a list is considered "eager". 
        For example, an enhanced for loop that iterates through an entire list to process two objects, is considered a more "eager" approach.
        */
        
        Collections.sort(personList, (p1,p2) -> p1.getName().compareTo(p2.getName()));
        
        personList.stream().filter((person) -> person.isHasCar()).forEach(System.out::println);
        
        /*
        As previously mentioned, a Stream is disposed of after its use. 
        Therefore, the elements in a collection cannot be changed or mutated with a Stream. 
        However, what if you want to keep elements returned from your chained operations? You can save them to a new collection. 
        The following code shows how to do just that.     
        */
        
        personList.stream().filter((p) -> p.getAge() > 20).collect(Collectors.toList());
        
        personList.stream().filter((p) -> !p.isHasCar()).mapToInt((p) -> p.getAge()).sum();
        
        OptionalDouble average = personList.parallelStream().filter((p) -> p.getAge() > 25).mapToDouble((p) -> p.getAge()).average();
        
        average.ifPresent(System.out::println); //dedukuje z kontektstu że chcemy użyć przeładowanej metody println która pobiera argument podawany w metodzie
    }
    
}
