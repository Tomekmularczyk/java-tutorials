package domain;

/**
 * @author tomek
 */
public class Kid extends Citizen {

    private String name;
    private boolean hasToy = true;

    public Kid(String name, String surname, int age, boolean hasToy) {
        super(surname, age);
        this.name = name;
        this.hasToy = hasToy;
    }

    public Kid(int age) {
        super("noname", age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasToy() {
        return hasToy;
    }

    public void setHasToy(boolean hasToy) {
        this.hasToy = hasToy;
    }

    @Override
    public String toString() {
        return super.toString() + "name=" + name + ", hasToy=" + hasToy;
    }

}
