package domain;

/**
 * @author tomek
 */
public abstract class HomoSappiens {
    private int age;
    
    public HomoSappiens(int age){
        this.age = age;
    }
    

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " age=" + age + " ";
    }
}
