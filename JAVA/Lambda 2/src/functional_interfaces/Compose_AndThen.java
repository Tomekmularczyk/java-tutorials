package functional_interfaces;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Compose_AndThen {
    public static void main(String[] args) {
        Function<Integer, Integer> times2 = n -> n * 2;
        Function<Integer, Integer> squared = n -> n * n;
        
        Integer compose = times2.compose(squared).apply(3); //najpierw wykona się argument compose a wynik zostanie przekazany do metody wywołującej
        Integer andThen = times2.andThen(squared).apply(3); //najpierw wykona się metoda wywołująca a potem to co w compose
        System.out.println("Wynik wywołania compose = " + compose + 
                "\nWynik wywołania andThen = " + andThen);
        
        BiFunction<Integer,Integer,Boolean> isTheSame = (n1, n2) -> Objects.equals(n1, n2);
        Function<Boolean, String> toString = String::valueOf;
        String result = isTheSame.andThen(toString).apply(333, 333); //BiFunction ma tylko andThen, ponieważ nie możemy przekazać rezultatu wywołania metody do metody która przyjmuje 2 argumenty
        System.out.println("Wywołanie andThen na BiFunctional interfejsie: " + result);
        
        Function<Boolean, String> func = String::valueOf;
    }
}
