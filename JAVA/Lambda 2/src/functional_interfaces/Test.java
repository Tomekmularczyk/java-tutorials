package functional_interfaces;

import domain.Citizen;
import domain.HomoSappiens;
import domain.Kid;
import domain.Person;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.*;

/**
 * @author tomek
 */
public class Test {
    /*
        Function - obiera i zwraca dane argumenty
        Operator - zawsze zwraca ten sam typ co przyjmuje
        Predicate - zwraca zawsze boolean
        Consumer - zwraca zawsze void i coś przyjmuje
        Supplier - zwraca zawsze dany typ nie pobierając nic
    
        Unary - pojedyńczy
        Bi(binary) - podwójny
    */
    
    public static void main(String[] args) {
        BiConsumer<Citizen, Kid> biConsumer = (cit, kid) -> cit = kid; //przyjmuje dwa typy i nic nie zwraca
        BiFunction<Double, Double, Integer> biFunction = Double::compare; //przyjmuje dwa typy i zwraca trzeci
        BinaryOperator<Integer> binaryOperator = Math::max; //przyjmuje dwa argumenty tego samego typu i zwraca też taki sam typ
        BiPredicate<Class<HomoSappiens>, HomoSappiens> biPredicate = Class::isInstance; //magia... przyjmuje dwa typy i zwraca boolean
        UnaryOperator<String> unaryOperator = String::toLowerCase; //pobiera dany typ i zwraca dany typ
        
        Function<String, Integer> function = Integer::valueOf; //pobiera jeden typ i zwraca drugi typ
        Predicate<String> predicate = String::isEmpty; //pobira dany typ i zwraca dany typ
        Consumer<String> consumer = String::toLowerCase; //znowu magia... przyjmuje jeden argument i zwraca void
        Supplier<Byte> supplier = () -> (byte) (3 * Math.random()); //nie pobiera nic zwraca dany typ
                
        BooleanSupplier booleanSupplier = () -> LocalDate.now().isAfter(LocalDate.parse("2016-11-17")); //zwraca boolean
        
        DoubleBinaryOperator doubleBinaryOperator = Double::compare; //przyjmuje dwa booleany i zwraca też boolean
        DoubleConsumer doubleConsumer = System.out::println; //pobiera wartość double
        doubleConsumer.andThen(d -> {System.out.println(".. " + d * d);});// nie hula????
        doubleConsumer.accept(4);
        DoubleFunction<Boolean> doubleFunction = Double::isFinite; //pobiera double i zwraca argument
        DoublePredicate doublePredicate = Double::isInfinite; //pobiera double i zwraca boolean
        DoubleSupplier doubleSupplier = Math::random; //pobiera nic i dostarcza double
        DoubleToIntFunction doubleToIntFunction = d -> (int) (d * d / 2); //pobiera double i zwraca int
        DoubleToLongFunction doubleToLongFunction; // podobnie jak wyżej
        DoubleUnaryOperator doubleUnaryOperator = d -> d * 0.343;
        

        LongUnaryOperator longUnaryOperator = Long::highestOneBit;
        
        /* jeśli się zastanowić wywołanie konstruktora słówkiem new jest tak naprawdę tym samym co wywołanie metody:
           - dostarczamy listę argumentów...
           - i dostajemy typ zwrotny (nowy obiekt)
           Dlatego możemy wykorzystywać referencje do konstruktora do podmiany metod funkcjonalnych
           tam gdzie lista argumentów i typ zwrotny odpowiada konstruktorowi obiektu.
        */
        IntFunction<Kid> intFunction = Kid::new; //od kontekstu zależy który zostanie wywołany konstruktor
        BiFunction<String, Integer, Citizen> citizenFunction = Citizen::new;
        
        List<Person> personList = Person.getSimpleList();
        Person[] toArray = personList.stream().toArray(Person[]::new); //możemy też podawać referencje do kontruktora tablicy. Tworzy tablice i wypełnia obiektami
        System.out.println(Arrays.toString(toArray));
        
    }
}
