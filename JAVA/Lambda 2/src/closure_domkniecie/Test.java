package closure_domkniecie;

import domain.Person;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author tomek
 */
public class Test {

    public Runnable dummy(String name, int age) {
        LocalDate localDate = LocalDate.now();

        Runnable runnable = () -> {
            for (int i = 0; i < 5; i++) {
                System.out.println(
                        name + ", " + age + ", "
                        + localDate.format(DateTimeFormatter.ISO_DATE)
                        + System.lineSeparator()
                );
            }
        };

        return runnable;
    }
    //Kod tego wyrażenia lambda może być wykonywany jeszcze długo po tym, jak wywołanie metody dummy się zakończy i zmienne parametrów przestaną być dostępne. 
    //W jaki sposób zmienne tekst i liczba pozostają dostępne przy wykonywaniu wyrażenia lambda?

    //Wyrażenie lambda przechowuje wartości tych zmiennych(tzw: zmienne wolne), natomiast muszą one być efektywnie finalne - efektywnie tzn. że nie muszą
    //mieć słówka kluczowego "final", ale muszą się tak zachowywać
    //techniczny termin określający coś takiego tzn blok kodu ze zmiennymi wolnymi nazywa się "Closure" (domknięcie)
    //wyrażenia lambda w javie są domknięciami
    public static void main(String[] args) {
        Test test = new Test();

        Runnable runnable = test.dummy("tomek", 26);
        new Thread(runnable).start();
    }

    //przykłady nie działające z powyższych powodów
    public void dummyTwo(String name) {
        Runnable runnable = () -> {
            //name = "newValue"; //must be final or effectively final
        };

        for (int i = 0; i < 5; i++) {
            Runnable runnable2 = () -> {
                //System.out.println(i); //must be final or effectively final
            };
        }

        List<Person> simpleList = Person.getSimpleList();
        for (Person person : simpleList) {
            Runnable runnable2 = () -> {
                person.setHasCar(false); //można użyć zmiennej person ponieważ ma zasięg pojedyńczej iteracji i za każdym razem jest tworzona od nowa
            };
        }

    }
}
