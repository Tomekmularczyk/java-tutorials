package scope_namespace;

import java.util.function.Consumer;

/**
 * @author tomek
 * 
 * Zasięg zmiennych w lambda jest trochę inny niż w klasach wewnętrznych i anonimowych
 * 
 */
public class Test {
    
    private String test = "Test.test"; //1
    
    public void method() {
        String test = "method.test"; //2
        
        
        class Inner implements Consumer<String> { //to samo poniżej tyczy się anonimowych klas
            private String test = "Comparator.test"; //3
            
            @Override
            public void accept(String test) { //4
               //String test = "compare.test"; //already defined
               
               System.out.println("Anonimowe klasy pozwalają na zagnieżdżanie bez przesłaniania: \n" 
               + test + "\n"
               + this.test + "\n"
               + Test.this.test + "\n"
               + this.toString() + "\n"
               );
            }
        };
        new Inner().accept("test");
        
        
        
        //teraz lambda -- Treść wyrażenia lambda ma taki sam zasięg jak zagnieżdżony blok kodu. 
        //Wewnątrz metody nie możesz mieć dwóch zmiennych lokalnych o tej samej nazwie, dlatego nie możesz też wprowadzać takich zmiennych w wyrażeniu lambda.
        Consumer<String> comparatorLambda = (/* test */ otherTest) -> { 
            //String test = "test"; //też już zdefiniowana
            System.out.println("Anonimowe klasy pozwalają na zagnieżdżanie bez przesłaniania: \n" 
               + test + "\n"
               + this.test + "\n"
               + Test.this.test + "\n"
               + this.toString() + "\n"
               );
        };
        comparatorLambda.accept("test");
        
        // W SKRÓCIE PRZESTRZEŃ NAZW I ZASIĘG WYRAŻEN W BLOKU LAMBDA JEST TAKI SAM JAK W PRZESTRZEŃ JĄ OTACZAJĄCEJ
        // DAJE TO EFEKT TAKI JAKGDYBY TEN KOD W BLOKU NIE BYŁ NICZYM OTOCZNY
        
        
    }
    
    
    
    
    
    public static void main(String[] args) {
        Test test = new Test();
        
        test.method();
    }
}
