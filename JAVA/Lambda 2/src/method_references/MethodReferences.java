package method_references;

import domain.HomoSappiens;
import java.util.function.BiPredicate;
import java.util.function.Consumer;

/**
 * @author tomek
 */
public class MethodReferences {

    public static void main(String[] args) {
        //jeżeli masz już metodę która jest semantycznie taka sama jak ta w funkcjonalnym interfejsie 
        //tzn: pobiera te same lub pochodne argumenty i zwraca ten sam lub pochodny typ(kowariancja typów),
        //to można ją podmienić zamiast tworzenia interfejsu funkcyjnego używając operatora ::

        double returnValue = doTheMath(6, 8, new MyFunctionalInterface() {
            @Override
            public double calculate(int x, int y) {
                return x * 10 / y + 0.003;
            }
        });

        double returnValue2 = doTheMath(19, 10, (x, y) -> x * 4 / y - 9.945);

        double returnValue3 = doTheMath(12, 9, MethodReferences::replacementMethod); //replacementMethod jest semantycznie taka sama więc OK, możemy podmienić

        //double returnValue4 = doTheMath(11,2, MethodReferences::replacementMethodPodrabianiec); //zupełnie inny zwracany typ
        double returnValue4 = doTheMath(11, 2, MethodReferences::replacementMethodPodrabianiec2); //ponieważ do double da sie przypisać int, odwrotnie już nie dałoby rady
        double returnValue5 = doTheMath(11, 2, MethodReferences::replacementMethodPodrabianiec3); //spoko, bo w taki sposób można rzutować z double na int

        // double returnValue5 = doTheMath(11,2, MethodReferences::replacementMethodPodrabianiec4);  //loss of conversion
        System.out.println("returnValue = " + returnValue);
        System.out.println("returnValue2 = " + returnValue2);
        System.out.println("returnValue3 = " + returnValue3);
        System.out.println("returnValue4 = " + returnValue4);

        MyFunctionalInterface returnValue6 = MethodReferences::replacementMethod;
        System.out.println("returnValue4 = " + returnValue6.calculate(4, 444));

        //a tutaj lower case wogóle nie przyjmuje nic, ale java wie żeby wywołać te metodę na przekazywanym stringu
        Consumer<String> consumer = String::toLowerCase;

        //chociaż to przeczy temu co napisałem wyżej bo metoda isInstance przyjmuje tylko jeden parameter…
        //jest to "Reference to an instance method of an arbitrary object of a particular type", czyli coś podobnego to referencji do metody konkretnego obiektu(obj::someMethod)
        //znaczy to: wykonaj daną metodę na pierwszym przekazywanym parametrze i przekaż resztę parametrów do tej właśnie metody
        BiPredicate<Class<HomoSappiens>, HomoSappiens> biPredicate = Class::isInstance; 
        //pierwszy przekazywany jest Class<HomoSappiens> i na nim wywołujemy metode przyjmującą reszte parametrów (obiekt HomoSappiens)
        //w skrócie: wywołaj metode z pierwszego parametru pasującą do reszty parametrów i typu zwracanego metody z interfejsu
        //wydaje się że Class odnosi się do Klasy, ale tak naprawdę odnosi się do instancji obiektu, który będzie przekazany jako parametr(pierwszy)
        
        //Powyższy "skrót lambdowy" przydaje się w przypadkach gdy przekazujemy parametr tylko po to by wywołać na nim metodę z resztą argumentów
    }

    private static double doTheMath(int x, int y, MyFunctionalInterface functionalInterface) {
        return functionalInterface.calculate(x, y);
    }

    @FunctionalInterface
    private static interface MyFunctionalInterface {

        double calculate(int x, int y);
    }

    private static double replacementMethod(int x, int y) {
        return x * 3 / y * 1.134;
    }

    private static void replacementMethodPodrabianiec(int x, int y) {
        double xxx = x * 3 / y * 1.134;
    }

    private static int replacementMethodPodrabianiec2(int x, int y) {
        return x * y * 4;
    }

    private static double replacementMethodPodrabianiec3(double x, int y) {
        return x * 3 / y * 1.134;
    }

    private static double replacementMethodPodrabianiec4(short x, int y) {
        return x * 3 / y * 1.134;
    }

}
