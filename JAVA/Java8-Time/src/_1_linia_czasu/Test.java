package _1_linia_czasu;

import com.sun.istack.internal.logging.Logger;
import java.time.Duration;
import java.time.Instant;
/**
        Wszystkie obiekty java.time są niemodyfikowalne.
 */
public class Test {
    private static final Logger LOG = Logger.getLogger(Test.class);
    
    public static void main(String[] args) throws Exception {
        //W java 8 klasa Instant reprezentuje czas bezwzględny mierzony w milisekundach (od 1970) podobnie jak dawniej Date
        //można powiedzieć, że jest to "czas maszynowy"
        Instant start = Instant.now();
        doCalculations();
        Instant end = Instant.now();
        
        //do mierzenia różnic w czasie służy klasa Duration
        Duration difference = Duration.between(start, end);
        LOG.info("Zadanie trwało: " + difference.toMillis() + " mls");
        
        //takie mniej więcej rzeczy można robić z Duration…
        difference.multipliedBy(10).minus(Duration.ZERO).plusHours(4);
    }
    
    private static void doCalculations() throws Exception{
        Thread.sleep(100);
    }
    
}
