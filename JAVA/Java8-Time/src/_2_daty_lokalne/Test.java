package _2_daty_lokalne;

import com.sun.istack.internal.logging.Logger;
import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;

/**
 * Czas reprezentowany przez ludzi został podzielony w Javie na czas lokalny i czas strefowy.
 * - czas lokalny zawiera informacje o dacie i godzinie, ale nie zawiera informacji o strefie czasowej
 */
public class Test {
    private static final Logger LOG = Logger.getLogger(Test.class);
    
    public static void main(String[] args) {
        //klasa LocalDate reprezentuje datę z rokiem, dniem i miesiącem
        LocalDate myBirthday = LocalDate.of(1989, Month.NOVEMBER, 17); //inaczej niż w starym API tutaj miesiące nie numerujemy od zera
        LocalDate agataBirthday = LocalDate.of(1993, Month.SEPTEMBER, 25);
        
        LocalDate ld1 = LocalDate.now(); 
        //LocalDate ld3 = LocalDate.from(Instant.now()); //nie da rady, dlaczego?
        LOG.info(ld1 + ", " + myBirthday);
        
        //Odpowiednikiem klasy duration dla dat lokalnych jest klasa Period, któ®a zawiera lata, dni i miesiące
        myBirthday.plusYears(1);
        myBirthday.plus(Period.ofYears(1));
        //myBirthday.plus(Duration.ofDays(365)); //nie da poprawnego wyniku w roku przestępnym!
        
        Period period = myBirthday.until(agataBirthday);
        long days = myBirthday.until(agataBirthday, ChronoUnit.DAYS);
        LOG.info("Różnica między dwoma datami wynosi: " + period.getYears() + " lata, miesiecy: " + period.getMonths());
        LOG.info("Różnica w dniach: " + days);
        
        //przy dodawaniu lub odejmowaniu dat api pilnuje by nie tworzyć nie istniejących dat np:
        LocalDate february = LocalDate.of(2000, Month.JANUARY, 31).plusMonths(1); //… wcale nie stworzy nam 31 lutego, tylko 29 lutego
        LOG.info(february.toString());
        
        //Są też klasy dat lokalnych, które reprezentują krótsze jednostki czasu
        MonthDay.from(myBirthday);
        YearMonth.from(agataBirthday);
        Year.of(2016);
    }
}
