package _3_modyfikatory_daty;

import static java.lang.System.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

/**
 * @author tomek
 */
public class Test {
    
    
    public static void main(String[] args) {
        //Nieraz potrzeba wykonać trywialne zadania, które mogą być problematyczne gdy trzeba obliczyć
        //pierwszy wtorek każdego miesiąca, albo następny piątek. Jest do tego klasa TemporalAdjusters
        LocalDate now = LocalDate.now();

        LocalDate nextMonday = now.with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY)); //zwróci kolejny poniedziałek, chyba że data wskazuje na poniedzialek to zwróci to samo
        //Jak zawsze metoda with zwraca nowy obiekt LocalDate bez modyfikowania oryginału.
        out.println("Nastepny poniedzialek: " + nextMonday);
        
        
        LocalDate tenthMonday = nextMonday.with(TemporalAdjusters.dayOfWeekInMonth(10, DayOfWeek.MONDAY));
        out.println("10 poniedziałek wypadnie w " + tenthMonday);
        
        
        //customowa metoda
        TemporalAdjuster NASTEPNY_DZIEN_PRACY = TemporalAdjusters.ofDateAdjuster(w -> {
            LocalDate result = w; // Bez rzutowania
            do {
                result = result.plusDays(1);
            } while (result.getDayOfWeek().getValue() >= 6);
            return result;
        });
        LocalDate backToWork = LocalDate.now().with(NASTEPNY_DZIEN_PRACY);
        out.println("Back to work " + backToWork);
        
        
        
        //inne ciekawe metody
        now.with(TemporalAdjusters.firstDayOfYear()); //first day of Month/ next Month/ next Year
        now.with(TemporalAdjusters.lastInMonth(DayOfWeek.MONDAY)); //last day of month/ last day of year
        
    }
}
