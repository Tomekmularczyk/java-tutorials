package trainee;

import static java.lang.System.*;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;
/**
 * @author tomek
 */
public class Training1 {
    public static void main(String[] args) throws InterruptedException {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());
  
        
        Clock clock = Clock.tickMinutes(ZoneId.systemDefault()); //tworzymy zegar, który będzie zmieniał swoją wartość co minutę, nie wcześniej
        out.println("Clock: " + pattern.format(clock.instant()));
        Thread.sleep(1000);
        out.println("Clock: " + pattern.format(clock.instant()));
        
        
        LocalDateTime someDate = LocalDateTime.parse("2016-10-11 22:35:23", pattern);
        Month currentMonth = someDate.getMonth();
        MonthDay monthDay = MonthDay.from(someDate);
        Year year = null;
        if(Year.isLeap(someDate.getYear())) //2016 to rok przestepny
            year = Year.from(someDate);
        YearMonth yearMonth = YearMonth.parse("1989 -- 12", DateTimeFormatter.ofPattern("yyyy -- MM", Locale.KOREAN));
        
        
        out.println("\n=== Human Date Representation ====");
        out.printf("LocalDateTime: %s \nMonth: %s\nMonthDay: %s\nYear: %s\nYearMonth: %s\n", someDate, currentMonth, monthDay, year, yearMonth);
        
        out.println("\n=== Chronology====");
        Chronology.getAvailableChronologies().stream().forEach(out::println);
        
        
        LocalTime time = LocalTime.of(12, 59);
        LocalDate date = LocalDate.now(clock);
        Duration durationBetween = Duration.between(time.plus(150, ChronoUnit.MICROS), someDate);
        Period periodBetween = Period.between(date, date.with(TemporalAdjusters.lastDayOfMonth()));
        
        TemporalAdjuster nextFriday = TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY);
        Period tillNextFriday = Period.between(date, date.with(nextFriday));
        
        out.println("\n=== Period & Duration ====");
        out.println(durationBetween.get(ChronoUnit.SECONDS) + " seconds");
        out.println(periodBetween.get(ChronoUnit.DAYS) + " days");
        out.println("Till next friday: " + tillNextFriday.getDays());
        
        tillNextFriday.getUnits().stream().forEach(out::println);

        ZonedDateTime zonedDate = ZonedDateTime.of(someDate, ZoneId.of("Europe/Berlin")); //z tego co kumam to ta klasa nie rózni się wiele od LocalDateTime
                                                                                          //oprócz tego, że przetrzymuje dane na temat strefy czasowej
        out.println(zonedDate);
    }
}
