package _5_czas_strefowy;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.stream.Collectors;

/**
 * Gdy projektujesz aplikacje kalendarza, musi ona działać poprawnie jeżeli ludzie będą przemieszczać się między krajami Jeśli masz
 * połączenie konferencyjne o dziesiątej w Nowym Jorku, ale w tej chwili jesteś w Berlinie, oczekujesz, że przypomnienie pojawi się o
 * odpowiedniej godzinie.
 *
 * Java korzysta z bazy danych IANA, która przechowuje informację o wszystkich znanych strefach czasowych
 */
public class Test {

    public static void main(String[] args) {
        String availableZoneIds = ZoneId.getAvailableZoneIds() //zwraca dostępne strefy czasowe (ok. 600)
                .stream()
                .filter(s -> s.contains("Warsaw"))
                .collect(Collectors.joining(", ", "Kraje w Amerykańskiej strefie czasowej: ", ""));
        System.out.println(availableZoneIds);

        //Klasa ZonedDateTime bierze przy obliczeniach pod uwagę strefę czasową użytkownika oraz zmieny z czasu letniego na zimowy
        ZoneId jamaica = ZoneId.of("America/Jamaica");
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime atZone = now.atZone(jamaica);
        System.out.println(atZone);
        //lub odrazu
        ZonedDateTime startApollo11 = ZonedDateTime.of(1969, 7, 16, 9, 32, 0, 0, ZoneId.of("America/New_York"));

        // Koniec czasu letniego 
        ZonedDateTime niejednoznaczne = ZonedDateTime.of(LocalDate.of(2013, 10, 27),
                LocalTime.of(2, 30), ZoneId.of("Europe/Berlin")); // 2013-10-27T02:30+02:00[Europe/Berlin]
        ZonedDateTime godzinePozniej = niejednoznaczne.plusHours(1); // 2013-10-27T02:30+01:00[Europe/Berlin]
        //Godzinę później czas ma taką samą godzinę i minutę, ale przesunięcie czasu dla strefy cza- sowej się zmieniło.

        System.out.println(godzinePozniej);

        //Musisz też zwrócić uwagę przy modyfikacjach daty przekraczających granicę zmiany czasu.Na przykład jeśli ustalasz spotkanie na następny tydzień, nie dodawaj siedmiu dni:
        ZonedDateTime meeting = LocalDateTime.of(2016, Month.MARCH, 17, 22, 15).atZone(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime nextMeeting = meeting.plus(Duration.ofDays(7)); // Uwaga! Nie zadziała przy sezonowej zmianie czasu
        //Zamiast tego użyj klasy Period.
        ZonedDateTime nextMeetingOK = meeting.plus(Period.ofDays(7)); // OK
        
        meeting.plus(Duration.ofMinutes(4));
    }
}
