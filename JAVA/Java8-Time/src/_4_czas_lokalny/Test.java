package _4_czas_lokalny;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        //Do reprezentowania godziny minut i sekund służy LocalTime
        LocalTime.now();
        LocalTime.from(LocalTime.MIDNIGHT);
        LocalTime.of(20, 11, 59, 999);
        
        //sama klasa local time operuje na 24 godzinnym systemie. AM/PM zostało przerzucone na formatery
        LocalTime.now().isBefore(LocalTime.NOON);
        LocalTime.now().atDate(LocalDate.of(1989, Month.NOVEMBER, 17)); //tworzy obiekt LocalDateTime, który jest też odpowiedni do 
                                                                        //reprezentowania punktów w czasie w ustalonej strefie czasowej.
                                                                        //
                                                                        //Jednak jeśli musisz wykonywać obliczenia, które biorą pod uwagę zmiany czasu z letniego na zimowy, 
                                                                        //lub obsługiwać użytkowników znajdujących się w różnych strefach czasowych, 
                                                                        //powinieneś użyć klasy ZonedDateTime
        
    }
}
