package _6_formatowanie_i_przetwarzanie;

import static java.lang.System.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 * Do formatowania czasu służy klasa DateTimeFormater, posiada on formatery specyficzne dla danej lokalizacji, predefiniowane standardowe
 * formatery i formatery dostosowane wzorcami
 */
public class Test {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        String formatedNow = DateTimeFormatter.ISO_DATE_TIME.format(now);
        out.println(formatedNow);

        //Standardowe formatery są stosowane do oznaczania czasu automatom,
        //podobnie jak mamy czas maszynowy Instant i znany ludziom LocalDate,
        //tak do oznaczania formatu daty oraz czasu możemy używać SHORT/MEDIUM/LONG/FULL
        LocalDate dateNow = LocalDate.now();
        LocalTime timeNow = LocalTime.now();
        LocalDateTime dateTimeNow = LocalDateTime.of(dateNow, timeNow);

        String dateShort = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(dateNow);
        out.println(dateShort);

        String timeMedium = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM).format(timeNow);
        out.println(timeMedium);

        DateTimeFormatter ofLocalizedDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL);
        //System.out.println(dateTimeNow.format(ofLocalizedDateTime)); //czemu kuhwa?
        //aby użyć innej lokalizacji niż domyslna…
        DateTimeFormatter ofLocalizedDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
        String localizedUSTime = ofLocalizedDate.withLocale(Locale.US).format(dateNow);
        out.println(localizedUSTime);

        //Wyliczenia DayOfWeek i Month zwracają nazwy dni tygodnia i miesięcy w różnych lokalizacjach i formatach
        for (DayOfWeek day : DayOfWeek.values()) {
            out.println(day.getDisplayName(TextStyle.FULL, Locale.ITALY));
        }

        //Można też utworzyć własny format daty
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("E yyyy-MM-dd HH:mm");
        out.println(pattern.format(dateTimeNow));

        //Aby przetworzyć wartości opisujące datę i czas z ciągu znaków, wykorzystaj jedną ze statycznych metod parse. Na przykład:
        LocalDate otwarcieKościoła = LocalDate.parse("1903-06-14");
        ZonedDateTime startApollo11 = ZonedDateTime.parse("1969-07-16 03:32:00-0400", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssxx"));
        //Pierwsze wywołanie wykorzystuje standardowy formater ISO_LOCAL_DATE, drugie — dostosowany formater.
    }
}
