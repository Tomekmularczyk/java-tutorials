/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _3_ograniczone_zmienne_typowe;

import static java.lang.System.out;
import model.Human;
import model.LivingCreature;
import model.Voiced;

/**
 *
 * @author tomek
 */
public class Test {

    public static void main(String[] args) {
        Object plainObject = new Object();
        LivingCreature snake = () -> "**heavy breathing**";
        Voiced robot = () -> "I'M.A.ROBOT.";

        Container container = new Container();
        // container.makeASound(plainObject); //musi implementować LivingCreature(ponieważ jest pierwsze na liście?)

        //kompilator na to pozwala chociaż ten obiekt nie implementuje Voiced.
        //Nie oparamteryzowaliśmy naszego obiektu, dlatego pozwalamy by container mógł pracować z
        //dowolnym obiektem LivingCreature (nie zgłasza błędu jeżeli nie implementuje Voiced chociaż to może rodzić problemy). 
        //I tutaj zaczynają rodzić się problemy: dostajemy wyjątek "cannot be cast to Voiced" ponieważ klasa container
        //zakłada że obiekty do niej przesyłane będą implementować interfejsy Voiced i LivingCreature.
        try {
            container.makeASound(snake);
        } catch (Exception e) {
            out.println(e.toString());
        }

        //container.makeASound(robot); //dlaczego skoro we wczesniejszym przypadku wystarczylo zeby tylko jeden interfejs był zaimplementowany
        //Container<Voiced> thing = new Container<>(); //not withing the bound
        //----- a teraz po bożemu :)
        Container<Human> validContainer = new Container<>();
        out.println(validContainer.makeASound(new Human()));

    }

    private static class Container<T extends LivingCreature & Voiced> {
        public String makeASound(T object) {
            return T.ID + ": " + object.breathe() + "... " + object.scream();
        }
    }
}
