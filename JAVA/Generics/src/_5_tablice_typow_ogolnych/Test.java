/*
 */
package _5_tablice_typow_ogolnych;

import model.Point;
import static java.lang.System.out;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

/**
 * nie powinno się tworzyć tablic typów generycznych tylko listy typowane
 *
 * @author tomek
 */
public class Test {

    public static void main(String[] args) {
        Point<Integer, Integer>[] pointArray;

        //error: generic array creation
        //nie wolno tak robić ponieważ po wymazaniu typów pointArray by była typu Point i można by było przypisać ją do tablicy Object
        //...co przy przypisaniu do elementu nie odpowiedniego typu powodowałoby wyjątek ArrayStoreException
        //... a przecież po to tworzymy typ parametryzowany by móc później bezpieczniej podawać mu typy obiektów
        //pointArray = new Point<Integer,Integer>[10];
        String[] sArray = new String[10];
        Object[] oArray = sArray;
        try {
            oArray[0] = 4; //przy typach parametryzowanych takie działania  są niedopuszczalne
        } catch (Exception e) {
            out.println(e.toString());
        }

        //obejście
        //ale ponoć niebezpieczne
        pointArray = (Point<Integer, Integer>[]) new Point<?, ?>[10];
        
        

        //--- A CO W PRZYPADKU METOD ZE ZMIENNĄ LICZBĄ PARAMETRÓW?? PRZECIEŻ TWORZONE SĄ TABLICE!
        //(zobacz deklarację metody addAll poniżej)
        Point<Double,Double> pointDouble = new Point<>(); //nie przejdzie
        addAll(new LinkedList<>(), new Point<>(), new Point<>() /*, pointDouble */);
        
    }
    
    private static <T> void addAll(Collection<T> collection, T... args){
        //teraz mamy dostęp do tablicy typów generycznych!
        for(T element : args){
            collection.add(element);
        }
        
        collection.addAll(Arrays.asList(args));
    }
}
