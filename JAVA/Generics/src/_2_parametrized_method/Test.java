package _2_parametrized_method;

/**
 *
 * @author tomek
 */
public class Test {

    static String[] array = {"1", "2", "3", "4", "5", "6", "7"};

    public static void main(String[] args) {
        //aby sprawdzić jaki typ kompilator stosuje do wywołania metody parametryzowanej, należy celowo popełnić błąd i odczytać błąd kompilatora
        //Integer middleOne = Static.getMiddleOne(array);
        String middleOne = Test.<String>getMiddleOne(array);
        //lub prościej (ponieważ kompilator domyśli się po typach parametru)
        String middleOne2 = getMiddleOne(array);

    }

    private static <T> T getMiddleOne(T[] array) {
        return array[array.length / 2];
    }
}
