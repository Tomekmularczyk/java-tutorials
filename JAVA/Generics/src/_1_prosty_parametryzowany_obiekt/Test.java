package _1_prosty_parametryzowany_obiekt;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tomek
 */
import model.Point;
import static java.lang.System.*;
import java.util.Date;

public class Test {

    public static void main(String[] args) {
        //klasy które są zadeklarowane jako parametryzowane wciąż możemy używać po staremu, czyli bez ograniczania typów,
        //wtedy możemy wstawić do nich dowolny Object
        Point unparametrized = new Point();
        unparametrized.x = new Date();
        unparametrized.y = new StringBuilder();
        out.println(unparametrized);

        //na szczęscie od Javy 5 wprowadzono typy paramteryzowane i twórca biblioteki może z góry nakreślić
        //co wolno wkładać do jego obiektów i metod
        Point<String, Integer> point = new Point<>();
        point.x = "170sc";
        point.y = 100;
        out.println(point);

    }

}
