package _15_typ_wieloznaczny_a_parametryzowany;

import java.io.Serializable;
import java.util.List;
import model.Couple;
import model.Manager;

/**
 * @author tomek
 */
public class Test {    
    /*
        "The general rule is to use wildcards when you can because code with wildcards is generally more readable 
        than code with multiple type parameters. When deciding if you need a type variable, ask yourself 
        if that type variable is used to relate two or more parameters, or to relate a parameter type with the return type. 
        If the answer is no, then a wildcard should suffice."
        - The Java Programming by James Gosling 4th edition
    */
    
    private static <T extends Number> void work(T type){} //a teraz spróbujmy tego samego za pomocą typu wieloznacznego..
    //private static void work2(? type){}   //...nie da rady
    
    //---------------------------------------------------------------------
    
    /* Obydwie poniższe metody robią to samo.
        "For maximum flexibility, use wildcard types on input parameters that represent producers or consumers.
        Do not use wildcard types as return types. Rather than providing additional flexibility for your users, 
        it would force them to use wildcard types in client code. 
        Properly used, wildcard types are nearly invisible to users of a class. 
        They cause methods to accept the parameters they should accept and reject those they should reject. 
        If the user of the class has to think about wildcard types, there is probably something wrong with the class's API."
        - Effective Java 2nd Edition, Item 28: Use bounded wildcards to increase API flexibility.
    */
    
    private static <T extends Number> Couple<T> calculate(Couple<T> couple){
        return couple;
    }
    private static Couple<?> calculate2(Couple<? extends Number> couple){ 
        return couple; 
    }
    
    //---------------------------------------------------------------------
    
    //A wildcard can have only one bound, while a type parameter can have several bounds.
    
    private static <T extends Comparable & Serializable & AutoCloseable> void findAndClose(List<T> type){}
    //private static void findAndClose(List<? extends Comparable & Serializable & AutoCloseable> type){} //tylko jedno ograniczenie!
    
    //---------------------------------------------------------------------
    
    //A wildcard can have a lower or an upper bound, while there is no such thing as a lower bound for a type parameter.
    private static void dance(List<? super Manager> people){}
    //private static <T super Manager> void dance2(List<T> people){} //nie da rady. typ parametryzowany nie może mieć dolnej granicy
        
    
    
    //Producer Extends Consumer Super :
    //kiedy chcemy zapisywać coś w obiekcie czy liście typ graniczny określamy przy pomocy "super"
        //-- ale wtedy nie możemy odczytywać
    //kiedy chcemy odczytywać coś w obiekcie czy liście typ graniczny określamy przy pomocy "extends"
        //-- ale wtedy nie możemy zapisywać
}