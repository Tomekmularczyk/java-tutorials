package _10_polimorfizm_i_typy_ogolne;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import model.Employee;
import model.Manager;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        ArrayList<Manager> managers = new ArrayList<>();
        
        //ogólna zasada jest taka, że bez względu na to co łączy Employee i Manager - nie ma to znaczenia przy obiektach parametryzowanych
        //employees = managers; //incopatible types, mimo, że Manager jest też typem Employee
        
        //gdy typ obiekt jest parametryzowany można powiedzieć że tworzony jest nowy typ dlatego nie działa tu polimorfizm....
        //... tak więc ArrayList<Employee> a ArrayList<Manager> to dwa różne typy!
        
        //dlaczego polimorfizm tu nie zadziałał? ze względów bezpieczeństwa.
        //Gdyby to było możliwe to teraz ktoś mógłby zrobić coś takiego:
        //employees.add(new Employee());
        //ale przecież lista employees odwołuje się w rzeczywistości do listy przechowującej managerów!
        //...teraz ktoś kto wywołałby managers.getLast() otrzymałby referencje do obiektu Employee! a przecież deklarował liste z Managerami.
        
        //Ponieważ każdy typ parametryzowany jest podtypem typu surowego (ze względu na zgodność ze starym kodem) możemy zrobić jednak Dirty Hack...
        ArrayList rawEmployees = managers; //teraz odnosimy się do managerów
        rawEmployees.add(new Employee()); //...jednak spowodujemy później w programie wyjątek ClassCastException
        rawEmployees.add(new File("path"));
        
        Manager manager0 = managers.get(0); //ClassCastException Employee cannot be cast to Manager
        Manager manager1 = managers.get(1); //ClassCastException File cannot be cast to Manager
        
        //it is allowed to to do this:
        List<Employee> employees2 = employees;
        List<Manager> managers2 = managers;
        //employees2 = managers2; // ale to już nie jest prawidłowe tak jak wytłumaczone powyżej
    }
}
