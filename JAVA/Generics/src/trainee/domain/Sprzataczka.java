package trainee.domain;

import model.Employee;

/**
 * @author tomek
 */
public class Sprzataczka extends Employee{
    private int yearsToRetire;

    public int getYearsToRetire() {
        return yearsToRetire;
    }

    public void setYearsToRetire(int yearsToRetire) {
        this.yearsToRetire = yearsToRetire;
    }

    @Override
    public String toString() {
        return super.toString() + ", yearsToRetire=" + yearsToRetire + '}';
    }
}
