package trainee.domain;

import model.Employee;

/**
 * @author tomek
 */
public class Accountant extends Employee{
    private int mathSkillsLevel = 100;

    public int getMathSkillsLevel() {
        return mathSkillsLevel;
    }

    public void setMathSkillsLevel(int mathSkillsLevel) {
        this.mathSkillsLevel = mathSkillsLevel;
    }

    @Override
    public String toString() {
        return super.toString() + ", mathSkillsLevel=" + mathSkillsLevel + '}';
    }
}
