package trainee;

import static java.lang.System.*;
import model.Employee;
import model.Manager;
import trainee.domain.Accountant;
import trainee.domain.Sprzataczka;
/**
 * @author tomek
 */
public class TraineeWheels {
    public static void main(String[] args) {
        PairExtended<String> pairEx = new PairExtended<>();
        Pair<String> pair = new Pair<>(); //można
        
        pair = pairEx;
        
        //===================
        
        EmployeeHelper helper = new EmployeeHelper();
        Employee employee = helper.employeeBuilder().setName("Kasia").setSurname("Bang").build();
        Manager manager = helper.managerBuilder().setName("Jerzy").setSurname("Jerzyk").build();
        Accountant accountant = helper.accountantBuilder().setName("Jasiu").setSurname("Kowalski").build();
        Sprzataczka sprzataczka = helper.sprzataczkaBuilder().setName("Katarzyna").setSurname("Zborowska").build();
        
        out.println(employee);
        out.println(manager);
        out.println(sprzataczka);
        out.println(accountant);
    }
    
    private static class Pair<T> {
        private T first;
        private T second;        

        public T getFirst() {
            return first;
        }
        public void setFirst(T first) {
            this.first = first;
        }
        public T getSecond() {
            return second;
        }
        public void setSecond(T second) {
            this.second = second;
        }
    }
    
    //przy dziedziczeniu musisz podać paramter typowy do dziedziczonej klasy inaczej będziesz dziedziczył po surowym typie
    public static class PairExtended<T> extends Pair<T>{

        
    }
}
