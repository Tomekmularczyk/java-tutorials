package trainee;

import model.Employee;
import model.Manager;
import trainee.domain.Accountant;
import trainee.domain.Sprzataczka;

/**
 * @author tomek
 */
public class EmployeeHelper {
    
    public EmployeeBuilder employeeBuilder(){
        return new EmployeeBuilder(Employee.class);
    }
    public ManagerBuilder managerBuilder(){
        return new ManagerBuilder(Manager.class);
    }
    public AccountantBuilder accountantBuilder(){
        return new AccountantBuilder(Accountant.class);
    }
    public SprzataczkaBuilder sprzataczkaBuilder(){
        return new SprzataczkaBuilder(Sprzataczka.class);
    }
    
    
    private class BasicBuilder<T extends Employee, B>{
        private String name;
        private String surname;
        private T instance;
        private final B builder;

        public BasicBuilder(Class<T> type){
            builder = (B) this;
            try{
                instance = type.newInstance();
            }catch(Exception exception){
                System.err.println(exception);
            }
        }
        
        public B setName(String name) {
            this.name = name;
            return builder;
        }
        public B setSurname(String surname) {
            this.surname = surname;
            return builder;
        }
        
        public T build(){
            instance.setName(name);
            instance.setSurname(surname);
            return instance;
        }
    }
    
    public class EmployeeBuilder extends BasicBuilder<Employee, EmployeeBuilder>{
        public EmployeeBuilder(Class<Employee> type) {
            super(type);
        }
    }
    
    public class ManagerBuilder extends BasicBuilder<Manager, ManagerBuilder>{
        private boolean hasOfficeKeys = true;

        public ManagerBuilder(Class<Manager> type) {
            super(type);
        }
        
        public ManagerBuilder setHasOfficeKeys(boolean hasOfficeKeys) {
            this.hasOfficeKeys = hasOfficeKeys;
            return this;
        }

        @Override
        public Manager build() {
            Manager manager = super.build();
            manager.setHasOfficeKeys(hasOfficeKeys);
            return manager;
        }
    }
    
    public class AccountantBuilder extends BasicBuilder<Accountant, AccountantBuilder>{
        private int mathSkills = 100;

        public AccountantBuilder(Class<Accountant> type){
            super(type);
        }
        
        public void setMathSkills(int mathSkills) {
            this.mathSkills = mathSkills;
        }

        @Override
        public Accountant build() {
            Accountant accountant = super.build();
            accountant.setMathSkillsLevel(mathSkills);
            return accountant;
        }
    }
    
    public class SprzataczkaBuilder extends BasicBuilder<Sprzataczka, SprzataczkaBuilder>{
        private int yearsToRetire = 0;

        public SprzataczkaBuilder(Class<Sprzataczka> type) {
            super(type);
        }

        public void setYearsToRetire(int yearsToRetire) {
            this.yearsToRetire = yearsToRetire;
        }

        @Override
        public Sprzataczka build() {
            Sprzataczka sprzataczka = super.build();
            sprzataczka.setYearsToRetire(yearsToRetire);
            return sprzataczka;
        }
    }
}
