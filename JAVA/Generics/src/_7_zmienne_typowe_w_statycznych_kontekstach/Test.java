/*
 */
package _7_zmienne_typowe_w_statycznych_kontekstach;

/**
 *
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
     
    }
    
    //Do zmiennych typowych nie można odwoływać się w polach i metodach statycznych.
    private static class Square<L,R,T,B>{
//        private static T type;
        private R r;
        
//        public static R getR(){
//            return null;
//        }
        
    }
}
