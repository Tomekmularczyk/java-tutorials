package _12_nadtypy_wieloznaczne;

import static java.lang.System.out;
import model.Couple;
import model.Human;
import model.LivingCreature;

/**
 * @author tomek
 */
public class Test {
    //Podsumowując, typy wieloznaczne z ograniczeniami nadtypów pozwalają na zapis w obiektach ogólnych,
    
    public static void main(String[] args) {
        Human kasia = new Human();
        LivingCreature dog = () -> "wrrr";
        
        Couple<LivingCreature> couple = new Couple<>();
        saveToCouple(couple, kasia, dog);
     
        out.println(couple);
    }
    
    /*  Tutaj zapisywanie jest bezpieczne ponieważ typ parametryzowany Couple nie będzie niżej w hierarchii dziedziczenia niż LivingCreature 
        więc zapisywanie do niego obiektów LivingCreature (i roszerzonych) jest bezpieczne. 
        Natomiast przy pobieraniu z niego wartości już być pewni być nie możemy czy dostaniemy LivingCreature czy Object
    */
    private static void saveToCouple(Couple<? super LivingCreature> couple, LivingCreature ying, LivingCreature yang){
        couple.ying = ying;
        couple.yang = yang;
        //LivingCreature creature = couple.yang; // nie da rady ponieważ yang jest obiektem matką ale nie wiadomo jakiego typu
        Object creature = couple.yang; //tylko tak
    }
    
    private static void readFromCouple(Couple<? extends LivingCreature> couple){
        LivingCreature creature = couple.yang; //można ponieważ typ parametryzowany będzie "conajmniej" typu LivingCreature
        //couple.yang = new Human(); //nie można ponieważ Couple by była typu HumanWithFourLegs?? nastąpiłoby niebezpieczne rzutowanie
    }
}
