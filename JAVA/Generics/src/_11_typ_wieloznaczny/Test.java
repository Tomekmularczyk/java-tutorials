package _11_typ_wieloznaczny;

import static java.lang.System.out;
import model.Couple;
import model.Employee;
import model.Manager;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        Couple<Employee> employeesCouple = new Couple<>();
        employeesCouple.ying = new Employee("Zosia","Kac");
        employeesCouple.yang = new Employee("Adam","Kendra");
        
        printAll(employeesCouple);
        
        
        Couple<Manager> managersCouple = new Couple<>();
        managersCouple.ying = new Manager("Kasia","Supa");
        managersCouple.yang = new Manager("Wojtek","Łonc");
        //printAll(managersCouple); //nie zadziała polimorfizm! tak jak wyjaśnione w poprzednej lekcji. Relacje typowych parametryzowanych nie ma znaczenia
        
        //możemy sobie z tym poradzić na 2 sposoby (Patrz metody)
        printAllAdvanced(managersCouple);
        printAllAdvanced2(managersCouple);
        
        //poniższy typ wieloznaczny oznacza dowolny typ ogólny Couple, którego parametr typowy jest podklasą klasy Employee, na przykład Pair<Manager>, ale nie Pair<String>.
        Couple<? extends Employee> couple2; 
        couple2 = employeesCouple;//teraz możemy przypisać obiekty jak przy polimorfizmie
        couple2 = managersCouple; //typ Couple<Manager> jest podtypem Couple<? extends Employee>
        //couple2.yang = new Employee(); //nie można przypisywać poniewać kompilator nie wie jakiego konkretnego typu jest dżoker i mogłoby nastąpić niebezpieczne rzutowanie
        
        //podsumowując typy wieloznaczne z ograniczeniami podtypów pozwalają na odczyt z obiektów ogólnych.
    }
    
    private static void printAll(Couple<Employee> couple){
        out.print(couple.ying + " kocha: ");
        out.println(couple.yang);
    }
    
    private static <T extends Employee> void printAllAdvanced(Couple<T> couple){
        out.print(couple.ying + " kocha: ");
        out.println(couple.yang);
        
        //couple.yang = new Employee();  //Employee cannot be converted to T
    }
    
    //nie możemy nadać takiej samej nazwy bo po wymazaniu typów będą mieć identyczną listę argumentów
    private static void printAllAdvanced2(Couple<? extends Employee> couple){
        out.print(couple.ying + " kocha: ");
        out.println(couple.yang);
        
        //couple.ying = new Employee(); //nie można przekonwertować Employee do świeżej zmiennej typowej ? która rozszerze Employee
    }
}
