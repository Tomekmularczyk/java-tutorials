/*
 */
package _8_obiekty_ogolne_a_wyjatki;

/**
 *
 * @author tomek
 */
public class Test {
    
//    private static class Wyjatek<T> extends Exception{
        //error: a generic class may not extend java.lang.Throwable
        //nie można roszerzać Throwable klasą parametryzowaną
//    }
    
    private static <T extends Throwable> void doSomeCrazyWork(Class<T> toy) throws Throwable{
  //     try{
            // foolin' around!
            throw toy.newInstance();
  //      }catch(T exception){ //nie można przechwytywać zmiennej typowej(required class found type parameter)
            
  //      }
    }
    
    private static <T extends Throwable> void doSomeDirtyWork(T t) throws T{
        try{
            //natomiast możemy używać zmiennej typowej w specyfikacjach wyjątku
        }catch(Throwable realCause){
            t.initCause(realCause);
            throw t;
        }
    }
    
}
