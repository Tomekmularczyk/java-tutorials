/*
 */
package _6_tworzenie_zmiennych_typowych;

import model.Point;
import static java.lang.System.out;
import java.util.Arrays;

/**
 *
 * @author tomek
 */
public class Test {

    public static void main(String[] args) throws ReflectiveOperationException {
        Point<String,String> point = NewPoint.getPointByType(String.class);
        Point<String,String> p = NewPoint.getInstanceOfType(Point.class);
        p.x = "dsf";
        p.y = "def";
        
        MyArrayList<String> mAL= new MyArrayList<>();
        mAL.add("to");
        mAL.add("jest");
        mAL.add("test");
        
        out.println(mAL);
    }

    private static class NewPoint{
        public static <T> Point<T,T> getPointByType(Class<T> type) {
            // T[] t = new T[3]; //-- generic array creation, wymazywanie typów spowodowałoby że zawsze dostawalibyśmy obiekty typu 'Object'
            
            return new Point<>();
        }
        
        public static <T> T getInstanceOfType(Class<T> type) throws ReflectiveOperationException{
            return type.newInstance();
        }
    }
    
    private static class MyArrayList<T>{
        private T[] elements;
        
        public MyArrayList(){
            elements = (T[]) new Object[0];
        }
        public void add(T element){
            elements = Arrays.copyOf(elements, elements.length +1);
            elements[elements.length-1] = element;
        }
        
        public T[] toArray(){
            Object[] mm = new Object[2];
            return (T[]) mm;
        }
        
        
        
        @Override
        public String toString(){
            return Arrays.toString(elements);
        }
    }
}
