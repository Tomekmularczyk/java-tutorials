package _4_wymazywanie_typów_metody_pomostowe;

import model.Point;
import java.util.Date;

/**
 *
 * @author tomek
 */
public class Test {

    public static void main(String[] args) {
        Triangle triangle = new Triangle();
        triangle.setX(new Double(3.4));  //tutaj wywoływane są metody pomostowe przyjmujące Object jako paramter
        triangle.setY(new Integer(3));
        triangle.z = new Float(3.44F);

        //  Fakty dotyczące translacji typów ogólnych:
        //  W maszynie wirtualnej nie ma typów ogólnych, tylko zwykłe klasy i metody.
        //  Parametry typowe są zastępowane odpowiadającymi im typami granicznymi.
        //  Metody pomostowe są syntetyzowane w celu zachowania polimorfizmu.
        //  Rzutowanie jest wstawiane w razie potrzeby w celu zachowania bezpieczeństwa typów.
        //===============instanceof
        Point<Integer, Double> pointA = new Point<>();

        //Każde użycie operatora instanceof lub zastosowanie rzutowania związane z typami ogólnymi będzie skutkowało zgłoszeniem przez kompilator ostrzeżenia, 
        //Obiekty w maszynie wirtualnej zawsze należą do konkretnego typu nieogólnego. Dlatego operacja sprawdzania sprawdza tylko, czy pointA jest obiektem jakiejkolwiek klasy Pair
//        if(pointA instanceof Point<Date,Float>){
//            System.out.print("!!");
//        }
        Point<Double, Date> pointB = new Point<>();
        if (pointA.getClass() == pointB.getClass()) {
            System.out.println("obydwa wywołania zwracają Point.class ponieważ obiekty parametryzowane nie istnieją w wirtualnej maszynie");
        }
    }

    private static class Triangle<Z extends Number> extends Point<Number, Number> {
        public Z z;

        //Wymazywanie typów zakłóca polimorfizm. Dlatego kompilator stara się uniknąć problemu, generując metodę pomostową
        //(wymazuje typowane parametry(Number) i przyjmująca Object jako paramatery)
    }
}
