/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tomek
 */
public interface LivingCreature{
    //domyślnie: public static final
    String ID =  String.valueOf(Math.random()*10000);

    String breathe();    

    
    default String tohString(){ 
        return breathe(); 
    }
}
