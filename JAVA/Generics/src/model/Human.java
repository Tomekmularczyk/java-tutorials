package model;

/**
 * @author tomek
 */
public class Human implements LivingCreature, Voiced {

    @Override
    public String breathe() {
        return "iiin and oooout...";
    }

    @Override
    public String scream() {
        return "This is SPARTA!";
    }

    @Override
    public String toString() {
        return "Human{" +  breathe() + " " + scream();
    }
}
