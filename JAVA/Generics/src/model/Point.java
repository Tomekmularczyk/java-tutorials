
package model;

/**
 * @author tomek
 * @param <X> x
 * @param <Y> y
 * 
 * Obiekty w maszynie wirtualnej zawsze należą do konkretnego typu nieogólnego
 * W "typie surowym" zmienne klasowe X i Y będą zamienione na podane do klamerek klasy
 */
public class Point<X,Y> {
    public X x;
    public Y y;

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + '}';
    }
    
    //-------------- przydadzą się w dalszej części do wymazywania typów i metodach pomostowych
    public X getX() {
        return x;
    }

    public void setX(X x) {
        this.x = x;
    }

    public Y getY() {
        return y;
    }

    public void setY(Y y) {
        this.y = y;
    }
    
}
