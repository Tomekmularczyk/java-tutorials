package model;

/**
 * @author tomek
 */
public class Manager extends Employee{
    private boolean hasOfficeKeys = true;

    public Manager(String name, String surname) {
        super(name, surname);
    }
    public Manager() {}
    
    
    public boolean isHasOfficeKeys() {
        return hasOfficeKeys;
    }
    public void setHasOfficeKeys(boolean hasOfficeKeys) {
        this.hasOfficeKeys = hasOfficeKeys;
    }

    @Override
    public String toString() {
        return super.toString() + ", hasOfficeKeys=" + hasOfficeKeys + '}';
    }
}
