package model;

/**
 * @author tomek
 * @param <T>
 */
public class Couple <T>{
    public T ying;
    public T yang;

    @Override
    public String toString() {
        return "Couple{" + "ying=" + ying + ", yang=" + yang + '}';
    }
}
