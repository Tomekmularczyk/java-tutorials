package _9_konflikty_po_wymazaniu_typow;
/**
 *
 * @author tomek
 */
public class Test {
    
    
    private class ContainerForTwo<P> {
        public P p1, p2;

        
        /* nastąpił konflik nazw. A to dlatego, że po wymazaniu typów (type erasure) tworzona będzie metoda pomostowa która zawsze zamienia 
         * wymazywany typ na Object. W tej sytuacji następuje konflikt z metodą equals(Object object) z super klasy. Należy zmienić nazwe metody.
         */
//        @Override
//        public boolean equals(P obj) {
//            return super.equals(obj); 
//        }
    }
    
    //Aby translacja poprzez wymazywanie typów była możliwa, klasa lub zmienna typowa nie może być w jednym czasie podtypem
    //dwóch interfejsów będących różnymi wersjami parametrycznymi tego samego interfejsów
    
    private abstract class Cyfra implements Comparable<Integer>{}
/*    
    private class Cyferki extends Cyfra implements Comparable<Double>{
//        interfejs nie może być dziedziczony z różnymi typami
//        ... dlaczego skoro jest przeładowanie metod?
//        ...powód tkwi głęboko. Po wymazaniu typów powstałby problem pomiędzy zsyntezowanymi metodami pomostowymi:
        public int compareTo(Object o){ //---metoda pomostowa
            //którą metodę mam wywołać??
            return compareTo((Integer) o);
            return compareTo((Double) o);
        }
    }
*/
}
