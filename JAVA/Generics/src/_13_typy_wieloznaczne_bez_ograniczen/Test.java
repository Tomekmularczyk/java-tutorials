package _13_typy_wieloznaczne_bez_ograniczen;

import static java.lang.System.out;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("Dziś");
        list.add("w");
        list.add("klubie");
        list.add("bedzie");
        list.add("bang");
        
        out.println(hasNull(list));
    }
    
    //typy wieloznaczne nadają tyko się do prostych działań, ponieważ nie możemy stworzyć elementu typu wieloznacznego(tylko przypisac do Object)
    private static boolean hasNull(List<?> list){
        //? element = list.get(0);
        Object get = list.get(0);
        //list.set(0, new Object()); //nie możemy robić żadnych przypisań
        
        Iterator<?> iterator = list.iterator();
        while(iterator.hasNext()){
            if(iterator.next() == null)
                return true;
        }
        
        return false;
    }
   
    
}
