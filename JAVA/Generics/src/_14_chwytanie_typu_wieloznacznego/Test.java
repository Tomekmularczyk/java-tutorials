package _14_chwytanie_typu_wieloznacznego;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        //================1====================
        List<String> list = new LinkedList<>();
        Collections.addAll(list, "dziś","w","klubie","bedzie", "bang");
        
        hasNull(list);
        
        
        //================2====================
        List<List<String>> listOfLists = new LinkedList<>();
        listOfLists.add(list);
        //hasNullLists(listOfLists); //nie można ponieważ nie ma związku pomięczy Listą typowaną jednego rodzaju a listą typowaną drugiego rodzaju
        //więc gdyby możliwa była taka konwersja to wtedy Lista mogłaby przechowywać niekompatybilne ze sobą obiekty(inne listy)
        hasNullListsHelper(listOfLists);
    }
    
    //================1====================
    private static void hasNull(List<?> list){
        //? element = list.get(5); //nie możemy odzyskać typu, możemy jedynie przypisać do Object
        //list.add(list.get(2)); //też nie możemy nić przypisywać ani dodawać
        //list.add(new Object());  //..też nie
        
        //ale możemy zastosować taki trik:
        hasNullHelper(list);
    }
    
    //metoda ogólna może przechwytywać obiekty z typem wieloznacznym
    private static <T> void hasNullHelper(List<T> list){
        T t = list.get(3);
        list.add(t);
        list.add(list.get(2));
    }
    
    //================2====================
    
    //...natomiast chwytanie typu wieloznacznego jest dozwolone w ściśle określonych warunkach
    //Kompilator musi być w stanie zagwarantować, że symbol wieloznaczny reprezentuje jeden określony typ. 
    private static void hasNullLists(List<List<?>> list){
        
    }
    private static <T> void hasNullListsHelper(List<List<T>> list){
        
    }
    
}
