package _1_tworzenie_strumieni;

import domain.Kid;
import domain.Person;
import static java.lang.System.*;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author tomek
 */
public class Test {

    /* W przypadku strumieni określasz co powinno zostać wykonane a nie - w jaki sposób.
     * Określasz źródło danych i parametry, a to biblioteka zajmuje się optymalizacją i wykonywaniem obliczeń
       (np korzystając z wielowątkowości) oraz zwróceniem rezultatu.
    
       W zwykłej pętli określamy dokładnie co ma się stać, i często musimy pracować z każdym elementem po kolei, natomiast
       strumień nie przechowuje ani nie modyfikuje danych, oraz co najistotniejsze są "leniwe" tzn. że ich działanie jest
       opóźnione do momentu gdy potrzebny jest wynik danego działania (a nie jak w przypadków pętli, wykonywane jest działanie na każdym elemencie).
    
       Praca ze strumieniem jest podzielona na 2 etapy:
        - operacje pośrednie (intermediate): przekształcające początkowy strumień do innej bardziej skonkretyzowanej postaci
        - operacje kończące (terminating): właściwe wykonanie "leniwych" operacji na strumieniu
       Po tych etapach strumień nie może być już dalej wykorzystany.
     */
    public static void main(String[] args) {
        //z każdej kolekcji można stworzyć strumień jak poniżej
        Person.getSimpleList().stream();

        //z tablicy także
        String str = "te nowe strumienie są całkiem spoko!";
        Stream.of(str.split(" "));

        Kid[] kidArray = Kid.getSimpleArray();
        Arrays.stream(kidArray, 2,6);
        
        double[][][] arr = new double[4][5][2];
        Arrays.stream(arr);

        Stream.of(0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 9);
       
        //możemy tworzyć nieskończone strumienie
        Stream.generate(Math::random);
        
        //..jak i nieskończone sekwencje 1,2,3,4,5...
        Stream<Integer> iterate = Stream.iterate(0, n -> n + 2);
        Object[] result = iterate.limit(10).toArray();
        out.println(Arrays.toString(result));
        
        out.println(  //lub krócej
                Arrays.toString(
                Stream.iterate(10, n -> n + 10)
                        .limit(10)
                        .toArray()));
        
        //lub też w zupełnie inny sposób
        Stream.iterate("", s -> s += (char) (Math.random() * 50 + 50))
                .limit(10)
                .forEach(out::println);
        
        //dodatkowo strumienie są "poukrywane" w różnych bibliotekach np.
        //Pattern.compile("\\PL+").splitAsStream(treść);
        //Stream<String> wiersze = Files.lines(ścieżka)
    }

}
