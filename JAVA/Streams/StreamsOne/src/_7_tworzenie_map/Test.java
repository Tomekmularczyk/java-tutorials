package _7_tworzenie_map;

import domain.Kid;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        Kid[] kids = Kid.getSimpleArray();
        
        Map<String, Kid> map = Stream.of(kids) //podajemy dwie funkcje do przetwarzania klucza i wartości. Function.identity() używamy jeżeli potrzebujemy całego obiektu
                .collect(Collectors.toMap(Kid::getSurname, Function.identity())); 
        
        //jeżeli jednak pojawią się dwa klucze o takiej samej wartości zostaje wyrzucony wyjątek: java.lang.IllegalStateException: Duplicate key
        //możemy jednak przesłonić to zachowanie…
        Stream.of(kids)
                .collect(Collectors.toMap(Kid::getName, Function.identity(), (existingValue, newValue) -> newValue)); //w przypadku pojawienia się konfliktu nakazujemy nadpisać obiekt
        

        //Jeżeli potrzebujemy mapy która ma w sobie kolekcje jako wartość…
        Map<String, Set<Kid>> kidsMappedByName = Stream.of(kids)
                .collect(Collectors.toMap(
                        Kid::getName,
                        k -> Collections.singleton(k), 
                        (old, neww) -> { //połączenie, zlepiamy elemnty
                            Set<Kid> set = new HashSet<>(old);
                            set.addAll(neww);
                            return set;
                        }));
        //taka sytuacja gdy potrzebujemy mieć kolekcje w kolekcji jest dość częste dlatego później zostanie przedstawiona prostsza metoda groupingBy()…
        
        //Jeżeli potrzebujemy konkretnego rodzaju mapy…
        TreeMap<Integer, Kid> treeMapKids = Stream.of(kids)
                .collect(Collectors.toMap(
                        Kid::getAge, 
                        Function.identity(),
                        (old, neww) -> neww,
                        TreeMap::new
                ));
        
        //wcześniej, że stworzyć kolekcje w mapie potrzebowaliśmy stworzyć kolekcje z jednym elementem, a potem ją scalać przy konflikcie kluczy
        //Jest jednak to częsty przypadek i da się to zrobić prościej…
        Locale[] locales = Locale.getAvailableLocales(); //zawiera informacje o regionach.
        Map<String, List<Locale>> collect = Stream.of(locales)
                .collect(Collectors.groupingBy( //grupuje wartości przy ponownym wystąpienia klucza
                        Locale::getCountry //jest to funkcja klasyfikująca grupowania
                ));
        
        //to co poprzednio tylko prościej
        Map<String, List<Kid>> kidsMappedByName2 = Stream.of(kids)
                .collect(Collectors.groupingBy(
                        Kid::getName
                ));
        
        
        //Jeżeli potrzebujemy podzielić mapę na dwa klucze TRUE/FALSE, czyli gdy funkcja klasyfikująca jest predykatem, 
        //to możemy użyć bardziej wydajnego partitioningBy() zamiast groupingBy()
        //znajdujemy wszystkie miejsca gdzie używa się angielskiego i wszystkie miejsca gdzie nie jest on używany…
        Map<Boolean, List<Locale>> enLocales = Stream.of(locales)
                .collect(Collectors.partitioningBy(
                        l -> l.getLanguage().equals("en")
                ));
        //właśnie podzieliliśmy miejsca na te gdzie uzywany jest angielski i te gdzie nie jest
        List<Locale> english = enLocales.get(true);
        List<Locale> nonEnglish = enLocales.get(false);
        
        System.out.println("English locales: \n" + english + "\nNonEnglish locales:\n" + nonEnglish);
    }
}
