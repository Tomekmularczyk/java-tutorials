package domain;

/**
 * @author tomek
 */
public abstract class HomoSappiens {
    private SEX sex;
    private int age;
    
    public HomoSappiens(int age){
        this.age = age;
    }
    

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public SEX getSex() {
        return sex;
    }

    public void setSex(SEX sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " age=" + age + " ";
    }
}
