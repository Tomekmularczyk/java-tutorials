package domain;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author tomek
 */
public class Person extends Citizen{
    private String name;
    private boolean hasCar = false;

    public Person(String name, String surname, int age, boolean hasCar) {
        super(surname, age, null);
        this.name = name;
        this.hasCar = hasCar;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasCar() {
        return hasCar;
    }

    public void setHasCar(boolean hasCar) {
        this.hasCar = hasCar;
    }
    
    public static List<Person> getSimpleList(){
        List<Person> personList = new LinkedList<>();
        Collections.addAll(personList, 
                new Person("Tomek", "Mularczyk", 26, false),
                new Person("Adam", "Pradzioch", 19, false),
                new Person("Monika", "Stępień", 29, true),
                new Person("Katarzyna", "Inglot", 45, true),
                new Person("Jan", "Kowalski", 34, false),
                new Person("Krystyna", "Czubówna", 66, true),
                new Person("Józef", "Lepiej", 73, true),
                new Person("Piotr", "Janda", 14, false)
        );
        
        return personList;
    }

    @Override
    public String toString() {
        return super.toString() + "name=" + name + ", age=" + getAge() + ", hasCar=" + hasCar + '}';
    }
}
