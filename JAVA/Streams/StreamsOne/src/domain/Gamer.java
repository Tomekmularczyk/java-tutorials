package domain;

/**
 * @author tomek
 */
public class Gamer extends Kid {

    private int skill;

    public Gamer(String nick, SEX sex, int age, int skill) {
        super("", nick, age, false);
        setSex(sex);
        this.skill = skill;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "Nick: " + getSurname() + ", age: " + getAge() + ", skill level: " + getSkill();
    }

    public static Gamer[] getSimpleArray() {
        Gamer[] array = new Gamer[13];

        int iterator = 0;
        array[iterator++] = new Gamer("Mularczyk", SEX.MALE, 16, 99);//0
        array[iterator++] = new Gamer("Pradzioch", SEX.MALE, 19, 15);//1
        array[iterator++] = new Gamer("Stępień", SEX.MALE, 9, 34);//2
        array[iterator++] = new Gamer("Inglot", SEX.FEMALE, 25, 54);//3
        array[iterator++] = new Gamer("Kowalski", SEX.MALE, 34, 23);//4
        array[iterator++] = new Gamer("Czubówna", SEX.FEMALE, 66, 74);//5
        array[iterator++] = new Gamer("Lepiej", SEX.FEMALE, 13, 74);//6
        array[iterator++] = new Gamer("Janda", SEX.MALE, 14, 45);//7
        array[iterator++] = new Gamer("Pelc", SEX.FEMALE, 31, 88);//8
        array[iterator++] = new Gamer("Łagowska", SEX.FEMALE, 83, 66);//9
        array[iterator++] = new Gamer("Zibi", SEX.MALE, 43, 5);//10
        array[iterator++] = new Gamer("Kogut", SEX.MALE, 62, 55);//11
        array[iterator++] = new Gamer("Hala", SEX.MALE, 11, 46);//12

        return array;
    }
}
