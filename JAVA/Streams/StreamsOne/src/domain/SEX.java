package domain;

/**
 * @author tomek
 */
public enum SEX {
    MALE, FEMALE
}
