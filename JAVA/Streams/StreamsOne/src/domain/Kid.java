package domain;

/**
 * @author tomek
 */
public class Kid extends Citizen {

    private String name;
    private boolean hasToy = true;

    public Kid(String name, String surname, int age, boolean hasToy) {
        super(surname, age, null);
        this.name = name;
        this.hasToy = hasToy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasToy() {
        return hasToy;
    }

    public void setHasToy(boolean hasToy) {
        this.hasToy = hasToy;
    }

    public static Kid[] getSimpleArray() {
        Kid[] array = new Kid[10];

        byte iterator = 0;
        array[iterator++] = new Kid("Tomek", "Mularczyk", 16, false);//0
        array[iterator++] = new Kid("Adam", "Pradzioch", 19, false);//1
        array[iterator++] = new Kid("Tomek", "Stępień", 9, true);//2
        array[iterator++] = new Kid("Katarzyna", "Inglot", 25, true);//3
        array[iterator++] = new Kid("Jan", "Kowalski", 34, false);//4
        array[iterator++] = new Kid("Krystyna", "Czubówna", 66, true);//5
        array[iterator++] = new Kid("Józef", "Lepiej", 13, true);//6
        array[iterator++] = new Kid("Piotr", "Janda", 14, false);//7
        array[iterator++] = new Kid("Jolanta", "Pelc", 31, false);//8
        array[iterator++] = new Kid("Zosia", "Łagowska", 32, true);//9

        return array;
    }

    @Override
    public String toString() {
        return super.toString() + ", name=" + name + ", hasToy=" + hasToy;
    }

}
