package domain;

/**
 * @author tomek
 */
public class Citizen extends HomoSappiens{
    private String surname;

    public Citizen(String surname, int age, SEX sex) {
        super(age);
        setSex(sex);
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return super.toString() + "surname=" + surname;
    }
    
}
