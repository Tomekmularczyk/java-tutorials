package _5_proste_redukcje;

import domain.Kid;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Stream;

/**
 * Redukcje są operacjami kończącymi (terminating), przetwarzają one strumień do wartości nie będącej strumieniem
 */
public class Test {
    public static void main(String[] args) {
        Kid[] kids = Kid.getSimpleArray();
        
        //count(), zlicza elemnty
        long count = Stream.of(kids)
                .filter( k -> k.hasToy() )
                .count();
        
        //najstarsze dziecko które ma zabawke
        OptionalInt max = Stream.of(kids) //Optional, ponieważ jeżeli nie było by żadnych elementów w strumieniu to zwracany byłby NULL, co nie jest do końca porządane
                .filter( k -> k.hasToy() )
                .mapToInt(Kid::getAge)
                .max(); 
        
        //min() działa odpowiednio do max()
        
        //Czy wśród dzieci które mają zabawkę jest dziecko o nazwisku "Janda"
        boolean anyMatch = Stream.of(kids)
                .filter(Kid::hasToy)
                .anyMatch(k -> k.getSurname().equalsIgnoreCase("Janda")); //poprzedni filter można by upakować tutaj, ale wtedy nie wykorzystalibyśmy 
                                                                          //"leniwego" przetwarzania! filter() to operacja pośrednia a anyMatch() kończąca! (pomijając że kod byłby mniej czytelny)
        
        //allMatch() działa odpowiednio do anyMatch()
        
        //Spośród dzieci które mają mniej niż 18 lat znajdz mi pierwsze z listy dziecko, które nie ma zabawki
        Optional<Kid> findFirst = Stream.of(kids)
                .filter(k -> k.getAge() < 18)
                .filter(k -> !k.hasToy())
                .findFirst();
        
        //findAny() działa podobnie jak findFirst(), z tym, że niekoniecznie zwraca nam pierwszy na liście pasujący element.
        //Ma to swoje zastosowanie wtedy gdy strumien jest przetwarzany równolegle i przekłada się na lepszą wydajność
        Optional<Kid> findAny = Stream.of(kids)
                .filter(k -> k.getAge() > 50)
                .findAny();
    }
}
