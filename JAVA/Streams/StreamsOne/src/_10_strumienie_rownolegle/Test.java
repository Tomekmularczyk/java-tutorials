package _10_strumienie_rownolegle;

import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**

* //Rozważ poniższy kod:
Stream<String> słowa = listaSłów.stream();
słowa.forEach(s -> if (s.length() < 12) listaSłów.remove(s));

* błąd! ingerencja


W dokumentacji JDK jest jasno określone by nie modyfikować elementów w strumieniu, ponieważ jego zachowanie będzie wtedy nieprzewidywalne.
* (nawet jezeli operacja jest przystosowana do programów wielowątkowych)
* Nazywane jest to zasadą "nieingerencji"
* 
* Pamiętaj, że strumienie nie przechowują swoich danych - te dane znajdują się w oddzielnej kolekcji
*/
public class Test {
    
    public static void main(String[] args) {
        /*
            Jeżeli strumień jest w trybie równoległym przed wykonaniem metody kończącej (terminating), 
            to wszystkie pośrednie operacje też zostana zrównoleglone
        */
        final String str = "Lorem Ipsum";
        str.chars().filter(Character::isAlphabetic).parallel().forEachOrdered(System.out::println); //operacje pośrednie też zostaną zrównoleglone
        
        //domyślnie strumienie powstające z kolekcji, zakresów, generatorów cz tablic są ** uporządkowane **,
        //jednak jezeli zależy nam na wydajności to możemy użyć nieuporządkowanego strumienia:
        OptionalDouble average = str.chars().parallel().unordered().average();
        
        
        //Collectors.groupingByConcurrent korzysta z mapy współdzielonej przez równoległe wątki. 
        //Aby zrównoleglenie dało korzyść, kolejność wartości mapy nie powinna być taka sama jak kolejność w strumieniu.
        final String[] words = {"Lorem", "Ipsum", "Ma", "da", "faka", "joł"};
        Map<Integer, List<String>> wynik = Stream.of(words).parallel()
                .collect(Collectors.groupingByConcurrent(String::length));
        
        
    }
        
}
