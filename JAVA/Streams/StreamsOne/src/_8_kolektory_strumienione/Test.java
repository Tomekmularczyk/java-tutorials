package _8_kolektory_strumienione;

import domain.Gamer;
import domain.SEX;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author tomek
 */
public class Test {
    //groupingBy() zwraca mapę której wartościami są listy. Aby dalej przetwarzać tę mapę należy korzystać z kolektorów strumienionych
    //kolektory strumienione pozwalają po prostu na dalszą pracę z wartościami mapy, które też są kolekcjami i mogę być przetwarzane strumieniowo

    public static void main(String[] args) {
        Locale[] locales = Locale.getAvailableLocales();
        
        
        Map<String, Set<Locale>> set = Stream.of(locales)
                .collect(Collectors.groupingBy(
                        Locale::getCountry, //funkcja klasyfikująca
                        Collectors.toSet() //kolektor strumieniony który zamienia liste na Set
                ));
        
        Map<String, Long> collect1 = Stream.of(locales) //zliczamy ilość wystąpień danego kraju
                .collect(Collectors.groupingBy(
                        Locale::getLanguage, 
                        Collectors.counting()
                ));
        
        System.out.println(collect1.get("pl"));
        
        
        Gamer[] gamers = Gamer.getSimpleArray();
        
        //dzielimy gamerów na płeć (można by użyć partitioningBy() ponieważ są tylko dwie możliwości) i zliczamy dla nich średnią skila
        Map<SEX, Double> skillBySex = Stream.of(gamers)
                .collect(Collectors.groupingBy(
                        Gamer::getSex,
                        Collectors.averagingInt(Gamer::getSkill)
                ));
        
        //możemy też podzielić Gamerów na płeć i zsumować ich wiek
        Map<SEX, Long> sumOfAge = Stream.of(gamers)
                .collect(Collectors.groupingBy(
                        Gamer::getSex,
                        Collectors.summingLong(Gamer::getAge)
                ));
        
        //najlepszych gamerów z danej płci
        Map<SEX, Optional<Gamer>> bestGamers = Stream.of(gamers)
                .collect(Collectors.groupingBy(
                        Gamer::getSex, 
                        Collectors.maxBy(
                                Comparator.comparingInt(Gamer::getSkill)
                        )
                ));
        
        //mapping() to, to samo co inne kolektory strumienione
        Map<SEX, Set<Integer>> collect = Stream.of(gamers)
                .collect(Collectors.groupingBy(
                        Gamer::getSex,
                        Collectors.mapping(
                                Gamer::getAge, //z tym że potrzebuje dodatkowego kolektora do przetworzenia wyników
                                Collectors.toSet()
                        )
                ));
        
        Comparator<Integer> comparator = (o1, o2) -> o1-o2;
        
        //znajdz najstarszych gamerów podzielonych na płeć…
        Map<SEX, Optional<Integer>> collect2 = Stream.of(gamers)
                .collect(
                        Collectors.groupingBy(
                                Gamer::getSex,
                                Collectors.mapping(
                                        Gamer::getAge, 
                                        Collectors.maxBy(
                                                //(o1, o2) -> o1-o2
                                                Integer::compare
                                        ))
                        ));
        
        collect2.get(SEX.MALE).ifPresent(System.out::println);
        collect2.get(SEX.FEMALE).ifPresent(System.out::println);
        
        //I jeszcze dla treningu, zbierz statystyki o gamerach, i podziel ze względu płeć oraz ich wiek
        Map<SEX, IntSummaryStatistics> collect3 = Stream.of(gamers)
                .collect(
                        Collectors.groupingBy(
                                Gamer::getSex, 
                                Collectors.summarizingInt(Gamer::getAge)
                        ));
    }
}
