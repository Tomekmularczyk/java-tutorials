package _6_kolekcje_wynikow;

import domain.Kid;
import static java.lang.System.*;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * @author tomek
 */
public class Test {

    public static void main(String[] args) {
        Kid[] kids = Kid.getSimpleArray();

        Iterator<Kid> iterator = Stream.of(kids)
                .filter(Kid::hasToy)
                .iterator(); //zwraca nam staroświecki iterator za pomocą którego możemy przeglądać obiekty

        Kid[] kidsArray = Stream.of(kids)
                .filter(Kid::hasToy)
                .toArray(Kid[]::new); //zwraca nam tablice z elementami

        //jeżeli chcielibyśmy wyniki zapisać to np. listy moglibyśmy zrobić tak:
        List<Kid> listOfKids = new LinkedList<>();
        Stream.of(kids)
                .filter(Kid::hasToy)
                .forEach(listOfKids::add);

        //… lub prościej użyć klasy Collectors
        List<Kid> collectedKids = Stream.of(kids)
                .filter(Kid::hasToy)
                .collect(Collectors.toList());
        
        //Jeżeli chcesz być pewny jakiego typu implementacje otrzymujesz użyj referencji do konstruktora
        ArrayList<Kid> collectedTreeSet = Stream.of(kids)
                .filter(Kid::hasToy)
                .collect(Collectors.toCollection(ArrayList::new));
        
        //Collectors.joining() służy do łączenia stringów
        String names = Stream.of(kids)
                .filter(Kid::hasToy)
                .map(Kid::getSurname) //potrzebujemy najpierw przekształcić stream na elemnty typu String
                .collect(Collectors.joining(", "));
        out.println(names);
        
        IntSummaryStatistics statistics = Stream.of(kids)
                .collect(
                        Collectors.summarizingInt(Kid::getAge));
        out.println("Age statistics for kids: \n"  +
                "Max: " + statistics.getMax() + "\n" +
                "Min: " + statistics.getMin() + "\n" +
                "Average: " + statistics.getAverage() + "\n" +
                "Sum: " + statistics.getSum() + "\n" 
        );
        
        
        
        Double collect = Stream.of(kids).collect(Collectors.averagingDouble(Kid::getAge));
        
    }
}
