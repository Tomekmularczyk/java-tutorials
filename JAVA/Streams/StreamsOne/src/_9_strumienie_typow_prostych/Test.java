package _9_strumienie_typow_prostych;

import domain.Person;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author tomek
 */
public class Test {

    public static void main(String[] args) {
        //By wygenerować struniem typów prostych jak int czy double możemy użyć zwykłych strumieni…
        Stream.iterate(0, i -> i++).limit(10);

        /* …lub skorzystać z gotowych strumieni IntStream, DoubleStream i LongStream, które nie opakowują prymitywnych wartości
            co sprawia, że są wydajniejsze.
        
            Jeśli chcesz zapisać wartość typu short, char, byte czy boolean, wykorzystaj IntStream, zaś dla wartości typu float wykorzystaj DoubleStream.     */
        
        
        IntStream.of(1,2,66,77,88,99,1000, 33, 11, 0); //strumień nie uporządkowany
        
        int[] arrayOfInts = {2,3,5,6,2,3,56,2,3,4,1};
        Arrays.stream(arrayOfInts); //strumień z tablicy
        
        IntStream od_0_do99 = IntStream.range(0, 100);
        IntStream od_0_do100 = IntStream.rangeClosed(0, 100);
        
        
        final String str = "Lorem Ipsum";
        IntStream stringToChars = str.chars();
        IntStream codePoints = str.codePoints();
        
        //można też przekształcać różnego rodzaju obiekty na strumienie typów prostych
        IntStream personToInt = Person.getSimpleList().stream().mapToInt(Person::getAge); //zwykła mapa przekształciła by to wyrażenie na Stream<Integer> (co wiąże się z niepotrzebnym opakowywaniem)
        
        //jeżeli chcemy przekształcić typy proste na typy opakowujące używamy boxed()
        Stream<Integer> boxed = IntStream.of(1,2,3,4,5,666,6,66,6,6,66).boxed(); 
        
        Random random = new Random();
        DoubleStream doubles = random.doubles(5, 10, 15); //5 elementów, od 10.00… - 14.99…
    }
}
