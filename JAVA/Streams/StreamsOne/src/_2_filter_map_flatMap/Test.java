package _2_filter_map_flatMap;

import domain.Citizen;
import domain.Kid;
import domain.Person;
import static java.lang.System.*;
import java.util.Arrays;

/**
 * @author tomek
 */
public class Test {
    /*
        Poniższe operacje są operacjami pośrednimi (intermediate) czyli przeksztającymi strumień, ale nie przetwarzającymi go
    */
    
    public static void main(String[] args) {
        Kid[] kids = Kid.getSimpleArray();
        
        //filter - zwraca nowy strumień z elementami spełniającymi podany warunek
        Arrays.stream(kids)
                .filter( k -> k.getAge() > 18 );
        
        //map - przekształca wszystkie elementy ze strumienia. Można powiedzieć że używamy map() by zmutować elemnty ze strumienia. 
        Arrays.stream(kids)
                .map( Test::nameToUpperCase );
        
        //flatMap "spłaszcza" wielowymiarowe tablice czy listy list
        Citizen[][] citizens = new Citizen[2][];
        citizens[0] = Person.getSimpleList().toArray(new Person[10]);
        citizens[1] = Kid.getSimpleArray();
        
        Arrays.stream(citizens)
                .flatMap(Arrays::stream)//można powiedzieć że poprzekłada wszystkie zagnieżdżony elementy do jednego strumienia
                .forEach(out::println);
        
        //map - zwraca jedną wartość dla każdego podanego argumentu, 
        //podczas gdy flatMap zwraca dowolną (0 lub więcej) ilość wartości dla danego argumentu
    }
    
    
    private static Kid nameToUpperCase(Kid kid){
        kid.setName(kid.getName().toUpperCase());
        return kid;
    }
}
