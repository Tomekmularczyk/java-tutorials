package _4_distinct_sorted_peek;

import domain.Kid;
import static java.lang.System.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;
/**
 * Poniższe przykłady metod przekształacają strumień ale go nie modyfikują
 */
public class Test {

    public static void main(String[] args) {
        String str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam lacinia convallis tempus. Vivamus finibus lacus elit, "
                + "nec ultricies neque dictum sit amet. Curabitur rhoncus lorem non enim vulputate iaculis. Nam id diam tincidunt felis consectetur auctor. "
                + "Sed in urna in massa egestas tristique quis quis mauris. Donec ut turpis ultricies.";

        Stream.of(str.split(" "))
                .distinct(); //znajduje słowa, które były użyte tylko jeden raz

        Stream.of(str.split(" "))
                .sorted();  //posortuje elementy (muszą implementować interfejs Comparable)

        Kid kids[] = Kid.getSimpleArray();
        Stream.of(kids)
                .sorted(); //przechodzi kompilacje ale w trakcie przetwarzania strumienia wywaliłoby "ClassCastException: Kid cannot be cast to Comparable"

        Stream.of(kids)
                .sorted((k1, k2) -> k1.getAge() - k1.getAge()); //zadziała bo podaliśmy komparator dla klasy Kid

        
        //peek jest przydatny przy debugowaniu, gdy chcemy się upewnić, że elementy strumienia przepływają w odpowiedni sposób
        IntStream.of(1, 2, 3, 4)
                .filter(e -> e > 2)
                .peek(e -> out.println("Filtered value: " + e))
                .map(e -> e * e)
                .peek(e -> out.println("Mapped value: " + e))
                .sum();
        
        out.println("===peek przykład 2 ======");
        
        long count = Stream.of(kids)
                .peek(k -> out.println("przed filtrem: " + k)) //na tym przykładzie widać leniwe przetwarzanie strumieni
                .filter(k -> k.getAge() > 18) //w tym miejscu niektóre obiekty zostają "wyeliminowane" ze strumienia
                .peek(k -> out.println("po filtrze (age > 18): " + k))
                .filter(k -> k.hasToy())
                .peek(k -> out.println("po filtrze (age > 18 & hasToy): " + k))
                .count();
    }
}
