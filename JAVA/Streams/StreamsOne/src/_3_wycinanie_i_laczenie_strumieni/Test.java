package _3_wycinanie_i_laczenie_strumieni;

import static java.lang.System.*;
import java.time.LocalDate;
import java.util.stream.IntStream;
import java.util.stream.Stream;
/**
 * @author tomek
 */
public class Test {
    public static void main(String[] args) {
        //przykłady wycinania strumieni (limit i skip):
        Stream<Double> a = Stream.generate(Math::random)
                .limit(3); //ograniczamy nieskończony stream

        String str = "somerandomletter";
        IntStream b = str.chars()
                .skip(5);
        
        
        //Przykłady łączenia (konkatencacji) streamów: 
        //Jeżeli pierwszy będzie nieskończony to drugi nigdy się nie wykona.
        //Stream.concat(a, b); //nie da rady, rózne typy, ale...
        //w końcu IntStream to też Stream, więc możemy zrócić Stream oparametryzowany typem tej Integer przypomocy metody
        Stream<Number> concatOne = Stream.concat(a, b.boxed());
        
        //inny przykład concat
        Stream<Double> doubled = Stream.generate(Math::random).limit(5);
        Stream<LocalDate> integerd = Stream.generate(() -> LocalDate.MAX).limit(5);
        Stream<?> concatTwo = Stream.concat(doubled, integerd);
        
        Stream.concat(concatOne, concatTwo).forEach(out::println);
    }
}
