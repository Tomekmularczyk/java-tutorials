package _bonus_optional;

import domain.Person;
import java.util.List;
import java.util.Optional;

/**
 * @author tomek
 */
public class JakNieUzywac_3 {
    /*
        Nie powinno się używać Opitonal:
        - z metodą get(), chyba że możemy być na 100% pewni, że Optional nie jest pusty
        - z metodą isPresent(), właśnie po to był Optional stworzony by nie musieć robić tego typu warunków
        - jako zmienna lub parametr metody
        - w miejscu gdzie zwracana jest lista bądź tablica rezultatów. Zamiast tego użyj pustej listy czy tablicy.
    */
    

    public static void main(String[] args) {
        List<Person> persons = Person.getSimpleList();

        Optional<Person> personWithCar = persons.stream()
                .filter(Person::hasCar)
                .findFirst();

        Person person = personWithCar.get(); //w tym momencie sens i idea Optional przestaje istnieć…
        //… jeżeli optional jest pusty to wyrzucony zostanie wyjątek NoSuchElementException
        //… i wracasz do punktu wyjścia, przecież Optional miał Cię chronić przed niekontrolowanymi wyjątkami

        /*
            Optional<T> optionalWartość = ...; 
            optionalWartość.get().jakaśMetoda()
            
            nie jest bezpieczniejsze niż…
        
            T wartość = ...; 
            wartość.jakaśMetoda();
         */
        
        
        if(personWithCar.isPresent()) //też przeczy sensowi istnienia Optional
            person = personWithCar.get();
        
        
        /*Tak samo Optional miał wyeliminować konieczność sprawdzania czy dany obiekt nie jest Nullem, więc
            if (optionalWartość.isPresent()) 
                optionalWartość.get().jakaśMetoda();
        
            nie jest wcale prostsze niż…
        
            if (wartość != null) 
                wartość.jakaśMetoda();
         */
    }
}
