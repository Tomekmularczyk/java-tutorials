package _bonus_optional;

import domain.Kid;
import static java.lang.System.*;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * OPTIONAL służy do tego by w jasny sposób zaprezentować że metoda może zwrócić pusty wynik, zamiast zwracać Null i pisać zbędne
 * instrukcje warunkowe sprawdzające czy obiekt nie jest nullem.
 * 
 * 
 * 
   * Przy pobieraniu wartości z optional możemy zaplanować 3 akcje alternatywne w razie gdyby nie można było zwrócić obiektu:
   *    1. zwrócić obiekt domyślny, podany jako argument ( orElse )
   *    2. wykonanać dodatkową akcję mającą na celu zwrócić oczekiwany obiekt ( orElseGet )
   *    3. wyrzucić wyjątek ( orElseThrow )
 */
public class ZwracanieAlternatywne_1 {
    public static void main(String[] args) {
        Kid[] kids = Kid.getSimpleArray();
        
        
        //orElse() - jzwraca obiekt, a jeżeli nie posiada to zwraca podany w argumencie
        Stream<String> searchEngine = Stream.of(kids)
                .map(k -> k.getName() + " " + k.getSurname())
                .map(String::trim)
                .filter(k -> k.startsWith("Jacek")); //szukamy dziecka o imieniu Jacek
        Optional<String> optionalResult = searchEngine.findAny();
        
        String orElse = optionalResult.orElse("nie mamy dziecka o imieniu Jacek"); //jeżeli został znaleziony to go zwróć, a jak nie to stwórz nowego dzieciaka
        out.println("Wynik wyszukiwania: " + orElse);
        
        //----------------------------------------
        
        Optional<Kid> below10 = Stream.of(kids) //szukamy dzieciaka poniżej 5 roku życia...
                .filter(k -> k.getAge() < 5)
                .findAny();        
        //orElseGet() - zwraca obiekt, a jeżeli go nie ma to wykonuje dodatkowe zadanie
        Kid foundedKid = below10.orElseGet(() -> { //... a jeżeli nie znajdziemy to szukamy poniżej 10 roku życia..
            Optional<Kid> below15 = Stream.of(kids)
                    .filter(k -> k.getAge() < 10)
                    .findAny();
            
            //orElseThrow() - zwraca obiekt, a jeżeli nie posiada to wyrzuca wyjątek
            return below15.orElseThrow(KidNotFoundException::new); //... a jak nie ma i dziecka poniżej 10 roku życia to wywal exception
        });
        out.println(foundedKid);
    }
    
    
    private static class KidNotFoundException extends RuntimeException{}
}
