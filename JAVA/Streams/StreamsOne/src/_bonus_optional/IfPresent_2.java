package _bonus_optional;

import domain.Kid;
import static java.lang.System.*;
import java.util.Optional;
import java.util.stream.Stream;
/**
 * Inną strategią przy pracy z wartościami opcjonalnymi jest wykorzystanie wartości tylko wtedy gdy jest obecna
 */
public class IfPresent_2 {
    public static void main(String[] args) {
        Kid[] kids = Kid.getSimpleArray();
        
        
        Optional<Kid> kidWithToy = 
                Stream.of(kids)
                        .filter(Kid::hasToy)
                        .findFirst();
        kidWithToy.ifPresent(out::println); //jeżeli wartość istnieje to wykonaj zadanie, jeżeli nie, to nie rób nic
        
        //możemy też przetworzyć dany obiekt, tylko jeżeli istnieje
        Optional<String> kidToString = kidWithToy.map((k) -> {
            k.setHasToy(false);
            k.setName("Zbigi");
            k.setSurname("Cycek");
            return k.toString();
        });
        out.println(kidToString.orElseThrow(WTFException::new));
    }
    
    private static class WTFException extends RuntimeException{}
}
