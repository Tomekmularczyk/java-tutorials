package string;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex1 {
	private static PrintStream out = new PrintStream(System.out);
	private static Formatter formatter = new Formatter(out);
	
	public static void main(String[] args){
		//WYRAŻENIA REGULARNE Z KLASY STRING:
		out("==========MATCHES===========\n");
		out("-1778".matches("-?\\d+") + "\n"); //sprawdza czy string to cyfry(min. jedna) z opcjonalnym znakiem minusa(stąd pytajnik)
		out("17783.44".matches("-?\\d+") + "\n"); //false bojest ".44"
		out("+13".matches("-?\\d+") + "\n"); //false bo jest znak plusa
		out("+13".matches("(-|\\+)?\\d+") + "\n"); //podwyrażenie(tutaj alternatywa) grupuje się w nawiasach. Znak plus jest poprzedzony \\ ponieważ to znak specjalny.
		
		out("=============SPLIT============\n");
		String text1 = "Fuck the police, commin' straight outta underground. Young Nigga got it bad cuz' I'm brown.";
		String[] result = text1.split(" "); //dzieli string w miejscu wystąpienia regularnej ekspresji i zwraca tablice(nie zawierającą ekspresji(delimeter))
		out(Arrays.toString(result) + "\n");
		out(Arrays.toString(text1.split("\\w\\.|\\!|\\?")) + "\n"); //(bardziej zwięźle). Dzielimy tekst na zdania przy pomocy kropki poprzedzonej słowem[a-zA-Z_0-9]
															 //(gdyby to było duże W, zniknęły by wszystkie znaki interpunkcyjne i znaki specjalne(jak np polskie znaki)).
		
		out("=============REPLACE============\n");
		out(text1.replaceFirst("F\\w+","Love") + "\n");
		out(text1.replaceAll("the|outta| [nN]\\w+", "") + "\n"); //the, outta i każde słowo które jest poprzedzone " n" lub " N" [nN] to samo co (n|N)
	}
	
	
	public static void out(String format, Object[] args){
		formatter.format(format, args);
	}
	public static void out(String txt){
		out.print(txt);
	}

}
