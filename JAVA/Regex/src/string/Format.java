package string;

import java.util.Date;

public class Format {

	public static void main(String[] args) {
		String str = "tekst";
		String str2 = "tekst_dwa";
		
		pisz("Sprawdźmy czy \"" + str + "\"=\"" + str2.substring(0,5) + "\":\n");
		if(str == str2.substring(0,5)) //nigdy nie używaj == do porównania String, powoduje on najgorszy rodzaj błędu która pojawia sie od czasu do czasu
			pisz("true");
		else
			pisz("false");
		
		pisz("\n==========================================\n");
		pisz(str.codePointAt(0) + ""); //zwraca char number
		//pisz(str[3]);     nie da rady
		
		int result = str.compareTo(str2); //-1 przed, 0 w tej samej, +1 - za
		pisz("\nW kolejności słownikowej \"" + str + "\" znajduje się ");
		if(result > 0) pisz("za \"" + str2 + "\"\n");
		else if(result < 0) pisz("przed \"" + str2 + "\"\n");
		else pisz("w takiej samej pozycji co \"" + str2 + "\"\n"); //musi byc 0
		
		
		pisz("\n=================FORMATOWANIE CYFR==============\n");
		double randomDouble = Math.random() * 10;
		char character = (char) ((Math.random() * 100) +30);
		boolean bool = ((Math.random() * 2) < 1)? false: true;
		System.out.printf("Zmienna randomDouble = %f %ncharacter = '%c' %nboolean = %b %n", randomDouble, character, bool);
		
		for(int i=0; i<5; ++i){
			float value1 = (float) Math.random() * 100;
			double value2 = Math.random() * 10000;
			String out = String.format("%10.4f \n%10.4f\n", value1, value2);
			pisz(out);
		}
		System.out.printf("\n%20.2f %n %1$+0,10.5f", randomDouble);
		
		pisz("\n=================FORMATOWANIE DATY==============\n");
		Date data = new Date(); //current date
		pisz("\nAktualna data w postaci surowej: " + data.getTime());
		print("\nPełna sformatowana data: %tc", data);
		print("\nRok: %tY, miesiąc: %<tB, dzień roku: %<tj, dzień miesiąca: %<te(%<tA), godzina: %<tT, "
				+ "%nprzesunięcie względem GMT: %<tz(strefa czasowa %<tZ)", data);
		
	}
	
	public static void pisz(String str){
		System.out.print(str);
	}
	public static void print(String format, Object... args){
		System.out.printf(format, args);
	}

}
