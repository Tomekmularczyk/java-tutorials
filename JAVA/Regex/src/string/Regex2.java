package string;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

 class Regex2 {
    
	private static PrintStream out = System.out;
	
	private static final String POEM = "Bez serc, bez ducha, to szkieletów ludy;\n"
									  +"Młodości! dodaj mi skrzydła!\n"
									  +"Niech nad martwym wzlecę światem\n"
									  +"W rajską dziedzinę ułudy:\n"
									  +"Kędy zapał tworzy cudy,\n"
									  +"Nowości potrząsa kwiatem\n"
									  +"I obleka w nadziei złote malowidła.\n";
	
	public static void main(String[] args){
		CharSequence[] sequence = { new String("interfejs CharSequence posiada String..."),
									new StringBuilder("...StringBuilder"),
									new StringBuffer("...czy też StringBuffer")};
		
		for(CharSequence element : sequence)
			print(element.length() + ", ");
		
		print("\n==============matches()========================\n");
		String text = sequence[0].toString() + sequence[1] +sequence[2];
		String regex = "[cC]har";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		print(matcher.matches() + "\n");
		matcher = Pattern.compile("[A-Za-z \\.ż]+{50,}").matcher(text); //sprawdzamy czy tekst nie zawiera cyfr(prościej [^0-9]+) i ma przynajmniej 50 znaków
		print(matcher.matches() + "\n");
		matcher = Pattern.compile(".*[pP](\\w+)?[aA].*").matcher(text); //szuka czy w tekście znajduje się "pa" lub "p-znaki słów(? - opcjonalnie)-a"
		print(matcher.matches() + "\n");
		
		
		print("==============lookingAt()========================\n");
		String text2 = "Java już obsługuje wyrażenia regularne";
		Matcher matcher2 = Pattern.compile("już").matcher(text2);
		print(matcher2.lookingAt() + "\n"); //sprawdza czy dany regex występuje na początku tekstu, nie wymaga by caly tekst pasował
		matcher2 = Pattern.compile("^Java").matcher(text2);
		print(matcher2.lookingAt() + "\n");
		
		
		print("==============find()========================\n");
		//zwykły split nie dołącza delimetru, dlatego trzeba było utowrzyć własną metodę
		String[] result2 = splitIncludeDelimeter(";", "a;b;8c;d");
		for(String element: result2)
			print(element + "\n");
		
		//lets test speed of two methods
		String str = null;
		try {
			str = readFile("long_text.rtf", Charset.defaultCharset());
			print("Speed test:\nNumber of characters in a string = " + str.length() + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(str != null){
			String testreg = "(\r\n|\n|\r)|(\\\\)|\\.";
			Date before = new Date();
			String[] result4 = splitIncludeDelimeter(testreg, str);
			Date after = new Date();
			print("Test 1: Lines: " + result4.length + ", time: " + (after.getTime()-before.getTime()) + "ms");
			
			before = new Date();
			String[] result5 = splitIncludeDelimeter(testreg, str);
			after = new Date();
			print("\nTest 2: Lines: " + result5.length + ", time: " + (after.getTime()-before.getTime()) + "ms");
		}
		
		print("\n============== groups and flags =====================\n");
		//grupy -()- przydają się gdy chcemy znaleźć tekst między jakimiś np. znacznikami, wtedy łatwo jest wyciągnąć sam tekst bez znaczników z regexu za pomocą numeru grupy
		printThreeLastWordsOfEachLine(POEM);
		
		
		print("============== appendReplacement() =====================\n");
		//ta metoda pozwala znaleźć wyrażenie(lub pierwsze -n wyrażen) i dopiero wtedy zadecydować jak je zamienić. W przeciwieństwie do replaceAll(), które zamienia wszystko tak samo
		print(replaceVowels(POEM));
		
		
		print("\n============== skanowanie wejścia =====================\n");
		Scanner scanner = new Scanner(System.in);
		print("Podaj swoje imię, ale tylko dużymi literami: ");
		String patt = "[A-Z]+";
		
		while(!scanner.hasNext(patt)){
			print("(Tylko duże litery): ");
			scanner.next();
		}
		String name = scanner.next(patt);
		print("\nDzięki!, twoje imię to: " + name);
		
	}

	private static void printThreeLastWordsOfEachLine(String poem) {
		//(?m)sprawdzaj po wierszu; 3 grupy: dowolna ilość białych znaków plus dowolna ilość znaków innych niż białe. $ - zacznij od końca 
		Matcher matcher = Pattern.compile("(?m)(\\S+)\\s+((\\S+)\\s+(\\S+))$").matcher(poem); 
		print("Liczba grup w wyrażeniu: " + matcher.groupCount() + "\n");
		
		while(matcher.find()){
			print(matcher.group(0) + " (" +matcher.group(4) + ")\n"); //0 znaczy całe wyrażenie, im wyższy numer tym bardziej zagnieżdżone wyrażenie
		}
			
	}
	
	//zamienia co drugą samogłoskę w tekscie, nie uwzględniając dużych i małych liter(z tąd flagi)
	private static String replaceVowels(String text){
		StringBuffer stringBuffer = new StringBuffer();
		Matcher matcher = Pattern.compile("(?iu)[aeiouyąęó]").matcher(text);
		
		for(int i = 0; matcher.find() ; i++){
			if(i % 2 == 0){
				matcher.appendReplacement(stringBuffer, matcher.group().toUpperCase());
			}
		}
		
		matcher.appendTail(stringBuffer); ///dodajemy reszte  ciagu
		
		return stringBuffer.toString();
	}
	


	public static void print(String string){
		out.print(string);
	}
	@SuppressWarnings("resource")
	public static void print(String format, String string){
		Formatter formatter = new Formatter(out);
		formatter.format(format, string);
	}
	
	//Dzieli tekst przy pomocy delimetru, włączając delimeter do elementów
	public static String[] splitIncludeDelimeter(String regex, String text){
		List<String> list = new LinkedList<>();
		Matcher matcher = Pattern.compile(regex).matcher(text);
		
		int now, old = 0;
		while(matcher.find()){  //wyszukuje danego wyrażenia w tekscie
			now = matcher.end();
			list.add(text.substring(old, now));
			old = now;
		}
		
		if(list.size() == 0)
			return new String[]{text};
		
		//adding rest of a text as last element
		String finalElement = text.substring(old);
		list.add(finalElement);
		
		return list.toArray(new String[list.size()]);
	}
	
	
	//its round 50% faster than first method
	public static String[] splitIncludeDelimeter2(String regex, String text){
		List<String> list = new LinkedList<>();
		Matcher matcher = Pattern.compile(regex).matcher(text);
		
		StringBuffer stringBuffer = new StringBuffer();
		while(matcher.find()){
			matcher.appendReplacement(stringBuffer, matcher.group());
			list.add(stringBuffer.toString());
			stringBuffer.setLength(0); //clear buffer
		}
		
		matcher.appendTail(stringBuffer); ///dodajemy reszte  ciagu
		list.add(stringBuffer.toString());
		
		return list.toArray(new String[list.size()]);
	}
	
	private static String readFile(String path, Charset encoding) throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}
