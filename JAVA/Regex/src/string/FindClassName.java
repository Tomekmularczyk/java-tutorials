package string;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindClassName {

	public static void main(String[] args) throws FileNotFoundException {
		Logger logger = Logger.getLogger("MOJ.Logger");
		
		//String directory = "./src/string/FindClassName.java"; //to będzie działać w eclipsie, ale jak zrobisz z tego jar to wszystkie pliki .java zostaną skompilowane na .class
		InputStream directory = FindClassName.class.getResourceAsStream("string/FindClassName.java");
		
		try(Scanner scanner = new Scanner(new InputStreamReader(directory))){
			String pattern = "class\\s*(\\w+)\\s*";
			List<String> list = new LinkedList<>();
			while(scanner.hasNext()){
				String line = scanner.nextLine();
				Matcher matcher = Pattern.compile(pattern).matcher(line);
				while(matcher.find()){
					list.add(matcher.group(1));
				}
			}
			System.out.println(list);
		}
	}
	
	
	static class CHUJ{
		
	}

}
