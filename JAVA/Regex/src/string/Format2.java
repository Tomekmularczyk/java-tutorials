package string;

import java.io.PrintStream;
import java.util.Formatter;

public class Format2 {
	private static PrintStream out = System.out;
	private static Formatter formatter = new Formatter(out); //mogłoby by być zwykłe System.out
	
	static int widthA = -15, widthB = -15, widthC = 5;
	
	public static void main(String args[]){
		printFormatted("PRODUKT", "CENA", "ILOŚĆ");
		printFormatted("=======", "====", "=====");
		printFormatted("","","");
		printFormatted("Jabłka", "12.20zł", "55");
		printFormatted("Pomarańcze", "2.23zł", "5");
	}
	
	
	public static void format(String format, Object... args){
		formatter.format(format, args); //to samo co printf
	}
	public static void printFormatted(String a, String b, String c){
		format("%"+widthA+"s %"+widthB+"s %"+widthC+"s\n", a,b,c);
	}
}
