package string;

public class Rekurecja {

	
	public static void main(String[] args){
		int ile = 5;
		System.out.printf("Silnia z %d wynosi: %d\n", ile, silnia(ile));
		System.out.printf("Fibonacci z wartosci %d wynosi: %d\n", ile, fibonacci(ile));
		System.out.printf("Potega z liczby %d podniesiona do potegi %d wynosi: %d", ile, 10, potega(10,ile));
	}
	
	
	public static int silnia(int n){
		if(n == 0) return 1;
		
		return n*silnia(n-1);
	}
	public static int fibonacci(int n){
		if(n <= 2) return 1;
		
		return fibonacci(n-1) + fibonacci(n-2);
	}
	public static int potega(int wykladnik, int wartosc){
		if(wykladnik == 0) return 1;
		
		return wartosc*potega(wykladnik-1,wartosc);
	}
}
