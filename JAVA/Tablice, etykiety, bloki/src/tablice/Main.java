package tablice;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {	
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);  //jeśli zamkniemy skaner to zamknie on System.in i nie bedzie można już wiecej wczytywać od użytkownika
		int[] tablicaA = {3,4,5,2,6,7,66,33,22,100};
		int tablicaB[] = new int[5];                 //tablice można deklarować na 2 sposoby
		
		wypisz("TABLICA A \n");
		for(int element: tablicaA)
			wypisz(element + " ");
		wypisz("\n");
		wypisz("TABLICA B \n");
		for(int element: tablicaB)
			wypisz(element + " ");
		kontynuuj();
		
		makeBreak("przypisuje tablicaB->tablicaA");
		tablicaB=tablicaA;                              //nie nastepuje kopiowanie! tablicaB to teraz wskaźnik na tablicaA!
		wypisz("TABLICA B\n");
		wypisz(Arrays.toString(tablicaB));  //szybsze wypisanie zawartosci tablicy przy pomocy ARRAYS
		kontynuuj();
		
		makeBreak("Zmieniamy jeden elemnt tablicy B");
		int liczba;
		{
			//int liczba;           // nie ma przesłaniania jak w C++
			int podanaLiczba;
			wypisz("Podaj liczbe jaką zastąpimy element tablicaB[7] : ");
			podanaLiczba = input.nextInt();
			tablicaB[7] = podanaLiczba;
			
			wypisz("TABLICA A\n");
			wypisz(Arrays.toString(tablicaA));
			wypisz("\nTABLICA B\n");
			wypisz(Arrays.toString(tablicaB));
			kontynuuj();
			
		}
		//podanaLiczba = 10;    // nie mamy dostepu do zmiennych z bloku 
		
		tablicaB = Arrays.copyOf(tablicaA, 2*tablicaA.length);  //  teraz zmieniająć tablicaB nie zmieniamy tablicaA
		makeBreak("Arrays.copyOf()");
		wypisz("kopiujemy tablicaA do tablicaB dzięki Arrays.copyOf i nadajemy jej 2 razy większą ilość elementów\nTABLICA B\n");
		wypisz(Arrays.toString(tablicaB)+ "\nTABLICA A\n" + Arrays.toString(tablicaA)); 
		kontynuuj();
	
		makeBreak("Na koniec przyklad etykiety...");
		//przykladEtykiety
		etykieta:
		{
			do{
				for( ; ; ){
					while(true){
						break etykieta; //gdy zakończenie pętli może przynieść nieoczekiwane skutki czasem bezpieczniej jest po prostu z nich wyskoczyć
					}
					//..instructions..
				}
				//..instructions..
			}while(true);
			//..instructions..
		}
		//po odwołaniu się do etykiety, program rozpocznie działanie od tego miejsca
		wypisz("\"Hurra wydostaliśmy się z nieskończonych pętli dzięki etykiecie!!\"");
	}
	
	
	//==================================================================================================
	
	public static void makeBreak(String text){
		wypisz("\n");
		for(int i=0; i < 10; i++){
			wypisz("=");	
		}
		wypisz(" "+text+" ");
		for(int i=0; i < 10; i++){
			wypisz("=");	
		}
		wypisz("\n");
	}
	
	public static void wypisz(String s){
		System.out.print(s);
	}
	
	public static void kontynuuj(){
		wypisz("\nNacisnij aby kontynuuować...");
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
