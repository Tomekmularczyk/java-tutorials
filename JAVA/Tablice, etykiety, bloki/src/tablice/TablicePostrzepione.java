package tablice;
import java.util.Arrays;

public class TablicePostrzepione {

	public static void main(String[] args) {
		int[][] tablica = new int[10][];//10 refrencji do tablic - narazie NULL, więc tablica[1][0] -> NullPointerException
		
		for(int i=0; i < tablica.length; i++){
			tablica[i] = new int[i+1];
		}

		//wypelnianie
		for(int i=0; i < tablica.length; i++)
			for(int j=0; j < tablica[i].length; j++)
				tablica[i][j] = (j*i) + j;
		
		for(int[] rzad : tablica){
			System.out.println();
			for(int element : rzad)
				System.out.print(element + ", ");
		}
		
		System.out.println("\n\nMIESZAMY POSZARPANĄ TABLICĄ\n");
		
		//Zamieniamy kilka rzedow miejscami
		int[] temporary = tablica[3];
		tablica[3] = tablica[5];
		tablica[5] = temporary;
		
		temporary = tablica[8];
		tablica[8] = tablica[1];
		tablica[1] = temporary;
		
		for(int[] row : tablica){
			Arrays.sort(row);
			System.out.println(Arrays.toString(row));
		}
		
		
		//=============== inicjalizacja tablicy wielowymiarowych
		int[][][] wielo = {
				{{3,4,1},{4,5,5},{9,9,0}},  //3 głowne klamry, w tym 3 mniejsze i po 3 elemnty w srodku.
				{{4,4,4},{1,2,3},{8,9,8}},	//daje nam to tablica [3][3][3]
				{{6,7,9},{3,4,5},{9,1,0}}	//
		};
	}

}
