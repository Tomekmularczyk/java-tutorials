package tablice;
import java.io.IOException;
import java.util.Arrays;

public class Main2 {

	public static void main(String[] args) {
		int wielkosc_tablicy = 20;
		int przedzial_losowan = 50;
		
		int[] tablica = new int[wielkosc_tablicy];
		
		wypisz(Arrays.toString(tablica));
		kontynuuj();
		
		for(int i=0; i < wielkosc_tablicy; i++)
			tablica[i] = (int) (Math.random() * przedzial_losowan);

		wypisz(Arrays.toString(tablica));
		kontynuuj();
		
		tablica[0] = tablica[tablica.length-1];
		wypisz(Arrays.toString(tablica));
		kontynuuj();
		
		tablica[tablica.length-1] = 200;
		wypisz(Arrays.toString(tablica));
		tablica[0] = 100;
		wypisz("\n" + Arrays.toString(tablica));
		
		kontynuuj();
		wypisz("\nzmienna a=" + wielkosc_tablicy + ", zmienna b=" + przedzial_losowan);
		wielkosc_tablicy = przedzial_losowan;
		przedzial_losowan = 14;
		wypisz("\nzmienna a=zmienna b\nzmienna b = 14");
		wypisz("\nzmienna a=" + wielkosc_tablicy + ", zmienna b=" + przedzial_losowan);
		
		Arrays.sort(tablica);
	}

	public static void wypisz(String string){
		System.out.print(string);
	}
	public static void kontynuuj(){
		wypisz("\nNacisnij aby kontynuuować...");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
