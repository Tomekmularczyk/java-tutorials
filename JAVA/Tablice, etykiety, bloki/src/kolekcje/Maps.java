package kolekcje;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Maps {
	
	public static void main(String[] str){
		Map<String, Integer> hashMap = new HashMap<>();
		String[] names = {"Tomek","Adam","Kazek","Andrzej","Józef","Celina","Małgorzata","Damian"};
		
		Random rand = new Random(5);
		for(String name : names){
			hashMap.put(name, rand.nextInt(6)+1);
		}
		
		hashMap.put("Tomek", -4);
		System.out.println(hashMap);
	}
}
