package kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Lists {
		
	public static void main(String[] args){
		Integer[] ordinaryArray = {3,7,23,6,76,23,6}; //nie może tutaj być int bo Arrays.asList() nie przyjmuje tablicy prymitywnych typów...
		
		List<Integer> intList = new ArrayList<>(Arrays.asList(ordinaryArray)); //rzutowanie w górę do interfejsu w celu późniejszej łatwej zmiany implementacji.
												   							   //nie zawsze jest to jednak porządane, ponieważ niektóre kontenery cechują się rozszerzoną funkcjonalnością
		Collection<Integer> intCollection = intList;//podstawowy interfejs dla innych interfejsów implementujących kontenery jak List, Map, Set czy Queue
		intCollection.add(44);
		
		//-------------------------
		List<Integer> listOne = Arrays.asList(3,5,6,7,4);
		listOne.set(1, 33);
	//	listOne.add(44); //exception, zapleczem dla tej listy jest zwykła tablica. Nie możemy jej modyfikować
		
		List<Integer> listTwo = new LinkedList<>();
		Collections.addAll(listTwo, ordinaryArray);
		listTwo.add(44);
	}
}
