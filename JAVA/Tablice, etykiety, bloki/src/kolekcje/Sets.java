package kolekcje;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Sets {

	
	public static void main(String[] args){
		Random rand = new Random(40);
		
		//set może posiadać tylko unikatowe wartości, mimo że pętla wykona się 1000 razy zostanie dodane max 30 elementów
		Set<Integer> hashSet = new HashSet<>();
		for(int i = 0; i < 1000; i++) 
			hashSet.add(rand.nextInt(30));
		
		System.out.println(hashSet);
		
		//ten set działa podobnie jak poprzedni ale wartosci będą posortowane.
		Set<Integer> treeSet = new TreeSet<>();
		for(int i = 0; i < 100; i++) 
			treeSet.add(rand.nextInt(30));
		
		System.out.println(treeSet);
	}
}
