package kolekcje;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Queues {
	public static void main(String[] args){
		Queue<Character> queue = new LinkedList<>();
		Collections.addAll(queue, 'K','O','L','E','K','C','J','E');
		
		System.out.println(queue);
		Character c1 = queue.poll();
		Character c2 = queue.poll();
		queue.addAll(Arrays.asList(c1,c2));
		System.out.println(queue);
	}
}
