package _3_przetwarzanie_anotacji_w_kodzie;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.stream.Collectors;

/**
 * @author tomek
 */
public class ToStringWorker {

    public static String convert(Object object) {
        if (object == null) {
            return "null";
        }

        Class<?> objClass = object.getClass();
        ToString annotation = objClass.getAnnotation(ToString.class);
        if (annotation == null) {
            return object.toString();
        }

        StringBuilder toStr = new StringBuilder();
        toStr.append(objClass.getSimpleName() + ":\nFields[");
        boolean pierwszy = true;
        for (Field field : objClass.getDeclaredFields()) {
            annotation = field.getAnnotation(ToString.class);
            if (annotation != null) {
                field.setAccessible(true);
                String name = field.getName();

                if (pierwszy) {
                    pierwszy = false;
                } else {
                    toStr.append(",");
                }

                if (annotation.shortName()) {
                    name = shortenName(name);
                }

                try {
                    toStr.append(name + "=" + field.get(object));
                } catch (ReflectiveOperationException ex) {
                    ex.printStackTrace();
                }
            }
        }
        toStr.append("]\nMethods:[");
        pierwszy = true;
        for (Method method : objClass.getDeclaredMethods()) {
            annotation = method.getAnnotation(ToString.class);
            if (annotation != null) {
                method.setAccessible(true);
                String name = method.getName();

                if (pierwszy) {
                    pierwszy = false;
                } else {
                    toStr.append(",");
                }

                if (annotation.shortName()) {
                    name = shortenName(name);
                }

                toStr.append(name);
            }
        }
        toStr.append("]");
        return toStr.toString();
    }

    private static String shortenName(String str) {
        char firstLetter = str.charAt(0);
        return firstLetter + str.chars()
                .mapToObj(c -> (char) c + "")
                .filter(c -> c.matches("^[A-Z]"))
                .collect(Collectors.joining());
    }
}
