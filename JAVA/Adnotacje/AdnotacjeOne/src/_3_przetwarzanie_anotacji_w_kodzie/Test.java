package _3_przetwarzanie_anotacji_w_kodzie;
/**
 * Anotacje moga być przetwarzane
 * - w działającym programie przez refleksje
 * - przez automatyczne przetwarzanie plików źródłowych i generowania dodatkowego kodu (lub innych plików)
 * - Trzecią możliwością jest przetwarzanie adnotacji w pliku klasy, zazwyczaj w locie podczas wczytywania go do maszyny wirtualnej. 
 *      Aby odnajdywać i analizować adnotacje oraz przepisywać kod bajtowy, potrzebujesz narzędzia takiego jak ASM 
 */
public class Test {
    public static void main(String[] args) {
        String toString = ToStringWorker.convert(new Person("Kaziu", 44));
        System.out.println(toString);
    }
    
    private static class Person{
        @ToString
        private String name;
        @ToString
        private int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
        
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        @ToString
        public int getAge() {
            return age;
        }
        @ToString
        public void setAge(int age) {
            this.age = age;
        }
        
        @ToString(shortName = true)
        public void doSomethingRealyAmazing(){
            //doin' it
        }
    }
}
