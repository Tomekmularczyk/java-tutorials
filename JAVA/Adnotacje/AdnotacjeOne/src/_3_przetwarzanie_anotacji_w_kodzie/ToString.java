package _3_przetwarzanie_anotacji_w_kodzie;

import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author tomek
 */
@Target({FIELD, METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ToString {
    boolean shortName() default false;
}
