package _1_uzywanie_adnotacji;

import com.sun.istack.internal.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Adnotacje to znaczniki przetwarzane przez inne narzędzia, na poziomie kodu źródłowego lub podczas kompilacji dzięki refleksji. Sama
 * adnotacja nic nie robi, potrzebne jest narzędzie, które je przetworzy. Adnotacje są traktowane jak modyfikatory public czy private oraz
 * Mogą posiadać pary klucz-wartość. Nie można ich rozszerzać.
 * Adnotacje nie zmieniają sposobu w jaki twoje programy są kompilowane, dlatego można ich używać praktycznie wszędzie.
 */
public class AdnotacjeOne {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        Function<String, Integer> function = new Function<String, Integer>() {
            @Override //przykład prostej adnotacji, jeżeli oznaczymy tak metodę to kompilator upewnia się, że faktycznie przesłaniamy metodę: R apply(T t)
            public Integer apply(String t) {
                return Integer.valueOf(t);
            }
        };
        
        @SuppressWarnings("unchecked")
        List<String> al = new ArrayList();
    }
    
    @Deprecated
    private static void doSome(@NotNull String str){}
}
