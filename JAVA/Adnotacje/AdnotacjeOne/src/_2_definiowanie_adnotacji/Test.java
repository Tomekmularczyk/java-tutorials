package _2_definiowanie_adnotacji;

/**
 * @author tomek
 */
public class Test {

    @Dummy(timeout = 1000)
    private static void importantMethod() {
        @Dummy2
        //@Dummy(importance = 2, timeout = 100) //nie da rady dla tego typu deklaracji
        String str = "";
    }

    //nic nie stoi na przeszkodzie by tworzyć adnotacje lokalne
    private @interface Dummy2{
        boolean someValue() default false;
    }
}
