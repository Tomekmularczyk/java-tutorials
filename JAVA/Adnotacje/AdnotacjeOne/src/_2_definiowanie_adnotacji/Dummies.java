package _2_definiowanie_adnotacji;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Containing annotation type
 */

//ten kontener dla Dummy musi przestrzegać pewnych reguł
//np. nie moze być stosowany do większej ilości miejsc niż Dummy, bo kompilator wywali błąd
@Target(ElementType.METHOD)
public @interface Dummies {
    Dummy[] value() default {}; //metoda/pole musi być koniecznie nazwan value(), żeby można było te adnotacje użyć w @Repeatable
}