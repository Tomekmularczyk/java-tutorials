package _2_definiowanie_adnotacji;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/* @Target i @Retention to metaadnotacje, …istnieją też @Documented, @Inherited, @Repeatable
    @Documented - mówi by umieścić informację o tej adnotacji w javadoc ponieważ jest to cenna informacja z punktu widzenia czytającego dokumentacje
    @Inherited - ma zastosowanie tylko do klas. Klasy roszerzające będą dziedziczyć tę adnotację.
    @Repeatable - mówi o tym, że dana adnotacja może być użyta w danym miejscu więcej niż jeden raz
 */
//cel adnotacji
@Target(ElementType.METHOD) //gdybyśmy nie podali tej adnotacji to poniższa adnotacja mogłaby być używana wszędzie
//…poza paramterami opisującymi typy ani opisem użytych typów
//ustawiamy dostępność
@Retention(RetentionPolicy.CLASS) //adnotacja jest dołączana do plików klasy ale maszyna wirtualna nie wczytuje jej. Opcja domyślna.
@Repeatable(Dummies.class) //teraz jeżeli użytkownik użyje Dummy więcej niż raz zostaną one automatycznie opakowane Adnotacją Dummies
public @interface Dummy {

    //lista parametrów przekazywana do adnotacji
    //metody te nie moga być uogólnione, posiadać parametrów czy wyrzucać wyjątków
    long timeout();

    int importance() default 3; //tego parametru nie trzeba dodawać

    /*
    elementem adnotacji może być:
    - wartość typu prostego
    - String
    - obiekt Class
    - enum
    - inna adnotacja
    - tablica powyższych
     */
}


