package demo;

/*
 * Funkcjonalny interfejs to taki, który posiada tylko jedną metodę do zdefiniowania
 */

public interface FunctionalInterface {
	public void doSomethingBro();
	
	/*
	 * Jeżeli zadeklarowalibyśmy więcej niż jedną metodę to w miejscu użycia Lambda dostaniemy błąd kompilatora:
	 * "The target type of this expression must be a functional interface"
	 */
	//public void doStuff();
}
