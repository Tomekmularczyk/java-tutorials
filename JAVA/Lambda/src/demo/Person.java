package demo;

public class Person {
	private String name;
	private String surname;
	
	public Person(String name, String surname){
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	
	public void introduceYourself(){
		System.out.println(name + " " + surname);
	}
}
