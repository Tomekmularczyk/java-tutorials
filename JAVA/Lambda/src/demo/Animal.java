package demo;

public interface Animal{
	public String makeASound(String object1, String object2);

	default void defaultMethod(){
		System.out.println("it Is Still Functional Interface");
	}
}
