package demo;

public class CCC {

	public static void main(String[] args) {
		class A {
			boolean f(A a) { return true; }
		}
		
		class B extends A {
			boolean f(A a) { return false; } // override A.f(A)
			boolean f(B b) { return true; }  // overload A.f
		}

		//przeładowanie dzieję się w trakcie kompilacji
		//nadpisanie działa w trakcie działania programu
		
		A a = new A();
		A ab = new B();
		B b = new B();
		
		System.out.printf("%b %b %b", ab.f(a), ab.f(ab), ab.f(b)); // true 
		System.out.println();
		System.out.printf("%b %b %b", b.f(a), b.f(ab), b.f(b));    
	}

}
