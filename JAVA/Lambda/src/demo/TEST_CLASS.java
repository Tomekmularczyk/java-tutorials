package demo;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TEST_CLASS {

	public static void main(String[] args) {
		//-------------- Functional Interface -----------------------------------
		
		FunctionalInterface inter = new FunctionalInterface(){
			@Override
			public void doSomethingBro() {
				System.out.println("I am working bro!");
			}
		};
		inter.doSomethingBro();
		
		inter = () -> System.out.println("Still working bro!");
		inter.doSomethingBro();
		
		inter = () -> {
			Date date = new Date();
			System.out.println("Today is: " + date.toString());
		};
		inter.doSomethingBro();
		
		
		Animal animal;
		animal = (String object1, String object2) -> {return object1 + " makes " + object2;};//longer way
		animal = (str1, str2) -> str1 + " makes " + str2;   //simple way
		System.out.println(animal.makeASound("dog", "grrr!"));
		
		
		List<Person> list = new LinkedList<>();
		list.add(new Person("Adam", "Kędra"));
		list.add(new Person("Janusz", "Mularczyk"));
		list.add(new Person("Ela", "Pała"));
		list.add(new Person("Józek", "Cap"));
		
		Collections.sort(list, new Comparator<Person>(){ //long way
			@Override
			public int compare(Person pers1, Person pers2) {
				return pers1.getName().compareTo(pers2.getName());
			}
		});
		Collections.sort(list, (pers1,pers2) -> pers1.getName().compareTo(pers2.getName())); //short way
		
		
		//----------------- Looping ------------------------------------------
		//new forEach method
		/* 
		list.forEach(new Consumer<Person>() {
			@Override
			public void accept(Person t) {
				
			}
		});
		*/
		
		// with lambda...
		System.out.println();
		list.forEach( p -> System.out.println(p.getName() + " " + p.getSurname()));
		System.out.println();
		list.forEach( Person::introduceYourself ); // In a case where a method already exists to perform an operation on the class, this syntax can be used instead of the normal lambda expression syntax
		
		
	}

}
