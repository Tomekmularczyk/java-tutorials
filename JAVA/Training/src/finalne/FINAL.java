package finalne;

import java.util.LinkedList;
import java.util.List;

final class FINAL { //nie moze byc dziedziczona
	private static final int VALUE_ONE = 3; //zmienna finalna
	private final List<?> list = new LinkedList<>(); //referencja finalna
	private final int[] array; //też referencja finalna, ponieważ możemy zmieniać wartość elementów
	
	public FINAL(){
		array = new int[55];
		
		List<?> newlist = new LinkedList<>();
	//	list = newlist;  //nie bo finalna referencja
	}
	
	private final void set(final int value){ //nie ma sensu deklarować metody prywatnej jako final ponieważ i tak jej nie przesłonimy w klasie pochodnej
		array[33] = value;
	}

}
