package listy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class SET {
	private static Logger loger = Logger.getLogger("LOG");
	
	public static void main(String[] args){
		
		Set<Integer> lotery = new HashSet<>(6,1);
		 
		while(lotery.size() < 6) 
			lotery.add((int) (Math.random() * 40 +1));
		 
		loger.info("Size: " + lotery.size() + "\n" + lotery.toString());
		
		//=======================================================================
		
		Map<Integer, String> map = new HashMap<>();
		map.put(333, "someValue1");
		map.put(775, "someValue2");
		map.put(281, "someValue4");
		map.put(130, "someValue7");
		map.put(829, "someValue2");
		
		loger.info(map.toString());
		
		
		//=======================================================================
		List<Double> linked = new LinkedList<>();
		
		linked.add(3.646);
		linked.add(5.66);
		linked.add(33.666);
		linked.add(67.696);
		linked.add(332.606);
		
		ListIterator<Double> iterator = linked.listIterator();
		while(iterator.hasNext())
			iterator.next();
		
		iterator.forEachRemaining(new Consumer<Double>() {
			@Override
			public void accept(Double t) {
				++t; //nic sie nie dzieje?
			}			
		});
		
		loger.info(linked.toString());
		
		//=========================================================================
		Integer[] array = new Integer[10];
		List<Integer> list = Arrays.asList(array); //to nie jest lista tylko widok! nie można dodawać ani usuwać elementów
		list.add(4); //error
		
		
	}
}
