package kontener;

import java.util.ArrayList;
import java.util.Date;

public class Para<ZmiennaTypowa> { //ogólna konwencja nakazuje używaż pojedyńczych liter jak S,W,T
	private ZmiennaTypowa a;
	private ZmiennaTypowa b;
	
	public Para(ZmiennaTypowa a, ZmiennaTypowa b){
		this.a=a;
		this.b=b;
	}
	
	public <T extends Comparable<?>> Para<T> metodaParametryzowana(T[] lista){ //Comparable to "typ graniczny"
		
		return new Para<>(lista[0], lista[1]);
	}
	
//	public void getSomeString(? extends Para f){
//		return "";
//	}
	
	public static void main(String[] args){
		Para<Integer> para = new Para<>(3,3);
		
		Para<String> l2 = para.metodaParametryzowana(new String[]{"",""});
		
		if(l2 instanceof Para){
			
		}
		
		Para<? extends Date> da;
		ArrayList<?> lista = new ArrayList<>();
		
		
		
		ArrayList<? super String> lista1 = new ArrayList<>();
		lista1.add("");
		Object s = lista1.get(0);
		
		
		
	}
}
