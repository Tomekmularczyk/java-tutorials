package kontener;

import java.util.Date;

public class Kontenerek<T extends Object> {
	private T one, two;

	public Kontenerek(T o, T t){
		one = o;
		two = t;
	}
	
	public T getOne(){
		return one;
	}
	public T getTwo(){
		return two;
	}
	public void setOne(T o){
		one = o;
	}
	public void setTwo(T t){
		two = t;
	}
	
	
	public static void main(String[] args){ //PECS(Producer extends, Consumer super
		Kontenerek<?> kontenerek = new Kontenerek<>("one","two");
		
		kontenerek.getOne();
		//kontenerek.setOne("dfd");
		
		Kontenerek<? extends Integer> kontenerek2 = new Kontenerek<>(1,1);//pozwala odczytywać(Producer extends)
		Integer integer = kontenerek2.getOne();
		kontenerek2.setTwo(null);
		//kontenerek2.setTwo(3); //nie da rady, bo nie wiadomo jakiego podtypu jest lista
		
		Kontenerek<? super Date> kontenerek3 = new Kontenerek<>(new Date(),new Date()); //pozwala zapisywać(Consumer super)
		Date someDate = (Date) kontenerek3.getOne(); //zwraca tylko Object
		kontenerek3.setTwo(new Date()); //można bo zadziała bezpieczny polimorfizm, czyli ewentualnie przesłonięcie niższego typu na wyższy
	}
}
