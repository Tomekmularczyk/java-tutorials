package dom;

import java.util.Arrays;

public class Test {

	public static void main(String[] args) {
		Pokoj pokoj1 = new Pokoj("Duży", 1);
		Pokoj pokoj2 = new Pokoj("Mały", 2);
		Pokoj pokoj3 = new Pokoj("Dwu-osobowy", 3);
		Kuchnia kuchnia = new Kuchnia("Kuchnia");
		Lazienka lazienka = new Lazienka("Łazienka");
		
		Wlasciciel wlasciciel = new Wlasciciel("Jurek", 38);
		Lokator lokator1 = new Lokator("Justyna", 24);
		Lokator lokator2 = new Lokator("Jacek", 21);
		
		Pomieszczenie[] pomieszczenia = new Pomieszczenie[]{pokoj1, pokoj2, pokoj3, kuchnia, lazienka};
		Lokator[] lokatorzy = new Lokator[]{lokator1, lokator2, wlasciciel};
		
		wlasciciel.setPokoj(pokoj2);
		lokator1.setPokoj(pokoj1);
		lokator2.setPokoj(pokoj3);
		
		pokoj1.setLokator(lokator1);
		pokoj2.setLokator(wlasciciel);
		pokoj3.setLokator(lokator2);
		kuchnia.setUzytkownicy(Arrays.asList(lokatorzy));
		lazienka.setUzytkownicy(Arrays.asList(lokatorzy));
		
		Mieszkanie mieszkanie = new Mieszkanie("Zamknięta 3/16", pomieszczenia, lokatorzy);
		
		System.out.println(mieszkanie);
	}

}
