package dom;

public class Pomieszczenie {
	private String name;
	
	public Pomieszczenie(String name){
		this.name = name;
	}
	
	public String getNazwa(){
		return name;
	}
	
	@Override
	public String toString(){
		return name;
	}
}
