package dom;

import java.util.List;

public class Lazienka extends Pomieszczenie{
	private List<Lokator> uzytkownicy;
	
	public Lazienka(String name){
		super(name);
	}
	
	public void setUzytkownicy(List<Lokator> uzytkownicy){
		this.uzytkownicy = uzytkownicy;
	}
	
	@Override
	public String toString(){
		StringBuilder string = new StringBuilder(super.toString() + ", użytkownicy: ");
		for(Lokator lokator : uzytkownicy)
			string.append(lokator.getImie() + ", ");
		
		return string.toString();
	}
}
