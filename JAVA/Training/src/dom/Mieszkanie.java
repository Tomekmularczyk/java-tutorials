package dom;

public class Mieszkanie {
	private String adres;
	private Pomieszczenie[] pomieszczenia;
	private Lokator[] lokatorzy;
	
	public Mieszkanie(String adres, Pomieszczenie[] pomieszczenia, Lokator[] lokatorzy){
		this.adres = adres;
		this.pomieszczenia = pomieszczenia;
		this.lokatorzy = lokatorzy;
	}
	
	@Override
	public String toString(){
		StringBuilder string = new StringBuilder(adres);
		
		string.append(" Liczba pomieszczen: " + pomieszczenia.length + "\n");
		for(Pomieszczenie pomieszczenie: pomieszczenia)
			string.append(pomieszczenie.toString() + "\n");
		
		string.append("\nLokatorzy:\n");
		
		for(Lokator lokator: lokatorzy)
			string.append(lokator + "\n");
			
		return string.toString();
	}
}
