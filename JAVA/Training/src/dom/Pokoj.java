package dom;

public class Pokoj extends Pomieszczenie{
	private int nrPokoju;
	private Lokator lokator;
	
	public Pokoj(String name, int nrPokoju){
		super(name);
		this.nrPokoju = nrPokoju;
	}
	
	public void setLokator(Lokator lokator){
		this.lokator = lokator;
	}
	
	@Override
	public String toString(){
		return "Nr. pokoju: " + nrPokoju + ", " + super.toString() + ", wynajmuje: " + lokator.getImie();
	}
}
