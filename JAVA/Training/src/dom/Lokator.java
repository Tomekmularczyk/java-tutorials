package dom;

public class Lokator {
	private String imie;
	private int wiek;
	private Pokoj pokoj;
	
	public Lokator(String imie, int wiek){
		this.imie = imie;
		this.wiek = wiek;
		pokoj = null;
	}
	
	public void setPokoj(Pokoj pokoj){
		this.pokoj = pokoj;
	}
	
	public String getImie(){
		return imie;
	}
	
	@Override
	public String toString(){
		return imie + ", lat: " + wiek + ", wynajmuje: " + pokoj.getNazwa();
	}
}
