package pilka;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BounceFrame extends JFrame{
	private BallComponent comp;
	public static final int STEPS = 1000; public static final int DELAY = 3;
	/**
	* Tworzy ramkę z komponentem zawierającym odbijającą się pi kę oraz przyciskami * Start i Zamknij.
	*/
	public BounceFrame() {
	setTitle("Piłka");
	comp = new BallComponent();
	add(comp, BorderLayout.CENTER);
	JPanel buttonPanel = new JPanel(); addButton(buttonPanel, "Start", new ActionListener()
	{
	public void actionPerformed(ActionEvent event) {
	addBall(); }
	});
	addButton(buttonPanel, "Zamknij", new ActionListener() {
	public void actionPerformed(ActionEvent event) {
	System.exit(0); }
	});
	add(buttonPanel, BorderLayout.SOUTH);
	pack(); }
	/**
	* Dodaje przycisk do kontenera.
	* @param c kontener
	* @param title tytu  przycisku
	* @param listener s uchacz akcji przycisku */
	public void addButton(Container c, String title, ActionListener listener) {
	JButton button = new JButton(title); c.add(button); button.addActionListener(listener);
	}
	/**
	* Dodaje odbijającą się pi kę do panelu i odbija ją 1000 razy */
	public void addBall() {
		try {
			Ball ball = new Ball(); comp.add(ball);
			for (int i = 1; i <= STEPS; i++) {
			ball.move(comp.getBounds()); comp.paint(comp.getGraphics()); Thread.sleep(DELAY);
		} }
		catch (InterruptedException e) {
		}
		}
}
