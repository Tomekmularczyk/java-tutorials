package pilka;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

public class BallComponent extends JPanel{
	private static final int DEFAULT_WIDTH = 450; 
	private static final int DEFAULT_HEIGHT = 350;
	private java.util.List<Ball> balls = new ArrayList<>();
	/**
	* Dodaje pi kę do komponentu
	* @param b pi ka, która ma zostać dodana */
	public void add(Ball b) {
	balls.add(b); }
	public void paintComponent(Graphics g)
	{
	super.paintComponent(g); // Czyszczenie t a 
	Graphics2D g2 = (Graphics2D) g;
	for (Ball b : balls)
	{
		g2.fill(b.getShape()); }
	}
	public Dimension getPreferredSize() { return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT); }
}
