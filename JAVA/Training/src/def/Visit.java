package def;

import java.util.Date;

public class Visit {
	private Date date;
	private Client client;
	private double serviceExpense;
	private double productExpense;
	private int membersDiscount;
	public Visit(Date date, Client client, double serviceExpense, double productExpense, int membersDiscount){
		this.date =  new Date(date.getTime());
		this.client = client;
		this.serviceExpense = serviceExpense;
		this.productExpense = productExpense;
		this.membersDiscount = membersDiscount;
	}
	
	public double getTotalExpense(){
		int serviceDiscount = client.getMembership().getDiscount();
		int productDiscount = (client.isMember()? membersDiscount : 0);

		return (serviceExpense - (serviceExpense / 100 * serviceDiscount)) +
				(productExpense - (productExpense / 100 * productDiscount));
	}
	
	@Override 
	public String toString(){
		int serviceDiscount = client.getMembership().getDiscount();
		int productDiscount = (client.isMember()? membersDiscount : 0);
		
		return "Date: " + date.toString() + ". Client: " + client + ". \n" +
				"Service expense: " + serviceExpense + ", productExpense: " + productExpense +
				".\nTotal expense(service: " + serviceDiscount + "%, product: " + productDiscount + "%)" +
				" = " + getTotalExpense();
	}
}
