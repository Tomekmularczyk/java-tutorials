package def;

import java.util.Date;

public class Tes {
	
	public static void main(String[] args){
		Client[] clients = new Client[3];
		clients[0] = new Client("Kazimierz", false, Membership.NONE);
		clients[1] = new Client("Adrian", true, Membership.GOLD);
		clients[2] = new Client("Lucyna", true, Membership.PREMIUM);
		
		Visit[] visit = new Visit[3];
		visit[0] = new Visit(new Date(2011,10,11), clients[0], 2000.50, 1300.0, 10);
		visit[1] = new Visit(new Date(2011,10,12), clients[1], 140.33, 300.30, 13);
		visit[2] = new Visit(new Date(2011,10,13), clients[2], 900.55, 1303.75, 2);
		
		for(Visit vis : visit)
			wypisz(vis.toString());
	}
	
	private static void wypisz(String s){
		System.out.println(s);
	}
}
