package def;

public enum Membership {
	NONE(0), PREMIUM(20), GOLD(15), SILVER(10);
	
	private int discount;
	private Membership(int percent){
		discount = percent;
	}
	public int getDiscount(){
		return discount;
	}
}
