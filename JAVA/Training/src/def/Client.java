package def;

public class Client {
	private String name;
	private boolean member = false;
	private Membership membership;
	
	public Client(String name, boolean member, Membership membership){
		this.name = name;
		this.member = member;
		this.membership = membership;
	}
	
	public boolean isMember(){
		return member;
	}
	public String getName(){
		return name;
	}
	public Membership getMembership(){
		return membership;
	}
	
	@Override
	public String toString(){
		return "Client: " + name + ". Member: " + member + ". Membership: " + membership.toString();
	}
}
