package klonowanie;

import java.util.GregorianCalendar;

public class Employee implements Cloneable{ //interfejs znacznikowy bez żadnych metod (nie używa sie już) musi tu być bo inaczej metoda wyrzuci CloneNotSupportedException
											//mimo, że przesłaniamy metode z klasy object. Clone sprawdza czy implememntujemy ten interfejs
											//jest to swojego rodzaju informacja że programista wie na czym polega kolonowanie
	private String name;
	private GregorianCalendar date;
	private int salary;
	public Employee(String name, int salary){
		this.name=name;
		this.salary=salary;
		date = new GregorianCalendar(2000,11,30);
	}
	
	@Override
	public Employee clone() throws CloneNotSupportedException { //dzięki kowariancji typów nie musimy zwracać Object a Employee. Dodatkowo musimy zrobić metodę publiczną
		Employee copy = (Employee) super.clone();
		copy.date = (GregorianCalendar) date.clone(); //deep copy  -- tworząc obiekt w jego klasie mamy bezpośredni dostęp do pól prywatnych
		
		return copy;
	}
}
