package klonowanie;

import java.util.Date;

public class Manager extends Employee implements Cloneable{
	private Date date;
	
	public Manager(){
		super("Manager", 100000);
		date = new Date();
	}
	
	@Override
	public Manager clone() throws CloneNotSupportedException {
		Manager copy = (Manager) super.clone();
		copy.date = (Date) date.clone();
		
		return copy;
	}
}
