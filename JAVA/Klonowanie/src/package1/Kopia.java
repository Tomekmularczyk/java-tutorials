package package1;
import java.io.IOException;
import java.util.Date;

public class Kopia {

	public static void main(String[] args) throws IOException {
		String s = "string";
		float f = 12.3f;
		char c = 'C';
		Date someDate = new Date(System.currentTimeMillis());
	
		Obiekt obiekt = new Obiekt(s,f,c,someDate);
		System.out.println(obiekt.toString());
		
		System.out.println("\nA teraz zmienimy stan klasy Obiekt nawet się do niej nie odwołując(Data). Kontynuuj...");
		System.in.read();
		
		someDate.setTime(System.currentTimeMillis());
		someDate = null;
		System.out.println(obiekt.toString());
		
		System.out.print("\n\nDzieje się tak ponieważ:\n1. Obiekt daty jest mutowalny.\n2. Klasa Obiekt kopiuje referencje do obiektu zamiast skopiować zawartość");
	}
	
	private static class Obiekt{
		private String s;
		private float f;
		private char c;
		private Date date;
		
		public Obiekt(String s, float f, char c, Date date){
			this.s = s;
			this.f = f;
			this.c = c;
			this.date = date;
			//this.date = new Date(date.getTime());          //the proper way to copy(defensively) mutable object
		}
		
		public String toString(){
			return "String = " + s + "; float = " + f +"; char = " + c + "; Date = " + date.toString();
		}
		
		public Date getDate(){
			return new Date(date.getTime()); //defensive copy - otrzymujemy wartość pola, ale nie obiekt. Użytkownik nie możemy ingerować w pole naszej klasy
		}
	}

}
