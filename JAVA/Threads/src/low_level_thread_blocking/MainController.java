package low_level_thread_blocking;

import java.lang.Thread.UncaughtExceptionHandler;
import java.net.URL;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class MainController implements Initializable{
	private Queue<Thread> threads = new LinkedList<>();
	private Runnable task, task2;
	private static int nrOfThread = 0;
	private static final Logger logger = Logger.getLogger("LOG");
	
	private Lock lock = new ReentrantLock();
	private Condition someCondition = lock.newCondition();
	
	@FXML
	private Label label;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		task = new Runnable(){
			@Override
			public void run(){
				lock.lock(); //to zpowoduje że tylko jeden wątek będzie mógł obsługiwać poniższy kod, a reszta wątków będzie czekać(Blocked) aż ten odblokuje kod.
				int threadNumber = ++nrOfThread;
				logger.info("Running thread nr: " + threadNumber);				
				try{
					try{
						while(!Thread.currentThread().isInterrupted()){ //nie potrzeba sprawdzać statusu przerwania ponieważ poniżej jest metoda sleep.
							Platform.runLater( () -> {					//wątek nie zostanie uśpiony dla wątku z ustawionym statusem przerwania i zostanie wywołany wyjątek
								label.setText(getCurrentTime());
							});
							
							Thread.sleep(3000);
						}
					}finally{ //nie należy uciszać
						lock.unlock(); //koniecznie trzeba odblokować
						
						Thread.currentThread().interrupt();
						Platform.runLater( () -> {
							label.setText("Zatrzymany");
						});
					}
				}catch(InterruptedException e){
					logger.info("thread nr: " + threadNumber + " interrupted");
				}
				
				logger.info("Thread nr: " + threadNumber + " has stopped.");
			}
		};
		
		
		task2 = new Runnable() {		
			@Override
			public void run() {
				int threadNumber = ++nrOfThread;
				logger.info("Running thread nr: " + threadNumber);	
				
				try {
					while(true){
						changeTime();
					}
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					someCondition.signalAll(); //... sygnalizujemy oczekującym wątkom a swojej zakończonej pracy
					Platform.runLater( () -> {
						label.setText("Zatrzymany");
					});
				}
				
				logger.info("Thread nr: " + threadNumber + " has stopped.");
			}
		};
	}
	
	private synchronized void changeTime() throws InterruptedException{	//synchro nie pozwala na dostep wecej niz 1 wątka
		while(threads.size() >= 10)  //każdy kolejny wątek będzie wchodził w stan oczekiwania na działanie, z tym że metoda jest synchroniczna i nie pozwoli na dzialanie wiecej niz 1 watkowi
									//wiec warunek nigdy nie zadziala
			someCondition.await(); //wykonywanie kodu się zatrzymuje
		
		Platform.runLater( () -> {					
			label.setText(getCurrentTime());
		});
			
		Thread.sleep(3000);
		
		
	}
	
	
	@FXML
	private void startButton(ActionEvent e){
		Thread thread = new Thread(task); //mozna przełączyć na task2
		thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread t, Throwable e) {
				logger.info("Thread has stopped because of excpetion in run method");
				
			}
		});
		thread.start();
		threads.add(thread); //dodajemy element do końca kolejki
	}
	@FXML
	private void cancelButton(ActionEvent e){
		if(threads.size() > 0)
			threads.poll().interrupt(); //usuwamy element z oczątku listy i wykonujemy dzialanie
	}
	
	private String getCurrentTime(){
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.HOUR) + ": " + calendar.get(Calendar.SECOND);
	}
}
