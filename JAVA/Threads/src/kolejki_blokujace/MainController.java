package kolejki_blokujace;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.DirectoryChooser;

public class MainController implements Initializable{
	private BlockingQueue<File> blockingQueue; //kolejka obiektów o ustalonej pojemnosci. Blokuje wątek jeśli chce pobrać z pustej lub dodać gdy jest pełna.
											  //kolejka ta pilnuje by wszystko działo się synchronicznie i wątki nie wchodziły sobie w drogę
	
	private Thread enumerateThread;
	private File path = null;
	private int nrOfCalls = 0;
	private final int FILE_QUEUE_SIZE = 10;
	private final int SEARCH_THREADS = 100;
	
	@FXML
	private Label directoryLabel;
	@FXML
	private TextArea textAreaOutput, textAreaInfo, textAreaResult;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		blockingQueue = new ArrayBlockingQueue<>(FILE_QUEUE_SIZE);  //ile plików
	}
	@FXML
	private void startButton(ActionEvent e){
		if(path != null){
			++nrOfCalls;
			textAreaInfo.setText("======START nr "+nrOfCalls+"=====\n");
			textAreaResult.appendText("\nWywołanie nr " + nrOfCalls + "...");
			blockingQueue.clear();
			
			EnumerateFilesTask enumerateFiles = new EnumerateFilesTask(path, blockingQueue, textAreaInfo, textAreaResult);
			enumerateThread = new Thread(enumerateFiles);
			enumerateThread.start();
			for(int i=0; i < SEARCH_THREADS; i++){ //tworzymy wątki przeszukujące
				BlockingQueueConsumer consumer = new BlockingQueueConsumer(i, blockingQueue, textAreaInfo, textAreaOutput);
				new Thread(consumer).start();
			}
		}
	}
	@FXML
	private void stopButton(ActionEvent e){
		if(enumerateThread.isAlive())
			enumerateThread.interrupt();
	}
	@FXML
	private void directoryButton(ActionEvent e){
		final DirectoryChooser directoryChooser = new DirectoryChooser();
		final File selectedDirectory = directoryChooser.showDialog(textAreaOutput.getScene().getWindow());
		if(selectedDirectory != null){
			path = selectedDirectory;
			directoryLabel.setText(path.getAbsolutePath());
		}
	}
}
