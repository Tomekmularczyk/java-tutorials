package kolejki_blokujace;

import java.io.File;
import java.util.concurrent.BlockingQueue;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.TextArea;

public class BlockingQueueConsumer extends Task<Void>{
	private int nr;
	private BlockingQueue<File> queue;//klasa obserwująca, jeśli coś do niej damy albo usuniemy, controlka która posiada ten obiekt zrobi autmatyczny update
	private TextArea info, output; 
	private StringBuilder outputText = new StringBuilder();
	
	public BlockingQueueConsumer(int nr, BlockingQueue<File> queue, TextArea info, TextArea output){
		this.nr = nr;
		this.queue = queue;
		this.info = info;
		this.output = output;
		output.textProperty().bind(messageProperty());
	}
	

	
	private synchronized void printInfo(String name){
		Platform.runLater(() ->{
			info.appendText(name);
		});
	//	System.out.println(paths);
	}
	private synchronized void printOutput(String txt){
		Platform.runLater(() ->{
			output.appendText(txt);
		});
//		System.out.println(txt);
	}

	@Override
	protected Void call() throws Exception {
		boolean isDone = false;
		
		printInfo("Uruchomiono wątek wyszukujący nr: " + nr + "\n");
		while(!isDone){
			try {
				File file = queue.take(); //pobiera i usuwa element z kolejki, a jeżeli nie ma nic to wątek wchodzi w stan oczekiwania/blokady 
				if(file == EnumerateFilesTask.LAST_FILE){
					queue.put(file);
					isDone = true;
				}else{
					outputText.append(file.getName() + "\n");
					updateMessage(outputText.toString());
					
				}
					
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		
		printInfo("Wątek " + nr +" zakończony\n");
		output.textProperty().unbind();
		return null;
	}
}
