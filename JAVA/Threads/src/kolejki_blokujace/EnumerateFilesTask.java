package kolejki_blokujace;

import java.io.File;
import java.util.Date;
import java.util.concurrent.BlockingQueue;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class EnumerateFilesTask implements Runnable{
	private int nrOfFiles = 0;
	private File startingDirectory;
	private BlockingQueue<File> queue;
	private TextArea textAreaInfo, textAreaResult;
	public static final File LAST_FILE = new File("");//Trik: publiczny obiekt. klasa wstawi go gdy skonczy dodawac pliki, 
													  //a inne klasy mogą sprawdzić czy praca na kolejce jest zakończona, sprawdzając czy dotarł już ten obiekt, sygnalizując koniec pracy
	
	public EnumerateFilesTask(File startingDirectory, BlockingQueue<File> queue, TextArea textAreaInfo, TextArea textAreaResult){
		this.startingDirectory = startingDirectory;
		this.queue = queue;
		this.textAreaInfo = textAreaInfo;
		this.textAreaResult = textAreaResult;
	}
	
	@Override
	public void run() {
		Date before = new Date();
		
		printInfo("Uruchomiono wątek wyliczający pliki.\n");
		
		try{
			try {
				enumerate(startingDirectory);
			}finally{
				queue.put(LAST_FILE);
				Date after = new Date();
				printResult("\nZnaleziono " + nrOfFiles + " plików w " + (after.getTime() - before.getTime()) + " miliseconds");
				printInfo("Zakończone wątek wyliczający pliki.\n");
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			printInfo("Wątek wyliczający pliki został niespodziewanie przerwany.\n");
			e.printStackTrace();
		} 
	}
	
	private void enumerate(File startingDirectory) throws InterruptedException{
		File[] files = startingDirectory.listFiles(); //nazwa files jest myląca ponieważ może "przechowywać" foldery(ich adresy)
		if(files != null)
			for(File file : files){
				if(file.isDirectory())	enumerate(file);  //działamy rekursywnie
				else{
					queue.put(file); //dodaje plik, a jeśli kolejka jest pełna blokuje(waiting) wątek i czeka aż się zwolni miejsce
					++nrOfFiles;
				}
			}
	}

	private synchronized void printInfo(String txt){
		Platform.runLater(() ->{
			textAreaInfo.appendText(txt);
		});
	}
	private synchronized void printResult(String txt){
		Platform.runLater(() ->{
			textAreaResult.appendText(txt);
		});
	}
}
