package inner;

import java.util.ArrayList;

import interfejsy.ClassImplementingInterface;
import interfejsy.SomeInterface1;

public class Main {

	public static void main(String[] args) {
		//anonimowa klasa nie jest statyczna dlatego musimy posiadać utworzony obiekt klasy zewnętrznej by utorzyć klasę wewnętrzną
		//Ma to sens ponieważ klasa wewnętrzna może korzystać z pól klasy otaczającej, więc kompilator pilnuje by była ona wcześniej utworzona.
		ClassImplementingInterface.SomeInnerClass object = new ClassImplementingInterface().new SomeInnerClass(); //pierwszy sposób
		ClassImplementingInterface classImplementing = new ClassImplementingInterface();
		ClassImplementingInterface.SomeInnerClass object2 = classImplementing.new SomeInnerClass();//sposób drugi
		
		
		Coffee normalCoffe = new Coffee();
		Coffee coffe2 = new Coffee() {  //anonimowa klasa rozszerzająca Coffe(ta deklaracja różni się od tworzenia zwykłego obiektu tylko klamrami!)
			private int newVariable;
		    { //konstruktor dla anonimowej klasy
				name = "anonimowa klasa rozszerzająca Coffe";
			}
		};
		SomeInterface1 interF = new SomeInterface1(){ //przypisujemy to referencji interfejsu klasę(anonimową) implementującą interfejs
			public String newVariable;
			{ //konstruktor
				newVariable = "anonimowa klasa z interfejsem SomeInterface";
			}
			@Override
			public void sayHay() {
				
			}
			@Override
			public String toString(){
				return newVariable;
			}
		};
		
		wypisz(normalCoffe.name);
		wypisz(coffe2.name);
		wypisz(interF.toString());
		
		//a teraz taki se trik ----
		ArrayList<Integer> arrayList = new ArrayList<Integer>(){{ add(3); add(5); add(12); add(4); }};
		/*utworzyliśmy anonimową klasę rozszerzającą ArrayList<String> a w anonimowym konstruktorze(zauważ klamry)
		  użyliśmy metodę klasy add() by zainicjować odrazu naszą liste wartościami.
		*/
	}
	
	private static void wypisz(String s){
		System.out.println(s);
	}

}
