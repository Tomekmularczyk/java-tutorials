package statyczna_wewnetrzna;

public class Main {

	public static void main(String[] args) {
		// Dzięki temu że klasa jest statyczna, nie musimy posiadać obiektu klasy zewnętrznej by ją utworzyć
		Coffee.StaticInner innerclass = new Coffee.StaticInner();
		
	}

}
