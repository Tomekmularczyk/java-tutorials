package statyczna_wewnetrzna;

public class Coffee extends inner.Coffee{
	
	public Coffee(){
		this.name = "Coffee extends package2.Coffee";
	}
	
	
	public static class StaticInner{ //zachowuje się jak zwykły obiekt, tylko nie ma dostępu do otaczającej go klasy
		public String about;    //nie statyczne zmienne, mimo, że klasa jest statyczna
		
		public StaticInner(){
			//this.name = "";  //nope. statyczna klasa nie ma dostepu do pól obiektu otaczającej go klasy
			about = "statyczna klasa wewnętrzna";
		}
	}
}
