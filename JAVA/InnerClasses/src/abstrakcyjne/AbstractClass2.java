package abstrakcyjne;

public abstract class AbstractClass2 extends AbstractClass{ //klasa nie musi implementować wszystkich metod klasy abstrakcyjnej jeżeli uczynimy ją też abstrakcyjną

	@Override
	protected void doIt() {
		System.out.println("");
	}
	
}
