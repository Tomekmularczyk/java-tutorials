package abstrakcyjne;

public abstract class WeirdAbstract { //abstrakcyjna klasa wcale nie musi miec abstrakcyjnych metod, możemy określić klase jako abstrakcyjną 
									  //jeżeli nie chcemy by ktoś tworzył jej egzemplarze. (Można by też było zrobić prywatny konstruktor)
	public static String COS = "COS";
	public static int ILES = 342343;
	
	public static String returnStr(){
		return "str";
	}
}
