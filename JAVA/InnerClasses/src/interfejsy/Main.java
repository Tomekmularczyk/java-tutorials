package interfejsy;

public class Main {

	public static void main(String[] args) {
		//SomeInterface interf = new SomeInterface(); //cannot instantiate interface it must be class or anonymous class implementing interface 
		
		SomeInterface1 interF = new SomeInterface1() { //anonymous inner class
			@Override
			public void sayHay() {
				System.out.println(s); //s to zmienna finalna w interfejsie
			}
		};

		
		ClassImplementingInterface classImpl = new ClassImplementingInterface();
		//ponieważ ClassImplementingInterface implementuje różne interfejsy, pozwala to dziedziczenie wielobazowe
		SomeInterface1 interF1 = classImpl; 
		SomeInterface2 interF2 = classImpl;
		SomeInterface3 interF3 = classImpl; 
	}

}
