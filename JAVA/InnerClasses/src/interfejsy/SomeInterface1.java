package interfejsy;

public interface SomeInterface1 {
	public String s = "SomeInterface"; //it is final
	
	public void sayHay();
	
	public default void action(){
		System.out.println("default interface1 method");
	};

}
