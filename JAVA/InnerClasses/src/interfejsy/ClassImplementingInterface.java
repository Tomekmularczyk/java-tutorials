package interfejsy;

public class ClassImplementingInterface implements SomeInterface1, SomeInterface2, SomeInterface3{
    //nie powinno pisać się interfejsów tylko po to by zmusić klasę do implementowania jakiejś metody


	public class SomeInnerClass{	}
	private interface PrivateInterface{ } //w klasie można utworzyć sobie prywatny interfejs

	@Override
	public void sayHay() {
		System.out.println("Hay!!");
	}

	
	//musimy przesłonić jedna metode ponieważ wszystkie interfejsy mają tak samo nazwaną metode!
	@Override
	public void action() {
		SomeInterface1.super.action();
	//	SomeInterface2.super.action();
		SomeInterface3.super.action();
	}
}
