package dziedziczenie_interfejsow;

public class TEST {

	public static void main(String[] args) {
		//---przykład dziedziczenia wielobazowego za pomocą interfejsów
		Wife wife = new Wife("Diana", 24);
		introduce(wife);
		cook(wife);
		clean(wife);
		wash(wife);
		sing(wife);
	}

	public static void cook(CanCook cook){
		cook.cook();
	}
	public static void clean(CanCookAndClean clean){
		clean.clean();
	}
	public static void wash(CanCookAndWash wash){
		wash.wash();
	}
	public static void introduce(HouseWife wife){
		wife.introduce();
	}
	public static void sing(Tenant tenant){
		tenant.sing();
	}
}
