package dziedziczenie_interfejsow;

public interface CanCookAndClean extends CanCook{

	public void clean();
}
