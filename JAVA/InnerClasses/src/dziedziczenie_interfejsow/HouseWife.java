package dziedziczenie_interfejsow;

public interface HouseWife extends CanCookAndClean, CanCookAndWash{
	public void introduce();
}
