package dziedziczenie_interfejsow;

public class Wife extends Tenant{
	private String name;
	public Wife(String name, int age){
		this.name=name;
		setAge(age);
	}

	@Override
	public void clean() {
		System.out.println("Cleaning...");
	}

	@Override
	public void cook() {
		System.out.println("Cooking...");
	}

	@Override
	public void wash() {
		System.out.println("Washing...");
	}

	@Override
	public void introduce() {
		System.out.printf("My name is %s!\n", name);
	}

	@Override
	public void sing() {
		System.out.println("la la la la...");
	}

}
