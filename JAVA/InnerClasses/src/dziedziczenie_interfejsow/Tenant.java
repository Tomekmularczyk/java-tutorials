package dziedziczenie_interfejsow;

public abstract class Tenant implements HouseWife{
	private int age = 0;
	
	public void setAge(int age){
		this.age=age;
	};
	
	public abstract void sing();
}
