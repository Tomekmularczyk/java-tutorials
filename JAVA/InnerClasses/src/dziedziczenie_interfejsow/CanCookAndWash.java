package dziedziczenie_interfejsow;

public interface CanCookAndWash extends CanCook{
	public void wash();
}
