package chess_dice;

public class TEST {  //projekt ze wzorca FactoryMethod

	
	private static class ChessFactory implements GameFactory{
		public Play getGame(){
			return new Chess();
		}
	}
	private static class DiceFactory implements GameFactory{
		public Play getGame(){
			return new Dice();
		}
	}
	
	
	private static void play(GameFactory game){
		game.getGame().play();
	}
	
	public static void main(String[] args) {
		play(new ChessFactory());
		play(new DiceFactory());

	}

}
