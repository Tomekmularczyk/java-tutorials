package chess_dice;

public interface GameFactory {
	public Play getGame();
}
