package one_to_one_bidirectional;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * OneToOne tworzy wypycha klucz z tabeli Rodzica do tabeli klienta. Dzięki temu powstaje powiązanie dwukierunkowe.
 */
@Entity
public class MusicRecord implements Serializable {

    @Id
    @GeneratedValue
    public long id;

    //to pole też będzie mapowane na kolumne
    public String author;

    @OneToOne(mappedBy = "musicRecord", cascade = CascadeType.ALL) //nazwa pola w MusicRecordDetails do której bedzie zapisywana referencja do tego obiektu, CascadeType.All oznacza, że przy usuwaniu jednego wiersza usuniety zostanie też wiersz w drugiej tabeli
    public MusicRecordDetails details;

    public MusicRecord() {
    }

    public MusicRecord(String author) {
        this.author = author;
    }
    
}
