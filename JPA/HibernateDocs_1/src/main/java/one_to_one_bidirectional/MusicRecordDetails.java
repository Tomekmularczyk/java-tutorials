package one_to_one_bidirectional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Tworzymy powiązanie dwukierunkowe. Ponieważ Details nie może istnieć bez MusicRecord, klucz obcy będzie wypchnięty tutaj.
 * Dzięki temu od strony Javy mamy łatwy dostęp do powiązanej tabeli.
 */
@Entity
public class MusicRecordDetails {
    @Id
    @GeneratedValue
    int id;
    
    public String details;
    
    @OneToOne
    public MusicRecord musicRecord;

    public MusicRecordDetails() {
    }

    public MusicRecordDetails(String details) {
        this.details = details;
    }
    
}
