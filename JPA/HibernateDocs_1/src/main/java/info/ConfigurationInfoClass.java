package info;

public class ConfigurationInfoClass {
    /**
     * 1. Projekt korzysta z konfiguracji JPA w Persistence.xml znajdującej się w folderze META-INF
     * 2. W Persistence.xml skonfigurowane są różne persistence-unit dla każdego przykładu. Przydaje się to po to, 
     *    by łatwo można było podejrzeć w bazie wyłącznie te tabele wygenerowane przez dany przykład.
     * 3. Każdy przykład można odpalić z pliku testowego dla danego pakietu.
     * 4. Zmienne tworzymy o dostępie publicznym tak by uniknąć zaciemniania geterami i seterami
     */
}
