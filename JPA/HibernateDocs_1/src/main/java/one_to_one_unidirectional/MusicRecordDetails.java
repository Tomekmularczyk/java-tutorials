package one_to_one_unidirectional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Ta encja nic nie wie o tym, że MusicRecord ma klucz do tej tabeli.
 */
@Entity
public class MusicRecordDetails {
    @Id
    @GeneratedValue
    int id;
    
    public String details;
}
