package one_to_one_unidirectional;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;


/**
 * OneToOne tworzy kolumne z kluczem do innej tabeli, ale ta druga tabela nie ma klucza do tej tabeli (Unidirectional).
 */
@Entity
public class MusicRecord implements Serializable {

    @Id
    @GeneratedValue
    public long id;
    
    //to pole też będzie mapowane na kolumne
    public String author;
    
    
    @OneToOne//stworzy kolumne z nazwą details_id
    // jeżeli chcielibyśmy zmienić nazwę, użylibyśmy: @JoinColumn(name = "sratytaty") 
    public MusicRecordDetails details;
}
