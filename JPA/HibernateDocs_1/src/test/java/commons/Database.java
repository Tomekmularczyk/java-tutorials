package commons;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Database {

    private final EntityManagerFactory emFactory;
    private final EntityManager em;

    public Database(String persistenceUnit) {
        emFactory = Persistence.createEntityManagerFactory(persistenceUnit);
        em = emFactory.createEntityManager();
    }

    public EntityManager getEntityManager() {
        em.getTransaction().begin();
        return em;
    }

    public void waitAndFinish() {
        em.getTransaction().commit();
        Console.stop();
        em.close();
        emFactory.close();
    }
}
