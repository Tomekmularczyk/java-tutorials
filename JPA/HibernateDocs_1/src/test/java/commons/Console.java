package commons;

import static java.lang.System.err;
import java.util.Scanner;

public class Console {
    
    private static final Scanner SCANNER = new Scanner(System.in);
    
    public static void stop(){
        err.println("Naciśnij dowolny klawisz...");
        SCANNER.nextLine();
    }
    
}
