package one_to_one;

import commons.Database;
import java.io.IOException;
import javax.persistence.EntityManager;
import one_to_one_unidirectional.MusicRecord;
import one_to_one_unidirectional.MusicRecordDetails;

public class OneToOneUnidirectionalTest {

    public static void main(String[] args) throws IOException {
        Database database = new Database("one-to-one-uni");
        EntityManager em = database.getEntityManager();
        
        MusicRecord musicRecord = new MusicRecord();
        musicRecord.author = "Autor";
        MusicRecordDetails details = new MusicRecordDetails();
        details.details = "detale";
        musicRecord.details = details;
        em.persist(musicRecord);
        em.persist(details);
        
        database.waitAndFinish();
    }

}
