package one_to_one;

import commons.Database;
import java.io.IOException;
import javax.persistence.EntityManager;
import one_to_one_bidirectional.MusicRecord;
import one_to_one_bidirectional.MusicRecordDetails;

public class OneToOneBidirectionalTest {

    public static void main(String[] args) throws IOException {
        Database database = new Database("one-to-one-bi");
        EntityManager em = database.getEntityManager();

        MusicRecord musicRecord = new MusicRecord("autor");
        MusicRecordDetails details = new MusicRecordDetails("detale");
        musicRecord.details = details;
        em.persist(musicRecord);

        database.waitAndFinish();
    }

    private static void uniqueConstraint(EntityManager entityManager, MusicRecord record) {
        MusicRecordDetails otherDetails = new MusicRecordDetails("T-Mobile");
        otherDetails.musicRecord = record;
        entityManager.persist(otherDetails);
        entityManager.flush();
        entityManager.clear();

        //jeżeli w tym wypadku record ma już zapisane coś pod details dostaniemy exception:
        //throws javax.persistence.PersistenceException: org.hibernate.HibernateException: More than one row with the given identifier was found: 1
    }
}
