package demo.dziedziczenie_single_table;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Studencik") //Domyślnie będzie podawana nazwa Klasy, ale jeżeli chcemy to zmienić to możemy podać anotacje
public class Student extends Person{
	private double averageMark;

	public double getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(double averageMark) {
		this.averageMark = averageMark;
	}
}
