package demo.dziedziczenie_single_table;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Profesorek") //Domyślnie będzie podawana nazwa Klasy, ale jeżeli chcemy to zmienić to możemy podać anotacje
public class Profesor extends Person{
	private double salary;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Profesor [salary=" + salary + "]";
	}
}
