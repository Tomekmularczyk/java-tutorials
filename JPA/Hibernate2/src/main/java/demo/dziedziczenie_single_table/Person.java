package demo.dziedziczenie_single_table;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="Persons")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)//jest to domyślna strategia i właściwie jest ona zbędna w tym przypadku
@DiscriminatorColumn(name="Jaki_to_obiekt") //domyślnie nadawana jest nazwa "DTYPE', jeżeli to nam wystarcza to także nie potrzebujemy tej adnotacji. Nazwa kolumn nie może mieć spacji!!
public abstract class Person {
	@Id @GeneratedValue
	private long id;
	
	private String name;
	private String lastName;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
}
