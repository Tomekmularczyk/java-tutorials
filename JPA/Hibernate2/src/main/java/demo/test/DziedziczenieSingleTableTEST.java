package demo.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import demo.dziedziczenie_single_table.Profesor;
import demo.dziedziczenie_single_table.Student;

/* W tym przykładzie pokazane jest, że klasy pochodne i macierzyste automatycznie zapisywane są w tej samej tabeli, 
 * która zawiera wszystkie potrzebne kolumny tworząc jedną dużą tabelę. 
 * W zależności jaki obiekt zapisujemy, jeżeli obiekt nie definiuje jakiegoś pola, to w tabeli będzie NULL w tym miejscu.
 * Dodatkowo dodawana jest kolumna('DTYPE') pozwalająca rozróżnić jaka dokładnie to klasa czy też encja 
 */

public class DziedziczenieSingleTableTEST {
	private static EntityManagerFactory eMF;
	private static EntityManager entityManager;
	
	public static void main(String[] args) {
		eMF = Persistence.createEntityManagerFactory("MyDatabase");
		entityManager = eMF.createEntityManager();
		
		Student student = new Student();
		student.setName("Tomek");
		student.setLastName("Mularczyk");
		student.setAverageMark(3.55);
		
		Profesor profesor = new Profesor();
		profesor.setName("Adrian");
		profesor.setLastName("Mikke");
		profesor.setSalary(2300);
		
		
		entityManager.getTransaction().begin();
		entityManager.persist(student);
		entityManager.persist(profesor);
		entityManager.getTransaction().commit();
		
		
		entityManager.close();
		eMF.close();
	}

}
