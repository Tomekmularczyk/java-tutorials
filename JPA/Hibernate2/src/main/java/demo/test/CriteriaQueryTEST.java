package demo.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import demo.dziedziczenie_single_table.Profesor;

public class CriteriaQueryTEST {
	//Criteria query pozwala w bezpieczniejszy sposób budować zapytania
	//w projekcie wykorzystamy sobie tabele Person
	
	private static EntityManagerFactory eMF;
	private static EntityManager entityManager;
	
	public static void main(String[] args) {
		eMF = Persistence.createEntityManagerFactory("MyDatabase");
		entityManager = eMF.createEntityManager();
		
		addSomeProffesors();
		
		//------criteria query
		CriteriaBuilder builder = entityManager.getCriteriaBuilder(); //tworzymy buildera
		CriteriaQuery<Profesor> criteriaQuery = builder.createQuery(Profesor.class); //ustalamy typ zwrotny
		Root<Profesor> profesor = criteriaQuery.from(Profesor.class); //określamy z jakiej tabeli bedziemy korzystać
		Path<Double> salary = profesor.get("salary"); //pobieramy całą kolumnę wartości
		criteriaQuery.select(profesor).where(builder.and(builder.greaterThan(salary, 2500.00), builder.lessThan(salary, 3000.00)));
		
		TypedQuery<Profesor> query = entityManager.createQuery(criteriaQuery);
		List<Profesor> resultList = query.getResultList();
		
		for (Profesor prof : resultList) {
			System.out.println(prof);
		}
		
		
		entityManager.close();
		eMF.close();
	}

	private static void addSomeProffesors() {
		entityManager.getTransaction().begin();
		
		Profesor[] profesors = new Profesor[5];
		profesors[0] = new Profesor();
		profesors[1] = new Profesor();
		profesors[2] = new Profesor();
		profesors[3] = new Profesor();
		profesors[4] = new Profesor();
		
		profesors[0].setName("Adrian");
		profesors[1].setName("Zenon");
		profesors[2].setName("Bartosz");
		profesors[3].setName("Kamil");
		profesors[4].setName("Jan");
		
		profesors[0].setLastName("Kozakiewicz");
		profesors[1].setLastName("Mularczyk");
		profesors[2].setLastName("Stonoga");
		profesors[3].setLastName("Mikke");
		profesors[4].setLastName("Walkiewicz");
		
		profesors[0].setSalary(2050);
		profesors[1].setSalary(2520);
		profesors[2].setSalary(2730);
		profesors[3].setSalary(2110);
		profesors[4].setSalary(2300);
		
		entityManager.persist(profesors[0]);
		entityManager.persist(profesors[1]);
		entityManager.persist(profesors[2]);
		entityManager.persist(profesors[3]);
		entityManager.persist(profesors[4]);
		
		entityManager.getTransaction().commit();
	}
}
