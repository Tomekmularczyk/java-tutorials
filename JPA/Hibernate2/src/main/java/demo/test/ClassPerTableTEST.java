package demo.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import demo.table_per_class.Car;
import demo.table_per_class.Motorcycle;

public class ClassPerTableTEST {
	/* Ta strategia tworzy osobną tabelę dla klas pochodnych posiadającą wszystkie kolumny z klasy matki.
	 * Jest tylko jeden problem, ponieważ klasy pochodne dziedziczą ID po klasie matce, żeby mapowanie miało sens w kodzie JAVY,
	 * musimy zrobić tak by każdemu obiektowi w tabeli zostało przydzielane unikatowe ID. Stworzy to też dodatkową tabelę
	 * 
	 */
	
	private static EntityManagerFactory eMF;
	private static EntityManager entityManager;
	
	public static void main(String[] args) {
		eMF = Persistence.createEntityManagerFactory("MyDatabase");
		entityManager = eMF.createEntityManager();
		
		Car car = new Car();
		car.setName("Subaru");
		car.setMaxSpeed(220);
		car.setSteeringWheelSize(14.5F);
		Car car2 = new Car();
		car2.setName("Daewoo");
		car2.setMaxSpeed(190);
		car2.setSteeringWheelSize(12.2F);
		
		Motorcycle motorcycle = new Motorcycle();
		motorcycle.setName("Ducatti");
		motorcycle.setMaxSpeed(289);
		motorcycle.setHandleBarSize(18.7F);
		Motorcycle motorcycle2 = new Motorcycle();
		motorcycle2.setName("Honda CX");
		motorcycle2.setMaxSpeed(310);
		motorcycle2.setHandleBarSize(17.0F);
		
		
		entityManager.getTransaction().begin();
		entityManager.persist(car);
		entityManager.persist(car2);
		entityManager.persist(motorcycle);
		entityManager.persist(motorcycle2);
		entityManager.getTransaction().commit();
		
		entityManager.close();
		eMF.close();
	}

}
