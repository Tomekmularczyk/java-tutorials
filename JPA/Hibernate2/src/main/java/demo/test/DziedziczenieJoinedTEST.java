package demo.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import demo.dziedziczenie_joined.Eagle;
import demo.dziedziczenie_joined.Seal;

public class DziedziczenieJoinedTEST {
	/* Dziedziczenie przy pomocy strategii JOINED jest bardzo podobne do SingleTable, z tym, że dla każdego obiektu tworzona jest osobna tabela
	 * Klasa matka zawiera główne ID, a to ID jest później przekazywane do tabel pochodnych gdzie znajdują się kolumny dla wyspecjalizowanego obiektu 
	 */
	private static EntityManagerFactory eMF;
	private static EntityManager entityManager;
	
	public static void main(String[] args) {
		eMF = Persistence.createEntityManagerFactory("MyDatabase");
		entityManager = eMF.createEntityManager();
		
		Seal seal = new Seal();
		seal.setSpecies("Water animal");
		seal.setCountry("Antarctica");
		seal.setDivingSpeed(30.5);
		Eagle eagle = new Eagle();
		eagle.setSpecies("Bird");
		eagle.setCountry("Poland");
		eagle.setFlyingSpeed(300.00);
		
		
		entityManager.getTransaction().begin();
		entityManager.persist(seal);
		entityManager.persist(eagle);
		entityManager.getTransaction().commit();
		
		entityManager.close();
		eMF.close();
	}

}
