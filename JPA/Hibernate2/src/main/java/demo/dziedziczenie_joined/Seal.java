package demo.dziedziczenie_joined;

import javax.persistence.Entity;

@Entity
public class Seal extends Animal{
	private double divingSpeed;

	public double getDivingSpeed() {
		return divingSpeed;
	}

	public void setDivingSpeed(double divingSpeed) {
		this.divingSpeed = divingSpeed;
	}
}
