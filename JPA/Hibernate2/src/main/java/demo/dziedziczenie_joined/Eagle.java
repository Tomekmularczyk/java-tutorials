package demo.dziedziczenie_joined;

import javax.persistence.Entity;

@Entity
public class Eagle extends Animal{
	
	private double flyingSpeed;

	public double getFlyingSpeed() {
		return flyingSpeed;
	}

	public void setFlyingSpeed(double flyingSpeed) {
		this.flyingSpeed = flyingSpeed;
	}
}
