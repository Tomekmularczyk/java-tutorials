package demo.table_per_class;

import javax.persistence.Entity;

@Entity
public class Car extends Vehicle{
	private float steeringWheelSize;

	public float getSteeringWheelSize() {
		return steeringWheelSize;
	}

	public void setSteeringWheelSize(float steeringWheelSize) {
		this.steeringWheelSize = steeringWheelSize;
	}
}
