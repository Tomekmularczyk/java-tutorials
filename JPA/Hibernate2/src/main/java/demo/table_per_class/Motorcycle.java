package demo.table_per_class;

import javax.persistence.Entity;

@Entity
public class Motorcycle extends Vehicle{
	private float handleBarSize;

	public float getHandleBarSize() {
		return handleBarSize;
	}

	public void setHandleBarSize(float handleBarSize) {
		this.handleBarSize = handleBarSize;
	}
}
