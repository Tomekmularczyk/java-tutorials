package demo.table_per_class;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) //nie możemy zapomnieć by zmienić strategie dziedziczenia
public abstract class Vehicle {
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)//musimy to ustawić by każda klasa pochodna od tej miała przydzielane unikatowe ID w bazie danych
	private long id;
	
	private String name;
	private int maxSpeed;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	
}
