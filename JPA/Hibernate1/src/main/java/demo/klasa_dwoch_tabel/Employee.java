package demo.klasa_dwoch_tabel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

@Entity
@Table(name = "Employees1") //wszystkie entity w projekcie musza mieć unikalną nazwę inaczej dojdzie do konfliktów!
@SecondaryTable(name = "Address1", pkJoinColumns = @PrimaryKeyJoinColumn(name="employeeID")) //ten drugi skomplikowany parametr jest tylko by zmienic nazwe automatycznego ForeignKey'a
public class Employee {
	@Id @GeneratedValue
	private long id;
	
	@Column(table = "Employees1")
	private String name;
	@Column(table = "Employees1")
	private String lastName;
	@Column(table = "Employees1")
	private int age;
	@Column(table = "Employees1")
	private double salary;
	
	@Column(table = "Address1")
	private String zipCode;
	@Column(table = "Address1")
	private String nationality;
	@Column(table = "Address1")
	private String street;
	@Column(table = "Address1")
	private int streetNr;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getStreetNr() {
		return streetNr;
	}
	public void setStreetNr(int streetNr) {
		this.streetNr = streetNr;
	}
}
