package demo.jeden_do_jeden;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Address3")
public class Address {
	@Id @GeneratedValue
	private long id;
	
	private String country;
	private String street;
	private int streetNr;
	
	//encja ta nie przechowuje informacji o tym do kogo należy ten adres, więc jest to relacja jeden-do-jeden ale jednokierunkowa
	//jeżeli jednak chcielibyśmy przechowywać informacje o pracowniku którego ten adres dotyczy, tak, 
	//żeby przy wyciąganiu adresu wyciągnąć też dane pracownika piszemy(relacja jeden-do-jeden dwukierunkowa):
/*	@OneToOne(mappedBy="address") //w nawiasie podajemy nazwę pola z klasy employee do którego odnosi się ta encja
	private Employee Employee;
*/	
	
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getStreetNr() {
		return streetNr;
	}
	public void setStreetNr(int streetNr) {
		this.streetNr = streetNr;
	}
}
