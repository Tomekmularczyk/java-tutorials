package demo.jeden_do_jeden;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Employees3")
public class Employee {
	@Id @GeneratedValue
	private long ID;
	private String firstName;
	private String lastName;
	private double salary;
	
	@JoinColumn(name = "addressID") //nadajemy nazwe kluczowi obcemu
	@OneToOne(cascade = CascadeType.ALL) //oznacza że jest to relacja jeden do jeden, czyli będą z tego dwie tabele...
										//...dodatkowy paramter cascade określa że w przypadku usunięcia tego pracownika bedzie usuniety też jego adress w drugiej tabeli		
	private Address address;
	
	
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "Employee [ID=" + ID + ", firstName=" + firstName + ", lastName=" + lastName + ", salary=" + salary
				+ ", address=" + address + "]";
	}
	
}
