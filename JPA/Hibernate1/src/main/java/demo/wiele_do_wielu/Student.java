package demo.wiele_do_wielu;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="Students")
public class Student {
	@Id @GeneratedValue
	private long id;
	
	private String name;
	private String lastName;
	
	@ManyToMany
	//--Nadajemy nazwy nowo powstałej tabeli łączącej i jej kolumnom
	@JoinTable(name="Nauczyciele_i_Studenci", 
				joinColumns={@JoinColumn(name="id_Nauczyciela")},
				inverseJoinColumns={@JoinColumn(name="id_Studenta")})
	private List<Teacher> teachers;

	
	
	public Student(){}
	public Student(String name, String lastName, List<Teacher> teachers) {
		this.name = name;
		this.lastName = lastName;
		this.teachers = teachers;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", lastName=" + lastName + ", teachers=" + teachers + "]";
	}
	
}
