package demo.wiele_do_wielu;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Teachers")
public class Teacher {
	@Id @GeneratedValue
	private long id;
	
	private String name;
	private double age;
	
	@ManyToMany(mappedBy="teachers") //mappedBy podajemy tylko po jednej stronie i wskazuje na pole w przeciwnej encji
	private List<Student> students;
	
	
	public Teacher() {}
	public Teacher(String name, double age, List<Student> students) {
		this.name = name;
		this.age = age;
		this.students = students;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@Override
	public String toString() {
		return "Teacher [id=" + id + ", name=" + name + ", age=" + age + ", students=" + students + "]";
	}
}
