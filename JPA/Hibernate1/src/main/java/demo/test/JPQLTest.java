package demo.test;

import demo.jpql.Product;
import java.util.Iterator;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class JPQLTest {

    private static EntityManagerFactory eMF;
    private static EntityManager entityManager;

    public static void main(String[] args) {
        eMF = Persistence.createEntityManagerFactory("MyDatabase");
        entityManager = eMF.createEntityManager();

        createSomeProducts();

        //wyświetlamy liste produktów przy pomocy JPQL. Zauważ że zamiast gwiazdki jest 'p', a zamiast nazwy tabeli 'Products' jest nazwa Encji - klasy jaka ją mapuje
        TypedQuery<Product> cheapProducts = entityManager.createQuery("SELECT p FROM Product p WHERE p.price < :priceParam", Product.class);
        cheapProducts.setParameter("priceParam", 5000.00f);//parametrem może być też List<> wrzucony do klauzuli IN()
        //cheapProducts.getSingleResult(); //--jeżeli bylibyśmy pewni że bedzie tylko jeden wynik
        for (Product product : cheapProducts.getResultList()) {
            System.out.println(product);
        }

        //wyciągamy wyniki które nie są mapowane na żadną klasę
        Query query = entityManager.createQuery("SELECT CONCAT(p.id, '. ', p.name), p.price * 0.23 AS tax FROM Product p");
        Iterator<?> iterator = query.getResultList().iterator();
        while (iterator.hasNext()) {
            //musimy wiedzieć co znajduje się w tabeli dlatego jest to mało bezpieczne podejście
            Object[] object = (Object[]) iterator.next();
            String nameWithId = (String) object[0];
            double tax = (Double) object[1];

            System.out.println("Name: " + nameWithId + ", tax: " + tax);
        }

        entityManager.close();
        eMF.close();
    }

    private static void createSomeProducts() {
        entityManager.getTransaction().begin();

        entityManager.persist(new Product("TV", (short) 5, 1500.00f));
        entityManager.persist(new Product("Golf ball", (short) 1, 2.50f));
        entityManager.persist(new Product("Subaru Impreza", (short) 12, 10500.00f));
        entityManager.persist(new Product("Ducatti motorcycle", (short) 3, 6750.00f));
        entityManager.persist(new Product("Shoes", (short) 1, 100.00f));
        entityManager.persist(new Product("Sofa", (short) 4, 710.30f));
        entityManager.persist(new Product("Chair", (short) 2, 1400.00f));
        entityManager.persist(new Product("Cellphone", (short) 2, 2500.00f));

        entityManager.getTransaction().commit();
    }

}
