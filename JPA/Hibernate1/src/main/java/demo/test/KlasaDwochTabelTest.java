package demo.test;

import demo.klasa_dwoch_tabel.Employee;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class KlasaDwochTabelTest {

    /* Przy pomocji adnotacji @Table i @SecondaryTable jesteśmy w stanie z jednej klasy zrobić entity, 
	 * które będzie podzielone na dwie tabele.
     */

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MyDatabase"); //MyDatabase z pliku xml
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Employee employee = new Employee();
        employee.setName("Tomek");
        employee.setLastName("Katora");
        employee.setAge(32);
        employee.setSalary(2300.00);
        employee.setZipCode("35-439");
        employee.setNationality("Polish");
        employee.setStreet("Polna");
        employee.setStreetNr(28);

        entityManager.getTransaction().begin();
        entityManager.persist(employee);
        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();
    }

}
