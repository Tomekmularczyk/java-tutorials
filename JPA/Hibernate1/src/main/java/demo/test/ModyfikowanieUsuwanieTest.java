package demo.test;

import demo.jeden_do_jeden.Address;
import demo.jeden_do_jeden.Employee;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ModyfikowanieUsuwanieTest {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MyDatabase");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        addSomeEmployees(entityManager);

        entityManager.getTransaction().begin();

        //--wyciągamy obiekt
        Employee employee = entityManager.find(Employee.class, 1l);//drugi parametr to PrimaryKey
        System.out.println(employee);

        //--modyfikujemy obiekt
        employee.setLastName("Kapucyński");
        employee.setSalary(2200.50);

        //--zapisujemy obiekt
        entityManager.persist(employee);

        //-----sprawdzamy jeszcze raz czy sie zapisało
        employee = entityManager.find(Employee.class, 1l);
        System.out.println(employee);

        //usuwamy pracownika z ID 3
        employee = entityManager.find(Employee.class, 3l);
        entityManager.remove(employee);

        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();

        //------------------ gdy podejrzewamy że w bazie danych zaszła jakaś zmiana i nasz obiekt jest nie aktualny, możemy użyć refresha...
        //entityManager.refresh(employee); 
    }

    //======================================================================
    public static void addSomeEmployees(EntityManager entityManager) {
        Employee[] employee = {new Employee(), new Employee(), new Employee()}; //wykorzystamy sobie gotowe jednostki(Employee3 i Address3) z pakietu jeden_do_jeden
        Address[] address = {new Address(), new Address(), new Address()};
        employee[0].setAddress(address[0]);
        employee[0].setFirstName("Tomek");
        employee[0].setLastName("Kapucyn");
        employee[0].setSalary(3400.0);
        address[0].setCountry("Polska");
        address[0].setStreet("Polna");
        address[0].setStreetNr(29);

        entityManager.getTransaction().begin();
        entityManager.persist(employee[0]);
        entityManager.persist(address[0]);
        entityManager.getTransaction().commit();

        employee[1].setAddress(address[1]);
        employee[1].setFirstName("Adrian");
        employee[1].setLastName("Zuber");
        employee[1].setSalary(2100.0);
        address[1].setCountry("Polska");
        address[1].setStreet("Domaniewska");
        address[1].setStreetNr(37);

        entityManager.getTransaction().begin();
        entityManager.persist(employee[1]);
        entityManager.persist(address[1]);
        entityManager.getTransaction().commit();

        employee[2].setAddress(address[2]);
        employee[2].setFirstName("Zuza");
        employee[2].setLastName("Loten");
        employee[2].setSalary(1850.0);
        address[2].setCountry("Niemcy");
        address[2].setStreet("Skladowa");
        address[2].setStreetNr(110);

        entityManager.getTransaction().begin();
        entityManager.persist(employee[2]);
        entityManager.persist(address[2]);
        entityManager.getTransaction().commit();
    }
}
