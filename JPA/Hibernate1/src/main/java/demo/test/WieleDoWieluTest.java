package demo.test;

import demo.wiele_do_wielu.Student;
import demo.wiele_do_wielu.Teacher;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class WieleDoWieluTest {

    private static EntityManagerFactory eMF;
    private static EntityManager entityManager;

    //wiele do wielu ponieważ student może mieć wielu nauczycieli i nauczyciel może mieć wielu studentów
    public static void main(String[] args) {
        eMF = Persistence.createEntityManagerFactory("MyDatabase");
        entityManager = eMF.createEntityManager();

        addSomeData();

        entityManager.close();
        eMF.close();
    }

    private static void addSomeData() {
        List<Student> studentsList1 = new LinkedList<>();
        List<Student> studentsList2 = new LinkedList<>();
        List<Student> studentsList3 = new LinkedList<>();
        Teacher[] teachers = {new Teacher("Drozda", 46, studentsList1),
            new Teacher("Kazimierczuk", 33, studentsList2),
            new Teacher("Pawłowicz", 51, studentsList3)};

        List<Teacher> teachersList1 = new LinkedList<>();
        List<Teacher> teachersList2 = new LinkedList<>();
        List<Teacher> teachersList3 = new LinkedList<>();
        Student[] students = {new Student("Tomek", "Mularczyk", teachersList1),
            new Student("Agata", "Szymańska", teachersList2),
            new Student("Agata", "Zadworna", teachersList3)};

        studentsList1.add(students[0]);
        studentsList1.add(students[1]);
        studentsList2.add(students[1]);
        studentsList2.add(students[2]);
        studentsList3.add(students[2]);
        studentsList3.add(students[1]);

        teachersList1.add(teachers[0]);
        teachersList1.add(teachers[1]);
        teachersList2.add(teachers[0]);
        teachersList2.add(teachers[2]);
        teachersList3.add(teachers[1]);
        teachersList3.add(teachers[2]);

        entityManager.getTransaction().begin();
        entityManager.persist(teachers[0]);
        entityManager.persist(teachers[1]);
        entityManager.persist(teachers[2]);
        entityManager.persist(students[0]);
        entityManager.persist(students[1]);
        entityManager.persist(students[2]);
        entityManager.getTransaction().commit();
    }

}
