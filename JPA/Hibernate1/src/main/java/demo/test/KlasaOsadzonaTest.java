package demo.test;

import demo.klasa_osadzona.Address;
import demo.klasa_osadzona.Employee;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class KlasaOsadzonaTest {

    /* Klasa osadzona jest wykorzystywana w innej klasie jakio kompozycja, jednak dzieki adnotacji @Embedabble i @Embedded
	 * Tworzy się jedna tabela posiadająca kolumny z pierwszej i drugiej klasy
     */

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MyDatabase");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Employee employee = new Employee();
        employee.setAge(22);
        employee.setName("Tomek");
        employee.setSalary(33333.04);
        employee.setLastName("Janda");
        Address address = new Address();
        employee.setAddress(address);
        address.setStreet("Polna");
        address.setStreetNr(29);
        address.setZipCode("40-420");

        entityManager.getTransaction().begin();
        entityManager.persist(employee);
        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();
    }

}
