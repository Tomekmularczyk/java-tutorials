package demo.test;

import demo.jeden_do_jeden.Address;
import demo.jeden_do_jeden.Employee;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JedenDoJedenTest {

    /*	Domyślnie stworzyliśmy tutaj jednokierunową relacje,
	 *  ale w komentarzach jest napisane co zrobić by była to dwukierunkowa relacja
     */
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MyDatabase"); //MyDatabase z pliku xml
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Employee employee = new Employee();
        employee.setFirstName("Tomek");
        employee.setLastName("Kapucyn");
        employee.setSalary(3400.0);
        Address address = new Address();
        address.setCountry("Polska");
        address.setStreet("Polna");
        address.setStreetNr(29);
        employee.setAddress(address);

        entityManager.getTransaction().begin();
        //--- musimy przesłać dwa obiekty!!
        entityManager.persist(employee);
        entityManager.persist(address);
        entityManager.getTransaction().commit();

        entityManager.close();
        entityManagerFactory.close();
    }

}
