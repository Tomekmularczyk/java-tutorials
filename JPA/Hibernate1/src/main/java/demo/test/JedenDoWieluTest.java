package demo.test;

import demo.jeden_do_wielu.Animal;
import demo.jeden_do_wielu.Owner;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JedenDoWieluTest {

    private static EntityManagerFactory eMF;
    private static EntityManager entityManager;

    public static void main(String[] args) {
        eMF = Persistence.createEntityManagerFactory("MyDatabase");
        entityManager = eMF.createEntityManager();

        addSomeData();

        entityManager.close();
        eMF.close();
    }

    private static void addSomeData() {
        List<Animal> petsList = Arrays.asList(
                new Animal("Dog", "Azor"),
                new Animal("Snake", "Andzia"),
                new Animal("Bird", "Papug"));
        Owner owner = new Owner("Tomek", 24, petsList);

        //--- dodajemy wszystkich właścicieli i zwierzątka do bazy
        entityManager.getTransaction().begin();
        entityManager.persist(owner);
        Iterator<Animal> iterator = petsList.iterator();
        while (iterator.hasNext()) {
            entityManager.persist(iterator.next());
        }
        entityManager.getTransaction().commit();
    }

}
