package demo.jeden_do_wielu;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Owners")
public class Owner {
	@Id @GeneratedValue
	private long id;
	
	private String name;
	private int age;
	
	@OneToMany //czyli jeden właściciel może mieć kilka zwierzątek. 
			  //Jeżeli ma być to dwukierunkowa relacja to w nawiasie podajemy mappedBy="nazwaPolaZKlasyAnimal"(Wtedy też @Join Column ma być po stronie anotacji ManyToOne)
	@JoinColumn(name="owner_id") //dodajemy kolumne z i nazywamy ją. Bez tej anotacji powstała by 3 tabela z ID właściciela i zwierzątka
	private List<Animal> pets;
	
	public Owner(){}
	public Owner(String name, int age, List<Animal> pets) {
		this.name = name;
		this.age = age;
		this.pets = pets;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Animal> getPets() {
		return pets;
	}

	public void setPets(List<Animal> pets) {
		this.pets = pets;
	}

	@Override
	public String toString() {
		return "Owner [id=" + id + ", name=" + name + ", age=" + age + ", pets=" + pets + "]";
	}
}
