package demo.jeden_do_wielu;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Animals")
public class Animal {
	@Id @GeneratedValue
	private long id;
	
	private String species;
	private String name;
	
	//jeżeli chcielibyśmy stworzyć dwukierunkową relację:
/*	@ManyToOne
	@JoinColumn(name="ownerID")
	private Owner owner;
*/		
	public Animal(){}
	public Animal(String species, String name) {
		this.species = species;
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSpecies() {
		return species;
	}
	public void setSpecies(String species) {
		this.species = species;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Animal [id=" + id + ", species=" + species + ", name=" + name + "]";
	}
	
	
}
