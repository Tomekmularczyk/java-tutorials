package demo.klasa_osadzona;

import javax.persistence.Embeddable;
import javax.persistence.Table;

@Embeddable //to znaczy że może zostać osadzone w innej klasie
@Table(name = "Address2")
public class Address {
	private String zipCode;
	private String street;
	private int streetNr;
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getStreetNr() {
		return streetNr;
	}
	public void setStreetNr(int streetNr) {
		this.streetNr = streetNr;
	}
	
	
	
}
