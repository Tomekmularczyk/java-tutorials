<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<!-- statyczny include. Jeżeli tekst w pliku się zmieni cały plik jsp musiałby się zrekompilować. Uzywamy gdy kontent się nie zmienia. -->
<!-- plik jest dodawany przed skompilowaniem jsp -->
<%@ include file="ImportantText.txt" %>

<br/><br/>
<!-- dynamiczny import - używamy gdy kontent się ciągle zmienia -->
<!-- już po skompilowaniu jsp wykonany był osobny request do serwera tomcat-->
<jsp:include page="Data.txt"/>

</p></p>

<!-- jeżeli chcemy dodać plik z kodem javy to musimy użyć statycznego importu -->
<%@ include file="variableToInclude.jsp" %>

<strong>Dzisiaj jest <% out.print(data); %> a, </strong>
<% out.println(newClass); %>

</body>
</html>