<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<!-- mimo, że w przegladarce uruchomiony jest forward.jsp to odpalony zostanie index.jsp -->
<jsp:forward page="index.jsp"></jsp:forward>

<!-- drugi sposób -->
<%
request.getRequestDispatcher("index.jsp").forward(request, response);
%>

<!-- redirect sprawia, że przeglądarka robi reqest do innego jsp(zmienia się link) -->

<%
response.sendRedirect("index.jsp");
%>

</body>
</html>