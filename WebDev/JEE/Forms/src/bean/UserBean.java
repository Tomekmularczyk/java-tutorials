package bean;

public class UserBean {
	private String name = "";
	private String password = "";
	private String message = "";
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMessage(){
		return this.message;
	}
	
	
	public boolean validate(){
		//...do something
		if(name.length()>3 && password.length()>4){
			message = "Everything fine!";
			return true;
		}
		
		message = "Incorrect data(name > 3 and password > 4)!";
		return false;
	}
}
