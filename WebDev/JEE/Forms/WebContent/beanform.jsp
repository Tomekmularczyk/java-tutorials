<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<strong>Your name please:</strong> 
<br/>

<!-- metoda POST przesyła parametry tak, że nie widać ich w linku(URL) oraz nie ma limitu na liczbe parametrów tak jak w GET -->
<!-- name i password to nazwy parametrów jakie bedą miały przy POST method -->
<form action="/Forms/beanformhandler.jsp" method="post">
	<input type="text" name="name"/>
	<input type="text" name="password"/>
	<input type="submit" value="Submit via POST"/>
</form>

<!-- tworzymy ziarenko, które będzie przechowywać dane użytkownika dla tej sesji -->
<jsp:useBean id="user" class="bean.UserBean" scope="session"></jsp:useBean>

</body>
</html>