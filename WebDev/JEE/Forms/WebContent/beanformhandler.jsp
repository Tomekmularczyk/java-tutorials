<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<!-- zapisujemy dane z formularza w ziarenku i dzieki temu są dostępne dla innych plików jsp w tej sesji -->
<jsp:useBean id="user" class="bean.UserBean" scope="session"></jsp:useBean>
<!-- przesłane tutaj parametry muszą odpowiadać setterom z UserBean dlatego gwiazdka -->
<jsp:setProperty property="*" name="user"/>

User name: <%=user.getName() %> <br/>
Password: <%=user.getPassword() %> 

</body>
</html>