<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<strong>Your name please:</strong> 
<br/>

<!-- metoda GET używa URL żeby przesłać parametry(widać to w linku) co oczywiście może nie być bezpieczne -->
<form action="/Forms/Controller" method="get">
	<input type="text" name="name"/>
	<input type="submit" value="Submit via GET" />
</form>

<br/>

<!-- metoda POST przesyła parametry tak, że nie widać ich w linku(URL) oraz nie ma limitu na liczbe parametrów tak jak w GET -->
<form action="/Forms/Controller" method="post">
	<input type="text" name="name"/>
	<input type="submit" value="Submit via POST"/>
</form>

</body>
</html>