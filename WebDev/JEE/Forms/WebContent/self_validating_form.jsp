<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<!-- tworzymy ziarenko i wczytujemy do niego dane z requestu(jeżeli są jakieś) -->
<jsp:useBean id="loginData" class="bean.UserBean" scope="session"></jsp:useBean>
<jsp:setProperty property="*" name="loginData"/>

<!-- jeżeli dane są w porządku idź do innej strony -->
<%
	//sprawdzamy czy użytkownik wysłał dane czy pierwszy raz otworzył tę stronę
	String action = request.getParameter("action");
	boolean formCompleted = (action != null)? true: false;
	
	//nie chcemy sprawdzać danych w ziarenku jeżeli ta strona została uruchomiona poraz pierwszy
	if(formCompleted && loginData.validate())
		request.getRequestDispatcher("/Controller").forward(request, response);
%>

<!-- nie chcemy by wyświetlało za pierwszym razem dlatego w formularzu wysyłamy "ukrytą" informacje, że przesłany został poraz formularz -->
<h2><jsp:getProperty property="message" name="loginData"/></h2>

<!-- dane były nie odpowiednie. -->
<!-- formularz do którego wpisywana będzie wartość poprzednia ziarenka -->
<form action="/Forms/self_validating_form.jsp" method="post">
	<input type="text" name="name" value='<jsp:getProperty property="name" name="loginData"/>'/>
	<input type="text" name="password" value='<jsp:getProperty property="password" name="loginData"/>'/>
	<input type="hidden" name="action" value="formsubmit" /> <!-- ukryta wartość mówiąca, że formularz został już wysłany -->
	<input type="submit" name="Wyślij"/>
</form>

</body>
</html>