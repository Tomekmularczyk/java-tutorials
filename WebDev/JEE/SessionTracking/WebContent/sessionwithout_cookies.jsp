<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<!-- pobieramy obiekt który dodaliśmy do sesji w servlecie. To jednak nie będzie działać jeżeli użytkownik ma wyłączone coockies! -->
<%@ page import="demo.*, servlets.*" %>
<% Player player = (Player) session.getAttribute("player"); %>

<h2>
<% 
if(player != null){
	out.print("Name: " + player.getName());
%>
</h2>
<br/>
<% 
	out.print("Health: " + player.getHealth());
}else{
%>
<h1>
	<% out.print("No player in this session."); %>
</h1>
<% 
}
%>

</body>
</html>