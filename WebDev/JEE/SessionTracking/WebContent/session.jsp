<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

Maxymalny czas niekatywności w sesji: <%=session.getMaxInactiveInterval() %>


<!-- pobieramy obiekt który dodaliśmy do sesji w servlecie. -->
<%@ page import="demo.*, servlets.*" %>
<% Player player = (Player) session.getAttribute("player"); %>

<h2>
<% 
if(player != null){
	out.print("Name: " + player.getName());
%>
</h2>
<% 
	out.print("Health: " + player.getHealth());
}else{
%>
<h1>
	<% out.print("No player in this session."); %>
</h1>
<% 
}
%>
<br/><br/>

<!-- jeżeli użytkownik ma wyłączone cookies(np w private mode) to żeby sesja nie wygasła przy przechodzeniu przez link - musimy użyć encodeUrl(...) -->
<a href="<%=response.encodeURL(request.getContextPath())+ "/sessionwithout_cookies.jsp"%>">Got to page that will supports session with cookies disabled</a>
</body>
</html>