package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Cookie
 */
@WebServlet("/Cookie")
public class CookiesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookiesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Ciastka są nam przydatne jeżeli chcemy przechowywac informacje o użytkowniku
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		//request ponieważ z każdym requestem przzesyłane są przez przeglądarke dostępne cookiesy
		Cookie[] userCookies = request.getCookies(); 
		
		if(userCookies == null){
			out.println("No cookies found!");
		}else{
			for(Cookie retrievedCookie: userCookies){
				String name = retrievedCookie.getName();
				String value = retrievedCookie.getValue();
				
				out.println("Cookie name: " + name + ", cookie value: " + value);
			}
		}
		
		Cookie cookie = new Cookie("user","Tomek");
		//normalnie czas życia cookie jest ustalony do czasu zamknięcia przeglądarki(ujemna wartość)
		cookie.setMaxAge(60 * 60 * 24 * 365 * 10); //--- ustalamy czas życia na 10 lat
		response.addCookie(cookie);
		
		out.println("Cookie set!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
