<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<!-- tworzymy ziarenko które będzie dostepne w innych jsp -->
<!-- session scope znaczy, że jest dostępne ziarenko dla całej sesji(10min?) w przeglądarce(coockies) -->
<!-- session page znaczy, że tylko na tej stronie ziarenko może być używane -->
<!-- request - dla pojedyńczego żądania -->
<!-- application scope jest niezależne od coockies i istnieje dopóki aplikacja działa na serwerze -->
<jsp:useBean id="bean" class="beans.JavaBean" scope="session"></jsp:useBean>
<jsp:setProperty name="bean" property="name" value="TOmek"/>
<jsp:setProperty property="age" value="11" name="bean"/>

</body>
</html>