<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<!-- importujemy ziarenko stworzone w beans.jsp -->
<!-- jedna wazna rzecz - jeżeli wcześniej nie był uruchomiony servlet beans.jsp, to ziarenko bean nie zostało jeszcze utworzone i jego wartości tutaj będą mieć NULL -->
<!-- potem te beansy zapisują się w coockies(w przypadku 'session' i nawet po ponownym uruchomieniu przeglądarki będą mieć swoje wartości(chyba że to było konto incognito) -->
<jsp:useBean id="bean" class="beans.JavaBean" scope="session"></jsp:useBean>

Name = <%= bean.getName() %> <br/>
Age = <%= bean.getAge() %> <br/>

<!-- http://localhost:8080/MVC/getbeans.jsp?name=pumcym&age=4 -->
<jsp:setProperty property="*" name="bean"/>

Name = <%= bean.getName() %> <br/>
Age = <%= bean.getAge() %> <br/>
</body>
</html>