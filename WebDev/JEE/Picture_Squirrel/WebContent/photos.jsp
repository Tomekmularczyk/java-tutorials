<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%-- dodajemy nasz tag --%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="myTag" %>

<c:import url="header.jsp">
	<c:param name="title" value="Photos"></c:param>
</c:import>

<!--   
<table>
	<c:set scope="page" var="tableWidth" value="8" />

	<c:forEach var="i" begin="800" end="860" step="1" varStatus="var">

		<c:if test='${i % tableWidth == 0}'>
			<tr>
		</c:if>

		<%--ustawiamy sobie zmienną --%>
		<c:set scope="page" var="imgname" value='${i}.jpg'></c:set>
		<td>
			<a href='<c:url value='/Controller?action=image&image=${imgname}'/>'> <%-- w ten sposób robimy link który działa nawet przy wyłączonych cookies --%>
				<img width="80" height="80" src='${pageContext.request.contextPath}/images/800-99/${imgname}'/>
			</a>
		</td>
		<c:if test='${(i + 1) % tableWidth == 0}'>
			</tr>
		</c:if>

	</c:forEach>
</table>
-->

<%-- stworzony przez nas tag, który wydrukowuje zdjęcia --%>
<myTag:library_images koniec="860" start="800"/>

<c:import url="footer.jsp"></c:import>