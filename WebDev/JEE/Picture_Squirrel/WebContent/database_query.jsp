<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:import url="header.jsp"> <c:param name="title" value="Database query"></c:param> </c:import>

<h3>Użytkownicy naszego serwisu: <br/></h3>

<sql:setDataSource var="ds" dataSource="jdbc/TEST" />
<sql:query dataSource='${ds}' sql="select * from users" var="result"/>


<c:forEach var="user" items='${result.rows}'>
	<p>${user.email} -  
	<c:forEach var="i" begin="0" end='${fn:length(user.password)}' step="1">   *	</c:forEach>
	</p>
</c:forEach>

<c:import url="footer.jsp"></c:import>