<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
#header {
	position: relative;
	min-height: 150px;
}

#header-content {
	position: fixed;
	bottom: 0;
	text-align: center;
	width: 100%;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${param.title}</title>

</head>
<body>
	<div>
		<img align="right"
			src='${pageContext.request.contextPath}/images/logo.png'
			height="100px" />
	</div>
	<h1 align="center">Picture Squirrel</h1>