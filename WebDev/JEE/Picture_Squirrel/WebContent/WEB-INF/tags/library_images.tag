<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="ileWRzedzie" required="false" type="java.lang.Integer" description="ile zdjęć w rzędzie" %> 
<%@ attribute name="start" required="true" type="java.lang.Integer" description="od którego zdjęcia zacząć" %> 
<%@ attribute name="koniec" required="true" type="java.lang.Integer" description="na którym zdjęciu skończyć" %> 

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:if test='${empty ileWRzedzie}'>
	<c:set var="ileWRzedzie" value="8"/>
</c:if>


<table>
	<c:forEach var="i" begin='${start}' end="${koniec}" step="1" varStatus="var">

		<c:if test='${i % ileWRzedzie == 0}'>
			<tr>
		</c:if>

		<%--ustawiamy sobie zmienną --%>
		<c:set scope="page" var="imgname" value='${i}.jpg'></c:set>
		<td>
			<a href='<c:url value='/Controller?action=image&image=${imgname}'/>'> <%-- w ten sposób robimy link który działa nawet przy wyłączonych cookies --%>
				<img width="80" height="80" src='${pageContext.request.contextPath}/images/800-99/${imgname}'/>
			</a>
		</td>
		<c:if test='${(i + 1) % ileWRzedzie == 0}'>
			</tr>
		</c:if>

	</c:forEach>

</table>