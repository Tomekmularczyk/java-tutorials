<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- importujemy dwa pliki(header i footer), które mogą być wielokrotnie używane w innych stronach. 
	 Ta technika to coś jak fragmenty w Androidzie --%>

<c:import url="header.jsp">
<%-- importujemy plik jsp ale przesyłamy mu paramater, który może wykorzystać. Tutaj wykorzystamy to do zmiany nagłówka --%>
<c:param name="title" value="Main Site"></c:param>
</c:import>

<h2 align="center"> This is home page! </h2>
<br/><br/><br/>

<table border="1px" width="400px">
  <tr>
    <th align="left">Content: </th>
  </tr>
  <tr>
    <td><a href='${ pageContext.request.contextPath }/database_query.jsp'>Users in database</a></td>
  </tr>
  <tr>
    <td><a href='${ pageContext.request.contextPath }/photos.jsp'>Show photos</a></td>
  </tr>
</table>



<c:import url="footer.jsp"></c:import>