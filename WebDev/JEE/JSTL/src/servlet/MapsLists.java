package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import demo.UserBean;

/**
 * Servlet implementation class MapsLists
 */
@WebServlet("/MapsLists")
public class MapsLists extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MapsLists() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//---map
		Map<String,String> map = new HashMap<>();
		map.put("owoc", "<a href='http:\\link'> jablko </a>");
		request.setAttribute("map", map);
		
		//---list
		List<UserBean> list = new LinkedList<>();
		list.add(new UserBean("Kazek", "***"));
		list.add(new UserBean("Gosia","*****"));
		list.add(new UserBean("Janek","*****"));
		request.getSession().setAttribute("mylist", list);
		
		request.getRequestDispatcher("/maps.lists.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
