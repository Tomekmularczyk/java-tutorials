package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import demo.UserBean;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class RequestSessionContext extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestSessionContext() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserBean user1 = new UserBean("Tomek", "zaq12wsx");
		UserBean user2 = new UserBean("Adrian", "qwerty");
		UserBean user3 = new UserBean("Kasia", "123456789");
		
		//request, session, context
		request.setAttribute("user1", user1);
		request.getSession().setAttribute("user2", user2);
		getServletContext().setAttribute("user3", user3); //context jest globalny dla całej aplikacji od uruchomienia serwera
		
		request.getRequestDispatcher("/request.session.context_attributes.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
