<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:out value='Mapa: ${map["owoc"]}'></c:out> <br/>
	Outputing directly without 'c:out':  ${map["owoc"]}
	
	<br/>
	
	<c:forEach var='user' items='${mylist}'>
		${user.name} : ${user.password} <br/>
	</c:forEach>
	
	<!-- trochę stylowania HTML do tej samej pętli co wyżej... -->
	<br/>
	<table style="border: 2px solid gray;">
		<c:forEach var="element" items='${mylist}'>
			<tr> <td>${element.name}</td> <td>${element.password}</td> </tr>
		</c:forEach>
	</table>
</body>
</html>