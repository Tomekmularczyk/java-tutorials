<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%-- odpowiednik if-else --%>
	<c:choose>
		<c:when test='${ param.id == 1 }'>
			<b>Id równa się 1</b>
		</c:when>
		<c:when test='${ param.id == 2 }'>
			<b>Id równa się 2</b>
		</c:when>
		<c:otherwise>
			Id nie jest równe ani 1 ani 2
		</c:otherwise>
	</c:choose>
	
	<p></p>
	
	<%-- pętle --%>
	<c:forEach var="i" begin="0" end="10" step="2">
		i = <c:out value='${ i }'/> <br/>
	</c:forEach>
	
	<%-- pętla z varStatus --%>
	<c:forEach var="i" begin="0" end="5" varStatus="status">
		<c:if test='${status.first}'>
			Starting the loop(<c:out value='i = ${i}'/>)
			<br/>
		</c:if>

		<c:if test='${status.last}'>
			Finishing the loop(<c:out value='i = ${i}'/>)
		</c:if>
	</c:forEach>
</body>
</html>