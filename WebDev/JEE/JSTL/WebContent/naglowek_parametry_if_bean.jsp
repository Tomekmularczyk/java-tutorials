<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!-- O ile nie jest to konieczne nie powinno używać się w plikach jsp skrypletów javy - <%%> itp. 
	 Jest to przestarzała nie efektywna technologia, zamiast tego używa się JSTL(Java Standard Tag Library),
	 oraz EL(Expression Language). Znaczniki JSTL dzielą się na:
	 - core tags 
	 - formating tags
	 - xml tags
	 - sql tags
	 - functions tags
-->

<%-- komentarzy tworzonych za pomocą skrypletu nie widać w wyjściowym pliku html --%>


<%-- do JSTL potrzebujemy takiego nagłówka. Nie ma znaczenia czy adres uri faktycznie istnieje. --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tytuł</title>
</head>
<body>

	<jsp:useBean id="bean1" class="demo.TestBean" scope="page"></jsp:useBean>

	<%-- uruchamiamy accessor, bez podawania przedrostka "get" i od małej litery --%>
	<c:out value="${ bean1.hello}">
	</c:out>

	<br />

	<%-- tak pobieramy parametry za pomocą JSTL i EL --%>
	Wartość parametru 'msg':
	<c:out value="${ param.msg }"></c:out>

	<%-- instrukcje warunkowe (http://localhost:8080/JSTL/jstl.jsp?msg=hej%20tam&name=Tomek) --%>
	<p>
		<c:if test='${ param.name == "Tomek" }'>
			Cześć Tomek!
		</c:if>

		<c:if test='${ param.name != "Tomek" }'>
			Cześć nieznajomy!
		</c:if>
	</p>
</body>
</html>