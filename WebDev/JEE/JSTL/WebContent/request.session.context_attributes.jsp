<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<%-- inaczej niż w przypadku skrypletów, nie trzeba się odwoływać do obiektów session, context czy request by "wyciągnąć"
		 z nich atrybuty. Język EL wyszykuje te atrybuty automatycznie --%>
	<c:out value="${user1.name}"/><br/> <!-- request -->
	<c:out value="${user2.name}"/><br/> <!-- session -->
	<c:out value="${user3.name}"/><br/> <!-- context -->
	<br/>
	<%-- jednak można też być precyzyjnym za pomocą "implicit objects" --%>
	<c:out value="${requestScope.user1.name}"/><br/> <!-- request -->
	<c:out value="${sessionScope.user2.name}"/><br/> <!-- session -->
	<c:out value="${applicationScope.user3.name}"/><br/> <!-- context -->
	<br/>
	

</body>
</html>