package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Account {
	private Connection conn;
	private String message;
	
	public Account(Connection connection) {
		this.conn = connection;
	}
	
	public boolean login(String email, String password) throws SQLException{
		String query = "SELECT COUNT(*) as count FROM users WHERE email=? and password=?;";
		try(PreparedStatement statement = conn.prepareStatement(query)){
			statement.setString(1, email);
			statement.setString(2, password);
			
			ResultSet result = statement.executeQuery();
			
			int count = 0;
			if(result.next()){
				count = result.getInt("count");
			}
			
			if(count == 0)
				return false;
			else
				return true;
		}
	}
	
	public void createUser(String email, String password) throws SQLException{
		String query = "INSERT INTO users VALUES(null, ?, ?);";
		
		try(PreparedStatement statement = conn.prepareStatement(query)){
			statement.setString(1, email);
			statement.setString(2, password);
			
			statement.executeUpdate();
		}
	}
	public boolean exists(String email) throws SQLException{
		String query = "SELECT * FROM users WHERE email=?;";
		try(PreparedStatement statement = conn.prepareStatement(query)){
			statement.setString(1, email);
			
			ResultSet result = statement.executeQuery();
			
			if(result.next()){
				return true;
			}else{
				return false;
			}
		}
	}
	
	public boolean validate(String email, String password){
		if(email == null || password == null || email.isEmpty() || password.isEmpty()){
			message = "Email or password not set.";
			return false;
		}
		if(! email.matches("\\w+@\\w+\\.\\w+")){
			message = "Wrong email.";
			return false;
		}
		if(password.length() < 4){
			message = "Password must be min 4 characters long.";
			return false;
		}
		
		return true;
	}
	
	public String getMessage(){
		return message;
	}
}
