package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import beans.Account;

/**
 * Żeby połączyć się z MYSQL i utworzyć działający obiekt DataSource trzeba:
 * 1. Dodać do projektu JDBC. Do ściągniecia ze strony mysql.
 * 2. Dodać odpowiedni kod do pliku web.xml.
 * 3. Dodać odpowiedni kog do context.xml, który znajduje się w plikach konfiguracyjnych serwera.
 * Więcej na stronie tomcat.apache po wpisaniu JNDI JDBC  
 * example: <Resource auth="Container" driverClassName="com.mysql.jdbc.Driver" maxActive="100" maxIdle="30" maxWait="10000" name="jdbc/TEST" password="but" type="javax.sql.DataSource" url="jdbc:mysql://localhost:3306/TEST" username="root"/>
 */
@WebServlet("/DataSourceDemo")
public class DataSourceDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource dataSource;
	
    public DataSourceDemo() {
        super();

    }

	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext initialContext = new InitialContext();
			
			Context context = (Context) initialContext.lookup("java:comp/env");
			dataSource = (DataSource) context.lookup("jdbc/TEST");
		} catch (NamingException e) {
			throw new ServletException("Context error " +e.getMessage());
		//	throw new ServletException(e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		request.setAttribute("email", "");
		request.setAttribute("password", "");
		request.setAttribute("message", "");
		
		if(action == null){
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}else if(action.equals("dologin")){
			request.getRequestDispatcher("/login_page.jsp").forward(request, response);
		}else if(action.equals("createaccount")){
			request.setAttribute("repeatpassword", "");
			request.getRequestDispatcher("/createaccount.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		if(action == null){
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}else if(action.equals("dologin")){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
		
			try(Connection connection = dataSource.getConnection()){		
				Account account = new Account(connection);
				
				request.setAttribute("email", email);
				request.setAttribute("password", "");
				if(account.login(email, password)){
					request.getRequestDispatcher("/loginsuccess.jsp").forward(request, response);
				}else{
					request.setAttribute("message", "Email or password unrecognised.");
					request.getRequestDispatcher("/login_page.jsp").forward(request, response);
				}
			} catch (SQLException e) {
				throw new ServletException(e);
			}
		}else if(action.equals("createaccount")){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String repeatPassword = request.getParameter("repeatpassword");
			
			try(Connection connection = dataSource.getConnection()){
				request.setAttribute("email", email);
				request.setAttribute("password", "");
				request.setAttribute("repeatpassword", "");
				//request.setAttribute("message", "");
				
				Account account = new Account(connection);
				if(!account.validate(email, password)){
					request.setAttribute("message", account.getMessage());
					request.getRequestDispatcher("/createaccount.jsp").forward(request, response);
				}else{
					if(!password.equals(repeatPassword)){
						request.setAttribute("message", "Repeat your password.");
						request.getRequestDispatcher("/createaccount.jsp").forward(request, response);
					}else{
						if(account.exists(email)){
							request.setAttribute("message", "This email has been already registrated.");
							request.getRequestDispatcher("/createaccount.jsp").forward(request, response);
						}else{
							account.createUser(email, repeatPassword);
							request.getRequestDispatcher("/createaccountsuccess.jsp").forward(request, response);
						}
					}
				}
			}catch(SQLException ex){
				throw new ServletException(ex);
			}
			
		}
	}

}
