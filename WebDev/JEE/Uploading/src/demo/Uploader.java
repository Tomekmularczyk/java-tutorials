package demo;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;


/**
 * Servlet implementation class Uploader
 */
@WebServlet("/Uploader")
public class Uploader extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Uploader() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		if(!ServletFileUpload.isMultipartContent(request)){
			out.println("Nothing uploaded");
			return;
		}
		
		FileItemFactory itemFactory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(itemFactory);
		
		try {
			Map<String, List<FileItem>> fileItems = upload.parseParameterMap(request);
			
			
			for(Map.Entry<String, List<FileItem>> entry : fileItems.entrySet()){
				for(FileItem item : entry.getValue()){
					String fileName = item.getName();
					String contentType = item.getContentType();
					
					if(!contentType.equals("image/png") && !contentType.equals("image/jpg") &&!contentType.equals("image/jpeg")){
						out.print("Only .png and .jpg format supported");
						continue;
					}
					
					out.println("File name: " + fileName + ", file type: " + contentType);
					
					File uploadFile = File.createTempFile("img", ".png", new File("/Users/tomek/Desktop/"));
					
					item.write(uploadFile);
					
					out.println("File saved!");
				}
			}
		} catch (Exception e) {
			out.println("upload failed");
			return;
		}
	}

}
