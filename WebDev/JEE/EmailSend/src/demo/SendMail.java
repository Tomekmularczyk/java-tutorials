package demo;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {
	private static String USER_NAME = "logiafilo";
	private static String PASSWORD = "logiafilo555";
	
	public static void sendMail(String[] recipients, String subject, String body){
		final String from = USER_NAME;
		
		Properties props = new Properties();
		String host = "smtp.gmail.com";
		props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", PASSWORD);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
		
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        
        try {
			message.setFrom(new InternetAddress(from));
			InternetAddress[] toAddress = new InternetAddress[recipients.length];
			
			// To get the array of addresses
			for( int i = 0; i < recipients.length; i++ ) {
                toAddress[i] = new InternetAddress(recipients[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            
            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, PASSWORD);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		String[] recipients = { "tomaszmularczyk89@gmail.com" };
		SendMail.sendMail(recipients, "WAŻNA WIADOMOŚĆ", "WITAM \n BANG BANG!!! \n pozdrawiam.");
	}
}
