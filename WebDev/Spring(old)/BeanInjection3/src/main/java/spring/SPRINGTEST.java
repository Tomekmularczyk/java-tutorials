package spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import spring.beans.DtbsSource;
import spring.beans.JVMBean;

public class SPRINGTEST {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans_config.xml"); //w tym miejscu beany które są singletonami zostaną już otworzone
		
		ApplicationContext ctx2 = new ClassPathXmlApplicationContext("property2.xml");
		JVMBean jvmBean = ctx2.getBean("jvm_bean", JVMBean.class);
		System.out.println("jvmBean: " + jvmBean.getProperty()); //teraz property powinno bc ustawione z argumentu podanego do wirtualnej maszyny
		
		//Bean z wartosciami wstrzykniętymi z pliku(przy pomocy placeholdera)
		DtbsSource dtbs = ctx2.getBean("myDTBS",DtbsSource.class);
		DtbsSource bigDTBS = ctx2.getBean("bigDTBS", DtbsSource.class);
		System.out.println(dtbs + "\n" + bigDTBS);

	}

}
 