package spring.beans;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


//ten interfejs zostanie wywołany zaraz po utworzeniu beana w xml, ale jeszcze przed metodą init
//-- nie jest zalecane implementowanie interfejsów AWARE, ponieważ powoduje to sprzężenie/zależność obiektu od frameworku Springa 
public class AwareBean implements BeanNameAware, ApplicationContextAware{
	private String name;
	private ApplicationContext ctx;
	
	//--metoda z interfejsu
	public void setBeanName(String arg0) {
		this.name=arg0;
	}
	
	
	public void init(){
		Map<String, AwareBean> beansMap = ctx.getBeansOfType(AwareBean.class);
		System.out.println("Nazywam się " + name + " i jest nas: " + beansMap.size());
	}


	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		this.ctx=arg0;
	}
}
