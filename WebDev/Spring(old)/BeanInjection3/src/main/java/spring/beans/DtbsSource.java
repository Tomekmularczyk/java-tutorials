package spring.beans;

public class DtbsSource {
	private String host, port, user, password;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "DtbsSource [host=" + host + ", port=" + port + ", user=" + user + ", password=" + password + "]";
	} 
	
	
	
}
