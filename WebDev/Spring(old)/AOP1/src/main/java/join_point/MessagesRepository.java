package join_point;

import java.util.LinkedList;
import java.util.List;

public class MessagesRepository {
	private List<String> messages = new LinkedList<>();
	
	public void addMessage(String message){
		messages.add(message);
	}
	
	public void addEmptyMessage(){
		messages.add("---");
	}
	
	@Override
	public String toString() {
		return "MessagesRepository [messages=" + messages + "]";
	}
}
