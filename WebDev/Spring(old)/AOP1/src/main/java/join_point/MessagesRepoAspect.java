package join_point;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;

public class MessagesRepoAspect {
	
	//Klasa JoinPoint zawiera informacje na temat metody i obiektu na rzecz którego działała. Wystarczy że podamy ją jako argument
	public void methodExecuted(JoinPoint joinPoint){
		StringBuilder info = new StringBuilder();
		
		info.append("Method executed:");
		info.append("\n	" + joinPoint.getKind());
		info.append("\n	" + joinPoint.getSignature());
		info.append("\n	" + joinPoint.getSignature().toShortString());
		info.append("\n	" + Arrays.toString(joinPoint.getArgs()));
		info.append("\n	" + joinPoint.getTarget().toString()); //oryginalny obiekt
		info.append("\n	" + joinPoint.getThis().toString()); //obiekt proxy
		
		System.out.println(info.toString());
	}
}
