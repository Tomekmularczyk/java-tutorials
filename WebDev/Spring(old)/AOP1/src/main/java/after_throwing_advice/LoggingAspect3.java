package after_throwing_advice;

import org.apache.log4j.Logger;

public class LoggingAspect3 {
	private Logger logger = Logger.getLogger(getClass().getSimpleName());
	
	public void serveException(Exception exception){
		logger.error("serveException wychwyciło exception: " + exception.getMessage());
	}
	
	
	
	public void afterMethodExecuted(){
		logger.info("after method exexcuted");
	}
}
