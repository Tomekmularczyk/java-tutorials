package after_throwing_advice;

public class CustomerRepositoryImpl3 implements CustomerRepository3{

	public void addCustomer(Customer3 customer) {
		if(customer.getName() == null)
			throw new NullPointerException("customer name nie może być NULL!");
		else
			System.out.println("Customer zostal dodany");
	}
	

}
