package before_advice;

public interface CustomerRepository {
	String getCustomerName(long id);
	Customer getCustomer(long id);
}
