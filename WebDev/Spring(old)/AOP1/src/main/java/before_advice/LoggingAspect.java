package before_advice;

import org.apache.log4j.Logger;

public class LoggingAspect {
	
	public void logExecution(){
		Logger logger = Logger.getLogger(getClass().getSimpleName());
		logger.info("method execution ");
	}
}
