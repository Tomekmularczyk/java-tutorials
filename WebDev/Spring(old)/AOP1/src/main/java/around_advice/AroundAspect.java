package around_advice;

import org.aspectj.lang.ProceedingJoinPoint;

public class AroundAspect {
	
	//ta metoda będzie zarejestrowana w xml jako Around Aspect co pozwala na przechwycenie wykonywanej metody i jej argumentów(dzięki ProceedingJoinPoint)
	//jeżeli nie wywołamy jej ręcznie to nie zostanie ona wogóle wykonana!
	public Object messureExecutionTime(ProceedingJoinPoint proceedingJoinPoint){
		long startTime = System.nanoTime();
		Object returnedValue = null;
		String method = proceedingJoinPoint.getSignature().getName();
		
		//==== sprawdzimy sobie czy nie została wywołana metoda addMessage(String messsage) z podanym do argumentu NULLem
		
		try{
			if(method.equals("addMessage")){
				String parameter = (String) proceedingJoinPoint.getArgs()[0];
				
				if(parameter == null){
					Object[] args = new String[]{"don't put null here!"};
					returnedValue = proceedingJoinPoint.proceed(args);
				}else
					returnedValue = proceedingJoinPoint.proceed(); 
			}else //wychodzi na to, że została wywołana metoda addEmptyMessage()
				returnedValue = proceedingJoinPoint.proceed();//oryginalna metoda na rzecz której został wywołany ten advice
		}catch(Throwable throwable){
			System.out.println("Został rzucony wyjątek: " + throwable.getMessage());
		}
		
		long endTime = System.nanoTime();
		
		System.out.printf("Czas wykonania metody trwał: %d nano sekundy.\n", endTime-startTime);
		
		return returnedValue;
	}
	
}
