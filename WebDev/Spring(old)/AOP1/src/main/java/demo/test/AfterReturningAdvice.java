package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import after_returning_advice.CustomerRepository2;



public class AfterReturningAdvice {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("after_returning_advice.xml");
		
		CustomerRepository2 customerRepo = ctx.getBean("customerRepo", CustomerRepository2.class);
		System.out.print(customerRepo.getCustomerName(2));
		System.out.print(customerRepo.getCustomer(2));
	}
}
