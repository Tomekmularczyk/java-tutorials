package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import args_designator.SomeBean;

public class ArgsDesignatorTest {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("args_designator.xml");
		
		SomeBean someBean = context.getBean("someBean", SomeBean.class);
		someBean.someMethod("abc");
		someBean.someMethod("def");
		someBean.someMethod("ghi");
		someBean.someMethod2("jkl");
		someBean.someMethod2("mno");
		someBean.someMethod2("prs");
	}

}
