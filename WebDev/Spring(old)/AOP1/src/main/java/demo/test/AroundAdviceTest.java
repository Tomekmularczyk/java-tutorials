package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import around_advice.MessagesRepository2;

public class AroundAdviceTest {

	public static void main(String[] args) throws InterruptedException {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("around_advice.xml");
		MessagesRepository2 messagesRepo = ctx.getBean("messagesRepo",MessagesRepository2.class);
		
		messagesRepo.addMessage("Chuj");
		System.out.println(messagesRepo);
		
		System.out.println("-------------------------------");
		messagesRepo.addEmptyMessage();
		System.out.println(messagesRepo);
		System.out.println("-------------------------------");
		
		messagesRepo.addMessage(null);
		System.out.print(messagesRepo);
	}

}
