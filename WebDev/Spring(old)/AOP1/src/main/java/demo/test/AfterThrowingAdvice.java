package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import after_throwing_advice.Customer3;
import after_throwing_advice.CustomerRepository3;



public class AfterThrowingAdvice {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("after_throwing_advice.xml");
		
		CustomerRepository3 customerRepo = ctx.getBean("customerRepo", CustomerRepository3.class);
		Customer3 customer = new Customer3(null, 0);
		
		//wyjątek wyrzucony przez addCustomer zostanie wykryty przeż aspekt i zostanie uruchomiony odpowiedni advice
		//natomiast nie możmy nic z tym wyjątkiem zrobić kiedy zostanie wyrzucony przez metodę, możemy go jedynie zauwazyć
		
		customerRepo.addCustomer(customer);
	}
}
 