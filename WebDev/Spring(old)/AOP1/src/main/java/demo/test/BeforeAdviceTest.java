package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import before_advice.CustomerRepository;

public class BeforeAdviceTest {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("before_advice.xml");
		
		CustomerRepository customerRepo = ctx.getBean("customerRepo", CustomerRepository.class);
		System.out.print(customerRepo.getCustomerName(2));
	}

}
 