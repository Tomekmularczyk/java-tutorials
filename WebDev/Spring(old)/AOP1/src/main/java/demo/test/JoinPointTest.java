package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import join_point.MessagesRepository;

public class JoinPointTest {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("join_point.xml");
		
		MessagesRepository repo = context.getBean("messagesRepo",MessagesRepository.class);
		repo.addMessage("test message!");
		repo.addEmptyMessage();
	}
}
