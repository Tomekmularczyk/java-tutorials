package after_returning_advice;

import org.apache.log4j.Logger;

public class LoggingAspect2 {
	private Logger logger = Logger.getLogger(getClass().getSimpleName());
	
	public void logExecution(){
		logger.info("method execution ");
	}
	
	//--- returnedValue to wartość zwrócona z metody po której ten advice bedzie wykonany
	public void warnIfNull(Object returnedValue){
		if(returnedValue == null)
			logger.warn("value is null!!"); 
	}
	
	
}
