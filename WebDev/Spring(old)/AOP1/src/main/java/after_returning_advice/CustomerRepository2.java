package after_returning_advice;

public interface CustomerRepository2 {
	String getCustomerName(long id);
	Customer2 getCustomer(long id);
}
