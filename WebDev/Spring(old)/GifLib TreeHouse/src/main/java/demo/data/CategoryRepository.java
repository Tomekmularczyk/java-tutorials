package demo.data;

import demo.model.Category;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CategoryRepository {
	private final List<Category> categoriesList = Arrays.asList(
			new Category(1, "Funny"),
			new Category(2, "Creepy"),
			new Category(3, "Technology")
			);
	
	public Category getCategoryByName(String name){
		for(Category category :categoriesList)
			if(category.getName().equals(name))
				return category;
		
		return null;
	}
	
	public Category getCategoryById(int id){
		for(Category category :categoriesList)
			if(category.getId() == id)
				return category;
		
		return null;	
	}
	
	public List<Category> getCategoriesList(){
		return categoriesList;
	}
}
