package demo.data;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import demo.model.Gif;

@Component //dzięki tej anotacji spring będzie wstanie wstrzyknąć ten obiekt przez autowire do kontrolera
public class GifRepository {

    private static final List<Gif> GIF_LIST = Arrays.asList(
            new Gif("android-explosion", LocalDate.of(2015, 2, 13), "Chris Ramacciotti", false, 3),
            new Gif("ben-and-mike", LocalDate.of(2015, 10, 30), "Ben Jakuben", true, 2),
            new Gif("book-dominos", LocalDate.of(2015, 9, 15), "Craig Dennis", false, 3),
            new Gif("compiler-bot", LocalDate.of(2015, 2, 13), "Ada Lovelace", true, 2),
            new Gif("cowboy-coder", LocalDate.of(2015, 2, 13), "Grace Hopper", false, 2),
            new Gif("infinite-andrew", LocalDate.of(2015, 8, 23), "Marissa Mayer", true, 1));

    public Gif findByName(String name) {
        for (Gif gif : GIF_LIST) {
            if (gif.getName().equals(name)) {
                return gif;
            }
        }

        return null;
    }

    public List<Gif> getGifsList() {
        return GIF_LIST;
    }

    public List<Gif> findById(int id) {
        List<Gif> list = new LinkedList<>();

        GIF_LIST.stream().filter((gif) -> (gif.getId() == id)).forEach((gif) -> {
            list.add(gif);
        });

        return list;
    }

    public List<Gif> returnFavorites() {
        final List<Gif> gifList = new LinkedList<>();

        GIF_LIST.forEach(gif -> {
            if (gif.isFavourite()) {
                gifList.add(gif);
            }
        });

        return gifList;
    }
}
