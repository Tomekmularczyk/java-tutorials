package demo.treehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@EnableAutoConfiguration
@ComponentScan("demo.controller , demo.data")
public class AppConfig {
	
	
	public static void main(String[] args){
		SpringApplication.run(AppConfig.class, args);
	}
}
