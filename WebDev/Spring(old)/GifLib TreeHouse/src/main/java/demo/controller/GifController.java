package demo.controller;

import demo.data.GifRepository;
import demo.model.Gif;
import demo.model.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GifController {
	@Autowired
	private GifRepository gifRepository;
    @Autowired
    private UserService userService;
	

	@RequestMapping("/") //context root of our application
	public String listGifs(ModelMap modelMap){
        if(!userService.authenticate()){
            modelMap.put("user", new User());
            return "login";
        }

		List<Gif> gifList = gifRepository.getGifsList();
		modelMap.put("gifsList", gifList);
		return "home";//thymeleaf bedzie wiedzial jaki widok ma uruchomić szukając w folderze src/main/resource/template
	}
	
	//ModelMap zostanie wysłany do htmla
	@RequestMapping("/gif/{name}") //to co po /gif/*** zostanie zapisane do zmiennej name
	public String gifDetails(@PathVariable String name, ModelMap modelMap){
		Gif gif = gifRepository.findByName(name);
		modelMap.put("gif", gif);
		return "gif-details";
	}
}
