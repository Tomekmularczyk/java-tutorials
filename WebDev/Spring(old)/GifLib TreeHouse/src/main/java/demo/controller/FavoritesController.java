package demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.data.GifRepository;

@Controller
public class FavoritesController {
	
	@Autowired
	private GifRepository gifRepo;
	
	@RequestMapping("/favorites")
	public String listFavourites(ModelMap modelMap){
		modelMap.put("gifs", gifRepo.returnFavorites());
		return "favorites";
	}
	
}
