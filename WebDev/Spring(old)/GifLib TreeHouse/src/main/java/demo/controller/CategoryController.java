package demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.data.CategoryRepository;
import demo.data.GifRepository;
import demo.model.Category;
import demo.model.Gif;

@Controller
public class CategoryController {
	@Autowired
	private CategoryRepository catRepo;
	
	@Autowired
	private GifRepository gifRepo;
	
	@RequestMapping("/categories")
	public String listCategories(ModelMap modelMap){
		modelMap.put("categories", catRepo.getCategoriesList());
		
		return "categories";
	}
	
	@RequestMapping("/category/{id}")
	public String category(ModelMap modelMap, @PathVariable int id){
		Category category = catRepo.getCategoryById(id);
		modelMap.put("category", category);
		
		List<Gif> gifList = gifRepo.findById(id);
		modelMap.put("gifList", gifList);
		
		return "category";
	}
}
