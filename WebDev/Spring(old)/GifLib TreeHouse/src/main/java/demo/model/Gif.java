package demo.model;

import java.time.LocalDate;

public class Gif {
	private String name;
	private LocalDate dateUploaded;
	private String userName;
	private boolean favourite;
	private int id;
	
	
	public Gif(String name, LocalDate dateUploaded, String userName, boolean favourite, int id) {
		this.name = name;
		this.dateUploaded = dateUploaded;
		this.userName = userName;
		this.favourite = favourite;
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateUploaded() {
		return dateUploaded;
	}
	public void setDateUploaded(LocalDate dateUploaded) {
		this.dateUploaded = dateUploaded;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public boolean isFavourite() {
		return favourite;
	}
	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
