/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.model;

/**
 *
 * @author tomek
 */
public class User {
   private String name;
   private String password;
   private String sessionIdentificator;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionIdentificator() {
        return sessionIdentificator;
    }

    public void setSessionIdentificator(String sessionIdentificator) {
        this.sessionIdentificator = sessionIdentificator;
    }
   
   
}
