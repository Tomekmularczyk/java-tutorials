package demo.przestrzenie_nazw;

public class SomeBean {
	private String stringValue;
	private int intValue;
	private User user;
	
	{
		System.out.println("Obiekt jest tworzony");
	}
	
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "SomeBean [stringValue=" + stringValue + ", intValue=" + intValue + ", user=" + user + "]";
	}
	
	
}
