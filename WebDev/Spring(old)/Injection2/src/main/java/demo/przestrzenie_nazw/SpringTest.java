package demo.przestrzenie_nazw;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("configuration.xml");
		
		SomeBean someBean = ctx.getBean("someBean", SomeBean.class);
		SomeBean someBean2 = ctx.getBean("someBean2", SomeBean.class);
		SomeBean someBean3 = ctx.getBean("someBean3", SomeBean.class);
		
		System.out.println("==============================================");
		System.out.println(someBean);
		System.out.println(someBean2);
		System.out.println(someBean3);
	}

}
