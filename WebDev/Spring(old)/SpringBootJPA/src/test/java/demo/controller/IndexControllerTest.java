package demo.controller;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class IndexControllerTest {
	private MockMvc mockMVC;
	private IndexController indexController;

	@Before
	public void setup() {
		indexController = new IndexController();
		// kod poniżej pozwala stworzyć instancje MVC opartą na indexControlerze
		// i skonfigurowanie tego z kontekstem MockMVC
		mockMVC = MockMvcBuilders.standaloneSetup(indexController).build();
	}

	@Test
	public void testIndex() throws Exception {
		// wykonujemy test
		mockMVC.perform(get("/")).
			andExpect(status().isOk()).
			andExpect(view().name("index"));
	}
}
