package demo.controller;

import demo.database.Product;
import demo.interfaces.ProductService;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import org.hamcrest.Matchers;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;



/*	Spring MVC Test pozwala na testowanie mappingu bez uruchamiania Spring Contextu i uruchamiania jakiegoś konteneru tomcata czy bazy danych, co sprawia że testowanie jest o wiele szybsze. 
 * 
 */

//Mock - atrapa, makieta
//@RunWith(MockitoJUnitRunner.class)
public class ProductsControllerTest {
	@Mock //mockito mock object (coś jak autowired), zostanie on wstrzykniety jako dependencja do kontrolera. Ponieważ nie jesteśmy w springu, to mockito musi zająć sie tworzeniem beanów
	private ProductService productService;
	
	@InjectMocks //Mockito montuje atrapę kontrolera i wstrzykuje do niego zależności (oznaczone @Mock). UWAGA: ponoć nie jest to dobra praktyka, a powinno się używać zwykłego konstruktora
	private ProductsController controller;
	
	private MockMvc mockMvc;
	
	@Before
	public void before(){
		MockitoAnnotations.initMocks(this); //inicjalizujemy te klase do testu. Mockito zaincjalizuje oznaczone atrapy i je wstrzyknie. Możemy też użyć @RunWith(MockitoJUnitRunner.class)
		
		//tworzymy instancje springa i konfigurujemy go z MockMVC
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	//==========================================
	@Test //testuje listAll z kontrolera
	public void testProductList() throws Exception{
		List<Product> productList = new LinkedList<>();
		productList.add(new Product());
		productList.add(new Product());
		
		//kiedy wywoływana jest metoda 'X' to zwróć Y
		Mockito.when(productService.findAll()).thenReturn(productList);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/product/product-list/")) //wykonaj taki request...
				.andExpect(MockMvcResultMatchers.status().isOk()) //sprawdz czy status http jest OK
				.andExpect(MockMvcResultMatchers.view().name("/product/product-list")) //sprawdz czy zwracany jest odpowiedni layout
				.andExpect(MockMvcResultMatchers.model().attribute("products", Matchers.hasSize(2))); // sprawdz czy mamy dwa obiekty przypisane do modelu(obiektu Model z metody)
	}
	
	@Test //tym razem mamy id podawane w requescie
	public void testViewProduct() throws Exception{
		long id = 1L; //tworzymy id dla tej metody
		
		Mockito.when(productService.findOne(id)).thenReturn(new Product()); //ta metoda ma zwrócić nowy obiekt product
		mockMvc.perform(MockMvcRequestBuilders.get("/product/1")) //symulujemy request pod dany adress..
			.andExpect(MockMvcResultMatchers.status().isOk()) //oczekujemy statusu http 200...
			.andExpect(MockMvcResultMatchers.view().name("/product/product")) //że dostaniemy ten właśnie widok...
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.instanceOf(Product.class)));//oraz że w obiekcie model tego widoku atrybut "product" bedzie instancji 'Product'
	}
	@Test //bardzo podobnie jak wyżej
	public void testEdit() throws Exception{
		long id = 1L;
		
		Mockito.when(productService.findOne(id)).thenReturn(new Product());
		mockMvc.perform(MockMvcRequestBuilders.get("/product/edit/1"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.view().name("/product/new-product-form"))
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.instanceOf(Product.class)));
	}
	
	@Test  //cos jest tutaj spierdolone
	
	public void testNewProduct() throws Exception{
		//upewniamy się że nie było żadnej interakcji z naszym servisem(ponieważ nie powinno być, z tego względu że do widoku wysyłamy nowo stworzony obiekt)
		Mockito.verifyZeroInteractions(productService);
		
		mockMvc.perform(MockMvcRequestBuilders.get("/product/new-product-form/"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.view().name("/product/new-product-form"))
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.instanceOf(Product.class)));
	}
	
	@Test
	public void testCreateNew() throws Exception{
		/*w tym teście musimy wziąć pod uwagę kilka spraw:
		 * - metoda jest zmapowana do otrzymywania danych w metodzie POST
		 * - dane z postu są bindowane przez springa do obiektu w metodzie
		 * - po wszystkim ma nastąpić redirect
		*/
		
		//najpierw tworzymy obiekt
		long id = 1L;
		String description = "Shoe";
		BigDecimal price = new BigDecimal("12.00");
		String imageURL = "example.org";
		
		Product returnProduct = new Product();
		returnProduct.setDescription(description);
		returnProduct.setId(id);
		returnProduct.setImageURL(imageURL);
		returnProduct.setPrice(price);
		
		//oczekujemy że po przesłaniu jakiegokolwiek obiektu Product otrzymamy z powrotem produkt który wcześniej przygotowaliśmy
		Mockito.when(productService.save(org.mockito.Matchers.<Product>any())).thenReturn(returnProduct);
		
		//mimikujemy request metodą POST...
		mockMvc.perform(MockMvcRequestBuilders.post("/product/create-new")
				.param("id", String.valueOf(id))
				.param("description", description)
				.param("price", price.toString())
				.param("imageURL", imageURL))
			.andExpect(MockMvcResultMatchers.status().is3xxRedirection())
			.andExpect(MockMvcResultMatchers.view().name("redirect:/product/1")) //oczekujemy redirectu
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.instanceOf(Product.class))) //sprawdzamy czy odpowiedni obiekt
			//...i sprawdzamy czy własciwości parametrów się zgadzają 
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.hasProperty("id", Matchers.is(id))))
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.hasProperty("description", Matchers.is(description))))
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.hasProperty("price", Matchers.is(price))))
			.andExpect(MockMvcResultMatchers.model().attribute("product", Matchers.hasProperty("imageURL", Matchers.is(imageURL))));
		
		//sprawdzamy czy spring dobrze zbindował wartości post do obiektu product
		ArgumentCaptor<Product> boundProduct = ArgumentCaptor.forClass(Product.class);
		Mockito.verify(productService).save(boundProduct.capture());
		
		assertEquals(id, boundProduct.getValue().getId());
		assertEquals(description, boundProduct.getValue().getDescription());
		assertEquals(price, boundProduct.getValue().getPrice());
		assertEquals(imageURL, boundProduct.getValue().getImageURL());
	}
	
	@Test
	public void testDelete() throws Exception{
		long id = 1L;
		mockMvc.perform(MockMvcRequestBuilders.get("/product/delete/1"))
			.andExpect(MockMvcResultMatchers.status().is3xxRedirection())
			.andExpect(MockMvcResultMatchers.view().name("redirect:/product/product-list"));
		
		//upewniamy się że productService był wywołany jeden raz i że metoda delete() była wywołana z daną wartością ID
		Mockito.verify(productService, Mockito.times(1)).delete(id);
	}
}
