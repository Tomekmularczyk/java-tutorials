package demo.controller;

import demo.database.Customer;
import demo.interfaces.CustomerService;
import java.util.LinkedList;
import java.util.List;
import org.hamcrest.Matchers;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/* Takie same testy jak w ProductsControllerTest tylko poprawiony, czystszy kod.
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

    private MockMvc mockMvc; //obiekt do wykonywania testów

    @Mock //atrapa @Autowired
    private CustomerService customerService;
    @InjectMocks
    private CustomersController customersController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(customersController).build();
    }

    //----------------------------------------------
    @Test
    public void testList() throws Exception {
        List<Customer> list = new LinkedList<>();
        list.add(new Customer());
        list.add(new Customer());

        Mockito.when(customerService.findAll()).thenReturn(list);
        mockMvc.perform(get("/customer/customer-list/")) //po wykonaniu tego requesta ma się stać to co poniżej:
                .andExpect(status().isOk()) //http status 200 - OK
                .andExpect(view().name("/customer/customer-list")) //uruchomiony odpowiedni view 
                .andExpect(model().attribute("customers", Matchers.hasSize(2))); //przesłane 2 obiekty

        //to samo co wcześniej tylko dla innego adresu
        mockMvc.perform(get("/customer/"))
                .andExpect(status().isOk())
                .andExpect(view().name("/customer/customer-list"))
                .andExpect(model().attribute("customers", Matchers.hasSize(2)));
    }

    @Test
    public void testCustomerView() throws Exception {
        long id = 1L;

        Mockito.when(customerService.findOne(id)).thenReturn(new Customer());
        mockMvc.perform(get("/customer/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/customer/customer"))
                .andExpect(model().attribute("customer", Matchers.instanceOf(Customer.class)));
    }

    @Test
    public void testNewCustomer() throws Exception {
        Mockito.verifyZeroInteractions(customerService);
        mockMvc.perform(get("/customer/new-customer"))
                .andExpect(status().isOk())
                .andExpect(view().name("/customer/customer-form"))
                .andExpect(model().attribute("customer", Matchers.instanceOf(Customer.class)));
    }

    @Test
    public void testPersistCustomer() throws Exception {
        long id = 1L;
        String firstName = "Józef";
        String lastName = "Nowak";
        String email = "nowakJ@o2.pl";
        long telephone = 679856885;
        String city = "Rzeszów";
        String state = "Podkarpackie";
        String zipcode = "35-061";
        String address = "Zamknięta 12";

        Customer customer = new Customer(firstName, lastName, email, telephone, city, state, zipcode, address);
        customer.setId(id);

        Mockito.when(customerService.save(org.mockito.Matchers.<Customer>any())).thenReturn(customer); //ustawaimy co ma zwrócić wywoływana w metodzie metoda saveOR...
        mockMvc.perform(MockMvcRequestBuilders.post("/customer/persist-new/")
                .param("id", String.valueOf(id))
                .param("firstName", firstName)
                .param("lastName", lastName)
                .param("email", email)
                .param("telephone", String.valueOf(telephone))
                .param("city", city)
                .param("state", state)
                .param("zipcode", zipcode)
                .param("address", address)
        )
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/customer/1"))
                .andExpect(model().attribute("customer", Matchers.instanceOf(Customer.class)))
                //sprawdzamy czy właściwości parametrów postu
                .andExpect(model().attribute("customer", Matchers.hasProperty("id", Matchers.is(id))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("firstName", Matchers.is(firstName))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("lastName", Matchers.is(lastName))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("email", Matchers.is(email))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("telephone", Matchers.is(telephone))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("city", Matchers.is(city))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("state", Matchers.is(state))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("zipcode", Matchers.is(zipcode))))
                .andExpect(model().attribute("customer", Matchers.hasProperty("address", Matchers.is(address))));

        //sprawdzamy czy wartości zostały odpowiednio zbindowane
        ArgumentCaptor<Customer> boundCustomer = ArgumentCaptor.forClass(Customer.class);
        Mockito.verify(customerService).save(boundCustomer.capture());

        assertEquals(id, boundCustomer.getValue().getId());
        assertEquals(firstName, boundCustomer.getValue().getFirstName());
        assertEquals(lastName, boundCustomer.getValue().getLastName());
        assertEquals(email, boundCustomer.getValue().getEmail());
        assertEquals(telephone, boundCustomer.getValue().getTelephone());
        assertEquals(city, boundCustomer.getValue().getCity());
        assertEquals(state, boundCustomer.getValue().getState());
        assertEquals(zipcode, boundCustomer.getValue().getZipcode());
        assertEquals(address, boundCustomer.getValue().getAddress());
    }

    @Test
    public void testEdit() throws Exception {
        long id = 1L;

        Mockito.when(customerService.findOne(id)).thenReturn(new Customer());
        mockMvc.perform(get("/customer/edit/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/customer/customer-form"))
                .andExpect(model().attribute("customer", Matchers.instanceOf(Customer.class)));
    }

    @Test
    public void testDelete() throws Exception {
        long id = 1L;

        mockMvc.perform(get("/customer/delete/1"))
                .andExpect(view().name("redirect:/customer/customer-list"))
                .andExpect(status().is3xxRedirection());

        //upewniamy się że productService był wywołany jeden raz i że metoda delete() była wywołana z daną wartością ID
        Mockito.verify(customerService, Mockito.times(1)).delete(id);
    }
}
