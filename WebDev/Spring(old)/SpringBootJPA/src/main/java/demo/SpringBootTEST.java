package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
public class SpringBootTEST {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SpringBootTEST.class, args);

//        ProductService pRepo = ctx.getBean("productService", ProductService.class);
//        pRepo.addSomeProducts();
//        CustomerService cRepo = ctx.getBean("customerService", CustomerService.class);
//        cRepo.addSomeCustomers();
    }
}
