package demo.database;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Customers")
public class Customer implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String firstName;
    private String lastName;
    private String city;
    private String state_;
    private String zipcode;
    private String address;
    private long telephone;
    private String email;

    @Version //Optimistic Locking
    //warto to robić bo gdybyś zapisywał obiekt, który został zmieniony od czasu gdy go odebrałeś z bazy danych to dostaniesz wyjątek,
    //przy systemach np bankowych jest to bardzo wazne.
    private Integer version;

    public Customer() {
    }

    public Customer(String firstName, String lastName, String email, long telephone, String city, String state,
            String zipcode, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.telephone = telephone;
        this.city = city;
        this.state_ = state;
        this.zipcode = zipcode;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTelephone() {
        return telephone;
    }

    public void setTelephone(long telephone) {
        this.telephone = telephone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state_;
    }

    public void setState(String state) {
        this.state_ = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
                + ", telephone=" + telephone + ", city=" + city + ", state=" + state_ + ", zipcode=" + zipcode
                + ", address=" + address + "]";
    }
}
