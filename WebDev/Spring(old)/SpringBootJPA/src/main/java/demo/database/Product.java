package demo.database;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Products")
public class Product implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String description;
    private BigDecimal price;
    private String imageURL;

    @Version //Optimistic Locking
    //warto to robić bo gdybyś zapisywał obiekt, który został zmieniony od czasu gdy go odebrałeś z bazy danych to dostaniesz wyjątek,
    //przy systemach np bankowych jest to bardzo wazne.
    private Integer version;

    public Product() {
    }

    public Product(String description, BigDecimal price, String imageURL) {
        this.description = description;
        this.price = price;
        this.imageURL = imageURL;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return "Product [id=" + id + ", description=" + description + ", price=" + price + ", imageURL=" + imageURL
                + "]";
    }

}
