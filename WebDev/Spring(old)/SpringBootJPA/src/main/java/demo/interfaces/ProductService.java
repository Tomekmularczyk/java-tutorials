package demo.interfaces;

import demo.database.Product;
import java.math.BigDecimal;
import javax.annotation.PostConstruct;
import org.springframework.data.repository.CrudRepository;


public interface ProductService extends CrudRepository<Product, Long> {

    @PostConstruct
    default void addSomeProducts() {
        Product[] products = {new Product("Bike", new BigDecimal(790), "bike"),
            new Product("Car", new BigDecimal(2800), "car"), new Product("Spoon", new BigDecimal(3), "spoon"),
            new Product("Pan", new BigDecimal(56), "pan"), new Product("Chair", new BigDecimal(190), "chair"),};

        for (Product product : products) {
            save(product);
        }
    }
}
