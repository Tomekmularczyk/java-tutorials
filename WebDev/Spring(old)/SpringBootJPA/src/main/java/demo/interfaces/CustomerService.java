package demo.interfaces;

import demo.database.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerService extends CrudRepository<Customer, Long> {

    
    default void addSomeCustomers() {
        Customer[] customers = {
            new Customer("Józef", "Nowak", "nowakJ@o2.pl", 679856885, "Rzeszów", "Podkarpackie", "35-061", "Zamknięta 12"),
            new Customer("Adrian", "Mularczyk", "adii333@wp.pl", 867569344, "Krosno", "Podkarpackie", "32-442", "Hynka 3/16"),
            new Customer("Janina", "Kuliga", "kulig_1989@onet.pl", 983711911, "Bydgoszcz", "Pomorskie", "99-100", "Piłsudskiego 88a"),
            new Customer("Tomasz", "Piłsudski", "myemailmy5@gmail.com", 723922499, "Gdańsk", "Pomorskie", "93-634", "3 Maja 10/13"),
            new Customer("Kazimierz", "Dejna", "sobieski22@weebly.com", 996435876, "Jarosław", "Podkarpackie", "25-122", "Korotyńskiego 11"),
            new Customer("Celina", "Dykiel", "celina.dykiel39@yahoo.org", 947845734, "Żywiec", "Śląskie", "54-333", "Polna 29")
        };
        
        for (Customer customer : customers) {
            save(customer);
        }
    }
}
