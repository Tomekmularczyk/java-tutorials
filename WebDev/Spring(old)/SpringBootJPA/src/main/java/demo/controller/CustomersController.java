package demo.controller;

import demo.database.Customer;
import demo.interfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/customer")
public class CustomersController {
	@Autowired
	private CustomerService repo;
	
	@RequestMapping({"/", "/customer-list"})
	public String list(Model model){
		model.addAttribute("customers", repo.findAll());
		return "/customer/customer-list";
	}
	
	@RequestMapping("{id}")
	public String customerView(@PathVariable long id, Model model){
		model.addAttribute(repo.findOne(id));
		return "/customer/customer";
	}
	
	@RequestMapping("/new-customer")
	public String newCustomer(Model model){
		model.addAttribute(new Customer());
		return "/customer/customer-form";
	}
	@RequestMapping(value="/persist-new", method=RequestMethod.POST)
	public String persistCustomer(Customer customer){
		repo.save(customer);
		return "redirect:/customer/" + customer.getId();
	}
	
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable long id, Model model){
		model.addAttribute(repo.findOne(id));
		return "/customer/customer-form";
	}
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable long id){
		repo.delete(id);
		return "redirect:/customer/customer-list";
	}
}
