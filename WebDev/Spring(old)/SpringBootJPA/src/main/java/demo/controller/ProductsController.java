package demo.controller;

import demo.database.Product;
import demo.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/* Jeden źle skonfigurowany RequestMapping bez cudzysłowów w nawiasie może spowodować, że wogóle nie wczyta się css
 * 
 */

@Controller
@RequestMapping("/product") //wszystkie requesty zaczynające się na ten adres będą przekierowane tutaj
public class ProductsController {

	@Autowired
	private ProductService repo;

	@RequestMapping({"/product-list" , "/"})
	public String listAll(Model model){
		model.addAttribute("products", repo.findAll());
		return "/product/product-list";
	}
	
	@RequestMapping("{id}")
	public String viewProduct(@PathVariable long id, Model model){
		model.addAttribute("product", repo.findOne(id));
		return "/product/product";
	}
	
	//otwieramy strone z formą do tworzenia obiektu (lub jego edycji)
	@RequestMapping("/new-product-form")
	public String productForm(Model model){
		model.addAttribute("product",  new Product());
		return "/product/new-product-form";
	}
	
	//-- pobieramy obiekt z requestu (z new-product-form), zapisujemy i robimy redirect
	@RequestMapping(value="/create-new", method=RequestMethod.POST)
	public String createNewProduct(Product product){
		repo.save(product);
		return "redirect:/product/" + product.getId(); //zamiast odsyłać do konkretnej strony robimy jeszcze raz request do kontrolera
	}
	
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable long id, Model model){
		model.addAttribute(repo.findOne(id));
		return "/product/new-product-form";
	}
	
	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable long id){
		repo.delete(id);
		return "redirect:/product/product-list";
	}
}
