package demo.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController implements ErrorController {

    @RequestMapping("/") //ścieżka dla której chcemy zmapować szablon
    public String index() {
        return "index"; //poprzez zwrócenie stringa z nazwą pliku, Spring bedzie szukał szablonu Thymeleafa
    }

    
    
    //bad URL error handling
    @RequestMapping("/error")
    public String error() {
        return "error";
    }
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
