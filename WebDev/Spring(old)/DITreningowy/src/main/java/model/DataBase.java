package model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class DataBase {
	@Value("${dtbs.host}")
	private String host; 
	@Value("${dtbs.port}")
	private String port;
	@Value("${dtbs.name}")
	private String name;
	@Value("${dtbs.password}")
	private String password;
	
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	//---------------------------------------
	public void startConnection(){
		System.out.println("Connecting do database...");
	}
	public void closeConnection(){
		System.out.println("Disconnecting from database...");
	}
	
	@Override
	public String toString() {
		return "DataBase [host=" + host + ", port=" + port + ", name=" + name + ", password=" + password + "]";
	}
	
}
