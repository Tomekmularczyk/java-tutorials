package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import demo.interfaces.Service;

@Configuration
@PropertySource("classpath:dtbs.properties") //--możemy użyć anotacji lub metody setLocation() żeby podać adres do pliku
@ComponentScan(basePackages = {"demo.implementations"})
public class AppConfiguration {
	
	@Autowired(required = false)
	@Qualifier(value = "madWorker")
	private Service<?> service;
	
	@Bean
	public Person person(){
		return new Person();
	}
	
	@Bean(initMethod="startConnection", destroyMethod="closeConnection")
	public DataBase dataBase(){
		return new DataBase();
	}
	
//	@Bean
//	public Service<String> worker(){
//		return new MadWorker();
//	}
	
	//------ ta metoda jest nam koniecznie potrzebna żeby załadować plik. Adres pliku podajemy w anotacji lub w metodzie setLocation()
	@Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer c = new PropertySourcesPlaceholderConfigurer();
      //  c.setLocation(new ClassPathResource("dtbs.properties"));
        return c;
    }
}
