package demo.interfaces;

public interface Service<T> {
	void operate(T t);
	T work(T t);
}
