package demo.implementations;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import demo.interfaces.Service;

@Component
public class Worker implements Service<List<String>>{

	public Worker(){
		System.out.println("Tworzenie " + getClass().getSimpleName());
	}
	
	public void operate(List<String> list) {
		for(String element : list){
			System.out.println(element.toString() + ", ");
		}
	}

	public List<String> work(List<String> list) {
		Collections.sort(list);
		return list;
	}




}
