package demo.implementations;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import demo.interfaces.Service;


@Component
public class MadWorker implements Service<List<String>>{

	public MadWorker(){
		System.out.println("Tworzenie " + getClass().getSimpleName());
	}

	public void operate(List<String> t) {
		System.out.println(t + "! ");
		
	}

	public List<String> work(List<String> t) {
		Collections.sort(t);
		return t;
	}

}
