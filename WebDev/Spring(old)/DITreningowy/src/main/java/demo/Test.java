package demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.implementations.Worker;
import demo.interfaces.Service;
import hidden_package.HiddenBean;
import model.AppConfiguration;
import model.DataBase;
import model.Person;

public class Test {
	public static void main(String[] args){
		ApplicationContext xCtx = new ClassPathXmlApplicationContext("config.xml");
		ApplicationContext aCtx = new AnnotationConfigApplicationContext(AppConfiguration.class);
		Person person = aCtx.getBean("person", Person.class);
		DataBase dataBase = aCtx.getBean("dataBase", DataBase.class);
		out(person + "\n" + dataBase );
		Service<?> worker = aCtx.getBean("worker", Worker.class);
		
		Person person2 = xCtx.getBean("person", Person.class);
		DataBase dataBase2 = xCtx.getBean("dataBase", DataBase.class);
		out(person2 + "\n" + dataBase2 );
		Service<?> worker2 = xCtx.getBean("worker", Worker.class);
		
		xCtx.getBean("hiddenBean",HiddenBean.class);
	}
	
	
	public static void out(String str){
		System.out.println(str);
	}
}
