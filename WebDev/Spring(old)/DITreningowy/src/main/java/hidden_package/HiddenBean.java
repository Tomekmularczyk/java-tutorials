package hidden_package;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HiddenBean {
	private String name;
	private String secretInfo;
	
	@Autowired
	private HiddenBean(@Value("default") String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecretInfo() {
		return secretInfo;
	}
	public void setSecretInfo(String secretInfo) {
		this.secretInfo = secretInfo;
	}

	@Override
	public String toString() {
		return "HiddenBean [name=" + name + "]";
	}
}
