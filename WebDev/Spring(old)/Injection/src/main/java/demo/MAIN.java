package demo;

import demo.api.Logger;
import demo.api.UsersRepository;
import demo.domain.User;
import demo.implementations.LoggerImpl;
import demo.implementations.UserRepositoryImpl;

public class MAIN {

	public static void main(String[] args) {
		Logger logger = new LoggerImpl();
		UsersRepository repository = new UserRepositoryImpl();
		repository.setLogger(logger);
		
		User adrian = repository.createUser("Adrian", "Kolanko");
		User janina = repository.createUser("Janina", "Nowak");
		User zenon = repository.createUser("Zenon", "Michalczeski");
	} 

}
