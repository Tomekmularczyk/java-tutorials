package demo.api;

import demo.domain.User;

public interface UsersRepository {
	User createUser(String name, String lastName);
	void setLogger(Logger logger);
}
