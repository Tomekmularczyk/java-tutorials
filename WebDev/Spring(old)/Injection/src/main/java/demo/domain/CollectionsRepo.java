package demo.domain;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CollectionsRepo {
	private static String className = "CollectionsRep";
	private long[] longList;
	private Map<String, String> stringMap;
	private List<Integer> integerList;
	
	public CollectionsRepo(long[] longList, Map<String, String> stringMap, List<Integer> integerList) {
		super();
		this.longList = longList;
		this.stringMap = stringMap;
		this.integerList = integerList;
	}
	public long[] getLongList() {
		return longList;
	}
	public void setLongList(long[] longList) {
		this.longList = longList;
	}
	public Map<String, String> getStringMap() {
		return stringMap;
	}
	public void setStringMap(Map<String, String> stringMap) {
		this.stringMap = stringMap;
	}
	public List<Integer> getIntegerList() {
		return integerList;
	}
	public void setIntegerList(List<Integer> integerList) {
		this.integerList = integerList;
	}
	
	
	@Override
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		
		strBuilder.append(Arrays.toString(longList) + "\n");
		
		for(Map.Entry<String, String> entry :stringMap.entrySet()){
			strBuilder.append("[" + entry.getKey() + "," + entry.getValue() + "]");
		}
		strBuilder.append("\n");
		
		Iterator<Integer> iterator = integerList.iterator();
		while(iterator.hasNext()){
			strBuilder.append(iterator.next() + ", ");
		}
		
		return strBuilder.toString();
	}
	
}
