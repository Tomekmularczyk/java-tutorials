package demo.domain;

import org.springframework.beans.factory.InitializingBean;

public class Couple implements InitializingBean{
	private User men;
	private User woman;
	
	public User getMen() {
		return men;
	}
	public void setMen(User men) {
		this.men = men;
	}
	public User getWoman() {
		return woman;
	}
	public void setWoman(User woman) {
		this.woman = woman;
	}
	
	@Override
	public String toString() {
		return "Couple [men=" + men + ", woman=" + woman + "]";
	}
	
	//ta metoda zostanie automatycznie uruchomiona po utworzeniu beaa w xml(coś jak init)
	//jednak lepiej jest to ustawić w XMLu niż tutaj ponieważ ten interfejs powoduje jakieś sprzężenie beana ze springiem
	public void afterPropertiesSet() throws Exception {
		int res = (int) (Math.random()*10);
		System.out.println("Nasza parka będzie razem przez " + res + " lat/a.");
	}
	
	public void mojInit(){
		System.out.println("Patrze brat w okno i widze samotność");
	}
}
