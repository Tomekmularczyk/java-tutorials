package demo;

import java.util.Calendar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.api.UsersRepository;
import demo.domain.CollectionsRepo;
import demo.domain.Couple;
import demo.domain.User;
import demo.implementations.UserRepositoryImpl;

public class MAIN_SPRING {

	public static void main(String[] args) {
		//-- ładujemy obiekty stworzone w XML. Beany które są singletonami zostaną tutaj utworzone
		ApplicationContext context = new ClassPathXmlApplicationContext("konfiguracja.xml");
		
		
		UsersRepository repository = context.getBean("repozytorium_uzytkownikow", UserRepositoryImpl.class);		
		User adrian = repository.createUser("Adrian", "Kowalski"); //-- działa
		
		User kazik = context.getBean("user_kazik", User.class);
		System.out.println(kazik);
		
		Calendar calendar = context.getBean("calendar", Calendar.class);
		//Calendar calendar = (GregorianCalendar) context.getBean("calendar");  //---lub rzutując
		System.out.println("Calendar type: " + calendar.getCalendarType());
		
		
		//------- kolekcje
		ApplicationContext context2 = new ClassPathXmlApplicationContext("kolekcje.xml");
		CollectionsRepo collectionsRepo = context2.getBean("collections_repo",CollectionsRepo.class);
		System.out.println(collectionsRepo);
		
		
		//---- autowire
		ApplicationContext context3 = new ClassPathXmlApplicationContext("autowire.xml");
		Couple couple = context3.getBean("couple", Couple.class);
		System.out.println(couple);
	}

}
