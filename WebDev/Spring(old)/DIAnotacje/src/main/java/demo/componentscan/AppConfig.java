package demo.componentscan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"demo.scope","demo.qualifier"}) //dzięki temu nie potrzebujemy konfiguracji XMLa
public class AppConfig {
	/* nie możemy dodać "demo.value" ponieważ DatabaseSource polega na property placeholderze w XML. Spring będzie
	 * próbował wstrzykiwać do pół język EL i przy wstrzykiwaniu Stringa do int się wysypie
	 * 
	 */
}
