package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.value.DatabaseSource;

public class ValueTEST {
	
	public static void main(String[] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans3.xml");
		
		DatabaseSource dtbsSource = ctx.getBean("databaseSource", DatabaseSource.class);
		System.out.println(dtbsSource);
	}
}
