package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.importconfig.ApplicationConfig;
import demo.importconfig.LoggerConfig;
import demo.importconfig.MenConfig;
import demo.qualifier.Ben;
import demo.qualifier.Pamela;
import demo.service_autowired.Logger;
import demo.service_autowired.LoggerImpl;

public class ImportTEST {
	public static void main(String[] args){
		ApplicationContext ctx = new AnnotationConfigApplicationContext(LoggerConfig.class, MenConfig.class);
		// ...lub 
		ApplicationContext ctx2 = new AnnotationConfigApplicationContext(ApplicationConfig.class); //...ponieważ importuje obydwie powyższe konfiguracje
		
		Logger logger = ctx.getBean("logger", LoggerImpl.class);
		Ben ben = ctx.getBean("ben", Ben.class);
		System.out.println(logger + ", " + ben);
		
		Logger logger2 = ctx2.getBean("logger", LoggerImpl.class);
		Pamela pamela = ctx2.getBean("pamela", Pamela.class);
		System.out.println(logger2 + ", " + pamela);
		
	}
}
