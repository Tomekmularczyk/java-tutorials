package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.scope.SingletonCounter;
import demo.scope.PrototypeCounter;

public class ScopeTEST {
	public static void main(String[] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans3.xml");
		SingletonCounter singletonCounter = ctx.getBean("singleton",SingletonCounter.class);
		PrototypeCounter prototypeCounter = ctx.getBean("prototype",PrototypeCounter.class);
		
		out("[singletonCounter] - " + singletonCounter.increment());
		out("[singletonCounter] - " + singletonCounter.increment());
		out("[prototypeCounter] - " + prototypeCounter.increment());
		out("[prototypeCounter] - " + prototypeCounter.increment());
		
		SingletonCounter singletonCounter2 = ctx.getBean("singleton",SingletonCounter.class);
		PrototypeCounter prototypeCounter2 = ctx.getBean("prototype",PrototypeCounter.class);
		
		out("[singletonCounter2] - " + singletonCounter2.increment());
		out("[singletonCounter2] - " + singletonCounter2.increment());
		out("[prototypeCounter2] - " + prototypeCounter2.increment());
		out("[prototypeCounter2] - " + prototypeCounter2.increment());
	}
	
	
	public static void out(String str){
		System.out.println(str);
	}
}
