package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.componentscan.AppConfig;
import demo.qualifier.Ben;
import demo.scope.SingletonCounter; 

public class ComponentScanTEST {

	public static void main(String[] args) {
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);//zwróć uwagę z jakiego pakietu importujemy klasę AppConfig

		Ben ben = ctx.getBean("ben",Ben.class);
		SingletonCounter singletonCounter = ctx.getBean("singleton",SingletonCounter.class);
		
		//--- to wywali error ponieważ nie mamy wstrzykniętego property placeholdera
		//DatabaseSource dtbsSource = ctx.getBean("databaseSource", DatabaseSource.class); 
		
		System.out.println(ben);
		System.out.println(singletonCounter);

	}

}
