package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.resource_jee.Couple2;

public class ResourceTEST {
	public static void main(String[] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans2.xml");
		
		Couple2 couple2 = ctx.getBean("couple2", Couple2.class);
		System.out.println(couple2);
	}
}
