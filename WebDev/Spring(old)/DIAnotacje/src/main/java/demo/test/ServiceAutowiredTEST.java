package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.service_autowired.UsersRepository;

public class ServiceAutowiredTEST {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		
		UsersRepository usersRepository = ctx.getBean("usersRepo", UsersRepository.class);
		usersRepository.createUser("Chujowy człek");
		
		//Ten bean był tworzony wyłącznie przy pomocy adnotacji @Service i @Autowired, bez użycia XMLa (jedynie tagu component-scan)
		UsersRepository usersRepo2 = ctx.getBean("usersRepo2", UsersRepository.class);
		usersRepo2.createUser("Basieńka kochana");
	}

}
