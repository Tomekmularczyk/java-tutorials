package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.qualifier.Couple;

public class QualifierTEST {
	public static void main(String[] args){
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans2.xml");
		
		Couple couple = ctx.getBean("couple", Couple.class);
		System.out.println(couple);
	}
}
