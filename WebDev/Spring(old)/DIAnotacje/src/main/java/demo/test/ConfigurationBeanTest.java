package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.conguration_bean.AppConfig;
import demo.service_autowired.UsersRepository;

public class ConfigurationBeanTest {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class); //zauważ że nie potrzebujemy konfiguracji XMLa
		
		UsersRepository usersRepo = ctx.getBean("usersRepositoryZiom", UsersRepository.class); //nazwa metody jako nazwa beana
		usersRepo.createUser("Kazik");
	}

}
