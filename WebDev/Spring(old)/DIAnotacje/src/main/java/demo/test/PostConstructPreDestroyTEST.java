package demo.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import demo.postconstruct_predestroy.BeansConfiguration;
import demo.postconstruct_predestroy.SomeBean;

public class PostConstructPreDestroyTEST {

	public static void main(String[] args) {
		//potrzebujemy tego abstrakcyjnego interfejsu żeby zarejestrować wydarzenia post-construct i pre-destroy
		AbstractApplicationContext abstrCtx = new AnnotationConfigApplicationContext(BeansConfiguration.class);
		abstrCtx.registerShutdownHook();
		
		abstrCtx.getBean("someBean", SomeBean.class);
	}

}
