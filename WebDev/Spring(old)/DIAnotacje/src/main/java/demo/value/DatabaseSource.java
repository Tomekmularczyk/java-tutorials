package demo.value;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/*
 * Wstrzykujemy za pomocą @Value wartości z pliku(dzięki zadeklarowanemu property placeholder w xml)
 * oraz wartości przekazane do Virtualnej Maszyny (sprawdz run configuration)
 */

@Service
public class DatabaseSource {
	@Value("${dtbs.port}")
	private int port;
	@Value("${dtbs.host}")
	private String host;
	@Value("${dtbs.host}")
	private String user;
	@Value("${dtbs.password}")
	private String password;
	
	@Value("#{systemProperties['databaseName']}")
	private String databaseName;
	@Value("#{systemProperties['persistenceTime']}")
	private int persistence;
	
	
	
	//========= tylko settery wystarczą do stworzenia przez springa beana
	public void setPort(int port) {
		this.port = port;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	@Override
	public String toString() {
		return "DatabaseSource [port=" + port + ", host=" + host + ", user=" + user + ", password=" + password
				+ "]:[databaseName=" + databaseName + ", persistence=" + persistence + "]";
	}
	
	

	
	
	
}
