package demo.resource_jee;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import demo.qualifier.Men;
import demo.qualifier.Woman;

@Service
public class Couple2 {
	@Resource(name = "ben"/*lub john, lub simpleBen(stworzony bean w XML)*/) 
	private Men men; 
	
	private Woman woman;

	public Men getMen() {
		return men;
	}
	public void setMen(Men men) {
		this.men = men;
	}
	public Woman getWoman() {
		return woman;
	}
	@Resource(name = "pamela") //resource jest odpowiednikiem @Autowired i @Qualifier tylko z JaveEE oraz można go używać do setterów(wtedy wyszukuje beana po nazwie settera(!)
	public void setWoman(Woman wom) {
		this.woman = wom;
	}

	@Override
	public String toString() {
		return "Couple2 [men=" + men.getClass().getSimpleName() + ", woman=" + woman.getClass().getSimpleName() + "]";
	}
}
