package demo.service_autowired;

import java.util.Date;

public class LoggerImpl implements Logger{

	public void logg(String str) {
		System.out.println(new Date() +": "+ str);
	}

}
