package demo.service_autowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("usersRepo2")  //ta adnotacja sprawie że cała klasa traktowana jest jako bean //można bez nawiasu wtedy domyślnie będzie nazwa klasy zaczynająca się od małej litery
public class UsersRepositoryImpl2 implements UsersRepository {
	@Autowired //automatyczne wiązanie innego beana 
	private Logger logger;
	
	public User createUser(String name) {
		logger.logg("tworzymy nowego użytkownika " + name);
		return new User();
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
