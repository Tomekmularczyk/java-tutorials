package demo.service_autowired;

public class UsersRepositoryImpl implements UsersRepository {
	private Logger logger;
	
	public User createUser(String name) {
		logger.logg("tworzymy nowego użytkownika " + name);
		return new User();
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
