package demo.service_autowired;

import java.util.Date;

import org.springframework.stereotype.Service;

@Service 
public class LoggerImpl2 implements Logger{

	public void logg(String str) {
		System.out.println(new Date() +": "+ str);
	}

}
