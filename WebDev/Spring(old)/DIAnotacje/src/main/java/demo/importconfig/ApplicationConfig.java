package demo.importconfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {LoggerConfig.class, MenConfig.class})
public class ApplicationConfig {
	
}
