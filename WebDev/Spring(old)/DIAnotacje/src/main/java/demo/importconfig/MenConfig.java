package demo.importconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.qualifier.Ben;
import demo.qualifier.Men;
import demo.qualifier.Pamela;
import demo.qualifier.Woman;

@Configuration
public class MenConfig {
	
	@Bean
	public Men ben(){
		return new Ben();
	}
	
	@Bean
	public Woman pamela(){
		return new Pamela();
	}
}
