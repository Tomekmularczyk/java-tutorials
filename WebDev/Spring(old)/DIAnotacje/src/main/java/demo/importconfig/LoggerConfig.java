package demo.importconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.service_autowired.Logger;
import demo.service_autowired.LoggerImpl;

@Configuration
public class LoggerConfig {

	@Bean
	public Logger logger(){
		return new LoggerImpl();
	}
}
