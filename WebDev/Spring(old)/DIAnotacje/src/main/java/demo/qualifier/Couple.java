package demo.qualifier;

import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class Couple {
	@Autowired
	@Qualifier("ben"/*lub john, lub simpleBen(stworzony bean w XML)*/) 
	private Men men; //gdyby nie Qualifier to wywali exception: 
					 //-- expected single matching bean but found 3: ben,john,demo.qualifier.Ben#0 - ponieważ i ben i john są typu Men, spring nie wie który powiązać
	@Autowired
	private Woman woman;

	@Autowired(required = false) //zabezpieczamy się. Jeżeli nie znajdzie odpowiedniego beana to nie wyrzuci exception
	private GregorianCalendar noBeanFound;
	
	public Men getMen() {
		return men;
	}
	public void setMen(Men men) {
		this.men = men;
	}
	public Woman getWoman() {
		return woman;
	}
	public void setWoman(Woman woman) {
		this.woman = woman;
	}

	@Override
	public String toString() {
		return "Couple [men=" + men.getClass().getSimpleName() + ", woman=" + woman.getClass().getSimpleName() + "]";
	}

}
