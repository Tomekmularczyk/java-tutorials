package demo.scope;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("singleton")
@Scope(BeanDefinition.SCOPE_SINGLETON) //tego w nawiasie nie musimy przy singletonie podawac
public class SingletonCounter {
	private int count = 0;
	
	public int increment(){
		return count++;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
