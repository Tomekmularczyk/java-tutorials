package demo.scope;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("prototype")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeCounter {
	private int count = 0;
	
	public int increment(){
		return count++;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
