package demo.conguration_bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.service_autowired.Logger;
import demo.service_autowired.LoggerImpl;
import demo.service_autowired.UsersRepository;
import demo.service_autowired.UsersRepositoryImpl;

@Configuration //pozwala na stworzenie konfiguracji beanów bez użycia XML
public class AppConfig {
	
	@Bean
	public Logger logger(){
		return new LoggerImpl();
	}
	
	@Bean
	public UsersRepository usersRepositoryZiom(){
		UsersRepository repo = new UsersRepositoryImpl();
		repo.setLogger(logger()); //dojemy beana z poprzedniej metody
		return repo;
	}
	
}
