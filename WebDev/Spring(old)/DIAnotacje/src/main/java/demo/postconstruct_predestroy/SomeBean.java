package demo.postconstruct_predestroy;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class SomeBean {

	/* te metody będą się odpowiednio uruchamiać po wstrzyknięciu zależności i przed zakończeniem kontekstu springa(czyli przed wyłączeniem wirtualnej maszyny)
	 * 
	 * jeden sposób jest przy użyciu anotacji @PostConstruct i @PreDestroy a drugi sposób jest wskazanie odpowiednich metod w anotacji @Bean danego beana
	 */
	@PostConstruct
	public void postConstruct(){
		System.out.println("postConstruct()...");
	}
	@PreDestroy
	public void preDestroy(){
		System.out.println("preDestroy()...");
	}
	

	public void postConstructWay2(){
		System.out.println("postConstructWay2()...");
	}
	public void preDestroyWay2(){
		System.out.println("preDestroyWay2()...");
	}
}
