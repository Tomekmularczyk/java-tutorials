package demo.postconstruct_predestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration {

	@Bean(initMethod = "postConstructWay2", destroyMethod = "preDestroyWay2") //jeżeli w Beanie mamy już anotacjami oznaczone metody init i destroy, to tutaj jest to zbędne
	public SomeBean someBean(){
		return new SomeBean();
	}
}
