package demo.jdbc_template;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

@Configuration
@ComponentScan("demo.jdbc_template ")
public class JDBCConfiguration {
	@Value("${dtbs.name}")
	private String name; 
	@Value("${dtbs.host}")
	private String host;
	@Value("${dtbs.user}")
	private String user;
	@Value("${dtbs.password}")
	private String password;
	
	
	@Bean
	public DataSource dataSource(){
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setDatabaseName(name);
		dataSource.setServerName(host);
		dataSource.setUser(user);
		dataSource.setPassword(password);
		
		return dataSource;
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate(){
		return new JdbcTemplate(dataSource());
	}
	
	
	@Bean //żeby wczytać plik
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer c = new PropertySourcesPlaceholderConfigurer();
        c.setLocation(new ClassPathResource("database.properties"));
        return c;
    }
}
