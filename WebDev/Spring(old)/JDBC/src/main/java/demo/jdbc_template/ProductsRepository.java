package demo.jdbc_template;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ProductsRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	//moglibyśmy też użyć takiego wiązania zamiast tworzyć nowego beana jdbcTemplate w konfiguracji
	//użyjemy tego wiązania do NamedParameterJdbcTemplate
	@Autowired
	public void setDataSource(DataSource dataSource){
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	
	//zwracamy pojdyńczy wynik
	public int getNumberOfProducts() {
		return jdbcTemplate.queryForObject("select count(*) from products", Integer.class);
	}

	//zwracamy wynik z podanym parametrem do zapytania
	public int getCountWherePriceGreaterThan(double price){
		String query = "select count(*) from products where buyPrice > ?";
		return jdbcTemplate.queryForObject(query, Integer.class, price);
	}
	
	//zwracamy listę typów prostych
	public List<String> getProductLines(){
		String query = "SELECT productLine FROM products GROUP BY productLine";
		return jdbcTemplate.queryForList(query, String.class);
	}
	
	//zwracamy pojedyńczy rząd
	public Map<String, Object> getProductWithCode(String code){
		String query = "SELECT productCode, productName, quantityInStock FROM products WHERE productCode=?";
		return jdbcTemplate.queryForMap(query, code);
	}
	
	//zwracamy listę rzędów
	public List<Map<String, Object>> getProductsWherePriceGreaterThan(double price){
		String query = "SELECT productCode, productName, quantityInStock FROM products WHERE buyPrice > ?  LIMIT 10";
		
		return jdbcTemplate.queryForList(query, price);
	}
	
	
	
	//---------------------- parametry nazwane -----------
	
	//zwracamy listę rzedów, ale używamy parametrów nazwanych
	public List<Map<String, Object>> getProductsWithPriceBetween(double min, double max){
		String query = "SELECT productCode, productName, quantityInStock FROM products WHERE buyPrice BETWEEN :minPrice AND :maxPrice LIMIT 10";
		
		//------- pierwszy sposób
		SqlParameterSource sqlParamsSource = new MapSqlParameterSource("minPrice", min).addValue("maxPrice", max);
		
		//------- drugi sposób
		Map<String, Object> mapParams = new HashMap<>();
		mapParams.put("minPrice", min);
		mapParams.put("maxPrice", max);
		
		
		return namedParameterJdbcTemplate.queryForList(query, mapParams);
	}
	
	//używamy Listy paramtetrów w klauzuli IN(args...), (bo z pytajnikami i przecinkami się nie da tego zrobić)
	public List<Map<String,Object>> getProductsWithProductLine(String... args){
		Map<String,Object> params = new HashMap<>();
		params.put("productLines", Arrays.asList(args));
		
		String query = "SELECT productName, productLine, buyPrice FROM products WHERE productLine IN(:productLines)";
		
		return namedParameterJdbcTemplate.queryForList(query, params);
	}
}
