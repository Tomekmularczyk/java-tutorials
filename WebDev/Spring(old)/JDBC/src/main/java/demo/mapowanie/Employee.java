package demo.mapowanie;

public class Employee {
	private int employeeNumber;
	private int reportsTo;
	private String jobTitle;
	private String officeCode;
	private String email;
	private String extension;
	private String firstName;
	private String lastName;
	
	
	@Override
	public String toString() {
		return "Employee [employeeNumber=" + employeeNumber + ", reportsTo=" + reportsTo + ", jobTitle=" + jobTitle
				+ ", officeCode=" + officeCode + ", email=" + email + ", extension=" + extension + ", firstName="
				+ firstName + ", lastName=" + lastName + "]";
	}
	
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public int getReportsTo() {
		return reportsTo;
	}
	public void setReportsTo(int reportsTo) {
		this.reportsTo = reportsTo;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getOfficeCode() {
		return officeCode;
	}
	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
}
