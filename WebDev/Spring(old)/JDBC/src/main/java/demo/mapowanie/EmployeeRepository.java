package demo.mapowanie;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	//mapujemy do pojedyńczego obiektu
	public Employee getEmployeeByID(int id){
		String query = "SELECT * FROM employees WHERE employeeNumber=?";
		
		return jdbcTemplate.queryForObject(query, new EmployeeRowMapper(), id);
	}
	
	//mapujemy do listy obiektów
	public List<Employee> getEmployees(int howMany){
		String query = "SELECT * FROM Employees ORDER BY employeeNumber LIMIT ?";
		
		return jdbcTemplate.query(query, new EmployeeRowMapper(), howMany);
	}
	
	
	
	//Opisujemy jak rezultat ma być tłumaczony do porządanego obiektu
	private static class EmployeeRowMapper implements RowMapper<Employee>{
		@Override
		public Employee mapRow(ResultSet rs, int nr) throws SQLException {
			Employee employee = new Employee();
			employee.setEmail(rs.getString("email"));
			employee.setEmployeeNumber(rs.getInt("employeeNumber"));
			employee.setExtension(rs.getString("extension"));
			employee.setFirstName(rs.getString("firstName"));
			employee.setJobTitle(rs.getString("jobTitle"));
			employee.setLastName(rs.getString("lastName"));
			employee.setOfficeCode(rs.getString("officeCode"));
			employee.setReportsTo(rs.getInt("reportsTo"));
			
			return employee;
		}
		
	}
	
}
