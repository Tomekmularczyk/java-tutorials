package demo.test;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.mapowanie.Employee;
import demo.mapowanie.EmployeeRepository;
import demo.mapowanie.MappingConfig;

public class MappingTest {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(MappingConfig.class);
		EmployeeRepository employeeRepo = context.getBean("employeeRepository", EmployeeRepository.class);
		Employee employee = employeeRepo.getEmployeeByID(1002);
		
		System.out.println(employee);
		
		List<Employee> employees = employeeRepo.getEmployees(5);
		employees.forEach(item -> {
			System.out.println(item);
		});
	}

}
