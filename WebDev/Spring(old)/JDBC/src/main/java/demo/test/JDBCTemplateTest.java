package demo.test;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.jdbc_template.JDBCConfiguration;
import demo.jdbc_template.ProductsRepository;

public class JDBCTemplateTest {
	
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JDBCConfiguration.class);//annotation configuration
	//	ApplicationContext context = new ClassPathXmlApplicationContext("config1.xml"); //xml configuration of the same

		ProductsRepository productsRepo = context.getBean("productsRepository", ProductsRepository.class);
		
		out("Number of products: " + productsRepo.getNumberOfProducts());
		
		out("Avvailable product lines: " + productsRepo.getProductLines());
		
		out("Number of products with price greater than 50: " + productsRepo.getCountWherePriceGreaterThan(50));
		
		out("Product with code S10_1949: " + productsRepo.getProductWithCode("S10_1949"));
		
		List<Map<String, Object>> listOfProducts = productsRepo.getProductsWherePriceGreaterThan(50);
		for(Map<String, Object> row : listOfProducts){
			out(row + "");
		}
		
		out("====================NAMED PARAMETERS===================");
		
		List<Map<String, Object>> listOfProducts2 = productsRepo.getProductsWithPriceBetween(20, 70);
		for(Map<String, Object> row : listOfProducts2){
			out(row + "");
		}
		
		out("------------------------");
		
		List<Map<String, Object>> listOfProducts3 = productsRepo.getProductsWithProductLine("Ships","Planes");
		for(Map<String, Object> row : listOfProducts3){
			out(row + "");
		}
		
	}
	
	private static void out(String str){
		System.out.println(str);
	}

}
