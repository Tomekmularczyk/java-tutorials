package demo.api;

public interface Logger {
	void setName(String name);
	void setVersion(int ver);
	void log(String message);
}
