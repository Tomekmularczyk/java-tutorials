package demo.implementations;

import demo.api.Logger;
import demo.api.UsersRepository;
import demo.domain.User;

public class UserRepositoryImpl implements UsersRepository{
	private Logger logger;

	public User createUser(String name, String lastName) {
		logger.log("tworzymy użytkownika " + lastName );
		return new User(name, lastName);
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
