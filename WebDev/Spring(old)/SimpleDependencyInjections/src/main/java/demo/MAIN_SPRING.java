package demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.api.UsersRepository;
import demo.domain.User;
import demo.implementations.UserRepositoryImpl;

public class MAIN_SPRING {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("konfiguracja.xml");
		//-- ładujemy obiekty stworzone w XML
		UsersRepository repository = context.getBean("repozytorium_uzytkownikow", UserRepositoryImpl.class);
		
		User adrian = repository.createUser("Adrian", "Kowalski");
		User agniecha = repository.createUser("Agniecha", "Bidula");
		
		
	}

}
