package demo.factory;

public class CatSoundFactory {
	public CatSound getCatSound(String language){
		switch(language){
			case "en": return new EnglishCat();
			case "pl": return new PolishCat();
			case "es": return new SpanishCat();
			default: return null;
		}
	}
}
