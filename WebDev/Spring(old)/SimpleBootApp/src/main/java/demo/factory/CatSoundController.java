package demo.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class CatSoundController {
	@Autowired //będą dostępne trzy beany, ale dzięki profilowaniu, spring będzie wiedział który wstrzyknąć
	private CatSound cat;
	
//	@Autowired
//	@Qualifier("catPL")   //dlaczego nie może znaleźć tego beana???
//	private CatSound polishCat;
	
	public void makeSound(){
		cat.makeSound();
	}
}
