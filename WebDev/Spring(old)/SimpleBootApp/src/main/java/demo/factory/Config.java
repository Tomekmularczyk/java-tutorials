package demo.factory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config {
	/*	w taki sposób korzystamy w springu z wzorca factory
	 * 
	 */
	
	@Bean
	public CatSoundFactory catSoundFactory(){
		return new CatSoundFactory();
	}
	
	@Bean
	@Profile({"english", "default"})
	public CatSound catSoundInEnglish(CatSoundFactory factory){
		return factory.getCatSound("en");
	}
	
	@Bean(name="catPL")//tak dla przypomnienia w jaki sposób mówimy springowi jak ma nazywać nasze beany. Defaultowo używa nazwy metody.
	@Profile("polish")
	public CatSound catSoundInPolish(CatSoundFactory factory){
		return factory.getCatSound("pl");
	}
	@Bean
	@Profile("spanish")
	public CatSound catSoundInSpanish(CatSoundFactory factory){
		return factory.getCatSound("es");
	}
}
