package demo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import demo.profiles.GreetingController;

@SpringBootApplication
@ComponentScan("demo.profiles")
public class ProfileTEST {
	/* Dzięki profilom możemy decydować jakie beany mają być wstrzykiwane (gdy np. pojawia się konflikt).
	 * 1. Oznaczamy komponenty adpowiednią nazwą profilu @Profile("jezyk_polski")
	 * 2. w pliku application.properties oznaczamy który profil ma być aktywowany 
	 */

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ProfileTEST.class, args);
		GreetingController greetingController = ctx.getBean("greetingController", GreetingController.class);
		
		greetingController.sayHello();
	}

}
