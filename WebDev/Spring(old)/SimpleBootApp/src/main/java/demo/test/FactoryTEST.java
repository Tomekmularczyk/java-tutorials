package demo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import demo.factory.CatSoundController;

@SpringBootApplication
@ComponentScan("demo.factory")
public class FactoryTEST {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(FactoryTEST.class, args);
		
		CatSoundController cSC = ctx.getBean("catSoundController", CatSoundController.class);
		cSC.makeSound();
	}

}
