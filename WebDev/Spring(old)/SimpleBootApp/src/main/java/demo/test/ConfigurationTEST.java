package demo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import demo.profiles.GreetingController;

@SpringBootApplication
@ComponentScan("demo.configuration")
public class ConfigurationTEST {
	/* W tym przykładzie spring znajduje konfiguracje która "udostępnia" inne beany
	 * 
	 */
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ConfigurationTEST.class, args);
		GreetingController greetingController = ctx.getBean("greetingController", GreetingController.class);
		
		greetingController.sayHello();
	}
}
