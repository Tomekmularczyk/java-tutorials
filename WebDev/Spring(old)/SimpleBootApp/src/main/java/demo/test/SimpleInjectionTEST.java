package demo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import demo.simple_injection.InjectedByConstructorService;
import demo.simple_injection.InjectedBySetterService;

@SpringBootApplication
@ComponentScan("demo.simple_injection")
public class SimpleInjectionTEST {

	/* Dwa rodzaje prostego wstrzykiwania zależności przez konstruktor i przez setter
	 * 
	 */
	
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SimpleInjectionTEST.class, args);
		
		InjectedByConstructorService iBC = ctx.getBean("injectedByConstructorService", InjectedByConstructorService.class);
		iBC.work();
		
		InjectedBySetterService iBS = ctx.getBean("injectedBySetterService", InjectedBySetterService.class);
		iBS.work();
	}

}
