package demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import demo.profiles.GreetingController;
import demo.profiles.HelloWorldPolish;
import demo.profiles.HelloWorldSpanish;
import demo.simple_injection.HelloWorld;

@Configuration
public class Config {
	
	@Bean
	public GreetingController greetingController(){
		return new GreetingController();
	}
	
	
	//poniżesz klasy wcale nie muszą być oznaczone jako komponent by zostały tutaj użyte jak beany
	@Bean
	@Profile("polish")
	public HelloWorld helloWorldSpanish(){
		return new HelloWorldSpanish();
	}
	
	@Bean
	@Profile({"default","spanish"})
	public HelloWorld helloWorldPolish(){
		return new HelloWorldPolish();
	}
}
