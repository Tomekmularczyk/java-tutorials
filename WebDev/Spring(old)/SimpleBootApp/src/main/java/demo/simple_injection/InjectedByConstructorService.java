package demo.simple_injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InjectedByConstructorService {
	private HelloWorld helloWorld;
	
	@Autowired
	public InjectedByConstructorService(HelloWorld hW){
		this.helloWorld = hW;
	}
	
	public void work(){
		helloWorld.sayHello();
	}
}
