package demo.simple_injection;

import org.springframework.stereotype.Component;

@Component
public class HelloWorldImpl implements HelloWorld{

	public void sayHello() {
		System.out.println("Hello world!");
	}

}
