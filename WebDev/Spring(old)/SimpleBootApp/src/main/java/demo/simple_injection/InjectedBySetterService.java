package demo.simple_injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InjectedBySetterService {
	private HelloWorld hW;

	public HelloWorld gethW() {
		return hW;
	}

	@Autowired
	public void sethW(HelloWorld hW) {
		this.hW = hW;
	}

	public void work() {
		hW.sayHello();
	}
}
