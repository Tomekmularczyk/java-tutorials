package demo.profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import demo.simple_injection.HelloWorld;

@Controller
public class GreetingController {
	private HelloWorld helloWorld;
	
	
	public HelloWorld getHelloWorld() {
		return helloWorld;
	}
	
	@Autowired //spring wykryje dwie dostępne implementacje tego interfejsu. Musimy mu powiedzieć o który nam chodzi
	//Możemy do tego użyć profili. Oznaczamy beany profilami a w application.properties ustawiamy który profil ma być aktywny
	public void setHelloWorld(HelloWorld helloWorld) {
		this.helloWorld = helloWorld;
	}



	public void sayHello(){
		helloWorld.sayHello();
	}
}
