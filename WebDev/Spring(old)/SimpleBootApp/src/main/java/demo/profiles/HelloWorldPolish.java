package demo.profiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import demo.simple_injection.HelloWorld;


@Component
@Profile({"polish", "default"}) //ustawiamy profile tego beana. "default" oznacza, że jeżeli żaden profil nie będzie aktywowany to w razie konfliktu spring podłapie te, które są defaultowe
public class HelloWorldPolish implements HelloWorld{

	public void sayHello() {
		System.out.println("Witaj świecie!");
	}

}
