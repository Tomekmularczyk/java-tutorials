package demo.profiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import demo.simple_injection.HelloWorld;

@Component
@Profile("spanish")
public class HelloWorldSpanish implements HelloWorld{

	public void sayHello() {
		System.out.println("Hola Mundo!");
	}

}
