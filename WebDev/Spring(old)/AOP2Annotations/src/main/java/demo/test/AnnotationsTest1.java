package demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import model.AppConfiguration;
import model.SomeBean;

public class AnnotationsTest1 {

	public static void main(String[] args){
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfiguration.class);
		SomeBean someBean = ctx.getBean("someBean",SomeBean.class); //tak naprawdę someBean możesz zobaczyć w debuggerze że jest typu innego niż SomeBean. Teraz jest to otoczająca klasa
		System.out.println("=========");
		
		someBean.someMethod();
		
		System.out.println("=========");

		try{
			someBean.throwSomeException();
		}catch(Exception ex){}
	}

}
