package model;

import org.springframework.stereotype.Component;

@Component
public class SomeBean implements Some{
	public String someMethod(){
		return "Hello world!";
	}
	
	public void throwSomeException() throws Exception{
		throw new Exception("Taka sobie exception zią!");
	}
}
