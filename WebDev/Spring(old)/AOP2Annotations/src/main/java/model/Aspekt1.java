package model;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Aspekt1 {
	private Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	@Pointcut("execution(* *(..))")
	public void allMethods(){}
	//--------------------------------------------
	
	@Before("allMethods()")
	public void before(JoinPoint jP){
		logger.info("before " + jP.getSignature().toShortString());
	}
	
	@After("allMethods()")
	public void after(JoinPoint jP){
		logger.info("after " + jP.getSignature().toShortString());
	}
	
	@Around("allMethods()")
	public Object around(ProceedingJoinPoint pJP) throws Throwable{
		Object returnedValue = null;
		
		logger.info("around " + pJP.getSignature().toShortString());
		returnedValue = pJP.proceed(); 
		
		
		//musimy zwrócić wywołanie metody inaczej afterReturning  otrzyma NULL
		return returnedValue;
	}
	
	@AfterThrowing(pointcut="allMethods()",throwing="exception")
	public void afterThrowing(JoinPoint jP, Throwable exception){
		logger.info("after throwing " + jP.getSignature().toShortString());
	}
	
	
	//nie zostanie wywołana jeżeli został wyrzucony wyjątek
	@AfterReturning(pointcut="allMethods()", returning="returnedValue")
	public void afterReturning(JoinPoint jP, Object returnedValue){
		logger.info("after returning " + jP.getSignature().toShortString() + ": " + returnedValue);
	}
}
