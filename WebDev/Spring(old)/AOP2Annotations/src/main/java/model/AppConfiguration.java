package model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("model")
@EnableAspectJAutoProxy(proxyTargetClass=true)//jeżeli nie wpiszemy tego w nawiasie to dostaniemy exception BeanNotOfRequiredTypeException: 
						 //Bean named 'someBean' must be of type [model.SomeBean], but was actually of type [com.sun.proxy.$Proxy19]
						//proxyTargetClass=true uruchamia drugi obiekt proxowania i mówi że podczas otaczania(proxowania) obiektu, by nadal był obiektem swojej klasy
public class AppConfiguration {
	
}
