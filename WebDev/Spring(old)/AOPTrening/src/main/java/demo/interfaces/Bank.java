package demo.interfaces;

public interface Bank {
	long giveMeMoney(int accountNumber, long howMuch);
	boolean depositMoney(int accountNumber, long money);
	int addAccount(Account account);
}
