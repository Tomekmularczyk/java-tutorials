package demo.interfaces;

public interface Account {
	long widthrawCash(long howMuch);
	boolean depositCash(long howMuch);
}
