package demo.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import demo.model.BankImpl;

@Aspect
@Component
public class BankAspect {

	@Pointcut("execution(* demo.model.*.*(..))")
	public void securePointcut() {
	}

	@Pointcut("@annotation(demo.interfaces.NeedsAuthentication) && execution(* demo.model.*.*(..))")
	public void authenticatePointcut() {
	}
	
	@Pointcut("execution(long demo.model.*.giveMeMoney(int, long))")
	public void sufficientPointCut(){}
	
	

	@Before("securePointcut()")
	public void secureConnection(JoinPoint jP) {
		String methodName = jP.getSignature().getName();
		System.out.println("...-----Connection Secured for method " + methodName + "-----...");
	}
	@After("securePointcut()")
	public void closeConnection(JoinPoint jP){
		String methodName = jP.getSignature().getName();
		System.out.println("...-----Connection Closed for method " + methodName + "-----...");
	}

	@Around("authenticatePointcut()")
	public Object authenticate(ProceedingJoinPoint pJP) throws Throwable {
		String methodName = pJP.getSignature().getName();
		BankImpl bank = ((BankImpl) pJP.getTarget());
		Object[] args = pJP.getArgs();
		Object returnObject = null;

		System.out.println("...-----Authentication for method " + methodName + "... -----...");
		boolean doesAccountExists = bank.doesAccountExists((int) args[0]);

		if (!doesAccountExists) {
			System.out.println("...-----... authentication FAILED !(" + methodName + ")-----...");
			throw new NullPointerException("Account with id: " + (int) args[0] + " doesnt exist!!!");
		} else {
			System.out.println("...-----... authentication SUCCEED !(" + methodName + ")-----...");
			returnObject = pJP.proceed();
		}

		return returnObject;
	}

	
}
