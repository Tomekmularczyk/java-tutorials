package demo.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("demo.model , demo.aspects")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class BankConfig {

}
