package demo.model;

import org.springframework.stereotype.Component;

import demo.interfaces.Account;

@Component
public class AccountImpl implements Account{
	private int id;
	private long avaliableMoney;
	
	public AccountImpl(){}
	
	public AccountImpl(int id, long avaliableMoney) {
		this.id = id;
		this.avaliableMoney = avaliableMoney;
	}

	@Override
	public long widthrawCash(long howMuch) {
		avaliableMoney -= howMuch;
		return howMuch;
	}

	@Override
	public boolean depositCash(long howMuch) {
		avaliableMoney += howMuch;
		return true;
	}

	@Override
	public String toString() {
		return "AccountImpl [id=" + id + ", avaliableMoney=" + avaliableMoney + "]";
	}
	

}
