package demo.model;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import demo.interfaces.Account;
import demo.interfaces.Bank;
import demo.interfaces.NeedsAuthentication;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BankImpl implements Bank{
	private static int ACCOUNT_ID = 0;
	private Map<Integer, Account> listOfAccounts = new HashMap<>();
	
	@PostConstruct
	private void createExampleUsers(){
		listOfAccounts.put(new Integer(ACCOUNT_ID++), new AccountImpl(1, 1000));
		listOfAccounts.put(new Integer(ACCOUNT_ID++), new AccountImpl(2, 10770));
		listOfAccounts.put(new Integer(ACCOUNT_ID++), new AccountImpl(3, 34566));
		listOfAccounts.put(new Integer(ACCOUNT_ID++), new AccountImpl(4, 323));
		listOfAccounts.put(new Integer(ACCOUNT_ID++), new AccountImpl(5, 9432));
		listOfAccounts.put(new Integer(ACCOUNT_ID++), new AccountImpl(6, 85443));
	};
	
	@NeedsAuthentication
	public long giveMeMoney(int accountNumber, long howMuch) {
		Account account = listOfAccounts.get(accountNumber);
		account.widthrawCash(howMuch);
		return howMuch;
	}

	@NeedsAuthentication
	public boolean depositMoney(int accountNumber, long money) {
		Account account = listOfAccounts.get(accountNumber);
		account.depositCash(money);
		return false;
	}

	public int addAccount(Account account) {
		listOfAccounts.put(ACCOUNT_ID++, account);
		return ACCOUNT_ID;
	}
	
	public boolean doesAccountExists(int id){
		return listOfAccounts.containsKey(id);
	}
}
