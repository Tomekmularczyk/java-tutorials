package demo.test;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.interfaces.Bank;
import demo.model.AccountImpl;
import demo.model.BankConfig;

public class BankTest {
	private static Logger log = Logger.getLogger(BankTest.class.getName());
	
	
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(BankConfig.class); 
		Bank bank = ctx.getBean("bankImpl", Bank.class);
		
		bank.addAccount(new AccountImpl(005, 455));
		
		try{
			bank.giveMeMoney(8, 10000);
		}catch(Exception e){
			log.error(e.getClass().getSimpleName() + ": " +e.getMessage());
		}
		
		long money = bank.giveMeMoney(3, 1000);
		log.info("I get " + money + " money from my account");
		
	}

}
