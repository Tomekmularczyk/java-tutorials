package strutsone;

import com.opensymphony.xwork2.ActionSupport;

public class HelloWorld extends ActionSupport{
    private static int counter = 0;
    
    @Override
    public String execute(){
        counter++;
        return SUCCESS;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        HelloWorld.counter = counter;
    }
    
}
