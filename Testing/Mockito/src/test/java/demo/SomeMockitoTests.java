package demo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 * What are the limitations of Mockito
 *
 * Needs Java 6+ (versions 1.x support Java 5+) 
 * - Cannot mock final classes 
 * - Cannot mock static methods 
 * - Cannot mock final methods - their real behavior is executed without any exception. Mockito cannot warn you about mocking final methods so be vigilant. 
 * - Cannot mock equals(), hashCode(). Firstly, you should not mock those methods. Secondly, Mockito defines and depends upon a specific implementation of these
 * methods. Redefining them might break Mockito. 
 * - Mocking is only possible on vms that are supported by Objenesis. Don't worry, most vms should work just fine. 
 * - Spying on real methods where real implementation references outer Class via OuterClass.this is impossible. Don't worry, this is extremely rare case.
 */
@RunWith(MockitoJUnitRunner.class)
public class SomeMockitoTests {

    /**
     * Raz stworzony mock zapamiętuje wszystkie interakcje.
     */
    @Test
    public void testMock() {
        List mock = mock(List.class);
        mock.add(3);
        mock.get(0);

        //verify(mock).get(1);  //argumenty też są zapamiętywane
        verify(mock).get(0);
        verify(mock).add(any());
    }

    @Test
    public void testStubbing() {
        LinkedList mock = mock(LinkedList.class);

        when(mock.add(0)).thenThrow(RuntimeException.class);
    }

    @Test
    public void testArgumentMatching() {
        ArrayList mock = mock(ArrayList.class);

        when(mock.get(anyInt())).thenReturn("dfdfdf");

        mock.get(4);

        verify(mock).get(anyInt());
        verify(mock, times(1)).get(4);
        verify(mock, never()).get(3);
    }

    @Test(expected = RuntimeException.class)
    public void stubbingVoidMethods() {
        List mock = mock(List.class);
        doNothing()
                .doThrow(RuntimeException.class)
                .when(mock)
                .add(anyInt(), any());

        //dopiero drugie wywołanie get wywoła exception
        mock.add(0, new Object()); //nic
        mock.add(0, new Object()); //exception
    }

    @Test
    public void mocksVerificationInOrder() {
        List mock = mock(List.class);
        List mock2 = mock(List.class);

        mock.add("first call");
        mock2.add("second call");

        InOrder inOrder = inOrder(mock, mock2); //uruchamiamy
        inOrder.verify(mock).add("first call");
        inOrder.verify(mock2).add("second call");
    }

    @Test
    public void verifyNoInteractions() {
        List mock = mock(LinkedList.class);
        mock.add(0);

        verify(mock, never()).get(anyInt());
        verify(mock).add(0);
        //Use it only when it's relevant. Abusing it leads to overspecified, less maintainable tests
        verifyNoMoreInteractions(mock); //sprawdzamy, że nie było więcej interakcji niż te sprawdzone powyżej.

        List mock2 = mock(List.class);
        List mock3 = mock(List.class);
        verifyZeroInteractions(mock2, mock3);
    }

    @Test
    public void stubbingConsecutiveCalls() {
        List mock = mock(List.class);

        when(mock.get(anyInt()))
                .thenReturn(4)
                .thenReturn(2)
                .thenReturn(1)
                .thenThrow(NullPointerException.class);

        //alternatywnie…
        when(mock.get(anyInt()))
                .thenReturn(4, 2, 1)
                .thenThrow(NullPointerException.class);
    }

    @Test
    public void stubbingWithCallbacks() {
        List mock = mock(List.class);

        //gdy potrzebujemy bardziej wyszukanego stubbowania niż thenReturn lub thenThrow
        when(mock.contains(anyString()))
                .thenAnswer((InvocationOnMock invocation) -> {
                    Object[] arguments = invocation.getArguments();
                    String methodArgument = (String) arguments[0];
                    return methodArgument.startsWith("mock");
        });

        boolean contains = mock.contains("mock");
        assertThat(contains).isTrue();
    }

    @Test
    public void do_Something_() {
        List mock = mock(List.class);
        //when(mock.clear()). //nie da rady stubować w taki sposób metody która zwraca void
        List spy = mock(List.class);
        doNothing()
                .doNothing()
                .doNothing()
                .doThrow(StackOverflowError.class)
                .when(spy).clear();

        /**
         * metody doThrow(), doAnswer(), doNothing(), doReturn() and doCallRealMethod(), przydają się kiedy: - potrzeba zestubować wywołania
         * void metod - zestubować metody dla obiektów SPY - gdy trzeba zestabować metodę więcej niż raz, tak by zestabować działanie mocka
         * w trakcie testy.
         */
    }

    @Test
    public void testSpy() {
        /**
         * Real spies should be used carefully and occasionally, for example when dealing with legacy code.
         */

        LinkedList<String> spy = spy(new LinkedList<String>());
        doNothing().when(spy).clear();
    }

    
    @Test //czasem chcemy zestubować wszystkie metody tak by zwracały jakąś wartość
    public void changingDefaultValuesOfUnstubbedInvocations() {
        //nie zestabowane metody zwracają często NULL. Zamiast tego możemy użyć SmartNulls, które dają nam lepszy stackTrace
        List mock = mock(List.class, RETURNS_SMART_NULLS);
        Object get = mock.get(0);
        
        List mock2 = mock(List.class, (Answer) (InvocationOnMock invocation) -> "test");
    }

    @Test
    public void testCapturingArgumentsForAssertions() {
        //używając verify mockito weryfikuje argumenty przy pomocy equals. Czasem jednak chcemy sami sprawdzić argumenty przekazane do metody.
        ArgumentCaptor<Integer> argCaptor = ArgumentCaptor.forClass(Integer.class);
        List mock = mock(List.class);

        mock.get(15);

        verify(mock).get(argCaptor.capture()); //argument captory powinny być używane tylko przy weryfikacji nigdy przy stubowaniu.

        assertThat(argCaptor.getValue()).isBetween(5, 19);
    }

    @Test
    public void testResetingMocks() {
        /**
         * Generalnie nie powinno się używać tej metody i w większości przypadków świadczy o zbyt kompleksowym teście.
         * Przydaje się ona tylko w tych rzadkich przypadkach gdy mocki są tworzone prze kontener.
         */
        List<Integer> mock = mock(List.class);
        when(mock.get(anyInt())).thenReturn(4);

        Integer get = mock.get(111);
        assertThat(get).isEqualTo(4);

        //reset
        reset(mock);

        Integer get2 = mock.get(111);
        assertThat(get2).isNull();
    }

    @Test
    public void testBDD_Aliases(){
        /**
         * BDD dzieli się na miejsca given, when, then.
         * W mockito może być mylące to, że when jest komponentem given, dlatego powstał alias.
         */
        
        // given
        List mock = mock(List.class);
        BDDMockito.given(mock.add(anyString())).willReturn(true);
        
        // when
        boolean isAdded = mock.add("test");
        
        // then
        assertThat(isAdded).isTrue();
        BDDMockito.then(mock).should(times(1)).add("test");
    }
    
    @Test
    public void testSerializable(){
        //w rzadkich przypadkach potrzebujemy by obiekt był serializowalny
        mock(List.class, withSettings().serializable());
        
        //serializowanie spy
        mock(LinkedList.class, withSettings()
                .spiedInstance(new LinkedList())
                .defaultAnswer(CALLS_REAL_METHODS)
                .serializable());
    }
    
    @Test
    public void testOneLinerStubs(){
        when(mock(List.class).get(anyInt()))
                .thenThrow(NullPointerException.class);
    }
    
    @Test
    public void testMockingNonStaticInnerClass(){
        class Outer {
            abstract class Inner{
            }
        }
        
        mock(Outer.Inner.class, withSettings()
                .useConstructor()
                .outerInstance(new Outer())
                .defaultAnswer(CALLS_REAL_METHODS));
        
    }
    
    @Test
    public void testCustomVerificationFailureMessage(){
        List mock = mock(List.class);
        BDDMockito.given(mock.get(0)).willReturn("string");
        
        mock.get(0);
        
        BDDMockito.then(mock)
                .should(times(1).description("metoda powinna wykonać się tylko jeden raz"))
                .get(0);
    }
    
    @Test
    public void testCustomArgumentMatcherWithLambda(){
        List<String> list = mock(List.class);
        list.add("raz");
        list.add("trzy");
        
        verify(list, times(2)).add(argThat(string -> string.length() < 5));
        
        //można też używać argument matcherów dla sekcji given
        when(list.add(argThat(str -> str.length() > 3)))
                .thenThrow(RuntimeException.class);
    }
    
    
}
