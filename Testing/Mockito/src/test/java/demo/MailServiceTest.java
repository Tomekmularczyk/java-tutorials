package demo;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MailServiceTest {
    
    @Mock
    private SystemInformator systemInformator;
    @Mock
    private SmtpService smtp;
    @InjectMocks 
    private MailService instance;
    
    
    @Test(expected = MailException.class)
    public void whenOSNotKnownThrowsMailException(){
        Mockito.when(systemInformator.getOS()).thenReturn(OsType.NOT_KNOWN); //Załóżmy, że metoda wykonuje czasochłonne operacje i najlepiej będzie ją zmokować
        
        instance.sendMail("mockito@teraz.pl", "ja nie moge, nie wierze, to nie ja pierdole.");
    }
    
    @Test
    public void sendingEmailShouldCallSendMethodOnSmtp(){
        Mockito.when(systemInformator.getOS()).thenReturn(OsType.LINUX);
        
        instance.sendMail("mockito@teraz.pl", "ja nie moge, nie wierze, to nie ja pierdole.");
        
        Mockito.verify(smtp, Mockito.times(1)).send(OsType.LINUX);
    }
    
    /**
     * Stubować możemy i Spy i Mock, natomiast domyślna implementacja Mocka nie robi i zwraca 0 lub false dla booleana lub pustą listę lub null.
     * a Spy pozwala zachować domyślną implementację i jej działanie.
     */
    @Test
    public void SpyVSMock(){
        MailService mockMS = Mockito.mock(MailService.class);
        MailService spyMS = Mockito.spy(MailService.class);
        
        int zero = mockMS.returnTWO();
        int two = spyMS.returnTWO();
        
        assertThat(zero).isEqualTo(0);
        assertThat(two).isEqualTo(2);
        
        Mockito.when(mockMS.returnTWO()).thenReturn(100);
        Mockito.when(spyMS.returnTWO()).thenReturn(200);
        
        assertThat(mockMS.returnTWO()).isEqualTo(100);
        assertThat(spyMS.returnTWO()).isEqualTo(200);
    }
    
}
