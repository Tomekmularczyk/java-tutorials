package demo;

public enum OsType {
    WINDOWS {
        @Override
        public String getEmailApp() {
            return "Microsoft Outlook";
        }
    }, 
    LINUX {
        @Override
        public String getEmailApp() {
            return "Jakiś linuchowy klient";
        }
    }, 
    OSX {
        @Override
        public String getEmailApp() {
            return "Mail";
        }
    }, 
    NOT_KNOWN {
        @Override
        public String getEmailApp() {
            throw new MailException();
        }
    };
    
    public abstract String getEmailApp();
}
