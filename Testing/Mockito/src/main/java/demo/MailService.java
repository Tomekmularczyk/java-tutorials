package demo;

import static java.lang.System.out;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    @Inject
    private SystemInformator systemInformator;
    @Inject
    private SmtpService smtp;
    
    public void sendMail(String email, String message) {
        OsType os = systemInformator.getOS();
        
        //jakas logika ify i switche
        
        out.println("Wysyłam email przy pomocy " + os.getEmailApp());
        smtp.send(os);
    }
    
    
    public int returnTWO(){
        return 2;
    }
}
