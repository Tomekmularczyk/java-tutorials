package demo;

import org.springframework.stereotype.Service;

@Service
public class SystemInformator {

    public OsType getOS() {
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("mac")) {
            return OsType.OSX;
        }
        if (os.contains("windows")) {
            return OsType.WINDOWS;
        }
        if (os.contains("linux")) {
            return OsType.LINUX;
        }

        return OsType.NOT_KNOWN;
    }

}
