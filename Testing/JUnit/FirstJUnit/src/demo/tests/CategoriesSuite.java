package demo.tests;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import demo.interfaces.VeryImportantCategory;
import demo.interfaces.VeryLongTask;

@RunWith(Categories.class) //odpalamy testy oznaczone poniższymi kategoriami
@IncludeCategory({VeryImportantCategory.class}) //dzieki tej anotacji uruchomimy tylko te oznaczone @Category(interfejs.class)
@ExcludeCategory({VeryLongTask.class}) //dzieki tej anotacji uruchomimy tylko te oznaczone @Category(interfejs.class)
@Suite.SuiteClasses({ HistoryItemTest.class, RulesTest.class }) //jakie klasy ma przeszukac
public class CategoriesSuite {

}
