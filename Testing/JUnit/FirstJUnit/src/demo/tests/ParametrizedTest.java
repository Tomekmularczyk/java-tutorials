package demo.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import demo.model.TrackingService;


/* Obiekt tej klasy będzie tworzony na nowo z każdym testem, a do konstruktora będą wstrzykiwane parametry z metody -> public static List<Object[]> oznaczonej @Parameters
 * Ponieważ trackingService jest statyczny, bedzie to ten sam obiekt dla każdego testu
 */
@RunWith(Parameterized.class)
public class ParametrizedTest {
	private static TrackingService trackingService = new TrackingService();
	
	private int input;
	private int expected;
	public ParametrizedTest(int input, int expected){
		this.input = input;
		this.expected = expected;
	}
	
	@Parameters
	public static List<Object[]> data(){
		return Arrays.asList(new Object[][]{
			//input, expected. Ponieważ tracking service jest globalny, każdy test będzie działał na tym samym obiekcie
			{3,3},
			{3,6},
			{-10,0},
			{4,4},
			{-1,3},
			{-1,3} //ten nie wypali
		});
	}
	@Test
	public void testIfCorrect(){
		//musimy jakoś rozróżnić czy chodzi o dodawanie czy usuwanie
		if(input >= 0){
			trackingService.addProtein(input);
		}else{
			trackingService.removeProtein(-input);
		}
		
		assertEquals(expected, trackingService.getTotal());
	}
	
}
