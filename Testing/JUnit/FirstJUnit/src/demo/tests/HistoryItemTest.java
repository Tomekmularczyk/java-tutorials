package demo.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import demo.interfaces.VeryImportantCategory;
import demo.model.HistoryItem;

@Category({VeryImportantCategory.class}) //wszystkie metody z tej klasy będą oznaczone tą kategorią
public class HistoryItemTest {
	private static HistoryItem historyItem;
	
	@Before
	public void before(){
		historyItem = new HistoryItem(1, 10, "adding", 10);
	}
	
	@Test
	public void doesConstructorWorkProperly() {
		assertEquals(1, historyItem.getID());
		assertEquals(10, historyItem.getAmount());
		assertEquals("adding", historyItem.getOperation());
		assertEquals(10, historyItem.getTotal());
	}

}
