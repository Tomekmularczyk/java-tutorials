package demo.tests;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;

import demo.exception.GoalNotReached;
import demo.model.TrackingService;

/* Rules rozszerzają użycie JUnit
 * 
 */

public class ExpectedExceptionTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void whenGoalIsLesThanZero() throws GoalNotReached{
		TrackingService ts = new TrackingService();
		ts.setGoal(100);
		
		thrown.expect(GoalNotReached.class);
		thrown.expectMessage("goal is not reached!");
		
		ts.isGoalMet();
	}
	
	
	//Ta zasada zostanie zastosowana do wszystkich testów
	@Rule
	public Timeout timeout = Timeout.millis(20);

}
