package demo.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import demo.exception.GoalNotReached;
import demo.interfaces.VeryImportantCategory;
import demo.interfaces.VeryLongTask;
import demo.model.TrackingService;


//-- wszystkie testy muszą być: public void, jedynie @BeforeClass i @AfterClass muszą być dodatkowo statyczne
public class RulesTest {

	private TrackingService service;
	
	//uruchamia się jednorazowo przed wszystkimi testami 
	@BeforeClass 
	public static void beforeClass(){
		System.out.println("beforeClass...");
	}
	@AfterClass 
	public static void afterClass(){
		System.out.println("afterClass...");
	}
	
	
	
	//---uruchamia się przed *każdym* testem
	@Before
	public void setUP(){
		System.out.println("Before...");
		service = new TrackingService();
	}
	
	//---uruchamia się po każdym teście. Używa się znacznie rzadziej niż @Before, jeżeli już to do przywrócenia jakiś zmian czy zwolnienia jakichś zasobów
	@After
	public void tearDown(){
		System.out.println("After...");
	}
	
	//==================== WŁAŚCIWE TESTY ===================================
	@Test
	public void newTrackingServiceTotalIsZero() {
		System.out.println("newTrackingServiceTotalIsZero");
		//pamiętaj że pierwszy argument(po wiadomości) to oczekiwana wartość, a drugi argument się porównuje.
		assertEquals("Service total was not zero!", 0, service.getTotal());
	}

	@Test
	public void whenAddingProteinTotalIncreasesByThatAmount(){
		System.out.println("whenAddingProteinTotalIncreasesByThatAmount");
		service.addProtein(10);
		assertEquals("Lipton tea!", 10, service.getTotal());
	}
	
	@Test
	@Ignore //test zostanie pominięty
	public void checkIfRemovesRight(){
		System.out.println("checkIfRemovesRight");
		
		service.removeProtein(10);
		assertNotEquals("Zle odjęte!", 10, service.getTotal());
	}
	
	//test przejdzie jedynie wtedy kiedy zostanie wyrzucony oczekiwany wyjątek
	@Test(expected = GoalNotReached.class) //podajemy typ wyjątku
	public void checkIfGoalIsMet() throws GoalNotReached{
		service.setGoal(5);
		service.isGoalMet();
	}
	
	//test przejdzie jeżeli metoda zakończy swoje działanie poniżej danego czasu
	@Test(timeout=300)
	public void checkIfIsQuickEnough(){
		for (int i = 0; i < 10000000; i++) {
			service.addProtein(1);
		}
	}
	
	
	//oznaczamy test kategorią do jakiej należy
	@Test
	@Category({VeryImportantCategory.class, VeryLongTask.class})
	public void testSomething(){
		
	}
}
