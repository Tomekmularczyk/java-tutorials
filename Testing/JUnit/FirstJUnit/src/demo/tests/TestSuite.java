package demo.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ HistoryItemTest.class, RulesTest.class })
public class TestSuite {
	//dzieki powyższym anotacjom możemy uruchomić kilka test caseów na raz
}
