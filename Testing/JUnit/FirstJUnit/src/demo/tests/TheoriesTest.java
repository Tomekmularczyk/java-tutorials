package demo.tests;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.junit.Assume;

import demo.model.TrackingService;


/* Teorie pobierają serie argumentów ale mają tylko jedną expected value
 * Poniżej tworzymy pewne założenie/teorie gdzie po dodaniu pozytywnych watości powinna zawsze być pozytywna wartość totalna
 */
@RunWith(Theories.class)
public class TheoriesTest {
	
	@DataPoints
	public static int[] data(){
		return new int[]{1,43,5,0,3,-4,23};
	}
	
	@Theory
	public void posistiveAddedValuesAlwaysHavePositiveTotals(int value){
		TrackingService trackingService = new TrackingService();
		trackingService.addProtein(value);
		
		//ta asercja zablokuje testy dla wartości ujemnych
		Assume.assumeTrue(value >= 0);
		
		assertTrue(trackingService.getTotal() >= 0);
	}
}
