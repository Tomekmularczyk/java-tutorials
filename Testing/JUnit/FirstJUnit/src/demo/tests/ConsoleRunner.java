package demo.tests;

import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;

public class ConsoleRunner {
	/* Pozwala na uruchomienie JUnit w konsoli a nie za pomocą frameworku(??)
	 * 
	 */
	public static void main(String[] args){
		JUnitCore jUnit = new JUnitCore();
		
		jUnit.addListener(new TextListener(System.out));
		
		jUnit.run(TestSuite.class);
	}
}
