package demo.exception;

public class GoalNotReached extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GoalNotReached(){}
	
	public GoalNotReached(String message){
		super(message);
	}
}
