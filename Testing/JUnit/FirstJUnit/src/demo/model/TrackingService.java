package demo.model;

import java.util.ArrayList;
import java.util.List;

import demo.exception.GoalNotReached;

public class TrackingService {
	private int total;
	private int goal;
	private int historyID;
	private List<HistoryItem> history = new ArrayList<>();
	
	public void addProtein(int amount){
		total += amount;
		history.add(new HistoryItem(historyID++, amount ,"add", total));
	}
	
	public void removeProtein(int amount){
		total -= amount;
		if(total < 0)
			total = 0;
		
		history.add(new HistoryItem(historyID++, amount, "substract", total));
	}
	
	public int getTotal(){
		return total;
	}
	
	public void setGoal(int value){
		goal = value;
	}
	
	public boolean isGoalMet() throws GoalNotReached{
		if(total < goal) throw new GoalNotReached("goal is not reached!");
		
		return true;
	}
	
	public List<HistoryItem> getHistory(){
		return history;
	}
	
}
